1
00:00:03,710 --> 00:00:11,490
moving right along from there we get to

2
00:00:08,850 --> 00:00:12,929
the image and T headers and so this is a

3
00:00:11,490 --> 00:00:14,370
structure where there's actually two

4
00:00:12,929 --> 00:00:16,560
structures embedded in it there's a

5
00:00:14,370 --> 00:00:18,300
signature which I'll have again a

6
00:00:16,560 --> 00:00:20,460
well-known value of PE and they're

7
00:00:18,300 --> 00:00:22,230
basically and then there's a file header

8
00:00:20,460 --> 00:00:23,460
which we'll talk about next which is

9
00:00:22,230 --> 00:00:25,710
embedded in the structure it's not

10
00:00:23,460 --> 00:00:27,300
pointed to but it's actually embedded so

11
00:00:25,710 --> 00:00:28,680
we'll show that structure next and then

12
00:00:27,300 --> 00:00:30,750
there's the optional header which again

13
00:00:28,680 --> 00:00:32,730
is embedded into this structure so it's

14
00:00:30,750 --> 00:00:34,440
basically signature then the next

15
00:00:32,730 --> 00:00:37,200
structure immediately after and then the

16
00:00:34,440 --> 00:00:39,960
next structure immediately after that so

17
00:00:37,200 --> 00:00:41,580
here okay so that's we care about all of

18
00:00:39,960 --> 00:00:42,990
those fields basically in this case so

19
00:00:41,580 --> 00:00:45,990
starting out with the signatures the

20
00:00:42,990 --> 00:00:50,040
first one we care about it'll be in big

21
00:00:45,990 --> 00:00:54,150
endian order it will be 0 0 0 0 4 5 5 0

22
00:00:50,040 --> 00:01:02,000
which is PE so the the P is the 50 and

23
00:00:54,150 --> 00:01:05,129
the 45 is the B size little endian yes

24
00:01:02,000 --> 00:01:07,710
all right so right from there now this

25
00:01:05,129 --> 00:01:09,810
one again is not pointed to but it's

26
00:01:07,710 --> 00:01:12,510
actually embedded in there so this is

27
00:01:09,810 --> 00:01:14,100
the first one and so I should say that

28
00:01:12,510 --> 00:01:15,360
you know the first round of our game is

29
00:01:14,100 --> 00:01:17,820
basically going to be going over

30
00:01:15,360 --> 00:01:19,530
daughters and five powers so just this

31
00:01:17,820 --> 00:01:21,540
information is all we're going to cover

32
00:01:19,530 --> 00:01:22,920
in the first round of our game the dots

33
00:01:21,540 --> 00:01:23,520
header like we said a few things you

34
00:01:22,920 --> 00:01:25,770
care about

35
00:01:23,520 --> 00:01:28,380
signature and the pointer to the next

36
00:01:25,770 --> 00:01:30,799
player and this one there's more stuff

37
00:01:28,380 --> 00:01:33,780
we care about so there's a machine-type

38
00:01:30,799 --> 00:01:35,760
there's number sectionals time needs to

39
00:01:33,780 --> 00:01:36,960
have characteristics these things right

40
00:01:35,760 --> 00:01:42,000
here we don't really care about that

41
00:01:36,960 --> 00:01:43,229
much all right so machine this is one of

42
00:01:42,000 --> 00:01:44,760
the things I forgot whether the previous

43
00:01:43,229 --> 00:01:46,860
class and so people were getting

44
00:01:44,760 --> 00:01:49,409
questions on this and they were the

45
00:01:46,860 --> 00:01:51,570
answers were machine basically it's

46
00:01:49,409 --> 00:01:53,670
trying to say what kind of architecture

47
00:01:51,570 --> 00:01:56,960
this is meant to be running on in terms

48
00:01:53,670 --> 00:01:59,729
of in terms of life

49
00:01:56,960 --> 00:02:02,159
assembly language like what sort of CPU

50
00:01:59,729 --> 00:02:05,280
is they supposed to be running on all

51
00:02:02,159 --> 00:02:07,560
right so but also this actually because

52
00:02:05,280 --> 00:02:09,239
there's different machine types for 32

53
00:02:07,560 --> 00:02:11,069
and 64-bit x86

54
00:02:09,239 --> 00:02:13,889
this gives us our first indication

55
00:02:11,069 --> 00:02:16,049
whether this is a 32 or 64 bit finer it

56
00:02:13,889 --> 00:02:17,440
doesn't have to necessarily be accurate

57
00:02:16,049 --> 00:02:20,020
but this gives

58
00:02:17,440 --> 00:02:23,650
our first kind of view so if the value

59
00:02:20,020 --> 00:02:27,160
is 1/4 C then this is going to be a next

60
00:02:23,650 --> 00:02:31,660
thing x86 binary or it would most likely

61
00:02:27,160 --> 00:02:34,240
be you know 32-bit x86 and 32-bit x86 or

62
00:02:31,660 --> 00:02:37,690
sorry 32-bit binary we typically called

63
00:02:34,240 --> 00:02:39,940
p32 funny thing is then 64-bit binaries

64
00:02:37,690 --> 00:02:43,660
they call it instead of p 64 they call

65
00:02:39,940 --> 00:02:46,660
it PE 32 plucks later on when you get

66
00:02:43,660 --> 00:02:50,260
questions about is a 32 or 64 min you

67
00:02:46,660 --> 00:02:53,080
know I say 32 for 64-bit 32 means this

68
00:02:50,260 --> 00:02:58,030
64 means this which it means this or

69
00:02:53,080 --> 00:02:59,380
means that all right so x86 64 that

70
00:02:58,030 --> 00:03:02,740
would at least sort of makes sense right

71
00:02:59,380 --> 00:03:06,430
you're saying this is the Machine value

72
00:03:02,740 --> 00:03:08,350
for x86 64 and so this is something

73
00:03:06,430 --> 00:03:13,390
which I've come to memorize by virtue of

74
00:03:08,350 --> 00:03:16,630
like my own PE 32 plus because it's

75
00:03:13,390 --> 00:03:18,460
meant to be extensible beyond 64 um I'm

76
00:03:16,630 --> 00:03:20,680
not really sure why they called it P 32

77
00:03:18,460 --> 00:03:23,320
plus because it actually does break

78
00:03:20,680 --> 00:03:25,390
backwards compatibility with 32-bit like

79
00:03:23,320 --> 00:03:27,640
the there's there's a location where

80
00:03:25,390 --> 00:03:29,920
some of them they like combine two

81
00:03:27,640 --> 00:03:32,730
32-bit fields now the new 64-bit and the

82
00:03:29,920 --> 00:03:35,470
offset to other things changes so it

83
00:03:32,730 --> 00:03:37,630
like that's why PU for instance can't

84
00:03:35,470 --> 00:03:39,250
parse the 64-bit things because it's

85
00:03:37,630 --> 00:03:42,880
just different enough that it breaks

86
00:03:39,250 --> 00:03:45,040
personal basically all right so this

87
00:03:42,880 --> 00:03:46,690
again machine filled in the file header

88
00:03:45,040 --> 00:03:48,550
is the first thing that gives us some

89
00:03:46,690 --> 00:03:50,050
indication of 32 performance

90
00:03:48,550 --> 00:03:52,959
well what it's really trying to say just

91
00:03:50,050 --> 00:03:54,340
to be clear is that this is more sort of

92
00:03:52,959 --> 00:03:56,130
what the architecture is that it's

93
00:03:54,340 --> 00:03:59,320
supposed to be running on

94
00:03:56,130 --> 00:03:59,769
all right time date stamp is as I said

95
00:03:59,320 --> 00:04:03,100
before

96
00:03:59,769 --> 00:04:05,410
pretty interesting one and it is a

97
00:04:03,100 --> 00:04:09,340
typical UNIX seconds since

98
00:04:05,410 --> 00:04:10,780
epochs of epoch is January 1st 1970 and

99
00:04:09,340 --> 00:04:14,410
it's saying you know how many seconds

100
00:04:10,780 --> 00:04:15,820
have elapsed since 1970 basically and so

101
00:04:14,410 --> 00:04:17,830
based on this you can figure out you

102
00:04:15,820 --> 00:04:20,950
know when it was actually what time it's

103
00:04:17,830 --> 00:04:23,320
referring to and the interesting thing

104
00:04:20,950 --> 00:04:25,210
is this is set at like time so whenever

105
00:04:23,320 --> 00:04:26,620
someone would compile some link to give

106
00:04:25,210 --> 00:04:29,169
a binary it adds in this

107
00:04:26,620 --> 00:04:32,110
Dean Stanfield and the ton babe

108
00:04:29,169 --> 00:04:33,940
Stanfield will basically say like this

109
00:04:32,110 --> 00:04:35,800
person compiling this file at this time

110
00:04:33,940 --> 00:04:40,060
that it's built in right there in the

111
00:04:35,800 --> 00:04:45,340
headers it's actually has been used for

112
00:04:40,060 --> 00:04:52,960
sort of I guess no more parentheses

113
00:04:45,340 --> 00:04:56,050
about so so there was an example talk

114
00:04:52,960 --> 00:04:58,120
back at like at Las Vegas LinkedIn where

115
00:04:56,050 --> 00:04:59,620
the slides are still not posted but we

116
00:04:58,120 --> 00:05:02,950
have the videos internally if you want

117
00:04:59,620 --> 00:05:04,360
to go watch that um where he was saying

118
00:05:02,950 --> 00:05:06,370
like here's all these different sort of

119
00:05:04,360 --> 00:05:08,440
chunks of the P file format that kind of

120
00:05:06,370 --> 00:05:10,510
help indicate you know whether when an

121
00:05:08,440 --> 00:05:12,430
attacker has compiled something you know

122
00:05:10,510 --> 00:05:13,720
what the compiled directory was and

123
00:05:12,430 --> 00:05:17,139
things like that kind of you know

124
00:05:13,720 --> 00:05:18,789
building up some some he was talking

125
00:05:17,139 --> 00:05:21,160
about attribution but you know it's it's

126
00:05:18,789 --> 00:05:23,139
the sort of circumstantial attribution

127
00:05:21,160 --> 00:05:24,820
where if an attacker is you know not

128
00:05:23,139 --> 00:05:25,960
aware of these sort of things then

129
00:05:24,820 --> 00:05:27,160
they're just going to do the same thing

130
00:05:25,960 --> 00:05:29,229
and they're not realizing that they're

131
00:05:27,160 --> 00:05:31,510
leaving the kind of evidence so so

132
00:05:29,229 --> 00:05:33,340
timing stamp is a big one because people

133
00:05:31,510 --> 00:05:35,110
will say you know when this file was

134
00:05:33,340 --> 00:05:36,760
compiled and you know it's been used

135
00:05:35,110 --> 00:05:37,660
subsequently for things like tracking

136
00:05:36,760 --> 00:05:39,310
the Stuxnet

137
00:05:37,660 --> 00:05:41,169
infections and stuff like that they said

138
00:05:39,310 --> 00:05:43,810
well we found this binary we searched

139
00:05:41,169 --> 00:05:45,280
back to our corpus and we found it turns

140
00:05:43,810 --> 00:05:47,560
out that something was that exact same

141
00:05:45,280 --> 00:05:49,570
byte sequences were found and that had a

142
00:05:47,560 --> 00:05:51,789
compile time of you know 2009 or

143
00:05:49,570 --> 00:05:53,050
whatever so from use for sort of

144
00:05:51,789 --> 00:05:54,849
retrospective analysis

145
00:05:53,050 --> 00:05:56,349
you don't need tackers that care will

146
00:05:54,849 --> 00:05:57,940
then actually change this there's like a

147
00:05:56,349 --> 00:06:01,810
kaspersky article where they're showing

148
00:05:57,940 --> 00:06:03,639
hey the the Stuxnet group is now

149
00:06:01,810 --> 00:06:06,039
changing their time date stamp right

150
00:06:03,639 --> 00:06:07,419
here and the file header but they're not

151
00:06:06,039 --> 00:06:09,070
actually changing the time date stamp

152
00:06:07,419 --> 00:06:11,039
and some subsequent headers that are

153
00:06:09,070 --> 00:06:13,510
deeper admitted in the system so

154
00:06:11,039 --> 00:06:14,979
attackers can you know more than the

155
00:06:13,510 --> 00:06:17,919
attackers know the better they can you

156
00:06:14,979 --> 00:06:18,970
know lie about the information but seems

157
00:06:17,919 --> 00:06:20,440
like you know first of all most

158
00:06:18,970 --> 00:06:23,050
attackers aren't carrying it up to

159
00:06:20,440 --> 00:06:24,340
actually manipulate these fields or you

160
00:06:23,050 --> 00:06:29,380
know basically because they probably

161
00:06:24,340 --> 00:06:31,240
don't know about it so it's alright so

162
00:06:29,380 --> 00:06:33,090
that's time/date stamp will see you know

163
00:06:31,240 --> 00:06:35,200
pee-yew interpreting

164
00:06:33,090 --> 00:06:38,080
next thing we care about the capacitor

165
00:06:35,200 --> 00:06:40,210
is number of sections and later on

166
00:06:38,080 --> 00:06:42,520
there's going to be an array of these

167
00:06:40,210 --> 00:06:44,830
section headers and so sections are

168
00:06:42,520 --> 00:06:46,630
these things like text I don't you know

169
00:06:44,830 --> 00:06:48,280
contain code they'll be an array of

170
00:06:46,630 --> 00:06:50,320
section enter and up here at the file

171
00:06:48,280 --> 00:06:52,750
header line this is what's telling the

172
00:06:50,320 --> 00:06:54,040
lowest loader how many of those section

173
00:06:52,750 --> 00:06:57,040
headers there are needing to be

174
00:06:54,040 --> 00:07:00,190
following immediately following this

175
00:06:57,040 --> 00:07:01,930
optional header there so that matters

176
00:07:00,190 --> 00:07:04,390
that that's right there that's been

177
00:07:01,930 --> 00:07:09,760
array of section headers that is tacked

178
00:07:04,390 --> 00:07:11,350
right after the MTU cutter Third Field

179
00:07:09,760 --> 00:07:13,210
we care about in the file header is

180
00:07:11,350 --> 00:07:14,800
characteristics so they'll actually be a

181
00:07:13,210 --> 00:07:16,210
bunch of different characteristic things

182
00:07:14,800 --> 00:07:19,540
that'll be kind of hard to keep track of

183
00:07:16,210 --> 00:07:20,950
what's where but some of the

184
00:07:19,540 --> 00:07:30,220
characteristics we care about here and

185
00:07:20,950 --> 00:07:31,660
actually alright so some of the

186
00:07:30,220 --> 00:07:33,070
characteristics are things like saying

187
00:07:31,660 --> 00:07:35,650
you know this is an executable image you

188
00:07:33,070 --> 00:07:37,090
have to have this on a you know deal

189
00:07:35,650 --> 00:07:38,260
dialed in an exe but you don't

190
00:07:37,090 --> 00:07:42,090
necessarily have to have that on the

191
00:07:38,260 --> 00:07:45,490
object file that's built by the compiler

192
00:07:42,090 --> 00:07:50,530
this is mighty key keys are trying to

193
00:07:45,490 --> 00:07:52,780
point to their typos with it they have

194
00:07:50,530 --> 00:07:54,870
typos in their window T dot H which has

195
00:07:52,780 --> 00:08:05,830
got have been there for like forever but

196
00:07:54,870 --> 00:08:08,560
nobody's ever things we care about are

197
00:08:05,830 --> 00:08:11,200
like image file large address where this

198
00:08:08,560 --> 00:08:14,560
is basically saying whether or not this

199
00:08:11,200 --> 00:08:19,090
particular binary and handle being

200
00:08:14,560 --> 00:08:21,580
loaded or whether it can actually handle

201
00:08:19,090 --> 00:08:24,990
well it says handle greater than two

202
00:08:21,580 --> 00:08:24,990
gigabyte address

203
00:08:28,680 --> 00:08:32,169
I'm trying to think whether I

204
00:08:30,520 --> 00:08:34,150
misdescribed up the previous class or

205
00:08:32,169 --> 00:08:37,120
not whatever I'm going to miss describe

206
00:08:34,150 --> 00:08:39,640
it here what I'll say is I believe that

207
00:08:37,120 --> 00:08:42,550
field actually is used for indicating

208
00:08:39,640 --> 00:08:43,960
whether or not this file can handle I

209
00:08:42,550 --> 00:08:45,580
may be thinking their characteristics

210
00:08:43,960 --> 00:08:47,650
there but I'll just say the same I

211
00:08:45,580 --> 00:08:50,560
believe this is used for whether or not

212
00:08:47,650 --> 00:08:53,980
that binary can handle being loaded into

213
00:08:50,560 --> 00:08:56,230
memory at addresses greater than 2

214
00:08:53,980 --> 00:09:00,330
gigabytes or 4 gigabytes basically for

215
00:08:56,230 --> 00:09:02,800
when we've had 64-bit binaries you know

216
00:09:00,330 --> 00:09:07,270
the operating system could load things

217
00:09:02,800 --> 00:09:09,940
above 4 gigabyte range and therefore if

218
00:09:07,270 --> 00:09:11,350
a binary is not compiled recognizing

219
00:09:09,940 --> 00:09:13,000
that it can be loaded up on that 4

220
00:09:11,350 --> 00:09:14,590
gigabyte range they can break because

221
00:09:13,000 --> 00:09:16,540
they're using you know if they have

222
00:09:14,590 --> 00:09:19,530
assumptions about what players should

223
00:09:16,540 --> 00:09:19,530
look like and things like that

224
00:09:20,730 --> 00:09:25,990
I have a feeling I'm miss describing

225
00:09:23,530 --> 00:09:27,730
that no but I'll fix it later I can fix

226
00:09:25,990 --> 00:09:29,740
the video that's the best part I just go

227
00:09:27,730 --> 00:09:32,190
in that menu and I say no that's what it

228
00:09:29,740 --> 00:09:32,190
actually is

229
00:09:39,250 --> 00:09:41,310
you

230
00:09:43,810 --> 00:09:50,560
all right image 32-bit machine this

231
00:09:47,079 --> 00:09:56,459
again is just trying to say that 32-bit

232
00:09:50,560 --> 00:09:59,620
system and that's not ever used this

233
00:09:56,459 --> 00:10:02,110
system is like system file a community

234
00:09:59,620 --> 00:10:05,069
is never using that's this file so I

235
00:10:02,110 --> 00:10:07,779
never actually seen that set anywhere

236
00:10:05,069 --> 00:10:10,149
file DLL is definitely one we care about

237
00:10:07,779 --> 00:10:12,550
this one has to be set if it's why do we

238
00:10:10,149 --> 00:10:14,410
used as a DLL take one set that ply

239
00:10:12,550 --> 00:10:16,660
gonna do on it's working

240
00:10:14,410 --> 00:10:21,300
the owner is definitely checking that

241
00:10:16,660 --> 00:10:23,860
for instance alright and this is just

242
00:10:21,300 --> 00:10:25,360
this is a field we don't officially care

243
00:10:23,860 --> 00:10:27,939
about as far as the class is concerned

244
00:10:25,360 --> 00:10:29,499
but just to give you an idea size of

245
00:10:27,939 --> 00:10:30,459
optional pattern later on it so there's

246
00:10:29,499 --> 00:10:32,199
a five-letter and then there's an

247
00:10:30,459 --> 00:10:34,439
optional header an optional header could

248
00:10:32,199 --> 00:10:37,749
theoretically be expanded or contracted

249
00:10:34,439 --> 00:10:40,329
and certainly after this class you'll be

250
00:10:37,749 --> 00:10:42,279
able to go and see people's interesting

251
00:10:40,329 --> 00:10:45,189
manipulations that they do with P files

252
00:10:42,279 --> 00:10:47,139
where they do things like that but for

253
00:10:45,189 --> 00:10:49,720
our purposes it will basically always be

254
00:10:47,139 --> 00:10:51,399
set to a fixed value but people who want

255
00:10:49,720 --> 00:10:53,350
to know mess with the binaries and mess

256
00:10:51,399 --> 00:10:56,069
with what's actually happening can

257
00:10:53,350 --> 00:10:56,069
change out the size


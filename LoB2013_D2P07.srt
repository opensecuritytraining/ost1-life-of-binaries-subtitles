1
00:00:03,570 --> 00:00:09,679
and the last thing I wanted to talk

2
00:00:06,509 --> 00:00:11,730
about is the notion of runtime importing

3
00:00:09,679 --> 00:00:13,919
you may have seen this a little bit in

4
00:00:11,730 --> 00:00:15,209
the malware class you definitely see

5
00:00:13,919 --> 00:00:17,700
this amount all the time so it's

6
00:00:15,209 --> 00:00:20,400
important thing it's kind of used behind

7
00:00:17,700 --> 00:00:22,529
the scenes in the delay loading so this

8
00:00:20,400 --> 00:00:23,880
is kind of the process which to lay

9
00:00:22,529 --> 00:00:25,890
loading behind the scenes when I had

10
00:00:23,880 --> 00:00:28,429
that hand wavy like stub code does

11
00:00:25,890 --> 00:00:31,140
something to load dll and find function

12
00:00:28,429 --> 00:00:33,090
how it what it does is it uses these two

13
00:00:31,140 --> 00:00:35,190
windows functions load library and get

14
00:00:33,090 --> 00:00:37,530
[ __ ] address and there's equivalents of

15
00:00:35,190 --> 00:00:41,940
these on linux as well try to remember

16
00:00:37,530 --> 00:00:44,399
them though it's like dl load and i

17
00:00:41,940 --> 00:00:46,170
can't remember when we go over to the

18
00:00:44,399 --> 00:00:50,129
linux vm i can do the man page and look

19
00:00:46,170 --> 00:00:52,320
them up but so basically the equip load

20
00:00:50,129 --> 00:00:55,410
library and get proc address the load

21
00:00:52,320 --> 00:00:57,059
library will first put a dll into the

22
00:00:55,410 --> 00:00:59,609
memory of this current process so if i

23
00:00:57,059 --> 00:01:01,590
have hello world just my own hello world

24
00:00:59,609 --> 00:01:04,890
program which normally just prints out

25
00:01:01,590 --> 00:01:07,080
hello world and i added a call to the

26
00:01:04,890 --> 00:01:10,080
function load library and I say load

27
00:01:07,080 --> 00:01:13,619
library and let's say I want to load you

28
00:01:10,080 --> 00:01:15,930
know my live dll low library takes a

29
00:01:13,619 --> 00:01:18,509
path to the dll that you want to load

30
00:01:15,930 --> 00:01:20,700
and it gives you back a handle to that

31
00:01:18,509 --> 00:01:23,040
dll and so the handle is this

32
00:01:20,700 --> 00:01:24,900
abstraction layer and so you're not i

33
00:01:23,040 --> 00:01:26,759
mean it's supposed to be an abstraction

34
00:01:24,900 --> 00:01:28,560
but in reality this handle is actually

35
00:01:26,759 --> 00:01:31,470
the base address for that vll got loaded

36
00:01:28,560 --> 00:01:33,720
so you run the library you give it the

37
00:01:31,470 --> 00:01:37,110
name of a dll it gives you back the base

38
00:01:33,720 --> 00:01:39,329
address where that DLL got loaded next

39
00:01:37,110 --> 00:01:41,490
if you want to actually use that profuse

40
00:01:39,329 --> 00:01:43,259
functions with the net well most

41
00:01:41,490 --> 00:01:44,610
programmers don't know the P header

42
00:01:43,259 --> 00:01:46,200
format so they don't know how to take

43
00:01:44,610 --> 00:01:48,299
that base address and go parse the

44
00:01:46,200 --> 00:01:50,880
desire to find the NT header to find the

45
00:01:48,299 --> 00:01:53,549
data directory and so forth right so how

46
00:01:50,880 --> 00:01:56,040
do normal programmers go look up a

47
00:01:53,549 --> 00:01:58,500
particular function they use get pakka

48
00:01:56,040 --> 00:02:00,270
dress so you get back the handle to the

49
00:01:58,500 --> 00:02:02,850
dll which is the base address you feed

50
00:02:00,270 --> 00:02:04,500
that into get proc address and behind

51
00:02:02,850 --> 00:02:06,659
the scenes get proc address because all

52
00:02:04,500 --> 00:02:09,479
this parsing of the headers and so you

53
00:02:06,659 --> 00:02:12,720
pass getprocaddress you know my function

54
00:02:09,479 --> 00:02:15,240
1 so i just loaded my dll and then i say

55
00:02:12,720 --> 00:02:16,770
look up the address of my function 1

56
00:02:15,240 --> 00:02:18,720
and I store that into a local variable

57
00:02:16,770 --> 00:02:21,690
art store that to a global variable and

58
00:02:18,720 --> 00:02:23,670
now if I want I can treat that as a

59
00:02:21,690 --> 00:02:27,750
function player and I can call directly

60
00:02:23,670 --> 00:02:30,300
to my function law within my C code so

61
00:02:27,750 --> 00:02:31,740
behind the scenes lay load if you look

62
00:02:30,300 --> 00:02:35,190
at the actual stub code that's running

63
00:02:31,740 --> 00:02:37,620
the late Lord just does it says okay I'm

64
00:02:35,190 --> 00:02:40,680
going to run load library if this dll is

65
00:02:37,620 --> 00:02:43,250
not already loaded and then I know that

66
00:02:40,680 --> 00:02:45,930
this stub corresponds to some particular

67
00:02:43,250 --> 00:02:47,310
delay loaded function and that I'm going

68
00:02:45,930 --> 00:02:51,900
to run get proper dress on that

69
00:02:47,310 --> 00:02:53,730
particular function so on yeah you'll

70
00:02:51,900 --> 00:02:55,830
see malware use this basically and then

71
00:02:53,730 --> 00:02:57,180
Matt wants to potentially obfuscate

72
00:02:55,830 --> 00:03:00,390
their imports right we've been looking

73
00:02:57,180 --> 00:03:01,770
at nice normal binaries nice normal

74
00:03:00,390 --> 00:03:03,210
wineries you can go and look at the

75
00:03:01,770 --> 00:03:05,250
import names table and you'll see oh

76
00:03:03,210 --> 00:03:06,720
this calls you know this thing that

77
00:03:05,250 --> 00:03:07,860
talks over the network and you don't

78
00:03:06,720 --> 00:03:09,930
know this thing is either you know

79
00:03:07,860 --> 00:03:12,600
sending packets or receiving packets or

80
00:03:09,930 --> 00:03:15,360
something like that so malware wants to

81
00:03:12,600 --> 00:03:17,220
obfuscate it's important aim stable

82
00:03:15,360 --> 00:03:19,020
essentially and they don't want to just

83
00:03:17,220 --> 00:03:20,760
have it in plain text the list of all

84
00:03:19,020 --> 00:03:23,250
the functions that they're importing and

85
00:03:20,760 --> 00:03:25,920
so what they'll do is they'll only

86
00:03:23,250 --> 00:03:27,390
import like load library and get proc

87
00:03:25,920 --> 00:03:29,130
address so they can go through some

88
00:03:27,390 --> 00:03:31,410
other rigmarole to not even import that

89
00:03:29,130 --> 00:03:32,700
and find it done in flavor on time well

90
00:03:31,410 --> 00:03:35,070
let's say we've got some malware that

91
00:03:32,700 --> 00:03:36,840
only imports two functions those two

92
00:03:35,070 --> 00:03:38,400
functions then what it can do is it can

93
00:03:36,840 --> 00:03:40,770
have its own you know betta structure

94
00:03:38,400 --> 00:03:42,690
its own global entry that appeals in

95
00:03:40,770 --> 00:03:44,640
once it starts running it fills in the

96
00:03:42,690 --> 00:03:46,830
equivalent of an import address table oh

97
00:03:44,640 --> 00:03:48,210
well you know potentially having those

98
00:03:46,830 --> 00:03:50,730
names that it's passing it to get

99
00:03:48,210 --> 00:03:53,220
dressed like X or encoded or encrypted

100
00:03:50,730 --> 00:03:55,680
or whatever got like some table there

101
00:03:53,220 --> 00:03:57,420
you know virtual they're fake import

102
00:03:55,680 --> 00:03:59,040
name stablemate encrypt that and then

103
00:03:57,420 --> 00:04:00,690
they decrypt it just in time when they

104
00:03:59,040 --> 00:04:02,220
start running and then they pass those

105
00:04:00,690 --> 00:04:04,590
to get proc addressed filling their own

106
00:04:02,220 --> 00:04:06,420
private import dressing that amount or I

107
00:04:04,590 --> 00:04:08,580
can't just look at the binary and disk

108
00:04:06,420 --> 00:04:10,200
and see alright it's importing these

109
00:04:08,580 --> 00:04:11,640
functions that manipulate the registry

110
00:04:10,200 --> 00:04:15,390
it's importing these functions that talk

111
00:04:11,640 --> 00:04:17,070
over the network so so yeah

112
00:04:15,390 --> 00:04:19,320
getprocaddress in the library are very

113
00:04:17,070 --> 00:04:21,770
common you'll see those used in a lot of

114
00:04:19,320 --> 00:04:25,520
malware that's doing basic optimization

115
00:04:21,770 --> 00:04:27,780
all right so here's some fail and or win

116
00:04:25,520 --> 00:04:28,590
the important thing is there will be a

117
00:04:27,780 --> 00:04:32,730
quiz later

118
00:04:28,590 --> 00:04:36,590
on what year the milk expired so 1538

119
00:04:32,730 --> 00:04:36,590
just FYI teaching them a test


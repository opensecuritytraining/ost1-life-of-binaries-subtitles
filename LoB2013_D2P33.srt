1
00:00:04,400 --> 00:00:07,440
anyways thanks for bearing with me for

2
00:00:06,000 --> 00:00:09,120
the through that we're at a little bit

3
00:00:07,440 --> 00:00:10,639
over but you know obviously you can see

4
00:00:09,120 --> 00:00:12,639
how it's important to

5
00:00:10,639 --> 00:00:14,559
at the end of the day say

6
00:00:12,639 --> 00:00:16,560
the viruses and the packers and things

7
00:00:14,559 --> 00:00:18,240
like that they manipulate these p header

8
00:00:16,560 --> 00:00:19,920
data structures they manipulate elf data

9
00:00:18,240 --> 00:00:21,840
structures there's enough similarity

10
00:00:19,920 --> 00:00:24,480
between p and alpha and maco and all of

11
00:00:21,840 --> 00:00:26,480
the binary formats that once someone

12
00:00:24,480 --> 00:00:28,560
knows how the binary format works they

13
00:00:26,480 --> 00:00:30,080
can just apply these same techniques of

14
00:00:28,560 --> 00:00:31,760
i'm going to attack myself on the end

15
00:00:30,080 --> 00:00:33,360
and point the headers to me and then

16
00:00:31,760 --> 00:00:35,520
i'll fix up whatever miscellaneous i

17
00:00:33,360 --> 00:00:37,440
have to fix up i can you know compress

18
00:00:35,520 --> 00:00:39,600
this thing down but make sure i allocate

19
00:00:37,440 --> 00:00:41,440
enough space to decompress into memory

20
00:00:39,600 --> 00:00:45,040
things like that

21
00:00:41,440 --> 00:00:47,520
so um back over to i guess video back

22
00:00:45,040 --> 00:00:47,520
onto me

23
00:00:48,239 --> 00:00:53,039
and at this point i believe

24
00:00:51,360 --> 00:00:54,719
i mean i have some miscellaneous lots of

25
00:00:53,039 --> 00:00:56,640
you know more references you can read

26
00:00:54,719 --> 00:00:58,879
about different you know

27
00:00:56,640 --> 00:01:00,320
you know max are immune to viruses you

28
00:00:58,879 --> 00:01:02,000
know you can just do the exact same

29
00:01:00,320 --> 00:01:03,440
thing on maco files that you can do on

30
00:01:02,000 --> 00:01:04,400
elf files that you can do on anything

31
00:01:03,440 --> 00:01:06,240
else

32
00:01:04,400 --> 00:01:07,840
right so a bunch of other things that

33
00:01:06,240 --> 00:01:09,439
you can go read to learn more about

34
00:01:07,840 --> 00:01:11,840
different particular instances of

35
00:01:09,439 --> 00:01:13,520
viruses beyond this you can go and read

36
00:01:11,840 --> 00:01:15,040
the source code and understand it a

37
00:01:13,520 --> 00:01:17,119
little better

38
00:01:15,040 --> 00:01:19,439
and there's attackers and

39
00:01:17,119 --> 00:01:20,720
leave that's pretty much it

40
00:01:19,439 --> 00:01:22,960
this is just sort of an older

41
00:01:20,720 --> 00:01:24,560
distribution of hackers more reading

42
00:01:22,960 --> 00:01:26,080
about packers

43
00:01:24,560 --> 00:01:27,520
and we don't have enough time for that

44
00:01:26,080 --> 00:01:29,680
anyone who wants to stay later we can

45
00:01:27,520 --> 00:01:32,560
talk about additional topics we can play

46
00:01:29,680 --> 00:01:34,479
some more game things but uh does anyone

47
00:01:32,560 --> 00:01:36,159
have any questions about the material

48
00:01:34,479 --> 00:01:37,600
before we uh

49
00:01:36,159 --> 00:01:39,280
officially say it's over for the day and

50
00:01:37,600 --> 00:01:41,119
we just go to miscellaneous stuff that

51
00:01:39,280 --> 00:01:43,520
anyone wants to do afterwards

52
00:01:41,119 --> 00:01:47,200
right so one last thing i'll say

53
00:01:43,520 --> 00:01:49,439
if you want to see games that nerds play

54
00:01:47,200 --> 00:01:52,640
people have they try to create the

55
00:01:49,439 --> 00:01:55,040
smallest possible p file xl file marco

56
00:01:52,640 --> 00:01:57,040
file that still runs and like returns

57
00:01:55,040 --> 00:01:58,399
you know value returns 42 or something

58
00:01:57,040 --> 00:02:00,719
like that

59
00:01:58,399 --> 00:02:01,759
this involves even crazier manipulation

60
00:02:00,719 --> 00:02:02,640
of headers

61
00:02:01,759 --> 00:02:04,479
than

62
00:02:02,640 --> 00:02:06,479
malicious code does this has to do with

63
00:02:04,479 --> 00:02:09,039
like you're putting code in you're using

64
00:02:06,479 --> 00:02:11,440
the mz as instructions and then you're

65
00:02:09,039 --> 00:02:13,680
like jumping through that in order to

66
00:02:11,440 --> 00:02:16,239
have extra code and stuff so

67
00:02:13,680 --> 00:02:17,599
i'd say you now have plenty of knowledge

68
00:02:16,239 --> 00:02:19,680
that you should be able to parse these

69
00:02:17,599 --> 00:02:21,520
sort of things like okay they build up

70
00:02:19,680 --> 00:02:22,640
they say hello world takes 400 bytes

71
00:02:21,520 --> 00:02:24,160
well let's see if we can do a little

72
00:02:22,640 --> 00:02:25,760
better let's strip out this information

73
00:02:24,160 --> 00:02:27,599
okay now it's this many bytes let's

74
00:02:25,760 --> 00:02:29,520
strip out this information let's overlap

75
00:02:27,599 --> 00:02:31,599
this information so that the imports

76
00:02:29,520 --> 00:02:34,080
table overlaps with uh you know whatever

77
00:02:31,599 --> 00:02:34,959
table and so you should be able to uh

78
00:02:34,080 --> 00:02:36,879
you should be able to read and

79
00:02:34,959 --> 00:02:38,720
understand these sort of things now and

80
00:02:36,879 --> 00:02:40,800
understand the implications of what it

81
00:02:38,720 --> 00:02:42,800
means when they're doing crazy tricks

82
00:02:40,800 --> 00:02:45,280
with the executable formats to make it

83
00:02:42,800 --> 00:02:46,720
as small as possible in order to still

84
00:02:45,280 --> 00:02:48,640
execute code but that's just a

85
00:02:46,720 --> 00:02:51,840
miscellaneous one all right any last

86
00:02:48,640 --> 00:02:53,599
questions from in in the room

87
00:02:51,840 --> 00:02:55,680
all right dismissed for the day anyone

88
00:02:53,599 --> 00:03:00,000
who wants to stay and play games and

89
00:02:55,680 --> 00:03:00,000
find bugs and ask questions you can

90
00:03:00,400 --> 00:03:03,640
that's right


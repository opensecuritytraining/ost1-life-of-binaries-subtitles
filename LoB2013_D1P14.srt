1
00:00:04,400 --> 00:00:12,270
so Bobby reports so we said that on disk

2
00:00:09,799 --> 00:00:13,799
the import address table points at that

3
00:00:12,270 --> 00:00:15,869
same data structure at the important

4
00:00:13,799 --> 00:00:17,400
names there in memory it gets flipped

5
00:00:15,869 --> 00:00:18,539
around and points out to other the

6
00:00:17,400 --> 00:00:21,060
allowance and things like that

7
00:00:18,539 --> 00:00:23,039
Bobby imports are basically pre filling

8
00:00:21,060 --> 00:00:25,560
in is that you port address table so

9
00:00:23,039 --> 00:00:27,660
that it points out to wherever at

10
00:00:25,560 --> 00:00:29,910
compile time you think those addresses

11
00:00:27,660 --> 00:00:31,740
would have pointed so basically the

12
00:00:29,910 --> 00:00:33,990
compiler goes around and says okay and

13
00:00:31,740 --> 00:00:37,410
notepad dot exe you are going to import

14
00:00:33,990 --> 00:00:39,510
from kernel32 well kernel32 is asking

15
00:00:37,410 --> 00:00:42,030
for this base address and it's saying

16
00:00:39,510 --> 00:00:44,460
that the function is that this offset so

17
00:00:42,030 --> 00:00:47,010
I think that at low time you're going to

18
00:00:44,460 --> 00:00:48,180
get this absolute virtual address I'm

19
00:00:47,010 --> 00:00:50,820
just going to go ahead and put that into

20
00:00:48,180 --> 00:00:52,649
your address table if it's wrong all

21
00:00:50,820 --> 00:00:55,109
right whatever the OS we'll fix it up

22
00:00:52,649 --> 00:00:56,190
after the fact but if it's right then

23
00:00:55,109 --> 00:00:57,929
you've got you know some speed

24
00:00:56,190 --> 00:00:59,820
optimization that the OS didn't have to

25
00:00:57,929 --> 00:01:03,149
go search an export table for a string

26
00:00:59,820 --> 00:01:05,940
printf so bombed imports are binding is

27
00:01:03,149 --> 00:01:07,650
basically just pre filling in the import

28
00:01:05,940 --> 00:01:10,470
dress table with the absolute virtual

29
00:01:07,650 --> 00:01:13,460
addresses that you assume that a

30
00:01:10,470 --> 00:01:15,660
particular entry will have at one time

31
00:01:13,460 --> 00:01:18,120
it's even the linker does the same job

32
00:01:15,660 --> 00:01:19,500
as an OS lower would have done by like

33
00:01:18,120 --> 00:01:23,540
running around and looking who you're

34
00:01:19,500 --> 00:01:23,540
exporting from just filling in the gym

35
00:01:23,780 --> 00:01:28,980
so now we already saw this this was the

36
00:01:27,090 --> 00:01:30,810
image import descriptor we're gonna use

37
00:01:28,980 --> 00:01:32,370
the same data structure again we're

38
00:01:30,810 --> 00:01:34,500
gonna care about the same fields again

39
00:01:32,370 --> 00:01:36,150
but the only difference here is that

40
00:01:34,500 --> 00:01:37,920
whereas previously there was a time date

41
00:01:36,150 --> 00:01:40,740
stamp and like I just said we don't even

42
00:01:37,920 --> 00:01:42,510
care about it it was actually 0 last

43
00:01:40,740 --> 00:01:44,100
time if you go back and look at your the

44
00:01:42,510 --> 00:01:46,350
import descriptors you see the Sun dates

45
00:01:44,100 --> 00:01:48,960
that was always zero there was no real

46
00:01:46,350 --> 00:01:52,050
timestamp there here it's negative one

47
00:01:48,960 --> 00:01:53,970
so again it's it's there's no real value

48
00:01:52,050 --> 00:01:57,060
there so we're gonna say the same data

49
00:01:53,970 --> 00:01:58,740
structure but this table is going to be

50
00:01:57,060 --> 00:02:00,360
separate from that other tables so bound

51
00:01:58,740 --> 00:02:02,970
imports table is going to be off to the

52
00:02:00,360 --> 00:02:06,690
side separate from the regular normal

53
00:02:02,970 --> 00:02:09,479
imports data all right so back to the

54
00:02:06,690 --> 00:02:12,239
native directory looks like in x11 even

55
00:02:09,479 --> 00:02:14,760
that ITT is index twelve there's going

56
00:02:12,239 --> 00:02:16,310
to be in a separate data directory entry

57
00:02:14,760 --> 00:02:20,810
which is going to point you to a date

58
00:02:16,310 --> 00:02:26,120
prick bound import array of these things

59
00:02:20,810 --> 00:02:28,870
right one for you ll and the intent for

60
00:02:26,120 --> 00:02:28,870
this basically

61
00:02:46,489 --> 00:02:53,219
yeah sorry I just missed describe that a

62
00:02:48,989 --> 00:02:54,989
little bit so sorry

63
00:02:53,219 --> 00:02:57,120
this data structure is not actually

64
00:02:54,989 --> 00:02:59,400
going to use for the bomb of the

65
00:02:57,120 --> 00:03:00,389
importance right there's going to be in

66
00:02:59,400 --> 00:03:02,489
a right but it's going to be different

67
00:03:00,389 --> 00:03:05,819
than this this is just how we slightly

68
00:03:02,489 --> 00:03:10,079
change the regular imports normal

69
00:03:05,819 --> 00:03:13,079
imports descriptor table or directory

70
00:03:10,079 --> 00:03:15,090
table whatever whereas in the normally

71
00:03:13,079 --> 00:03:17,280
imports this time Zeta is going to be 0

72
00:03:15,090 --> 00:03:19,709
in order to say basically if it's 0 it's

73
00:03:17,280 --> 00:03:21,510
saying I use normally imports and my

74
00:03:19,709 --> 00:03:23,849
import address table is not pretty

75
00:03:21,510 --> 00:03:26,159
filled in if it's got negative 1 in

76
00:03:23,849 --> 00:03:28,409
saying the OS loader my import address

77
00:03:26,159 --> 00:03:30,449
table is going to be filled in you need

78
00:03:28,409 --> 00:03:32,280
to go you know check against this other

79
00:03:30,449 --> 00:03:35,250
table to determine whether it's building

80
00:03:32,280 --> 00:03:37,139
with accurate information and and this

81
00:03:35,250 --> 00:03:38,790
next table is going to be all about you

82
00:03:37,139 --> 00:03:40,560
know how we can determine whether

83
00:03:38,790 --> 00:03:42,269
something is accurate or not basically

84
00:03:40,560 --> 00:03:44,760
what we're going to be notionally going

85
00:03:42,269 --> 00:03:46,290
towards is you pre fill in this table

86
00:03:44,760 --> 00:03:47,939
but you got to put some information off

87
00:03:46,290 --> 00:03:49,650
to the side saying what version of the

88
00:03:47,939 --> 00:03:51,599
dll's it was that you found a good

89
00:03:49,650 --> 00:03:53,159
because obviously they can get out of

90
00:03:51,599 --> 00:03:55,829
date right you can say at the time I

91
00:03:53,159 --> 00:03:58,530
compiled my application we had this

92
00:03:55,829 --> 00:04:03,419
version of kernel32.dll and you know

93
00:03:58,530 --> 00:04:07,319
printf was that offset mm bear with me

94
00:04:03,419 --> 00:04:10,590
people say I miss VCR 100 got DLL and we

95
00:04:07,319 --> 00:04:13,199
found against MSV sierra 100 ll print up

96
00:04:10,590 --> 00:04:16,769
was that you know hex 2000 now there's

97
00:04:13,199 --> 00:04:19,759
an update for ms VCR 100 ft ll and i'm

98
00:04:16,769 --> 00:04:22,440
printf is on 2020 or something like that

99
00:04:19,759 --> 00:04:24,900
everybody who compiled and bound against

100
00:04:22,440 --> 00:04:27,180
that thing they're pre filled in entries

101
00:04:24,900 --> 00:04:28,680
would be wrong right and so what you

102
00:04:27,180 --> 00:04:30,210
want to do is when you pre fill in

103
00:04:28,680 --> 00:04:32,550
entries you want to say look this is

104
00:04:30,210 --> 00:04:34,919
correct if you still have the same

105
00:04:32,550 --> 00:04:37,020
version of ms VCR or harder not to level

106
00:04:34,919 --> 00:04:38,909
your system so the OS loader is going to

107
00:04:37,020 --> 00:04:40,199
use this next table to figure out if

108
00:04:38,909 --> 00:04:41,880
it's still got the right version because

109
00:04:40,199 --> 00:04:43,710
if it just can see you right off the bat

110
00:04:41,880 --> 00:04:45,509
I don't even have the same version as

111
00:04:43,710 --> 00:04:48,120
you found against then it knows it just

112
00:04:45,509 --> 00:04:50,760
has to be the same process as we talked

113
00:04:48,120 --> 00:04:52,560
about before for doing any ports but if

114
00:04:50,760 --> 00:04:54,120
it sees it still got the same version

115
00:04:52,560 --> 00:04:56,130
and it can just go ahead and

116
00:04:54,120 --> 00:04:59,970
trust that these these pre-built and

117
00:04:56,130 --> 00:05:03,630
entries are correct all right so this

118
00:04:59,970 --> 00:05:06,660
table points at a different type of data

119
00:05:03,630 --> 00:05:07,889
structure and that data structure we're

120
00:05:06,660 --> 00:05:09,960
only going to care about one thing and

121
00:05:07,889 --> 00:05:11,940
that's a time native this is the time

122
00:05:09,960 --> 00:05:13,919
date stamp basically specifying the

123
00:05:11,940 --> 00:05:26,729
version of the DLL that you're bound

124
00:05:13,919 --> 00:05:27,690
against Austin module name I guess we

125
00:05:26,729 --> 00:05:31,320
kind of care about it

126
00:05:27,690 --> 00:05:33,180
I kind of don't it's just it's the

127
00:05:31,320 --> 00:05:34,740
offset to the name that's associated for

128
00:05:33,180 --> 00:05:37,320
this particular yellow because we're

129
00:05:34,740 --> 00:05:39,590
going to have a list of these data

130
00:05:37,320 --> 00:05:46,050
structures which one pretty or elegant

131
00:05:39,590 --> 00:05:50,550
for each image of elegance all right so

132
00:05:46,050 --> 00:05:51,990
we have a new sort of new things it will

133
00:05:50,550 --> 00:05:54,030
show you different data structure we'll

134
00:05:51,990 --> 00:05:57,570
call it the file import directory table

135
00:05:54,030 --> 00:06:00,000
so we're normal import directory table

136
00:05:57,570 --> 00:06:01,710
is just the important recreative there's

137
00:06:00,000 --> 00:06:03,870
a separate bound indirect import

138
00:06:01,710 --> 00:06:06,000
directory table it's got three things

139
00:06:03,870 --> 00:06:08,220
like I said we only care about the time

140
00:06:06,000 --> 00:06:10,139
date stamp really it's saying you know

141
00:06:08,220 --> 00:06:17,400
here's the version that I bound against

142
00:06:10,139 --> 00:06:19,710
and then all this is is a big array of

143
00:06:17,400 --> 00:06:22,949
saying look i prefilled in all your comm

144
00:06:19,710 --> 00:06:25,680
dlg 32 imports but I prefilled them in

145
00:06:22,949 --> 00:06:28,530
from a comm dlg that had this time date

146
00:06:25,680 --> 00:06:30,240
staff in its errors I'd love to tell you

147
00:06:28,530 --> 00:06:31,620
that that time/date stamp came from the

148
00:06:30,240 --> 00:06:33,599
one that you've already learned about it

149
00:06:31,620 --> 00:06:36,840
it actually comes from the export guys

150
00:06:33,599 --> 00:06:39,120
table there's some version number

151
00:06:36,840 --> 00:06:40,889
associated with the file and it's

152
00:06:39,120 --> 00:06:43,470
politically that version of varying

153
00:06:40,889 --> 00:06:45,330
right here and saying I used come dlg

154
00:06:43,470 --> 00:06:47,400
that had a time date step of that I used

155
00:06:45,330 --> 00:06:49,590
shell 32 that I'm a time eight step for

156
00:06:47,400 --> 00:06:51,690
that if the OS is loading this thing up

157
00:06:49,590 --> 00:06:53,340
and saying oh I see you use bound

158
00:06:51,690 --> 00:06:55,740
imports let me go check if I have that

159
00:06:53,340 --> 00:06:57,990
version yep yep yep oh that calm

160
00:06:55,740 --> 00:06:59,710
controlled 32 times yellow I have a

161
00:06:57,990 --> 00:07:02,320
different time date stamp in my call

162
00:06:59,710 --> 00:07:04,630
control three GFDL so I know I shouldn't

163
00:07:02,320 --> 00:07:05,860
believe your pre filled in entries and

164
00:07:04,630 --> 00:07:09,479
I'm just going to go fill those in the

165
00:07:05,860 --> 00:07:12,580
normal way that I do with every other so

166
00:07:09,479 --> 00:07:15,100
and I'm trying to ignore this for now

167
00:07:12,580 --> 00:07:17,680
but later on when we talk about exports

168
00:07:15,100 --> 00:07:19,330
will pretty much come back to that there

169
00:07:17,680 --> 00:07:21,220
are some cases where there's an extra

170
00:07:19,330 --> 00:07:23,110
major structure I think I actually end

171
00:07:21,220 --> 00:07:26,590
up telling you about this anyways

172
00:07:23,110 --> 00:07:28,900
there's other questions but since I want

173
00:07:26,590 --> 00:07:41,530
to get you to break a little come back

174
00:07:28,900 --> 00:07:44,139
to that yeah teaching to the test in

175
00:07:41,530 --> 00:07:48,130
this round we will have questions to the

176
00:07:44,139 --> 00:07:50,310
effect of how many you know found will

177
00:07:48,130 --> 00:07:52,900
basically be saying how many found

178
00:07:50,310 --> 00:07:57,009
important descriptor entries are there

179
00:07:52,900 --> 00:08:00,039
were no say how many bounds

180
00:07:57,009 --> 00:08:01,840
yeah unimportant huge amount import

181
00:08:00,039 --> 00:08:05,740
descriptor entries are there in this

182
00:08:01,840 --> 00:08:09,159
particular binary so basically they look

183
00:08:05,740 --> 00:08:11,229
exactly the same but it turns out we

184
00:08:09,159 --> 00:08:15,250
interpret these values differently if

185
00:08:11,229 --> 00:08:18,070
they have a zero as this last field or

186
00:08:15,250 --> 00:08:21,310
they have a nonzero last field oh this

187
00:08:18,070 --> 00:08:24,190
is basically a structure that is mixed

188
00:08:21,310 --> 00:08:26,949
together and so everything that has a

189
00:08:24,190 --> 00:08:28,960
zero at the last entry we're going to

190
00:08:26,949 --> 00:08:30,610
call an image bound import descriptor

191
00:08:28,960 --> 00:08:33,700
that's we're interpreting it as that

192
00:08:30,610 --> 00:08:36,190
data structure but once you have a one

193
00:08:33,700 --> 00:08:38,770
it's still an about import descriptor

194
00:08:36,190 --> 00:08:40,990
but then the next thing meeting after it

195
00:08:38,770 --> 00:08:42,729
there will be one data structure

196
00:08:40,990 --> 00:08:45,760
immediately after it of the exact same

197
00:08:42,729 --> 00:08:49,270
size but we interpret that as an image

198
00:08:45,760 --> 00:08:50,890
bound forwarder rep structure so the

199
00:08:49,270 --> 00:08:52,420
only reason I tell you this the only

200
00:08:50,890 --> 00:08:56,350
reason I'm forced to tell you this for

201
00:08:52,420 --> 00:08:59,829
this particular round in the quiz is if

202
00:08:56,350 --> 00:09:01,899
I say you know how many image file

203
00:08:59,829 --> 00:09:03,640
import descriptors are in this or if I

204
00:09:01,899 --> 00:09:06,790
say how many found the reports are in

205
00:09:03,640 --> 00:09:07,500
this thing what I want is all of these

206
00:09:06,790 --> 00:09:10,510
entries

207
00:09:07,500 --> 00:09:12,710
except maybe I mean you could like count

208
00:09:10,510 --> 00:09:14,600
all these up and subtract you know

209
00:09:12,710 --> 00:09:16,790
or you know if there were a 1 and a 1

210
00:09:14,600 --> 00:09:19,070
here you'd subtract you but the point is

211
00:09:16,790 --> 00:09:23,089
just whenever you see some nonzero entry

212
00:09:19,070 --> 00:09:25,820
you're going to see that many other data

213
00:09:23,089 --> 00:09:30,080
structures immediately in line after it

214
00:09:25,820 --> 00:09:32,450
and then we now continue on so we see

215
00:09:30,080 --> 00:09:35,270
one other thing and then we continue on

216
00:09:32,450 --> 00:09:35,779
with the parable so if I ask you on this

217
00:09:35,270 --> 00:09:38,750
thing

218
00:09:35,779 --> 00:09:42,370
how many about import descriptors do we

219
00:09:38,750 --> 00:09:42,370
have how many do you think we have

220
00:09:50,230 --> 00:10:00,730
no in its correct believe it's not one

221
00:09:56,500 --> 00:10:03,520
two one two three four five six seven

222
00:10:00,730 --> 00:10:10,900
this is still it this is not it eight

223
00:10:03,520 --> 00:10:18,670
nine this really only matters for please

224
00:10:10,900 --> 00:10:23,400
clarify questions if fun you got the

225
00:10:18,670 --> 00:10:26,740
question right so what should i clarify

226
00:10:23,400 --> 00:10:30,100
okay the number that you have marked

227
00:10:26,740 --> 00:10:33,670
there says one if that said to would the

228
00:10:30,100 --> 00:10:36,490
right answer be eight yes basically

229
00:10:33,670 --> 00:10:38,590
although if it's said to this entry down

230
00:10:36,490 --> 00:10:40,540
here would say reserved like this entry

231
00:10:38,590 --> 00:10:42,700
was I mean that's the little tiny tip

232
00:10:40,540 --> 00:10:44,470
off that this is something else right

233
00:10:42,700 --> 00:10:46,450
everybody else says number of foreigner

234
00:10:44,470 --> 00:10:48,910
modules it's interpreting it that way

235
00:10:46,450 --> 00:10:50,590
this one says reserved because when it

236
00:10:48,910 --> 00:10:52,710
is one of these forward or at things

237
00:10:50,590 --> 00:10:55,570
like the last field is that are used as

238
00:10:52,710 --> 00:10:57,910
reserve but yes you're correct that if

239
00:10:55,570 --> 00:10:59,260
it said to this would be AIDS and this

240
00:10:57,910 --> 00:11:01,300
would just be slightly different

241
00:10:59,260 --> 00:11:03,070
yeah so you have to have at least one

242
00:11:01,300 --> 00:11:06,760
found important descriptor before you

243
00:11:03,070 --> 00:11:08,170
can have any of these yes yes exactly

244
00:11:06,760 --> 00:11:09,910
you'd never be able to have any of these

245
00:11:08,170 --> 00:11:11,920
forward things unless you are binding

246
00:11:09,910 --> 00:11:13,870
against at least one thing to reference

247
00:11:11,920 --> 00:11:14,950
that we've got so it actually is related

248
00:11:13,870 --> 00:11:16,720
to the thing about that

249
00:11:14,950 --> 00:11:20,740
exactly and that's all we'll see when we

250
00:11:16,720 --> 00:11:23,110
go to export this ntdll is in some way

251
00:11:20,740 --> 00:11:25,300
related to the kernel32 as far as

252
00:11:23,110 --> 00:11:27,310
binding and imports and exports is

253
00:11:25,300 --> 00:11:28,930
determined when we get to exports I'll

254
00:11:27,310 --> 00:11:31,270
show you this sort of weird case that

255
00:11:28,930 --> 00:11:33,910
there's this well just skip ahead and

256
00:11:31,270 --> 00:11:36,070
just tell your straight away there's a

257
00:11:33,910 --> 00:11:37,900
place where kernel32 who has a function

258
00:11:36,070 --> 00:11:39,250
it says I explored this function then

259
00:11:37,900 --> 00:11:42,490
you go to try to important sense oh

260
00:11:39,250 --> 00:11:44,530
actually that's an ntdll not zero and so

261
00:11:42,490 --> 00:11:45,940
that's called a forward in export and so

262
00:11:44,530 --> 00:11:47,590
this is just trying to kind of keep

263
00:11:45,940 --> 00:11:49,870
track of that information because the

264
00:11:47,590 --> 00:11:52,630
binding purpose is if you find out that

265
00:11:49,870 --> 00:11:54,370
your kernel32 is on a date that could

266
00:11:52,630 --> 00:11:56,860
have implications if you were importing

267
00:11:54,370 --> 00:11:59,710
something from ntdll secretly you know

268
00:11:56,860 --> 00:12:01,090
that may or may not be out of date as

269
00:11:59,710 --> 00:12:03,670
far as we're concerned for teaching to

270
00:12:01,090 --> 00:12:15,190
the test I may ask you questions

271
00:12:03,670 --> 00:12:16,990
you just need to in big picture the only

272
00:12:15,190 --> 00:12:19,330
point of this data structure is to tell

273
00:12:16,990 --> 00:12:21,760
the OS loader whether or not prefilled

274
00:12:19,330 --> 00:12:24,990
in imported rest table entries are

275
00:12:21,760 --> 00:12:26,950
probably invalid or probably valid

276
00:12:24,990 --> 00:12:28,900
saying as long as you've got this

277
00:12:26,950 --> 00:12:31,450
version where a version just assumed by

278
00:12:28,900 --> 00:12:35,620
ton dates and then your entries are

279
00:12:31,450 --> 00:12:38,260
probably good question yes so these are

280
00:12:35,620 --> 00:12:41,110
all simply just fine optimizations in

281
00:12:38,260 --> 00:12:42,580
terms of loading right I guess it must

282
00:12:41,110 --> 00:12:45,450
be significant when you do it but

283
00:12:42,580 --> 00:12:49,390
doesn't seem like it should

284
00:12:45,450 --> 00:12:52,540
well I think it is I mean it depends on

285
00:12:49,390 --> 00:12:54,730
your determination right so what I'll

286
00:12:52,540 --> 00:12:56,860
say is that I don't think it's

287
00:12:54,730 --> 00:12:57,370
significant anymore and what I'm really

288
00:12:56,860 --> 00:12:59,290
curious

289
00:12:57,370 --> 00:13:03,820
my my question is one of these studies

290
00:12:59,290 --> 00:13:06,220
on Windows 7 because this has to do with

291
00:13:03,820 --> 00:13:08,380
like assuming that the base address is

292
00:13:06,220 --> 00:13:11,230
whatever the base address it's assuming

293
00:13:08,380 --> 00:13:18,970
that you know Perl 32 will be loaded at

294
00:13:11,230 --> 00:13:21,790
the base address in in tears so if

295
00:13:18,970 --> 00:13:23,320
Colonel 32 gets loaded at this location

296
00:13:21,790 --> 00:13:25,450
all those pre filled in entries are

297
00:13:23,320 --> 00:13:28,200
wrong based on the assumptions that's

298
00:13:25,450 --> 00:13:32,680
what they would have to be fixed up so

299
00:13:28,200 --> 00:13:34,330
yeah it's apparently enough that must be

300
00:13:32,680 --> 00:13:38,980
hacked up Microsoft thinks it's worth

301
00:13:34,330 --> 00:13:47,440
keeping around and doing so performance

302
00:13:38,980 --> 00:13:50,530
testing all right so this is again we

303
00:13:47,440 --> 00:13:52,450
had just seen that interpretation of you

304
00:13:50,530 --> 00:13:56,170
know this data structure for normal

305
00:13:52,450 --> 00:13:57,760
imports you know we said IAT they stuff

306
00:13:56,170 --> 00:13:59,050
we don't care about well that time date

307
00:13:57,760 --> 00:14:00,310
step that we didn't care about before

308
00:13:59,050 --> 00:14:02,200
that should have always been zero it's

309
00:14:00,310 --> 00:14:03,970
not apps this is kind of telling you

310
00:14:02,200 --> 00:14:06,490
there's gonna be some bodily ports where

311
00:14:03,970 --> 00:14:08,620
but the real authoritative source of the

312
00:14:06,490 --> 00:14:10,930
factors in five airports is to go to the

313
00:14:08,620 --> 00:14:12,070
data directory and that found import

314
00:14:10,930 --> 00:14:14,050
entry has a nonzero

315
00:14:12,070 --> 00:14:15,700
that's saying look there's a pointer to

316
00:14:14,050 --> 00:14:19,060
a Danish structure for bobbed influence

317
00:14:15,700 --> 00:14:22,150
whose information this is sort of the

318
00:14:19,060 --> 00:14:24,850
normal entry there is if an estate pi32

319
00:14:22,150 --> 00:14:28,150
and the key point is that these these

320
00:14:24,850 --> 00:14:31,810
entries for the actual IAT entries are

321
00:14:28,150 --> 00:14:33,460
pre filled in with real real addresses

322
00:14:31,810 --> 00:14:35,980
you can see these are more like absolute

323
00:14:33,460 --> 00:14:39,810
virtual addresses in the user space and

324
00:14:35,980 --> 00:14:39,810
so these are prefilled in the setup RPAs

325
00:14:40,890 --> 00:14:56,860
to show ya the problem is basically

326
00:14:44,200 --> 00:14:59,110
since pv doesn't say again how do I know

327
00:14:56,860 --> 00:15:03,700
those are bound importance partially

328
00:14:59,110 --> 00:15:05,680
just because so it's again kind of weird

329
00:15:03,700 --> 00:15:23,110
something the right way to do this let's

330
00:15:05,680 --> 00:15:23,980
just look at this in the Explorer import

331
00:15:23,110 --> 00:15:25,810
directory

332
00:15:23,980 --> 00:15:30,640
it's got EPS that are negative ones I

333
00:15:25,810 --> 00:15:34,180
said back here the time date stamp is

334
00:15:30,640 --> 00:15:37,720
negative one in about saying it's bound

335
00:15:34,180 --> 00:15:40,150
importance but I have to have like a

336
00:15:37,720 --> 00:15:44,050
compilation that's like quarry

337
00:15:40,150 --> 00:15:45,730
especially drops in where I want to like

338
00:15:44,050 --> 00:15:52,540
have us couldn't show where it's just

339
00:15:45,730 --> 00:15:53,710
like but the real authoritative way that

340
00:15:52,540 --> 00:15:55,510
I know that these are you know found

341
00:15:53,710 --> 00:15:58,030
imports and that that's a pre filled in

342
00:15:55,510 --> 00:16:00,310
sort of import address territory is

343
00:15:58,030 --> 00:16:04,570
because I go to the data directory I go

344
00:16:00,310 --> 00:16:07,090
to index whatever this is 11 and this is

345
00:16:04,570 --> 00:16:11,440
not 0 no this is just a plugging cff

346
00:16:07,090 --> 00:16:13,810
Explorer it is it's actually not this

347
00:16:11,440 --> 00:16:16,570
RBA is not actually within any section

348
00:16:13,810 --> 00:16:19,270
it actually it turns out this particular

349
00:16:16,570 --> 00:16:22,540
bottom entry it's putting it after the

350
00:16:19,270 --> 00:16:24,460
up after the section headers but before

351
00:16:22,540 --> 00:16:25,840
the first section so it's things like

352
00:16:24,460 --> 00:16:27,550
optional header side

353
00:16:25,840 --> 00:16:28,900
Cheddar's and then it turns out they put

354
00:16:27,550 --> 00:16:30,280
the data structure for the founding

355
00:16:28,900 --> 00:16:32,620
ports like right there after these

356
00:16:30,280 --> 00:16:34,390
section headers and then like here's the

357
00:16:32,620 --> 00:16:36,460
data for the dot text section later on

358
00:16:34,390 --> 00:16:39,970
so it's actually kind of interesting

359
00:16:36,460 --> 00:16:41,230
that that just happens the way that we

360
00:16:39,970 --> 00:16:43,390
know that it's using founding parts

361
00:16:41,230 --> 00:16:46,810
because it's got a non zero on import

362
00:16:43,390 --> 00:16:48,460
RBA telling us that if we go to 2 e 0 we

363
00:16:46,810 --> 00:16:52,150
will see the data structure for about

364
00:16:48,460 --> 00:16:54,280
imports and this doesn't actually parse

365
00:16:52,150 --> 00:16:58,540
not of imports therefore happening like

366
00:16:54,280 --> 00:17:06,970
kind of you would the menu related to e

367
00:16:58,540 --> 00:17:11,650
0 so now I would interpret these as like

368
00:17:06,970 --> 00:17:13,060
time/date stamp our VA to file name and

369
00:17:11,650 --> 00:17:18,430
then number of forwarder wraps

370
00:17:13,060 --> 00:17:20,920
I'm date-stamp rb8 the file name number

371
00:17:18,430 --> 00:17:24,220
of forwarder reps this one actually has

372
00:17:20,920 --> 00:17:26,260
more things than that for the reps I'm a

373
00:17:24,220 --> 00:17:29,290
person in this right let's see that in

374
00:17:26,260 --> 00:17:31,030
preview notepads and this is why I

375
00:17:29,290 --> 00:17:32,260
couldn't just open up notepad before

376
00:17:31,030 --> 00:17:44,830
when I was trying to talk about normal

377
00:17:32,260 --> 00:17:47,670
imports and I know that right 64 bit you

378
00:17:44,830 --> 00:17:50,890
can't look at it so this like I said is

379
00:17:47,670 --> 00:18:05,080
start getting into trouble huge 32-bit

380
00:17:50,890 --> 00:18:07,870
and keep you well let me think if all

381
00:18:05,080 --> 00:18:10,600
right so it's pretty much going to be cm

382
00:18:07,870 --> 00:18:13,030
topics well yeah we're 64 we can

383
00:18:10,600 --> 00:18:14,710
basically up to UCF up explore and you

384
00:18:13,030 --> 00:18:17,230
know if I ask you what the r-va of a

385
00:18:14,710 --> 00:18:21,280
particularly thing is this is actually

386
00:18:17,230 --> 00:18:22,960
an ABA so I actually have hopefully

387
00:18:21,280 --> 00:18:24,400
don't ask you that the RBA is because

388
00:18:22,960 --> 00:18:26,020
then you'd have to go and find the

389
00:18:24,400 --> 00:18:28,120
kernel 32 and then you have to get in

390
00:18:26,020 --> 00:18:30,160
space address and subtract obviously

391
00:18:28,120 --> 00:18:31,170
hopefully I'm just asking you what's the

392
00:18:30,160 --> 00:18:34,890
virtual address

393
00:18:31,170 --> 00:18:38,580
or particular entry this round alright

394
00:18:34,890 --> 00:18:40,830
well that's the easy extra import thing

395
00:18:38,580 --> 00:18:43,320
let's take a five-minute break and then

396
00:18:40,830 --> 00:18:45,510
let's go on to the heart of them talking

397
00:18:43,320 --> 00:18:48,210
about the found import information again

398
00:18:45,510 --> 00:18:50,850
in the Navy directory there's this entry

399
00:18:48,210 --> 00:18:54,510
points at these data structures that

400
00:18:50,850 --> 00:18:56,640
kind of aid status and offsets names it

401
00:18:54,510 --> 00:18:58,920
looks like that the purposes of the

402
00:18:56,640 --> 00:19:03,300
question if you see something that has

403
00:18:58,920 --> 00:19:05,190
border reps subtract that many entries

404
00:19:03,300 --> 00:19:12,510
from how many felony or descriptor

405
00:19:05,190 --> 00:19:14,150
entries and yeah so I guess what I

406
00:19:12,510 --> 00:19:21,300
didn't cover before we broke for lunch

407
00:19:14,150 --> 00:19:23,010
rate is the question is like how do you

408
00:19:21,300 --> 00:19:25,020
actually buy something right so if you

409
00:19:23,010 --> 00:19:27,060
want to go find your binary how do you

410
00:19:25,020 --> 00:19:29,790
do it well there's an API but finding

411
00:19:27,060 --> 00:19:32,340
the jex the installer notes have

412
00:19:29,790 --> 00:19:34,380
actually a by the image option that you

413
00:19:32,340 --> 00:19:36,780
can actually set something to find that

414
00:19:34,380 --> 00:19:38,400
install times back you know it's like if

415
00:19:36,780 --> 00:19:40,380
you're doing a new Windows install you

416
00:19:38,400 --> 00:19:41,880
set it up to install and it finds it

417
00:19:40,380 --> 00:19:43,860
then it you know does the speed hack a

418
00:19:41,880 --> 00:19:48,410
little bit there used to be a five dot

419
00:19:43,860 --> 00:19:48,410
exe this is not there anymore

420
00:19:58,910 --> 00:20:04,110
but but how I did bind for these

421
00:20:02,460 --> 00:20:06,240
template binaries that you're going to

422
00:20:04,110 --> 00:20:07,950
be away from the classes just one of

423
00:20:06,240 --> 00:20:10,260
these nice little things about cff

424
00:20:07,950 --> 00:20:13,350
Explorer you can throw any binary in

425
00:20:10,260 --> 00:20:15,810
there go to the rebuilder divide the

426
00:20:13,350 --> 00:20:17,760
import table and then cff Explorer will

427
00:20:15,810 --> 00:20:20,730
go out and run around and say okay well

428
00:20:17,760 --> 00:20:23,100
I see that this guy imports you know

429
00:20:20,730 --> 00:20:25,860
okay so that's good example that's Tunis

430
00:20:23,100 --> 00:20:27,300
just for the sake of it you said this is

431
00:20:25,860 --> 00:20:29,400
doing normal imports right now that

432
00:20:27,300 --> 00:20:31,890
you've already rows here instead of

433
00:20:29,400 --> 00:20:33,930
negative ones right I click on one of

434
00:20:31,890 --> 00:20:34,900
these entries that looks like an RBA

435
00:20:33,930 --> 00:20:37,630
that doesn't look like

436
00:20:34,900 --> 00:20:40,180
virtual address right our PA is not

437
00:20:37,630 --> 00:20:43,140
absolute virtual addresses pandu

438
00:20:40,180 --> 00:20:47,590
rebuilder click find import table we go

439
00:20:43,140 --> 00:20:48,910
back to the imports and now that looks a

440
00:20:47,590 --> 00:20:50,710
little more like an absolute virtual

441
00:20:48,910 --> 00:21:20,680
address but a little more clear down

442
00:20:50,710 --> 00:21:24,210
here these are I think that would know

443
00:21:20,680 --> 00:21:29,620
just like alright well it's possible

444
00:21:24,210 --> 00:21:31,720
this time so I guess I can just show you

445
00:21:29,620 --> 00:21:39,420
what this is 739 actually changed the

446
00:21:31,720 --> 00:21:39,420
DLL originally it actually changes the I

447
00:21:46,200 --> 00:22:40,120
mean the only difference between as I

448
00:22:37,990 --> 00:22:42,760
was kind of already saying the others

449
00:22:40,120 --> 00:22:44,950
are and binding or kind of in opposition

450
00:22:42,760 --> 00:22:46,900
to each other fighting makes the

451
00:22:44,950 --> 00:22:49,030
assumption that you're going to load

452
00:22:46,900 --> 00:22:50,770
this thing and if you have a dress and

453
00:22:49,030 --> 00:22:54,190
ice are moves the thing around now that

454
00:22:50,770 --> 00:22:58,090
said it's not entirely invalidates and

455
00:22:54,190 --> 00:23:01,630
things right so interest or sorry ng if

456
00:22:58,090 --> 00:23:03,220
kernel32.dll says I want a dress seven

457
00:23:01,630 --> 00:23:07,420
six zero zero zero zero

458
00:23:03,220 --> 00:23:09,700
they almost look at that 77070 right the

459
00:23:07,420 --> 00:23:11,170
OS loader could still fix up all of

460
00:23:09,700 --> 00:23:13,120
those found on the imports by just

461
00:23:11,170 --> 00:23:15,010
adding the offset between what AMS

462
00:23:13,120 --> 00:23:15,900
artists and where it actually ended up

463
00:23:15,010 --> 00:23:18,730
so it doesn't have to necessarily

464
00:23:15,900 --> 00:23:20,350
recalculate them it could just add the

465
00:23:18,730 --> 00:23:25,150
offset to all those pre filling and

466
00:23:20,350 --> 00:23:26,560
entry so there's some what I thought I

467
00:23:25,150 --> 00:23:32,640
don't know if I should say I don't know

468
00:23:26,560 --> 00:23:35,500
if it actually does that or not I know

469
00:23:32,640 --> 00:23:39,060
but basically you know this R decreases

470
00:23:35,500 --> 00:23:39,060
the efficacy in this


1
00:00:03,960 --> 00:00:08,920
okay so I'm gonna do a quick like

2
00:00:06,040 --> 00:00:10,510
picture here where I show the difference

3
00:00:08,920 --> 00:00:14,590
so we've got the elf header and then

4
00:00:10,510 --> 00:00:16,360
we've got the array of program headers

5
00:00:14,590 --> 00:00:19,060
or segment headers you can call them

6
00:00:16,360 --> 00:00:20,770
either we've got this array of program

7
00:00:19,060 --> 00:00:22,390
headers and what are the offsets what

8
00:00:20,770 --> 00:00:25,660
are the file sizes where would it map in

9
00:00:22,390 --> 00:00:27,130
the file so the P header thing is

10
00:00:25,660 --> 00:00:27,789
literally just mapping over its own

11
00:00:27,130 --> 00:00:29,890
table

12
00:00:27,789 --> 00:00:33,730
it starts at 40 which is right after the

13
00:00:29,890 --> 00:00:37,000
elf header and then it has file size P

14
00:00:33,730 --> 00:00:44,020
interp is that 2:38 and this is not you

15
00:00:37,000 --> 00:00:45,670
know just scale anyways then you've got

16
00:00:44,020 --> 00:00:47,290
the P and Terp after these program

17
00:00:45,670 --> 00:00:48,550
headers it's just a string saying this

18
00:00:47,290 --> 00:00:50,710
is the thing that has to be loaded

19
00:00:48,550 --> 00:00:53,530
before my programs and then we've got

20
00:00:50,710 --> 00:00:55,600
the PT load so as I said load is the

21
00:00:53,530 --> 00:00:57,160
only thing that says take this chunk of

22
00:00:55,600 --> 00:00:59,200
the file and move it into memory so

23
00:00:57,160 --> 00:01:01,809
those other two things are obviously

24
00:00:59,200 --> 00:01:03,430
well within that load segment so they

25
00:01:01,809 --> 00:01:07,180
will still make their way into memory

26
00:01:03,430 --> 00:01:08,709
but but the PT load will have its

27
00:01:07,180 --> 00:01:11,499
permissions which will be applied to

28
00:01:08,709 --> 00:01:13,869
this big chunk of memory all right

29
00:01:11,499 --> 00:01:15,759
oh here's that sort of gap I was showing

30
00:01:13,869 --> 00:01:16,630
right here's the first load segment

31
00:01:15,759 --> 00:01:18,609
here's the second one

32
00:01:16,630 --> 00:01:22,270
there's some weird no-man's land between

33
00:01:18,609 --> 00:01:24,509
60 C and E 1/8 where there's stuff in

34
00:01:22,270 --> 00:01:28,539
the file that's not actually going to be

35
00:01:24,509 --> 00:01:31,179
mapped into memory and then down here

36
00:01:28,539 --> 00:01:34,719
with this last one we've got you know

37
00:01:31,179 --> 00:01:37,289
file size of 208 mm size of to 1/8 so

38
00:01:34,719 --> 00:01:40,420
what does that mean we've got a BSS

39
00:01:37,289 --> 00:01:42,099
information then we've got dynamic which

40
00:01:40,420 --> 00:01:45,130
you can see is well within that last

41
00:01:42,099 --> 00:01:48,639
load segment we've got note which is in

42
00:01:45,130 --> 00:01:54,189
that personal segment a Nui frame stack

43
00:01:48,639 --> 00:02:04,239
is 0 size and that's how the things are

44
00:01:54,189 --> 00:02:06,399
overall so all right I'm gonna give you

45
00:02:04,239 --> 00:02:08,560
a 5-minute break and then we're going to

46
00:02:06,399 --> 00:02:11,380
kind of talk about there's a little

47
00:02:08,560 --> 00:02:14,380
interesting thing with the elf load

48
00:02:11,380 --> 00:02:18,060
segments where I'll basically be proving

49
00:02:14,380 --> 00:02:21,310
to you that although for instance

50
00:02:18,060 --> 00:02:24,370
we've got this second load segment right

51
00:02:21,310 --> 00:02:27,159
and I say it starts at e1 8 and it ends

52
00:02:24,370 --> 00:02:28,780
at 10:20 like that's all the file data

53
00:02:27,159 --> 00:02:31,510
that should be moved from file to memory

54
00:02:28,780 --> 00:02:35,609
right what I will justify for you in the

55
00:02:31,510 --> 00:02:38,319
next section is showing the OS loader is

56
00:02:35,609 --> 00:02:40,560
much more I don't know what I say lazy

57
00:02:38,319 --> 00:02:43,329
but I want to say

58
00:02:40,560 --> 00:02:45,939
coarse-grained and so the OS loader

59
00:02:43,329 --> 00:02:48,010
grabs like chunks of hex 1000 out of the

60
00:02:45,939 --> 00:02:50,319
file so even though this is you know not

61
00:02:48,010 --> 00:02:52,750
hex 1000 worth of data it just grabs

62
00:02:50,319 --> 00:02:55,689
chunks of hex 1000 aligned chunks of the

63
00:02:52,750 --> 00:02:57,669
file and just flops that into memory so

64
00:02:55,689 --> 00:02:59,470
all of that no man's land stuff will end

65
00:02:57,669 --> 00:03:02,560
up in memory and all of a bunch of stuff

66
00:02:59,470 --> 00:03:04,659
after this from 1 0 to 0 in file all the

67
00:03:02,560 --> 00:03:07,359
way to hex 2000 and file or wherever the

68
00:03:04,659 --> 00:03:09,129
file ends a bunch of extra superfluous

69
00:03:07,359 --> 00:03:12,340
data is going to get mapped into memory

70
00:03:09,129 --> 00:03:15,579
by the by the OS loader just because it

71
00:03:12,340 --> 00:03:18,639
grabs big fat hex 1000 chunks of file

72
00:03:15,579 --> 00:03:20,079
and flops them into memory so I'll let

73
00:03:18,639 --> 00:03:22,060
just take five and then we'll come back

74
00:03:20,079 --> 00:03:25,060
justify that with the debugger and move

75
00:03:22,060 --> 00:03:27,840
on to the section inner we left off Xena

76
00:03:25,060 --> 00:03:33,510
was telling you that the elf or the

77
00:03:27,840 --> 00:03:36,129
Linux OS loader was being a bit too

78
00:03:33,510 --> 00:03:37,870
loose with its interpretation of the

79
00:03:36,129 --> 00:03:40,150
program headers of the segment's he was

80
00:03:37,870 --> 00:03:43,989
saying just because it says that a

81
00:03:40,150 --> 00:03:48,069
particular segment only goes from let's

82
00:03:43,989 --> 00:03:49,629
say e 1 8 to 1 0 to 0 doesn't mean

83
00:03:48,069 --> 00:03:50,919
that's the only file data that's

84
00:03:49,629 --> 00:03:54,489
actually going to be mapped into

85
00:03:50,919 --> 00:03:57,040
memories so now to justify this I'm

86
00:03:54,489 --> 00:04:00,819
going to show so my argument will be

87
00:03:57,040 --> 00:04:05,019
basically that the OS loader will grab

88
00:04:00,819 --> 00:04:08,229
stuff in X 1000 chunk file size hex 1000

89
00:04:05,019 --> 00:04:10,299
byte file size chunks so first of all

90
00:04:08,229 --> 00:04:13,269
what that would mean is when it first

91
00:04:10,299 --> 00:04:15,430
maps this segment into memory it's going

92
00:04:13,269 --> 00:04:19,180
to go from 0 to X 1000 and it's gonna

93
00:04:15,430 --> 00:04:22,389
map everything into memory at 400,000

94
00:04:19,180 --> 00:04:25,029
wherever it happens to happen right all

95
00:04:22,389 --> 00:04:26,830
right and so can you prove that right I

96
00:04:25,029 --> 00:04:29,800
can pick something that's file data

97
00:04:26,830 --> 00:04:30,400
outside of this thing's memory space and

98
00:04:29,800 --> 00:04:31,840
I can show the

99
00:04:30,400 --> 00:04:35,020
that's getting mapped into memory right

100
00:04:31,840 --> 00:04:38,199
now so I'm gonna pick X 6 or 700 as

101
00:04:35,020 --> 00:04:40,720
something that's greater than 60 see but

102
00:04:38,199 --> 00:04:43,990
less than e 1 age so heck 700 will be

103
00:04:40,720 --> 00:04:47,139
our magic file data place where there's

104
00:04:43,990 --> 00:04:49,870
gonna be something mapped into memory so

105
00:04:47,139 --> 00:04:56,050
to show this I'm going to use the

106
00:04:49,870 --> 00:04:59,530
debugger and yeah okay first I argue it

107
00:04:56,050 --> 00:05:02,350
in first I argue in and slide form then

108
00:04:59,530 --> 00:05:04,539
I'll argue it in debug report all right

109
00:05:02,350 --> 00:05:06,970
so here's what I'm saying basically the

110
00:05:04,539 --> 00:05:07,419
OS loader is gonna grab hex 1000 bytes

111
00:05:06,970 --> 00:05:11,350
at a time

112
00:05:07,419 --> 00:05:12,940
so where as this really says 6d see I'm

113
00:05:11,350 --> 00:05:15,520
saying that it's actually gonna grab all

114
00:05:12,940 --> 00:05:18,550
the weight X 1000 and so we've got extra

115
00:05:15,520 --> 00:05:20,470
data from between 60 C and 1000 that's

116
00:05:18,550 --> 00:05:22,720
getting mapped from file into memory

117
00:05:20,470 --> 00:05:26,380
even though it has no relevant it's no

118
00:05:22,720 --> 00:05:27,310
program should touch it and so forth all

119
00:05:26,380 --> 00:05:29,650
right so that's what's gonna actually

120
00:05:27,310 --> 00:05:31,419
get mapped to memory file data this is

121
00:05:29,650 --> 00:05:33,310
the memory data because this is the

122
00:05:31,419 --> 00:05:37,810
400,000 where it's actually going to be

123
00:05:33,310 --> 00:05:42,880
mapped into memory so you knows believe

124
00:05:37,810 --> 00:05:46,930
it or not I'm going to show that you

125
00:05:42,880 --> 00:05:49,360
know this data is actually gonna be

126
00:05:46,930 --> 00:05:52,030
mapped into memory and I'm actually not

127
00:05:49,360 --> 00:05:56,440
going to use 6dc I'm going to use hex

128
00:05:52,030 --> 00:06:00,099
700 in this round number and easier so

129
00:05:56,440 --> 00:06:10,210
if I look at the data at let's say 700

130
00:06:00,099 --> 00:06:14,789
now and so 700 should obviously be

131
00:06:10,210 --> 00:06:16,900
mapped at 400 400 thousand seven hundred

132
00:06:14,789 --> 00:06:18,490
all right so I want to show you the file

133
00:06:16,900 --> 00:06:19,720
data and then I'll say like that file

134
00:06:18,490 --> 00:06:22,539
date is definitely going to be mapped

135
00:06:19,720 --> 00:06:27,690
into memory at that virtual address all

136
00:06:22,539 --> 00:06:34,830
right so I can do that with hex don't

137
00:06:27,690 --> 00:06:34,830
see next up - C - s

138
00:06:36,930 --> 00:06:45,400
Ashtyn for eight fights right skip s is

139
00:06:42,460 --> 00:06:47,950
skip 0 X 700 bytes into the file

140
00:06:45,400 --> 00:06:52,930
don't the number of bytes is 8 and the

141
00:06:47,950 --> 00:06:55,330
file is hello all right so I'm going to

142
00:06:52,930 --> 00:06:58,510
show you that zeros and file have been

143
00:06:55,330 --> 00:07:03,340
mapped to zeros and memory that's not a

144
00:06:58,510 --> 00:07:07,510
particular convincing example right Oh

145
00:07:03,340 --> 00:07:09,310
don't you hate zeros unacceptable so I'm

146
00:07:07,510 --> 00:07:11,530
going to show that really the data a the

147
00:07:09,310 --> 00:07:13,540
exact data that I'm saying on disk is

148
00:07:11,530 --> 00:07:15,210
getting mapped into memory so I'm gonna

149
00:07:13,540 --> 00:07:32,440
change that

150
00:07:15,210 --> 00:07:33,910
700 do E's and B's all right it's more

151
00:07:32,440 --> 00:07:35,800
like you know if I go show you the

152
00:07:33,910 --> 00:07:37,660
memory and there's zeros there that's

153
00:07:35,800 --> 00:07:39,670
not a good proof that like those zeros

154
00:07:37,660 --> 00:07:41,950
were the zeros from file right it'd just

155
00:07:39,670 --> 00:07:44,110
be any other zeros so I want to show you

156
00:07:41,950 --> 00:07:46,510
very specifically I'm going to put data

157
00:07:44,110 --> 00:07:51,070
there at the 700 and that was definitely

158
00:07:46,510 --> 00:07:52,510
the data from file and now I'll prove

159
00:07:51,070 --> 00:07:54,160
that at the data that gets into memory

160
00:07:52,510 --> 00:07:56,260
that will show that it's reading too

161
00:07:54,160 --> 00:07:59,169
much data from that first segment and

162
00:07:56,260 --> 00:08:05,890
mapping it into memory all right so

163
00:07:59,169 --> 00:08:09,760
exited hello go to offset 700 put in one

164
00:08:05,890 --> 00:08:14,440
for one for one or two for two A's and

165
00:08:09,760 --> 00:08:16,810
B's file save the file hello I'm just

166
00:08:14,440 --> 00:08:18,100
overriding this data that I claim is

167
00:08:16,810 --> 00:08:19,630
more like padding data it's not

168
00:08:18,100 --> 00:08:21,970
legitimate data that's used by anybody

169
00:08:19,630 --> 00:08:25,090
could never mapped into memory it's not

170
00:08:21,970 --> 00:08:30,580
covered by any program segment it was it

171
00:08:25,090 --> 00:08:34,300
could be used by a segment section okay

172
00:08:30,580 --> 00:08:38,460
so you can use gdb they can do debugger

173
00:08:34,300 --> 00:08:45,130
to run this program hello

174
00:08:38,460 --> 00:08:48,070
I'm going to put a break on main and I

175
00:08:45,130 --> 00:08:50,030
am going to run it alright so start at

176
00:08:48,070 --> 00:08:52,160
the program put a breakpoint I manage

177
00:08:50,030 --> 00:08:53,990
starts with the very stops at the very

178
00:08:52,160 --> 00:08:55,730
beginning and now what I want to see is

179
00:08:53,990 --> 00:08:58,100
that four hundred thousand seven hundred

180
00:08:55,730 --> 00:09:03,040
he's going to have my for one for one

181
00:08:58,100 --> 00:09:05,240
for one so I'm going to do X slash eight

182
00:09:03,040 --> 00:09:07,850
characters so I'm going to deter prett

183
00:09:05,240 --> 00:09:11,300
the memory going to examine memory eight

184
00:09:07,850 --> 00:09:15,370
characters starting at four hundred

185
00:09:11,300 --> 00:09:20,710
thousand seven hundred there we go AAA

186
00:09:15,370 --> 00:09:20,710
BBB or should I think that's thoughtful

187
00:09:22,390 --> 00:09:31,970
yeah I can do it in just hex bytes two

188
00:09:29,170 --> 00:09:32,630
for one for one for one for two for two

189
00:09:31,970 --> 00:09:35,510
for two

190
00:09:32,630 --> 00:09:37,430
so hex 700 data from file has been

191
00:09:35,510 --> 00:09:39,830
mapped into memory at the expected

192
00:09:37,430 --> 00:09:41,360
virtual memory address even though no

193
00:09:39,830 --> 00:09:44,960
one ever asked for it to be mapped into

194
00:09:41,360 --> 00:09:47,000
memory there so the program the OS

195
00:09:44,960 --> 00:09:49,760
loader is grabbing these big chunks

196
00:09:47,000 --> 00:09:51,890
where it's rounding sizes down and

197
00:09:49,760 --> 00:09:53,630
rounding sizes up to the nearest X 1000

198
00:09:51,890 --> 00:09:56,180
so it started at 0 so when it rounds

199
00:09:53,630 --> 00:09:58,790
that down that's still 0 and when it

200
00:09:56,180 --> 00:10:04,640
rounds it up it's 1,000 so the same sort

201
00:09:58,790 --> 00:10:07,660
of thing will happen on the so you throw

202
00:10:04,640 --> 00:10:12,040
four ones and four twos at the problem

203
00:10:07,660 --> 00:10:12,040
it's the same thing will happen here so

204
00:10:13,780 --> 00:10:19,550
yes so what I'm going to claim here is

205
00:10:16,339 --> 00:10:22,250
that it starts at a 1/8 and it's going

206
00:10:19,550 --> 00:10:24,440
to round to the start address down to

207
00:10:22,250 --> 00:10:26,360
the nearest hex 1000 aligned thing so

208
00:10:24,440 --> 00:10:29,540
the nearest X 1000 a line thing that's

209
00:10:26,360 --> 00:10:32,510
smaller than e 1/8 is just 0 so it's

210
00:10:29,540 --> 00:10:35,080
aligned in the sense that it doesn't

211
00:10:32,510 --> 00:10:38,420
have any of the least significant bits F

212
00:10:35,080 --> 00:10:41,180
so that goes down to 0 and it's going to

213
00:10:38,420 --> 00:10:45,260
round up at the end address so 1020

214
00:10:41,180 --> 00:10:46,760
rounded up is 2,000 so the file data

215
00:10:45,260 --> 00:10:49,250
that it's actually going to grab is from

216
00:10:46,760 --> 00:10:50,630
0 to 2000 so some of that data right

217
00:10:49,250 --> 00:10:52,880
there is you know going to have been

218
00:10:50,630 --> 00:10:53,360
overwritten by the first segment so they

219
00:10:52,880 --> 00:10:57,950
won't

220
00:10:53,360 --> 00:11:00,350
wellactually sorry so that data will be

221
00:10:57,950 --> 00:11:02,420
so all of this data will actually be it

222
00:11:00,350 --> 00:11:03,560
mapped in the reason it doesn't smash

223
00:11:02,420 --> 00:11:06,410
the other

224
00:11:03,560 --> 00:11:09,020
load thing is that this segment gets

225
00:11:06,410 --> 00:11:11,090
loaded at 600,000 not 400,000 so the

226
00:11:09,020 --> 00:11:13,430
first one loaded at for a dozen second

227
00:11:11,090 --> 00:11:15,470
with loads at 600,000 so even though it

228
00:11:13,430 --> 00:11:17,029
rounds down and like grabs all that

229
00:11:15,470 --> 00:11:20,870
extra data it's not like smashing the

230
00:11:17,029 --> 00:11:22,940
previous so we've got all this extra

231
00:11:20,870 --> 00:11:24,440
data that's mapped in so really all of

232
00:11:22,940 --> 00:11:26,529
the code and everything else from the

233
00:11:24,440 --> 00:11:29,360
beginning in the file happens to be at

234
00:11:26,529 --> 00:11:32,570
400,000 and it also happens to be

235
00:11:29,360 --> 00:11:35,510
somewhere around 600,000 so this is just

236
00:11:32,570 --> 00:11:36,980
sort of a it's up says this is the only

237
00:11:35,510 --> 00:11:38,330
reason I put this in here is because I

238
00:11:36,980 --> 00:11:40,100
had to deal with this when I was dealing

239
00:11:38,330 --> 00:12:00,260
with Packers and things like that but

240
00:11:40,100 --> 00:12:02,330
yeah it says the sizes so basically the

241
00:12:00,260 --> 00:12:04,339
the real data is still going to be

242
00:12:02,330 --> 00:12:06,230
mapped at the right location and that's

243
00:12:04,339 --> 00:12:08,720
part of why it does this round down as

244
00:12:06,230 --> 00:12:11,630
well so it rounds down the file data and

245
00:12:08,720 --> 00:12:14,180
the reason why in the headers it says

246
00:12:11,630 --> 00:12:18,860
the virtual address of the data starting

247
00:12:14,180 --> 00:12:20,390
at a 1/8 in the file is 600 a 1/8 the

248
00:12:18,860 --> 00:12:22,400
reason that works out is because they

249
00:12:20,390 --> 00:12:25,760
round down the read in and then they

250
00:12:22,400 --> 00:12:28,580
just slap that thing at 600,000 and then

251
00:12:25,760 --> 00:12:30,770
it ultimately you know if you start at 0

252
00:12:28,580 --> 00:12:32,990
and you moved to ep-1 8 it would have

253
00:12:30,770 --> 00:12:36,170
just basically mapped up perfectly so

254
00:12:32,990 --> 00:12:38,270
that they could have absolutely said

255
00:12:36,170 --> 00:12:40,339
like I'm going to force this thing to be

256
00:12:38,270 --> 00:12:42,560
mapped at 600,000 so that it would have

257
00:12:40,339 --> 00:12:46,880
started with the data from a 1/8 and

258
00:12:42,560 --> 00:12:49,280
moved it to 6 1,000 our 600 a 1/8 no

259
00:12:46,880 --> 00:12:50,660
sorry 600,000 they could have changed

260
00:12:49,280 --> 00:12:53,750
this round anyway they wanted basically

261
00:12:50,660 --> 00:12:56,600
but in order to make it simple and easy

262
00:12:53,750 --> 00:12:59,300
for the OS loader to deal with because

263
00:12:56,600 --> 00:13:00,589
the compiler knew that the OS loader is

264
00:12:59,300 --> 00:13:02,720
doing this thing where it's routing

265
00:13:00,589 --> 00:13:06,410
stuff down it shows the start address

266
00:13:02,720 --> 00:13:09,529
for this segment to be 600 a 1/8 instead

267
00:13:06,410 --> 00:13:11,390
of just say 600,000 because it knew that

268
00:13:09,529 --> 00:13:14,060
the OS loader would be reading the file

269
00:13:11,390 --> 00:13:14,470
data starting from 0 because it rounded

270
00:13:14,060 --> 00:13:16,150
down the

271
00:13:14,470 --> 00:13:18,490
one eight two zero and then it would

272
00:13:16,150 --> 00:13:21,700
just slap stuff in and so the real data

273
00:13:18,490 --> 00:13:24,250
would still end up at 681 eight the real

274
00:13:21,700 --> 00:13:26,260
data that they really wanted math it's

275
00:13:24,250 --> 00:13:28,720
basically that's all extra data that's

276
00:13:26,260 --> 00:13:30,970
all extra data this small little section

277
00:13:28,720 --> 00:13:34,750
right here is the real thing according

278
00:13:30,970 --> 00:13:37,120
to the program headers right that's the

279
00:13:34,750 --> 00:13:38,650
real data it's trying to map but because

280
00:13:37,120 --> 00:13:42,430
of this rounding that happens on either

281
00:13:38,650 --> 00:13:44,050
side it over maps stuff in the file but

282
00:13:42,430 --> 00:13:57,700
this real data still gets where it needs

283
00:13:44,050 --> 00:13:59,800
to go so real data still so that's a

284
00:13:57,700 --> 00:14:00,940
good question and I'm not sure so

285
00:13:59,800 --> 00:14:02,140
there's there's two different kinds of

286
00:14:00,940 --> 00:14:04,210
extra data right there's extra day

287
00:14:02,140 --> 00:14:06,340
that's just mapped up the rounded stuff

288
00:14:04,210 --> 00:14:08,500
on either side and there's extra data

289
00:14:06,340 --> 00:14:12,010
which is more like the difference

290
00:14:08,500 --> 00:14:14,080
between these two segments right there's

291
00:14:12,010 --> 00:14:15,610
a hole between those two and the

292
00:14:14,080 --> 00:14:18,910
question is what is that stuff that's

293
00:14:15,610 --> 00:14:21,760
actually in here right I'm going to

294
00:14:18,910 --> 00:14:24,400
guess that that's section information so

295
00:14:21,760 --> 00:14:26,830
those are particular sections which are

296
00:14:24,400 --> 00:14:28,510
probably optional and probably don't

297
00:14:26,830 --> 00:14:29,680
necessarily need to be in there like it

298
00:14:28,510 --> 00:14:32,110
probably could have gotten away with

299
00:14:29,680 --> 00:14:36,610
taking this and like slapping it right

300
00:14:32,110 --> 00:14:38,200
after this but I think based on the

301
00:14:36,610 --> 00:14:40,210
compiler options it's probably just

302
00:14:38,200 --> 00:14:41,740
having extra information that's coming

303
00:14:40,210 --> 00:14:43,660
in from the linker and just merging it

304
00:14:41,740 --> 00:14:45,310
into the file and it's putting it there

305
00:14:43,660 --> 00:14:47,440
it's information that doesn't need to be

306
00:14:45,310 --> 00:14:49,840
mapped into memory necessarily think of

307
00:14:47,440 --> 00:14:52,720
for instance the relocations back in the

308
00:14:49,840 --> 00:14:54,640
P files relocation information it gets

309
00:14:52,720 --> 00:14:56,350
used when the thing gets loaded up if

310
00:14:54,640 --> 00:14:58,420
there's any sort of movement in memory

311
00:14:56,350 --> 00:15:00,940
but then it gets thrown away and maps

312
00:14:58,420 --> 00:15:02,590
back out of memory so you could do the

313
00:15:00,940 --> 00:15:03,970
same sort of thing here with like not

314
00:15:02,590 --> 00:15:05,530
even mapping into memory you could put

315
00:15:03,970 --> 00:15:08,530
all that relocation information right

316
00:15:05,530 --> 00:15:10,570
there in this gap the OS loader could

317
00:15:08,530 --> 00:15:14,170
just read that information use it and

318
00:15:10,570 --> 00:15:16,030
then throw it away so basically the

319
00:15:14,170 --> 00:15:18,160
short answer is I don't know I haven't

320
00:15:16,030 --> 00:15:20,440
investigated what's in there I just took

321
00:15:18,160 --> 00:15:23,830
this hello world and compiled it and

322
00:15:20,440 --> 00:15:25,270
made pictures for it but I'm fairly

323
00:15:23,830 --> 00:15:27,460
certain that this is all going to be

324
00:15:25,270 --> 00:15:29,560
section information

325
00:15:27,460 --> 00:15:31,060
the linking stages which is going to be

326
00:15:29,560 --> 00:15:33,460
kind of optional it's not used at

327
00:15:31,060 --> 00:15:36,160
runtime it's not executable information

328
00:15:33,460 --> 00:15:39,490
but it was stuff that just got basically

329
00:15:36,160 --> 00:15:45,399
merged in my virtue of it was used at

330
00:15:39,490 --> 00:15:48,040
linking time and it's optional no I

331
00:15:45,399 --> 00:15:59,320
absolutely wouldn't expect it to be just

332
00:15:48,040 --> 00:16:01,060
plain data on the disk so I would say

333
00:15:59,320 --> 00:16:02,649
the biggest security sort of implication

334
00:16:01,060 --> 00:16:05,230
from this is that people who don't

335
00:16:02,649 --> 00:16:06,550
realize this is happening that's like I

336
00:16:05,230 --> 00:16:09,279
said part of the reason I put this in

337
00:16:06,550 --> 00:16:11,440
here we've got this gap right here and

338
00:16:09,279 --> 00:16:13,060
if you just like don't realize that the

339
00:16:11,440 --> 00:16:15,209
loader does this rounding down and

340
00:16:13,060 --> 00:16:17,649
rounding up as far as you're concerned

341
00:16:15,209 --> 00:16:20,050
there's nothing important in that hole

342
00:16:17,649 --> 00:16:21,370
right the attacker can put code in there

343
00:16:20,050 --> 00:16:22,779
and when you're looking at the file

344
00:16:21,370 --> 00:16:24,399
you're just saying like okay there's the

345
00:16:22,779 --> 00:16:26,080
code and there's this other code that's

346
00:16:24,399 --> 00:16:28,839
what I got to focus on right and there

347
00:16:26,080 --> 00:16:30,940
could be other code that's in here and

348
00:16:28,839 --> 00:16:32,529
it does get mapped into memory and it

349
00:16:30,940 --> 00:16:34,660
can be used at runtime they just got to

350
00:16:32,529 --> 00:16:36,550
jump into that gap where that stuff

351
00:16:34,660 --> 00:16:37,660
happened to be getting mapped right and

352
00:16:36,550 --> 00:16:39,330
so they can jump in there and play

353
00:16:37,660 --> 00:16:41,410
around as much as they want in there and

354
00:16:39,330 --> 00:16:43,540
it's just when you're looking at the

355
00:16:41,410 --> 00:16:45,010
file post facto you're looking at doing

356
00:16:43,540 --> 00:16:46,779
forensic analysis you're doing you know

357
00:16:45,010 --> 00:16:49,060
static analysis if you're not

358
00:16:46,779 --> 00:16:50,529
recognizing that that stuff is gonna get

359
00:16:49,060 --> 00:16:53,440
put into memory and that stuff is

360
00:16:50,529 --> 00:16:55,300
available for runtime code then you're

361
00:16:53,440 --> 00:16:57,010
potentially missing stuff and you can

362
00:16:55,300 --> 00:16:58,839
also play games with the loader things

363
00:16:57,010 --> 00:17:00,550
where you can overlap segments and stuff

364
00:16:58,839 --> 00:17:02,020
like that so you can smash one segment

365
00:17:00,550 --> 00:17:04,709
with the other segment reading different

366
00:17:02,020 --> 00:17:08,020
data from files and things like that so

367
00:17:04,709 --> 00:17:09,459
that's the main security relevant reason

368
00:17:08,020 --> 00:17:11,410
why I really thought it's still

369
00:17:09,459 --> 00:17:13,030
important to understand that there's

370
00:17:11,410 --> 00:17:15,459
this round up and round down going on

371
00:17:13,030 --> 00:17:16,929
because when we get to the Packers we're

372
00:17:15,459 --> 00:17:19,510
dealing with people who are explicitly

373
00:17:16,929 --> 00:17:21,069
manipulating header information so that

374
00:17:19,510 --> 00:17:22,329
they can you know map stuff wherever

375
00:17:21,069 --> 00:17:24,130
they want and if they want they can

376
00:17:22,329 --> 00:17:26,050
overlap stuff if it's to their benefit

377
00:17:24,130 --> 00:17:30,660
it just all depends what exactly they're

378
00:17:26,050 --> 00:17:30,660
trying to do there any other questions

379
00:17:30,900 --> 00:17:36,900
alrighty so good I think we're still

380
00:17:38,400 --> 00:17:43,150
okay so that was proof that there's this

381
00:17:41,380 --> 00:17:45,250
over mapping going on where it rounds

382
00:17:43,150 --> 00:17:48,400
down and rounds up on what it reads from

383
00:17:45,250 --> 00:17:50,740
file and so this is basically what I

384
00:17:48,400 --> 00:17:52,690
expect the same memory there's this

385
00:17:50,740 --> 00:17:55,270
stuff which it really wanted to map in

386
00:17:52,690 --> 00:17:56,710
and then there's this over red stuff and

387
00:17:55,270 --> 00:17:58,450
there's this stuff which you really

388
00:17:56,710 --> 00:18:00,070
wanted to map in and then there's this

389
00:17:58,450 --> 00:18:02,110
over red stuff and you can see that the

390
00:18:00,070 --> 00:18:04,180
overhead stuff includes the ALF headers

391
00:18:02,110 --> 00:18:05,890
and this includes the out letters it's

392
00:18:04,180 --> 00:18:07,510
just this is the real stuff that gets

393
00:18:05,890 --> 00:18:09,790
used and this is the real stuff that

394
00:18:07,510 --> 00:18:13,330
gets used so people can play games with

395
00:18:09,790 --> 00:18:15,880
that all day long and the key thing is

396
00:18:13,330 --> 00:18:19,810
one segment is mapped at 400,000 the

397
00:18:15,880 --> 00:18:22,540
other segment is mapped at 600,000 or

398
00:18:19,810 --> 00:18:23,830
you know it's all just based on this PT

399
00:18:22,540 --> 00:18:25,780
load information right where's the

400
00:18:23,830 --> 00:18:28,300
virtual address 400,000 or is the

401
00:18:25,780 --> 00:18:29,980
virtual address 600 a one-eighth but

402
00:18:28,300 --> 00:18:34,900
there's still some actual vital data

403
00:18:29,980 --> 00:18:36,760
that started at 600,000 straight up to

404
00:18:34,900 --> 00:18:39,730
the thing and then just to be clear

405
00:18:36,760 --> 00:18:40,960
remember this thing had file size 208

406
00:18:39,730 --> 00:18:44,290
member size 2 honey

407
00:18:40,960 --> 00:18:46,450
so there's an extra ESS stuff that got

408
00:18:44,290 --> 00:18:48,910
allocated that's not from disk so it's

409
00:18:46,450 --> 00:18:52,360
all valid it's just this top stuff is

410
00:18:48,910 --> 00:18:55,480
from disk that's from disk this little

411
00:18:52,360 --> 00:18:57,790
sliver right there is just yes and

412
00:18:55,480 --> 00:19:01,570
that's how the overall different

413
00:18:57,790 --> 00:19:02,920
segments got put somewhere in here just

414
00:19:01,570 --> 00:19:06,100
by virtue of the fact that they're whole

415
00:19:02,920 --> 00:19:08,620
file chunk got mapped into memory so I

416
00:19:06,100 --> 00:19:11,800
need like an old-school Apple logo that

417
00:19:08,620 --> 00:19:15,060
I can like place on top but I need one

418
00:19:11,800 --> 00:19:15,060
with the transparent background


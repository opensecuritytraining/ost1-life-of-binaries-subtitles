1
00:00:10,290 --> 00:00:16,299
okay so we've got hello dot C go ahead

2
00:00:13,930 --> 00:00:18,760
and run this command in your directory

3
00:00:16,299 --> 00:00:24,040
that you were in hello - oh I'm sorry

4
00:00:18,760 --> 00:00:30,820
GCC Oh hello and hello dot C in your

5
00:00:24,040 --> 00:00:34,480
Linux VM and once you've done that then

6
00:00:30,820 --> 00:00:51,070
you're going to use read L - lowercase L

7
00:00:34,480 --> 00:00:56,350
and then hello GCC - oh hello hello dot

8
00:00:51,070 --> 00:00:57,700
C and then read L - L hello all right

9
00:00:56,350 --> 00:01:00,219
it's we're going to scroll up a little

10
00:00:57,700 --> 00:01:02,140
bit and we want to look at this array

11
00:01:00,219 --> 00:01:07,060
where it's talking about program headers

12
00:01:02,140 --> 00:01:10,330
right so unfortunately it's not all nice

13
00:01:07,060 --> 00:01:12,700
and horizontal but what we're going to

14
00:01:10,330 --> 00:01:15,100
see is you're going to have offset and

15
00:01:12,700 --> 00:01:17,619
then virtual address and then file size

16
00:01:15,100 --> 00:01:20,770
below offset and Memphis eyes below

17
00:01:17,619 --> 00:01:26,280
virtual address so offset is saying

18
00:01:20,770 --> 00:01:26,280
basically is that P underscore pH off

19
00:01:29,940 --> 00:01:34,240
that's the file offset is saying where

20
00:01:32,200 --> 00:01:36,720
does this segment start and so in this

21
00:01:34,240 --> 00:01:40,690
case it's saying for the pH header so

22
00:01:36,720 --> 00:01:42,940
the type was P T underscore PA chatter

23
00:01:40,690 --> 00:01:46,110
it's just read alphas leaving off the PT

24
00:01:42,940 --> 00:01:50,190
underscore and so the type is P header

25
00:01:46,110 --> 00:01:54,819
the start address in the file is hex 40

26
00:01:50,190 --> 00:01:57,849
the file size so go down from there the

27
00:01:54,819 --> 00:02:01,390
file size is 1 f8 so it's going from 40

28
00:01:57,849 --> 00:02:03,099
to 1 f8 and then because this is not a

29
00:02:01,390 --> 00:02:05,349
type load it's not going to be mapped

30
00:02:03,099 --> 00:02:07,330
into memory so it doesn't really matter

31
00:02:05,349 --> 00:02:09,340
what this virtual address and Memphis

32
00:02:07,330 --> 00:02:12,459
eyes are because it's not actually

33
00:02:09,340 --> 00:02:15,459
getting loaded up it still filled it in

34
00:02:12,459 --> 00:02:16,690
I don't know why but it's not going to

35
00:02:15,459 --> 00:02:20,230
actually be used for mapping in the

36
00:02:16,690 --> 00:02:21,909
memory then there's this physical

37
00:02:20,230 --> 00:02:23,710
address we said that we don't even care

38
00:02:21,909 --> 00:02:25,030
about this field I didn't even talk

39
00:02:23,710 --> 00:02:26,590
you'll most commonly see the physical

40
00:02:25,030 --> 00:02:27,970
address just set to the same thing as

41
00:02:26,590 --> 00:02:30,850
the virtual address but it's not

42
00:02:27,970 --> 00:02:33,250
actually used and then we've got the

43
00:02:30,850 --> 00:02:34,660
flags this is read and execute so in

44
00:02:33,250 --> 00:02:36,460
this sense the segment's are much

45
00:02:34,660 --> 00:02:37,960
simpler than the section header

46
00:02:36,460 --> 00:02:41,140
characteristics that we add in PE

47
00:02:37,960 --> 00:02:43,210
there's no table there's no not cache

48
00:02:41,140 --> 00:02:44,110
there's no discardable it's just

49
00:02:43,210 --> 00:02:45,790
read/write/execute

50
00:02:44,110 --> 00:02:47,170
so it's saying if you're gonna map it

51
00:02:45,790 --> 00:02:49,540
into memory then it would have

52
00:02:47,170 --> 00:02:53,440
particular characteristics again this is

53
00:02:49,540 --> 00:02:56,620
not going to be mapped in delivery it

54
00:02:53,440 --> 00:02:59,920
will be by virtue of this other load

55
00:02:56,620 --> 00:03:01,450
segment so the load segments are well

56
00:02:59,920 --> 00:03:05,320
okay before we get there interpreter

57
00:03:01,450 --> 00:03:07,210
section interpret segment by PT interp

58
00:03:05,320 --> 00:03:11,560
it starts at offset

59
00:03:07,210 --> 00:03:14,020
3 2 3 8 into the file and it's 1c big

60
00:03:11,560 --> 00:03:15,970
and then read elf has just nicely pulled

61
00:03:14,020 --> 00:03:19,060
that out for you and said if you were to

62
00:03:15,970 --> 00:03:22,960
go 1 3 8 into this file you would have

63
00:03:19,060 --> 00:03:26,710
this string slash Lib 64 / LD Linux x86

64
00:03:22,960 --> 00:03:33,040
64 this is the dynamic linker or the 64

65
00:03:26,710 --> 00:03:34,870
bit system the LD is linker dynamic so

66
00:03:33,040 --> 00:03:36,820
this is the string this is the actual

67
00:03:34,870 --> 00:03:39,280
binary which will be loaded into memory

68
00:03:36,820 --> 00:03:41,020
before I execute hello world so I can

69
00:03:39,280 --> 00:03:42,610
actually stop at the bug I can use it a

70
00:03:41,020 --> 00:03:44,800
bugger I can stop it before it actually

71
00:03:42,610 --> 00:03:46,209
loads hello world and I can see that

72
00:03:44,800 --> 00:03:50,170
that's going to be mapped into memory

73
00:03:46,209 --> 00:03:51,520
before the rest of my binary so these

74
00:03:50,170 --> 00:03:53,800
are the things that we really care about

75
00:03:51,520 --> 00:03:55,780
the load segments these are what's going

76
00:03:53,800 --> 00:03:58,840
to say what chunks of file are going to

77
00:03:55,780 --> 00:04:03,120
be loaded into memory so you've got one

78
00:03:58,840 --> 00:04:05,410
that starts at zero and goes to 6 DC and

79
00:04:03,120 --> 00:04:09,700
it's going to be loaded at virtual

80
00:04:05,410 --> 00:04:12,790
address X 400,000 and the total mm size

81
00:04:09,700 --> 00:04:14,590
is 6 DC so we've got no extra memory

82
00:04:12,790 --> 00:04:18,070
padding on this thing there's no BSS

83
00:04:14,590 --> 00:04:20,440
going on it's just going to take this

84
00:04:18,070 --> 00:04:23,440
memory and put it in this file data and

85
00:04:20,440 --> 00:04:26,440
put it into memory and then we've got a

86
00:04:23,440 --> 00:04:29,260
section second code segment that starts

87
00:04:26,440 --> 00:04:31,660
at e1 8 so here's the first thing we can

88
00:04:29,260 --> 00:04:34,390
infer from this we had one that goes

89
00:04:31,660 --> 00:04:35,930
from 0 to 60 C and then we've got

90
00:04:34,390 --> 00:04:38,660
another one that starts that you want

91
00:04:35,930 --> 00:04:41,270
there's like this gap between 60c in the

92
00:04:38,660 --> 00:04:43,789
file anyone a which is like a no-man's

93
00:04:41,270 --> 00:04:45,289
land we don't know like what's in there

94
00:04:43,789 --> 00:04:47,030
we just know that whatever is in there

95
00:04:45,289 --> 00:04:49,160
is not explicitly being mapped into

96
00:04:47,030 --> 00:04:53,690
memory because the next memory mapped

97
00:04:49,160 --> 00:04:58,669
portion starts right here at e1 8 so e1

98
00:04:53,690 --> 00:05:00,860
8 gets mapped into memory at 600,000 a 1

99
00:04:58,669 --> 00:05:03,110
8 so it's not like mapping it into

100
00:05:00,860 --> 00:05:06,080
memory at the 0 that you know just

101
00:05:03,110 --> 00:05:07,699
600,000 for instance and we'll see why

102
00:05:06,080 --> 00:05:10,250
this is later that they kind of like

103
00:05:07,699 --> 00:05:12,349
align the file offset with the virtual

104
00:05:10,250 --> 00:05:13,849
memory offset basically just make it

105
00:05:12,349 --> 00:05:16,849
simpler on themselves and they read from

106
00:05:13,849 --> 00:05:18,949
file into memory all right and the total

107
00:05:16,849 --> 00:05:22,130
size it's from e1 8 and its total size

108
00:05:18,949 --> 00:05:24,440
of 208 and now the virtual size is 2 1 8

109
00:05:22,130 --> 00:05:27,259
so here we've got a virtual sighs that's

110
00:05:24,440 --> 00:05:29,210
hex 10 bigger than the file size so

111
00:05:27,259 --> 00:05:31,220
we've got some sort of DSS stuff going

112
00:05:29,210 --> 00:05:33,349
on here we've got more memory than we've

113
00:05:31,220 --> 00:05:35,870
got data on disk and so there's gonna be

114
00:05:33,349 --> 00:05:38,960
hex 10 worth of data at the end of this

115
00:05:35,870 --> 00:05:41,000
that is going to be filled in at home

116
00:05:38,960 --> 00:05:44,300
time basically uninitialized a little

117
00:05:41,000 --> 00:05:45,620
very very kind of stuff all right so

118
00:05:44,300 --> 00:05:47,539
that's the main stuff we really care

119
00:05:45,620 --> 00:05:50,330
about and then we've got dynamic and

120
00:05:47,539 --> 00:05:52,729
we've got no two other canoe specific

121
00:05:50,330 --> 00:05:54,470
things but we really care about and

122
00:05:52,729 --> 00:05:56,539
especially matters for packing layers

123
00:05:54,470 --> 00:05:58,639
you'll see you know packed file house

124
00:05:56,539 --> 00:06:00,229
just dips and that's all you're going to

125
00:05:58,639 --> 00:06:01,880
see you're just going to know it's going

126
00:06:00,229 --> 00:06:03,260
to map stuff into memory of particular

127
00:06:01,880 --> 00:06:06,470
locations and it's going to allocate

128
00:06:03,260 --> 00:06:09,080
certain virtual memory sizes now the

129
00:06:06,470 --> 00:06:10,550
thing that was shown below this is it's

130
00:06:09,080 --> 00:06:13,669
actually trying to show you the mappings

131
00:06:10,550 --> 00:06:16,460
from segments to sections so this would

132
00:06:13,669 --> 00:06:19,130
be the segment index so starting up here

133
00:06:16,460 --> 00:06:21,650
at index 0 P header it's saying P header

134
00:06:19,130 --> 00:06:26,389
segment has no sections that actually

135
00:06:21,650 --> 00:06:29,090
map to it 1p there's a dot intersection

136
00:06:26,389 --> 00:06:31,009
that maps to the interpret segment and

137
00:06:29,090 --> 00:06:32,750
then you know once we get to the load

138
00:06:31,009 --> 00:06:35,810
sections which cover a big chunk of the

139
00:06:32,750 --> 00:06:37,430
file yeah most all of the sections after

140
00:06:35,810 --> 00:06:42,500
that so going back to this sort of

141
00:06:37,430 --> 00:06:45,680
picture that I showed before which shows

142
00:06:42,500 --> 00:06:47,150
the overlap between the two this is what

143
00:06:45,680 --> 00:06:48,700
read Alf is basically trying to show you

144
00:06:47,150 --> 00:06:50,710
it's saying

145
00:06:48,700 --> 00:06:52,660
there's some program headers and they're

146
00:06:50,710 --> 00:06:54,370
going to specify particular segments

147
00:06:52,660 --> 00:06:56,050
there's section headers which are going

148
00:06:54,370 --> 00:06:58,660
to specify particular sections and

149
00:06:56,050 --> 00:07:00,730
typically there's more sections than

150
00:06:58,660 --> 00:07:02,980
there are segments and so you're going

151
00:07:00,730 --> 00:07:04,810
to have many of these sections actually

152
00:07:02,980 --> 00:07:06,400
map within a particular segment and

153
00:07:04,810 --> 00:07:08,020
especially those to load segments

154
00:07:06,400 --> 00:07:10,510
there's going to be you know one big one

155
00:07:08,020 --> 00:07:13,240
that has multiple different sections

156
00:07:10,510 --> 00:07:15,520
within it so for now this is mostly just

157
00:07:13,240 --> 00:07:17,530
a curiosity just saying that there's

158
00:07:15,520 --> 00:07:21,640
many different little segment sections

159
00:07:17,530 --> 00:07:27,490
running around inside these segments all

160
00:07:21,640 --> 00:07:34,330
right so I think we're mostly done with

161
00:07:27,490 --> 00:07:35,440
program headers don't shake right so

162
00:07:34,330 --> 00:07:37,240
this is just the mapping between

163
00:07:35,440 --> 00:07:40,170
different segments and all of the

164
00:07:37,240 --> 00:07:40,170
sections contain


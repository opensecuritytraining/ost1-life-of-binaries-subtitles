1
00:00:03,560 --> 00:00:10,070
all right good unique on how did you

2
00:00:07,130 --> 00:00:12,889
round two all right cancel out here you

3
00:00:10,070 --> 00:00:14,150
know class competition why don't you got

4
00:00:12,889 --> 00:00:16,070
a running make sure you only have one

5
00:00:14,150 --> 00:00:19,039
running at the same time and this time

6
00:00:16,070 --> 00:00:21,289
for simplicity now you can skip two

7
00:00:19,039 --> 00:00:22,730
rounds by basically putting the ROM that

8
00:00:21,289 --> 00:00:25,730
you want to skip to at the end of your

9
00:00:22,730 --> 00:00:28,820
command line so you i pod in front I

10
00:00:25,730 --> 00:00:33,050
base 2 and then you'll skip right round

11
00:00:28,820 --> 00:00:35,540
two all right and spend again just do

12
00:00:33,050 --> 00:00:37,670
your you know on your own single player

13
00:00:35,540 --> 00:00:40,280
mode and start trying to answer the

14
00:00:37,670 --> 00:00:43,070
questions give you about five minutes

15
00:00:40,280 --> 00:00:45,350
for this if you ten minutes i will go

16
00:00:43,070 --> 00:00:48,490
five minutes five minutes and then we'll

17
00:00:45,350 --> 00:00:48,490
do the class competition

18
00:00:55,220 --> 00:00:59,150
yep so if you get stuck with a sixty

19
00:00:57,560 --> 00:01:02,990
four-bit binary you have no choice but

20
00:00:59,150 --> 00:01:04,580
to use though choice with you cff

21
00:01:02,990 --> 00:01:06,380
explorer so if you want to still figure

22
00:01:04,580 --> 00:01:08,540
out what the mapping is between the name

23
00:01:06,380 --> 00:01:12,140
that cff explorer uses and the names

24
00:01:08,540 --> 00:01:14,120
that p view uses you can just open up in

25
00:01:12,140 --> 00:01:16,940
your Python demo folder there's template

26
00:01:14,120 --> 00:01:20,030
32 and template 64 you can just open

27
00:01:16,940 --> 00:01:24,790
template 32 in p view template 32 in c

28
00:01:20,030 --> 00:01:29,600
FF explorer and look at you know ok c FF

29
00:01:24,790 --> 00:01:33,110
so often your Python demo folder you

30
00:01:29,600 --> 00:01:35,150
have a template 32 WX see if I go there

31
00:01:33,110 --> 00:01:38,330
and I go to the optional header and the

32
00:01:35,150 --> 00:01:41,240
characteristics my dll characteristics

33
00:01:38,330 --> 00:01:43,700
exit parrot says ll can move well that's

34
00:01:41,240 --> 00:01:45,950
the lsr one or the dynamic bass one

35
00:01:43,700 --> 00:01:49,150
right so I basically just need to do the

36
00:01:45,950 --> 00:01:57,770
32-bit one open it and then over Eve you

37
00:01:49,150 --> 00:02:00,680
open that as well wait 30 to WSC look at

38
00:01:57,770 --> 00:02:02,690
the characteristics so I can see over P

39
00:02:00,680 --> 00:02:05,840
dudes a dynamic bass the next compact

40
00:02:02,690 --> 00:02:08,570
terminal server over here it says dll to

41
00:02:05,840 --> 00:02:10,850
a genetics compatible in terminal server

42
00:02:08,570 --> 00:02:12,500
water so you can see the correspondence

43
00:02:10,850 --> 00:02:15,680
between things like that and you can not

44
00:02:12,500 --> 00:02:18,019
hold it in 64 as I said before this is

45
00:02:15,680 --> 00:02:25,519
where limitations of the tools start to

46
00:02:18,019 --> 00:02:28,310
get start to smell your badge it'd be so

47
00:02:25,519 --> 00:02:29,480
much more later alright well so if you

48
00:02:28,310 --> 00:02:31,519
get through around here when you get

49
00:02:29,480 --> 00:02:33,920
back to route what are the crashes yeah

50
00:02:31,519 --> 00:02:36,040
and I think that's the new cobra I'm

51
00:02:33,920 --> 00:02:39,200
trying to avoid p you kind of stuff

52
00:02:36,040 --> 00:02:41,540
looks like it's not working as i ended

53
00:02:39,200 --> 00:02:43,870
but i mean its way because if i skip to

54
00:02:41,540 --> 00:02:46,280
that then i go back seems to work so

55
00:02:43,870 --> 00:02:49,880
well we'll figure it out at some point

56
00:02:46,280 --> 00:02:54,860
today but for now let's compete so

57
00:02:49,880 --> 00:02:56,690
everybody close any p view windows that

58
00:02:54,860 --> 00:02:59,739
you have open just in case there you

59
00:02:56,690 --> 00:02:59,739
have open

60
00:03:00,550 --> 00:03:07,790
is the hardware support yes GDP data

61
00:03:05,810 --> 00:03:11,120
execution prevention which has to do

62
00:03:07,790 --> 00:03:16,460
with the x5 as the ice flag set its

63
00:03:11,120 --> 00:03:24,980
support step was from this right here

64
00:03:16,460 --> 00:03:29,450
this extra medical stuff cause that the

65
00:03:24,980 --> 00:03:31,690
eggs okay so everybody should cancel out

66
00:03:29,450 --> 00:03:35,390
of all your games close all your P views

67
00:03:31,690 --> 00:03:38,690
we're going to do a new Python bin hunch

68
00:03:35,390 --> 00:03:43,280
space to at this time you're going to

69
00:03:38,690 --> 00:03:49,390
say 24 class load and then you're going

70
00:03:43,280 --> 00:03:49,390
to wait for your seed your seat is 246

71
00:03:56,620 --> 00:04:01,030
alright we've got our gold silver and

72
00:03:58,659 --> 00:04:03,220
bronze medalist if no one else going

73
00:04:01,030 --> 00:04:10,540
once going twice anyone have less than

74
00:04:03,220 --> 00:04:12,940
160 all right good job everyone thank

75
00:04:10,540 --> 00:04:15,730
you what mrs. G going on there's

76
00:04:12,940 --> 00:04:17,739
cheating don't option really if anyone

77
00:04:15,730 --> 00:04:20,500
finds the cheat codes you can use the

78
00:04:17,739 --> 00:04:24,280
cheap it's go look at the source there's

79
00:04:20,500 --> 00:04:25,780
cheat codes a little loose actually you

80
00:04:24,280 --> 00:04:27,490
know eventually if you like really look

81
00:04:25,780 --> 00:04:29,139
at all the files the section editors

82
00:04:27,490 --> 00:04:30,850
will eventually tell you that she codes

83
00:04:29,139 --> 00:04:33,550
but you got to look at a lot of files

84
00:04:30,850 --> 00:04:35,770
before you figure it out section over

85
00:04:33,550 --> 00:04:37,330
eventually well no I mean don't actually

86
00:04:35,770 --> 00:04:39,669
look for things that are written

87
00:04:37,330 --> 00:04:41,620
backwards they got like an arrow playing

88
00:04:39,669 --> 00:04:43,389
that way that's working backwards if you

89
00:04:41,620 --> 00:04:45,460
find enough of those thing is there

90
00:04:43,389 --> 00:04:47,830
randomly choosing there but there's like

91
00:04:45,460 --> 00:04:50,110
a cheap this is the chicco the cheat

92
00:04:47,830 --> 00:04:51,880
code is food that's all written

93
00:04:50,110 --> 00:04:54,280
backwards one section of signing it

94
00:04:51,880 --> 00:04:55,479
through a lot of men and ones eventually

95
00:04:54,280 --> 00:04:56,800
when are we going to binary it'll be

96
00:04:55,479 --> 00:04:58,600
harder you'll have to find the chickens

97
00:04:56,800 --> 00:05:00,870
that way right now you just look at the

98
00:04:58,600 --> 00:05:00,870
source


1
00:00:03,600 --> 00:00:10,080
all right so backing your programs we

2
00:00:08,790 --> 00:00:11,639
don't want to look at it in this

3
00:00:10,080 --> 00:00:12,540
anything we'd like to look at it a

4
00:00:11,639 --> 00:00:14,999
decent size

5
00:00:12,540 --> 00:00:18,740
so back in your VM we're gonna use

6
00:00:14,999 --> 00:00:18,740
retail capital S this time

7
00:00:18,980 --> 00:00:28,080
awesome password for the win we're gonna

8
00:00:23,160 --> 00:00:30,089
do retail - capital S on your hello go

9
00:00:28,080 --> 00:00:31,529
ahead and hit enter and it's going to

10
00:00:30,089 --> 00:00:32,730
show you all of the section header

11
00:00:31,529 --> 00:00:33,870
information and you're going to see

12
00:00:32,730 --> 00:00:36,059
there's going to be lots of section

13
00:00:33,870 --> 00:00:45,300
editors and they've each got these lots

14
00:00:36,059 --> 00:00:47,309
of different fields starts out with one

15
00:00:45,300 --> 00:00:48,690
that has typed null so that's there's

16
00:00:47,309 --> 00:00:53,460
nothing there it's just a bunch of empty

17
00:00:48,690 --> 00:00:54,660
stuff then it has an interpreter section

18
00:00:53,460 --> 00:00:56,579
and it's just calling this generic

19
00:00:54,660 --> 00:01:00,480
program it's right so I said that's the

20
00:00:56,579 --> 00:01:03,179
catch-all type and it can be used for

21
00:01:00,480 --> 00:01:05,660
anything then the address where that's

22
00:01:03,179 --> 00:01:09,210
going to be mapped into memory is

23
00:01:05,660 --> 00:01:12,840
400,000 to 38 if we go back and we look

24
00:01:09,210 --> 00:01:14,540
at the segment information we see for

25
00:01:12,840 --> 00:01:17,100
the intersection

26
00:01:14,540 --> 00:01:19,189
yep that also said it was going it

27
00:01:17,100 --> 00:01:22,350
wanted to be you know it would be at

28
00:01:19,189 --> 00:01:24,000
400,000 to 38 in memory I mean it's not

29
00:01:22,350 --> 00:01:25,619
just by itself getting mapped into

30
00:01:24,000 --> 00:01:27,750
memory it's just a consequence of this

31
00:01:25,619 --> 00:01:30,090
first load segment getting mapped into

32
00:01:27,750 --> 00:01:31,500
memory this thing will just end up at

33
00:01:30,090 --> 00:01:36,360
this particular address because it has

34
00:01:31,500 --> 00:01:39,570
this particular file offset right so but

35
00:01:36,360 --> 00:01:40,860
down in the section information you know

36
00:01:39,570 --> 00:01:43,530
it's saying where it's going to end up

37
00:01:40,860 --> 00:01:45,899
in memory it's saying if it as well it's

38
00:01:43,530 --> 00:01:47,729
saying what the offset is on on disk to

39
00:01:45,899 --> 00:01:49,560
this particular thing and so since we

40
00:01:47,729 --> 00:01:51,840
have these many more section headers

41
00:01:49,560 --> 00:01:54,299
right we have more granular information

42
00:01:51,840 --> 00:01:56,430
about where particular information is

43
00:01:54,299 --> 00:01:59,610
mapped on this so yeah perfect this is

44
00:01:56,430 --> 00:02:01,020
our opportunity to see if there was any

45
00:01:59,610 --> 00:02:05,549
second you know I said before you know

46
00:02:01,020 --> 00:02:08,070
what's between 6d C and E 1/8 I said

47
00:02:05,549 --> 00:02:10,380
maybe it's section information I can see

48
00:02:08,070 --> 00:02:14,340
based on this that there's no you know

49
00:02:10,380 --> 00:02:16,660
section information b12 great how big is

50
00:02:14,340 --> 00:02:22,090
this one

51
00:02:16,660 --> 00:02:23,760
six six zero plus seven second I mean

52
00:02:22,090 --> 00:02:30,520
it's still not big enough overall but

53
00:02:23,760 --> 00:02:43,750
just curious though we got six zero plus

54
00:02:30,520 --> 00:02:49,030
seven speed right six DC so basically

55
00:02:43,750 --> 00:02:51,550
this last this exception frame one right

56
00:02:49,030 --> 00:02:54,850
there that section header starts at six

57
00:02:51,550 --> 00:02:57,070
six zero goes to six DC so that

58
00:02:54,850 --> 00:03:00,130
basically is the last data in that first

59
00:02:57,070 --> 00:03:02,620
load segment that's going to get mapped

60
00:03:00,130 --> 00:03:04,510
up into memory and so there's no section

61
00:03:02,620 --> 00:03:07,030
under information specifying that

62
00:03:04,510 --> 00:03:09,400
they've got anything in that 700 or you

63
00:03:07,030 --> 00:03:12,220
know anything beyond 60 C and less than

64
00:03:09,400 --> 00:03:14,380
equal eight so going back to your

65
00:03:12,220 --> 00:03:17,950
question about where does that data come

66
00:03:14,380 --> 00:03:19,570
from I don't know it's seems to be

67
00:03:17,950 --> 00:03:21,210
mostly zeros but I can't say it's all

68
00:03:19,570 --> 00:03:23,350
zeros I didn't check that it's all zeros

69
00:03:21,210 --> 00:03:25,000
so basically the point is we have this

70
00:03:23,350 --> 00:03:26,680
much more granular view of where the

71
00:03:25,000 --> 00:03:27,910
specific information that the linker was

72
00:03:26,680 --> 00:03:30,640
using what I was putting stuff together

73
00:03:27,910 --> 00:03:32,350
was because if we only have the segment

74
00:03:30,640 --> 00:03:34,240
information it's all just kind of

75
00:03:32,350 --> 00:03:35,770
jumbled together and mapped up into

76
00:03:34,240 --> 00:03:37,330
memory the program will still run the

77
00:03:35,770 --> 00:03:39,430
program internally has got all of its

78
00:03:37,330 --> 00:03:41,620
offsets at the right places and all that

79
00:03:39,430 --> 00:03:43,810
it's just if we're trying to analyze the

80
00:03:41,620 --> 00:03:45,970
program the section information is in

81
00:03:43,810 --> 00:03:48,190
from is important so that we know that a

82
00:03:45,970 --> 00:03:50,050
particular region is used for you know

83
00:03:48,190 --> 00:03:52,480
constructors or the particular region is

84
00:03:50,050 --> 00:03:54,070
used for destructors this is important

85
00:03:52,480 --> 00:03:57,430
if you take like Cory's exploits one

86
00:03:54,070 --> 00:03:59,590
class for instance you know he uses the

87
00:03:57,430 --> 00:04:01,710
Constructors he uses you know a buffer

88
00:03:59,590 --> 00:04:04,000
overflow that goes in it overwrites

89
00:04:01,710 --> 00:04:08,740
actually I don't think it's but I think

90
00:04:04,000 --> 00:04:10,540
it was heap overflow that overwrites

91
00:04:08,740 --> 00:04:13,060
some information with the constructors

92
00:04:10,540 --> 00:04:16,570
function pointers and then he he

93
00:04:13,060 --> 00:04:18,640
manipulates those two when a constructor

94
00:04:16,570 --> 00:04:21,100
actually I think it's destructors when

95
00:04:18,640 --> 00:04:23,290
the destructors are called then it

96
00:04:21,100 --> 00:04:25,840
causes the attackers code to run and he

97
00:04:23,290 --> 00:04:27,220
can invoke things so if you don't know

98
00:04:25,840 --> 00:04:27,990
where the destructors are if you don't

99
00:04:27,220 --> 00:04:30,660
have segmented

100
00:04:27,990 --> 00:04:32,340
raishin you would have to potentially do

101
00:04:30,660 --> 00:04:34,110
you know full static analysis of this

102
00:04:32,340 --> 00:04:37,220
thing to see when it went and where it's

103
00:04:34,110 --> 00:04:39,690
going to call destructor information so

104
00:04:37,220 --> 00:04:42,030
so basically this just gives us a much

105
00:04:39,690 --> 00:04:44,099
more granular view of what's going on in

106
00:04:42,030 --> 00:04:45,990
the program and so now I'm going to talk

107
00:04:44,099 --> 00:04:49,560
about a couple of these that we care

108
00:04:45,990 --> 00:04:53,910
about I think the main ones are going to

109
00:04:49,560 --> 00:04:57,930
be the got PLT and it got so got is

110
00:04:53,910 --> 00:05:00,090
global offset table and got PMT the PLT

111
00:04:57,930 --> 00:05:02,909
stands for precede your linkage table

112
00:05:00,090 --> 00:05:05,280
and so the global offsets able to

113
00:05:02,909 --> 00:05:08,159
proceed your linkage table is basically

114
00:05:05,280 --> 00:05:10,289
going to be the import address table as

115
00:05:08,159 --> 00:05:12,360
far as we're concerned so I'll show the

116
00:05:10,289 --> 00:05:13,590
dynamic linking that happens and it's

117
00:05:12,360 --> 00:05:17,639
going to have to do with this dot dot

118
00:05:13,590 --> 00:05:19,440
dot PLT and you can see we've got BSS

119
00:05:17,639 --> 00:05:24,659
sections and we've got data sections and

120
00:05:19,440 --> 00:05:27,000
all that VSS is set to no Vince right so

121
00:05:24,659 --> 00:05:29,190
although it has a a particular size of

122
00:05:27,000 --> 00:05:30,960
10 right you're not going to treat that

123
00:05:29,190 --> 00:05:38,699
as a file size because the type is set

124
00:05:30,960 --> 00:05:44,880
to no bits okay say good boy pops

125
00:05:38,699 --> 00:05:46,620
is it happening this year oh he has one

126
00:05:44,880 --> 00:05:49,020
class he is the first class is two days

127
00:05:46,620 --> 00:05:51,240
on Linux it covers the basic buffer over

128
00:05:49,020 --> 00:05:53,070
closing heap overflows Linux and then

129
00:05:51,240 --> 00:05:55,770
later on he has the second date he has

130
00:05:53,070 --> 00:05:57,780
the second class two days on Windows and

131
00:05:55,770 --> 00:05:59,340
its basic buffer overflows with then

132
00:05:57,780 --> 00:06:01,819
additional techniques to bypass that

133
00:05:59,340 --> 00:06:01,819
vanilla


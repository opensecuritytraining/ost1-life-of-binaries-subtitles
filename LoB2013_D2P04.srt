1
00:00:03,870 --> 00:00:06,990
all right there's one other thing that I

2
00:00:05,549 --> 00:00:09,529
missed yesterday that I wanted to go

3
00:00:06,990 --> 00:00:09,529
back and cover

4
00:00:18,300 --> 00:00:20,359
you

5
00:00:20,869 --> 00:00:27,089
we go all right so one thing that I

6
00:00:24,269 --> 00:00:28,470
wanted to talk about was import address

7
00:00:27,089 --> 00:00:31,610
table looking and I'll show you the

8
00:00:28,470 --> 00:00:33,420
magic acts before I go and explain a few

9
00:00:31,610 --> 00:00:36,659
but what I'm going to claim this

10
00:00:33,420 --> 00:00:38,250
basically if the attacker has done DLL

11
00:00:36,659 --> 00:00:40,290
injection and they're in the same memory

12
00:00:38,250 --> 00:00:42,269
space as your process we've got this

13
00:00:40,290 --> 00:00:43,680
import address table and we know that

14
00:00:42,269 --> 00:00:45,629
that holds pointers to all these

15
00:00:43,680 --> 00:00:50,640
functions on a given process attitude

16
00:00:45,629 --> 00:00:52,290
wants to call and so if the attacker to

17
00:00:50,640 --> 00:00:53,910
manipulate this import address table so

18
00:00:52,290 --> 00:00:55,890
that it calls the attacker instead of

19
00:00:53,910 --> 00:00:57,660
calling the original function and you

20
00:00:55,890 --> 00:00:59,789
can eat a monitor stuff like that API

21
00:00:57,660 --> 00:01:02,059
monitoring that we saw in the malware

22
00:00:59,789 --> 00:01:05,580
class or he can like I said yesterday

23
00:01:02,059 --> 00:01:07,470
slice basically filter results right so

24
00:01:05,580 --> 00:01:11,070
he gets the results on the list of

25
00:01:07,470 --> 00:01:18,300
processes and filter them out so context

26
00:01:11,070 --> 00:01:23,310
of task manager we have too much stuff

27
00:01:18,300 --> 00:01:26,040
that's incriminating in this DM all

28
00:01:23,310 --> 00:01:27,689
right so task manager right it gets a

29
00:01:26,040 --> 00:01:30,420
list of all the running processes right

30
00:01:27,689 --> 00:01:32,880
now we can see that kalkaji XE is

31
00:01:30,420 --> 00:01:34,619
showing up just fine Calchas here and so

32
00:01:32,880 --> 00:01:37,009
how task manager does this is it calls

33
00:01:34,619 --> 00:01:39,299
the function NT query system information

34
00:01:37,009 --> 00:01:41,369
when it calls that function it gets back

35
00:01:39,299 --> 00:01:42,600
just a list of here's all the processes

36
00:01:41,369 --> 00:01:46,290
that are running right now the colonel

37
00:01:42,600 --> 00:01:48,060
just fold it who's running ok so this is

38
00:01:46,290 --> 00:01:51,479
normal task manager called a normal

39
00:01:48,060 --> 00:01:53,610
aunty query system information and if I

40
00:01:51,479 --> 00:01:57,659
then install an import address table

41
00:01:53,610 --> 00:01:59,850
attack and I'm just going to say look

42
00:01:57,659 --> 00:02:03,119
magic trick you know installed in for

43
00:01:59,850 --> 00:02:05,460
address table attack one's task manager

44
00:02:03,119 --> 00:02:07,950
again now task manager doesn't have

45
00:02:05,460 --> 00:02:10,200
helped that exe anymore because that

46
00:02:07,950 --> 00:02:12,510
little dot reg file that i set it just

47
00:02:10,200 --> 00:02:14,549
basically set a registry entries so that

48
00:02:12,510 --> 00:02:16,290
with this registry entry set my dll will

49
00:02:14,549 --> 00:02:18,150
be loaded into all the new processes

50
00:02:16,290 --> 00:02:21,049
that start up so I just started task

51
00:02:18,150 --> 00:02:23,220
manager my dll my attacker do Elgin's

52
00:02:21,049 --> 00:02:25,049
inserted into the memory space of tax

53
00:02:23,220 --> 00:02:27,120
manager and the first thing that DLL

54
00:02:25,049 --> 00:02:29,579
does when it runs is goes and finds the

55
00:02:27,120 --> 00:02:30,860
process that it's inside of its memory

56
00:02:29,579 --> 00:02:32,720
space it finds its import

57
00:02:30,860 --> 00:02:34,040
table by parsing these data structures

58
00:02:32,720 --> 00:02:35,690
that we've been talking about throughout

59
00:02:34,040 --> 00:02:38,420
the class you know it goes to the

60
00:02:35,690 --> 00:02:40,520
decider goes to the NT header goes to

61
00:02:38,420 --> 00:02:43,280
the data directory goes to the import

62
00:02:40,520 --> 00:02:46,340
add rush table finds the thing for NPS

63
00:02:43,280 --> 00:02:48,020
rnt query system information and then it

64
00:02:46,340 --> 00:02:51,440
takes and it replaces that pointer with

65
00:02:48,020 --> 00:02:53,630
a pointer to itself and so because it

66
00:02:51,440 --> 00:02:56,450
can do that it then you know take some

67
00:02:53,630 --> 00:02:59,300
filters out the results of this this

68
00:02:56,450 --> 00:03:02,870
call to that function so that's that

69
00:02:59,300 --> 00:03:05,690
magic I'm going to show you a picture

70
00:03:02,870 --> 00:03:08,150
for that in a second and yes okay good

71
00:03:05,690 --> 00:03:11,840
we're not at the virus lab yet so we

72
00:03:08,150 --> 00:03:14,239
refer down so that only works you have

73
00:03:11,840 --> 00:03:16,430
to restart the task manager in order to

74
00:03:14,239 --> 00:03:19,459
know it can actually work on demand as

75
00:03:16,430 --> 00:03:21,230
well so I did this just using a way that

76
00:03:19,459 --> 00:03:24,230
doesn't do like on-demand DLL injection

77
00:03:21,230 --> 00:03:25,989
it just goes simple low Tom DLL

78
00:03:24,230 --> 00:03:28,250
injection but I could have used other

79
00:03:25,989 --> 00:03:30,620
programs that like we'll just force

80
00:03:28,250 --> 00:03:32,180
injected yellen's and things so I think

81
00:03:30,620 --> 00:03:36,620
in the malware class you showed how

82
00:03:32,180 --> 00:03:38,180
malware would like use you know we use

83
00:03:36,620 --> 00:03:40,220
create remote thread and stuff like that

84
00:03:38,180 --> 00:03:41,930
to force load library to be called in

85
00:03:40,220 --> 00:03:43,580
the process and then that load library

86
00:03:41,930 --> 00:03:47,959
would load the militia scale on which

87
00:03:43,580 --> 00:03:50,780
could then do this yellow this import

88
00:03:47,959 --> 00:03:52,930
address table hijacking so picture wise

89
00:03:50,780 --> 00:03:56,000
what I was just showing basically

90
00:03:52,930 --> 00:03:58,880
something like this you've got a normal

91
00:03:56,000 --> 00:04:00,410
program calling a normal dll and this

92
00:03:58,880 --> 00:04:03,650
would be task manager and this would be

93
00:04:00,410 --> 00:04:05,690
only in TV ll for instance he showed

94
00:04:03,650 --> 00:04:07,610
before you know how does the call

95
00:04:05,690 --> 00:04:09,440
actually look well it calls and it uses

96
00:04:07,610 --> 00:04:12,709
square brackets to look something up out

97
00:04:09,440 --> 00:04:14,720
of a table this table says you know 401

98
00:04:12,709 --> 00:04:16,340
once you see that's just some function

99
00:04:14,720 --> 00:04:19,790
here it's empty query system information

100
00:04:16,340 --> 00:04:22,100
and so when it does that this calls to

101
00:04:19,790 --> 00:04:24,140
some function goes over here runs

102
00:04:22,100 --> 00:04:26,660
whatever code and then when that codes

103
00:04:24,140 --> 00:04:28,820
down it comes back so when we've gotten

104
00:04:26,660 --> 00:04:32,060
attacker in here what he does is he

105
00:04:28,820 --> 00:04:34,250
takes any fills in the pointer that's

106
00:04:32,060 --> 00:04:37,160
inside the imported rest table with the

107
00:04:34,250 --> 00:04:39,740
pointer to his coat and so it calls his

108
00:04:37,160 --> 00:04:41,570
code instead so antique recent

109
00:04:39,740 --> 00:04:42,009
information called attackers and keep

110
00:04:41,570 --> 00:04:44,589
grace

111
00:04:42,009 --> 00:04:46,389
some information he calls the original

112
00:04:44,589 --> 00:04:48,400
antique worrisome system information

113
00:04:46,389 --> 00:04:50,050
because he doesn't know how to get the

114
00:04:48,400 --> 00:04:51,490
list of processes he just says I'm going

115
00:04:50,050 --> 00:04:53,619
to get that from the place she would

116
00:04:51,490 --> 00:04:56,680
have originally gotten it but once he

117
00:04:53,619 --> 00:04:58,990
has that list once he's passed the last

118
00:04:56,680 --> 00:05:00,849
up here's command that exe here's the

119
00:04:58,990 --> 00:05:02,830
explora dot exe here's count that exe

120
00:05:00,849 --> 00:05:05,020
said oh hell I don't want to show that

121
00:05:02,830 --> 00:05:06,550
I'll remove that from the list and then

122
00:05:05,020 --> 00:05:08,589
I'll pass it back to the guy who thought

123
00:05:06,550 --> 00:05:11,080
that he just called the original antique

124
00:05:08,589 --> 00:05:12,610
we're system information so so on the

125
00:05:11,080 --> 00:05:14,800
room kids class we call this sort of

126
00:05:12,610 --> 00:05:16,809
like post base man in the middle because

127
00:05:14,800 --> 00:05:18,129
you're basically doing this hooking and

128
00:05:16,809 --> 00:05:20,680
redirecting it so that it goes through

129
00:05:18,129 --> 00:05:23,319
you so that you can either monitor or

130
00:05:20,680 --> 00:05:24,490
manipulate the results coming back so

131
00:05:23,319 --> 00:05:26,199
that's what I didn't cover yesterday

132
00:05:24,490 --> 00:05:28,539
because I didn't have my vm set up and

133
00:05:26,199 --> 00:05:31,539
ready to go but all it is basically as

134
00:05:28,539 --> 00:05:33,789
the attacker comes in whines his way to

135
00:05:31,539 --> 00:05:35,080
the import dress table and changes out

136
00:05:33,789 --> 00:05:38,860
those function pointers with his

137
00:05:35,080 --> 00:05:41,520
function pointers alright any questions

138
00:05:38,860 --> 00:05:41,520
about that


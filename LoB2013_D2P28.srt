1
00:00:03,550 --> 00:00:09,700
all right so the notorious PLT proceed

2
00:00:07,390 --> 00:00:11,860
your linkage table and this is the

3
00:00:09,700 --> 00:00:15,910
component that I'm basically saying has

4
00:00:11,860 --> 00:00:18,039
to do with the funky-fresh right

5
00:00:15,910 --> 00:00:21,460
function the resolution I guess has to

6
00:00:18,039 --> 00:00:23,560
do with the equivalent of i-80 all right

7
00:00:21,460 --> 00:00:25,060
so now we're going to well you don't

8
00:00:23,560 --> 00:00:27,010
have to do this we're just going to call

9
00:00:25,060 --> 00:00:30,010
include to think back when I was talking

10
00:00:27,010 --> 00:00:32,020
about delay load right we said let's say

11
00:00:30,010 --> 00:00:33,820
we have some program that calls the

12
00:00:32,020 --> 00:00:35,469
function once it does a delay load

13
00:00:33,820 --> 00:00:37,329
import and then it calls it the second

14
00:00:35,469 --> 00:00:39,910
time and the second time it's filled in

15
00:00:37,329 --> 00:00:42,370
so notion so we in your directory there

16
00:00:39,910 --> 00:00:45,160
is a hello to dot C and you can compile

17
00:00:42,370 --> 00:00:46,810
it but but I'll just walk you through

18
00:00:45,160 --> 00:00:48,399
the picture this is going to be the

19
00:00:46,810 --> 00:00:51,850
exact same sort of picture as we saw

20
00:00:48,399 --> 00:00:53,290
with the lay load imports for Windows so

21
00:00:51,850 --> 00:00:56,260
first of all first thing we need to know

22
00:00:53,290 --> 00:00:59,050
is that the printf turns into a put s so

23
00:00:56,260 --> 00:01:00,610
that's a foot string it just is the

24
00:00:59,050 --> 00:01:04,500
behind-the-scenes thing that typically

25
00:01:00,610 --> 00:01:07,509
will implement brunet functionality so

26
00:01:04,500 --> 00:01:09,609
the first thing that we have here yep so

27
00:01:07,509 --> 00:01:11,770
the PLT I guess looks like the stub and

28
00:01:09,609 --> 00:01:15,509
the got PLT is going to be the actual

29
00:01:11,770 --> 00:01:17,829
delay mode import address table so

30
00:01:15,509 --> 00:01:20,880
you're running along happily in your

31
00:01:17,829 --> 00:01:24,969
hello to you hit the first printf call

32
00:01:20,880 --> 00:01:28,719
put s act PLT so the first time this

33
00:01:24,969 --> 00:01:31,630
call this is sorry this is to AT&T

34
00:01:28,719 --> 00:01:35,109
syntax assembly rather than Intel syntax

35
00:01:31,630 --> 00:01:37,779
Linux uses 18t syntax windows uses intel

36
00:01:35,109 --> 00:01:39,099
syntax so rather than the square

37
00:01:37,779 --> 00:01:43,899
brackets thing that i was showing you

38
00:01:39,099 --> 00:01:47,859
before at these angle brackets so call

39
00:01:43,899 --> 00:01:53,709
put us at PLT is going to be calling

40
00:01:47,859 --> 00:01:56,289
down to here this put us at PLT so that

41
00:01:53,709 --> 00:01:58,959
is going to jump and then this is the

42
00:01:56,289 --> 00:02:04,299
actual dereferencing it's saying jump to

43
00:01:58,959 --> 00:02:05,739
the value at dot dot dot PLT plus 24 so

44
00:02:04,299 --> 00:02:08,229
first you're actually calling to the

45
00:02:05,739 --> 00:02:10,810
stub here but then the stub is doing a

46
00:02:08,229 --> 00:02:16,510
jump and it's dereferencing the dot dot

47
00:02:10,810 --> 00:02:17,080
dot PLT plus 24 well the dot PLT plus 24

48
00:02:16,510 --> 00:02:18,910
down here

49
00:02:17,080 --> 00:02:21,040
putting absolute virtual addresses I'm

50
00:02:18,910 --> 00:02:23,170
just putting you know relative things

51
00:02:21,040 --> 00:02:25,260
here maybe that's part of the problem I

52
00:02:23,170 --> 00:02:27,850
tried to make this too interpreted

53
00:02:25,260 --> 00:02:31,660
anyways this jump is basically saying

54
00:02:27,850 --> 00:02:34,780
take whatever's at got klt plus 24 pull

55
00:02:31,660 --> 00:02:36,400
that address out and jump there and so

56
00:02:34,780 --> 00:02:40,840
I'm saying that filled in there was the

57
00:02:36,400 --> 00:02:42,100
address of what s at PLT plus 6 so it's

58
00:02:40,840 --> 00:02:46,080
actually the address of the next

59
00:02:42,100 --> 00:02:51,190
instruction is what it's jumping to and

60
00:02:46,080 --> 00:02:54,540
the next info let's keep walking so it's

61
00:02:51,190 --> 00:02:57,010
pulling a value out of the table and

62
00:02:54,540 --> 00:02:58,450
it's jumping there turns out at the

63
00:02:57,010 --> 00:03:00,190
beginning when we're still dealing with

64
00:02:58,450 --> 00:03:01,900
sub code before it's had a chance to

65
00:03:00,190 --> 00:03:03,580
fill in this thing right we're we're

66
00:03:01,900 --> 00:03:05,110
trying to work ourselves again towards

67
00:03:03,580 --> 00:03:07,510
the world where we fill that in with the

68
00:03:05,110 --> 00:03:09,550
real oh right we really want to go up

69
00:03:07,510 --> 00:03:11,980
here to put a so we first jump down here

70
00:03:09,550 --> 00:03:15,310
to the sub code stub code pulls from the

71
00:03:11,980 --> 00:03:17,230
entry and to start with it just goes to

72
00:03:15,310 --> 00:03:20,470
the next instruction so what is the next

73
00:03:17,230 --> 00:03:22,930
instruction do it pushes hex 10 and then

74
00:03:20,470 --> 00:03:25,810
it jumps to this other sub code up here

75
00:03:22,930 --> 00:03:28,660
so the push x10 is kind of equivalent to

76
00:03:25,810 --> 00:03:32,410
the to the pushing the moving stuff into

77
00:03:28,660 --> 00:03:34,930
EAX that we saw before so it pushes hex

78
00:03:32,410 --> 00:03:36,600
10 and then it jumps to DL stub which is

79
00:03:34,930 --> 00:03:40,440
the common code this is the hand-wavy

80
00:03:36,600 --> 00:03:43,570
code is going to you know somewhere do

81
00:03:40,440 --> 00:03:47,470
you know load libraries and things like

82
00:03:43,570 --> 00:03:48,640
that I know here that had wavy stuff is

83
00:03:47,470 --> 00:03:52,570
up in the dynamic linker

84
00:03:48,640 --> 00:03:55,060
so jump to DL stub and we jump back to

85
00:03:52,570 --> 00:03:58,390
here and so it's going to push a value

86
00:03:55,060 --> 00:04:01,930
of the PLT plus 8 and then it's going to

87
00:03:58,390 --> 00:04:05,800
jump to whatever value is inside of PLT

88
00:04:01,930 --> 00:04:07,900
got PLT plus 16 so it's pushing the

89
00:04:05,800 --> 00:04:11,890
value of this and it's jumping to

90
00:04:07,900 --> 00:04:13,870
whatever values here and so it pushes

91
00:04:11,890 --> 00:04:16,269
and then jumps and it goes to this

92
00:04:13,870 --> 00:04:18,519
dynamic linker address and that goes up

93
00:04:16,269 --> 00:04:21,070
here too this is more of the hand-wavy

94
00:04:18,519 --> 00:04:22,870
dynamic linker does some stuff and

95
00:04:21,070 --> 00:04:25,510
eventually is going to fill in the table

96
00:04:22,870 --> 00:04:27,850
that stuff is the equivalent of load

97
00:04:25,510 --> 00:04:30,160
library and get proc address in order to

98
00:04:27,850 --> 00:04:30,680
look up the real address for puts

99
00:04:30,160 --> 00:04:32,330
because

100
00:04:30,680 --> 00:04:34,460
want to put the real address for puts

101
00:04:32,330 --> 00:04:35,840
into this table so that the next time

102
00:04:34,460 --> 00:04:39,669
you call something that looks up from

103
00:04:35,840 --> 00:04:41,990
this table it goes to the real place so

104
00:04:39,669 --> 00:04:46,070
imma Klinker finds the real address

105
00:04:41,990 --> 00:04:49,160
inputs uses awesome animation to move it

106
00:04:46,070 --> 00:04:51,380
into the slot and then the next time

107
00:04:49,160 --> 00:04:53,870
okay and then calls just like before

108
00:04:51,380 --> 00:04:55,610
without ever thinking it does have wavy

109
00:04:53,870 --> 00:04:56,990
stuff fills in the table and then calls

110
00:04:55,610 --> 00:04:59,449
to the function you really want it

111
00:04:56,990 --> 00:05:02,630
because now it has its address then when

112
00:04:59,449 --> 00:05:04,039
that's done it returns back and you know

113
00:05:02,630 --> 00:05:05,870
it's gonna return back to wherever it

114
00:05:04,039 --> 00:05:07,490
got called from in the first place so

115
00:05:05,870 --> 00:05:09,440
you're running along again you hit the

116
00:05:07,490 --> 00:05:12,889
second time that you're calling print

117
00:05:09,440 --> 00:05:16,009
out and then this time you jump down to

118
00:05:12,889 --> 00:05:19,759
here the puts PLT but this time the jump

119
00:05:16,009 --> 00:05:21,590
pulls from this got PLT plus 24 and it

120
00:05:19,759 --> 00:05:23,120
this time goes directly alright so the

121
00:05:21,590 --> 00:05:24,680
first time around you run around through

122
00:05:23,120 --> 00:05:27,530
stub cone and dynamic linking code

123
00:05:24,680 --> 00:05:28,970
second time around you go directly to

124
00:05:27,530 --> 00:05:31,430
where you want to go but the thing is

125
00:05:28,970 --> 00:05:34,639
this is the default on linux

126
00:05:31,430 --> 00:05:36,530
whereas on windows this is much less

127
00:05:34,639 --> 00:05:39,889
common and you know you just see this

128
00:05:36,530 --> 00:05:43,840
occasionally so any questions on this

129
00:05:39,889 --> 00:05:43,840
it's really same thing as we saw before

130
00:05:51,430 --> 00:05:57,229
no I mean when I say it's more common on

131
00:05:54,979 --> 00:06:00,139
Linux what I mean is like this is the

132
00:05:57,229 --> 00:06:02,449
default right whereas on Windows it's an

133
00:06:00,139 --> 00:06:04,159
option but it's not the default so if

134
00:06:02,449 --> 00:06:05,479
you're just default compiling on Windows

135
00:06:04,159 --> 00:06:07,220
you've got this import address table

136
00:06:05,479 --> 00:06:09,320
that the OS loader feels in everything

137
00:06:07,220 --> 00:06:10,849
at the very beginning and then it lets

138
00:06:09,320 --> 00:06:13,580
you run so it takes longer to get

139
00:06:10,849 --> 00:06:14,000
started but then you run faster after

140
00:06:13,580 --> 00:06:16,849
the fact

141
00:06:14,000 --> 00:06:17,960
whereas this you start faster because

142
00:06:16,849 --> 00:06:20,659
you don't have to resolve all those

143
00:06:17,960 --> 00:06:22,970
imported rest table entries but then it

144
00:06:20,659 --> 00:06:24,889
takes more time because every time you

145
00:06:22,970 --> 00:06:26,389
want to import something you run a

146
00:06:24,889 --> 00:06:28,039
little bit of extra code and then you

147
00:06:26,389 --> 00:06:29,870
call that identical one a little extra

148
00:06:28,039 --> 00:06:31,430
code then call the dynamic linker after

149
00:06:29,870 --> 00:06:34,880
you resolve all of them again you just

150
00:06:31,430 --> 00:06:38,210
run just as fast because you're minus

151
00:06:34,880 --> 00:06:44,090
one instruction of indirection

152
00:06:38,210 --> 00:06:46,310
but yeah alright so we are not going to

153
00:06:44,090 --> 00:06:48,259
go into the stepping through in the

154
00:06:46,310 --> 00:06:49,610
debugger and I mean we could do this and

155
00:06:48,259 --> 00:06:53,660
I could show you it going into the

156
00:06:49,610 --> 00:06:56,240
dynamic linker and stuff like that I

157
00:06:53,660 --> 00:06:58,729
guess here's with some real addresses if

158
00:06:56,240 --> 00:07:28,240
I were actually stepping through just to

159
00:06:58,729 --> 00:07:33,530
be clear about all right all right

160
00:07:28,240 --> 00:07:37,460
so I can show you the do I have enough

161
00:07:33,530 --> 00:07:38,840
that I can do this yeah so I'm not gonna

162
00:07:37,460 --> 00:07:40,009
show this in a pretty much time for

163
00:07:38,840 --> 00:07:41,900
break anyways and then we're gonna go

164
00:07:40,009 --> 00:07:46,159
back and talk about the virus lab and be

165
00:07:41,900 --> 00:07:48,169
packing so but just as an FYI there's an

166
00:07:46,159 --> 00:07:50,780
environment variable called LD bind now

167
00:07:48,169 --> 00:07:52,310
if you set that equal to one it's

168
00:07:50,780 --> 00:07:54,560
basically going to tell the dynamic

169
00:07:52,310 --> 00:07:56,810
linker that you need to go ahead and do

170
00:07:54,560 --> 00:07:59,240
it more like the windows way and resolve

171
00:07:56,810 --> 00:08:02,210
everything all up at the front and then

172
00:07:59,240 --> 00:08:03,800
it's going to just fill in all the

173
00:08:02,210 --> 00:08:05,479
import address table entries right at

174
00:08:03,800 --> 00:08:07,310
the beginning and then let you go ahead

175
00:08:05,479 --> 00:08:10,969
and look and so but this is just an

176
00:08:07,310 --> 00:08:13,310
optional environment variable that you

177
00:08:10,969 --> 00:08:15,860
can send and so I can show you this but

178
00:08:13,310 --> 00:08:17,509
let's just believe me for now let's take

179
00:08:15,860 --> 00:08:20,060
our five minutes break and we'll come

180
00:08:17,509 --> 00:08:22,669
back I'll show you packing with upx on

181
00:08:20,060 --> 00:08:24,740
Linux and then I'll show you packing

182
00:08:22,669 --> 00:08:27,289
with upx on Windows and then I'll show

183
00:08:24,740 --> 00:08:28,550
you the virus lab and some of the two

184
00:08:27,289 --> 00:08:30,050
packing things they're going to show how

185
00:08:28,550 --> 00:08:33,140
you manipulate headers and how the

186
00:08:30,050 --> 00:08:35,919
things can get benefit from that and

187
00:08:33,140 --> 00:08:38,300
then the virus lab will show you how

188
00:08:35,919 --> 00:08:40,490
particular type of code can like walk

189
00:08:38,300 --> 00:08:42,229
through metadata in order to go find the

190
00:08:40,490 --> 00:08:44,149
library and get proc address and stuff

191
00:08:42,229 --> 00:08:45,620
like that without ever having to use

192
00:08:44,149 --> 00:08:47,449
imports and stuff so it's appropriate

193
00:08:45,620 --> 00:08:50,240
for you some shell Co appropriate for

194
00:08:47,449 --> 00:08:51,860
use in viruses and so forth alright

195
00:08:50,240 --> 00:08:54,829
and same thing there can be pee

196
00:08:51,860 --> 00:08:56,180
evoking say where there's IAT looking do

197
00:08:54,829 --> 00:08:58,850
the exact same thing attack her in

198
00:08:56,180 --> 00:09:02,200
Jackson the memory overrides PLT and

199
00:08:58,850 --> 00:09:02,200
trees rather than ayat


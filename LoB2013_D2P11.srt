1
00:00:03,580 --> 00:00:13,000
so back to the data directory or into

2
00:00:07,750 --> 00:00:15,910
index 0 1 2 3 4 5 6 index 6 in the data

3
00:00:13,000 --> 00:00:19,029
directory will be what we call image

4
00:00:15,910 --> 00:00:21,670
directory entry debug so this is going

5
00:00:19,029 --> 00:00:24,880
to be as usual an RV a that points at a

6
00:00:21,670 --> 00:00:26,289
different data structure this data

7
00:00:24,880 --> 00:00:30,699
structure is called the image debug

8
00:00:26,289 --> 00:00:33,550
directory and so we've got five things

9
00:00:30,699 --> 00:00:34,960
we care about in this header there's a

10
00:00:33,550 --> 00:00:36,190
time date stamp and this is what I'm

11
00:00:34,960 --> 00:00:38,409
going to say is the third time date

12
00:00:36,190 --> 00:00:40,420
stamp that we care about for you know

13
00:00:38,409 --> 00:00:43,330
malware forensics purposes and things

14
00:00:40,420 --> 00:00:45,280
like that so the time/date stamp here I

15
00:00:43,330 --> 00:00:46,600
said before you've got the 500 time date

16
00:00:45,280 --> 00:00:48,100
stamp that changes whenever you

17
00:00:46,600 --> 00:00:49,720
recompile you've got the export

18
00:00:48,100 --> 00:00:51,580
time/date stamp that changes whenever

19
00:00:49,720 --> 00:00:54,339
any of the export information changes

20
00:00:51,580 --> 00:00:56,110
and the debug time date-stamped should

21
00:00:54,339 --> 00:00:58,210
change whenever the debug information is

22
00:00:56,110 --> 00:00:59,920
changing so sort of similar you know you

23
00:00:58,210 --> 00:01:01,449
may be just playing around with strings

24
00:00:59,920 --> 00:01:03,610
in your thing and recompiling it and

25
00:01:01,449 --> 00:01:05,470
then you know the export information may

26
00:01:03,610 --> 00:01:08,710
not change and if the export information

27
00:01:05,470 --> 00:01:10,690
doesn't change the debug information may

28
00:01:08,710 --> 00:01:12,550
or may not change it kind of depends so

29
00:01:10,690 --> 00:01:14,710
the strings may be exported as debug

30
00:01:12,550 --> 00:01:19,210
information in which case the time/date

31
00:01:14,710 --> 00:01:21,010
stamp will change but again this is this

32
00:01:19,210 --> 00:01:22,330
is yet another time date stamp that can

33
00:01:21,010 --> 00:01:24,010
kind of indicate when this thing was

34
00:01:22,330 --> 00:01:25,630
potentially compiled or when the last

35
00:01:24,010 --> 00:01:28,540
time the debug information changed for

36
00:01:25,630 --> 00:01:30,070
this particular file alright the next

37
00:01:28,540 --> 00:01:33,070
field we care about beyond time/date

38
00:01:30,070 --> 00:01:35,560
stamp is type and for our purposes type

39
00:01:33,070 --> 00:01:37,690
will basically always be too so it can

40
00:01:35,560 --> 00:01:39,640
have many different values but we're

41
00:01:37,690 --> 00:01:42,370
always going to see too because this

42
00:01:39,640 --> 00:01:45,990
code view type is what Microsoft uses in

43
00:01:42,370 --> 00:01:53,080
order to structure its debug information

44
00:01:45,990 --> 00:01:55,750
for for newer systems all right so type

45
00:01:53,080 --> 00:01:57,580
is that specific type code view and size

46
00:01:55,750 --> 00:01:59,860
of data is basically just going to say

47
00:01:57,580 --> 00:02:01,600
whatever this type is specifying that's

48
00:01:59,860 --> 00:02:04,360
going to be a particular struct that's

49
00:02:01,600 --> 00:02:06,640
coming up next and the size of data is

50
00:02:04,360 --> 00:02:08,289
going to say the size of that struct now

51
00:02:06,640 --> 00:02:11,230
we kind of have something that's looking

52
00:02:08,289 --> 00:02:13,360
a little bit like back in the section

53
00:02:11,230 --> 00:02:15,910
headers we've got address of raw data

54
00:02:13,360 --> 00:02:17,140
and pointer to raw data so this

55
00:02:15,910 --> 00:02:18,310
structure is basically

56
00:02:17,140 --> 00:02:20,530
going to tell us there's some other

57
00:02:18,310 --> 00:02:22,030
debugging structure available but it's

58
00:02:20,530 --> 00:02:23,980
only actually telling us the file

59
00:02:22,030 --> 00:02:25,959
offsets to get that debugging structure

60
00:02:23,980 --> 00:02:28,209
and that kind of makes sense because

61
00:02:25,959 --> 00:02:29,500
who's going to be dealing with this most

62
00:02:28,209 --> 00:02:32,110
of the time is the debugger right a

63
00:02:29,500 --> 00:02:34,390
debugger is going to say okay well

64
00:02:32,110 --> 00:02:35,950
here's the file and I'm going to go back

65
00:02:34,390 --> 00:02:37,690
to disk and I'm going to pull out the

66
00:02:35,950 --> 00:02:39,700
first structure for its debugging

67
00:02:37,690 --> 00:02:41,530
information but that doesn't need to

68
00:02:39,700 --> 00:02:44,440
necessarily be mapped into memory and

69
00:02:41,530 --> 00:02:47,050
sitting around in memory right so

70
00:02:44,440 --> 00:02:48,370
address of raw data is like the other

71
00:02:47,050 --> 00:02:50,830
thing that was called raw data it's a

72
00:02:48,370 --> 00:02:52,630
file off set off set into the file where

73
00:02:50,830 --> 00:02:55,090
you got this debug structure and then

74
00:02:52,630 --> 00:02:59,650
pointer to run wait that's what I'll do

75
00:02:55,090 --> 00:03:02,890
then sorry address of raw data is the

76
00:02:59,650 --> 00:03:05,620
RVA and pointer to raw data is the file

77
00:03:02,890 --> 00:03:08,920
offset so I just miss described that

78
00:03:05,620 --> 00:03:12,670
terribly but they're both the same size

79
00:03:08,920 --> 00:03:14,019
because we don't have any of that BSS

80
00:03:12,670 --> 00:03:16,150
type thing going on we don't have any

81
00:03:14,019 --> 00:03:19,420
file padding going on so to use a single

82
00:03:16,150 --> 00:03:21,790
size but there is the virtual address so

83
00:03:19,420 --> 00:03:23,709
i guess it is mapped into memory I don't

84
00:03:21,790 --> 00:03:26,500
think it necessarily needs to be but it

85
00:03:23,709 --> 00:03:27,970
is it is mapped into memory and so the

86
00:03:26,500 --> 00:03:30,190
address of raw data will tell you where

87
00:03:27,970 --> 00:03:31,989
it is in memory and the pointer to raw

88
00:03:30,190 --> 00:03:36,940
data which I'll tell you where it is on

89
00:03:31,989 --> 00:03:39,730
file all right so now based on the fact

90
00:03:36,940 --> 00:03:41,860
that it's a so you know I don't expect

91
00:03:39,730 --> 00:03:43,810
you to memorize any of this this is not

92
00:03:41,860 --> 00:03:46,000
I don't believe going to be quizzed on

93
00:03:43,810 --> 00:03:50,980
but this is just giving you a notion

94
00:03:46,000 --> 00:03:53,650
that when it says the type is when it

95
00:03:50,980 --> 00:03:55,269
says the type is code view there's two

96
00:03:53,650 --> 00:03:57,430
different types of structures that it

97
00:03:55,269 --> 00:04:00,850
can be pointing at it can be doing this

98
00:03:57,430 --> 00:04:03,459
code view info PDB 20 and it can be this

99
00:04:00,850 --> 00:04:05,709
code view info PDB 70 and these are

100
00:04:03,459 --> 00:04:07,840
basically just versions for like 2 point

101
00:04:05,709 --> 00:04:09,790
0 and seven point oh oh yeah it says

102
00:04:07,840 --> 00:04:15,459
that right there 2 point 0 and seven

103
00:04:09,790 --> 00:04:18,039
point 0 so which of these it actually is

104
00:04:15,459 --> 00:04:19,570
how you interpret this particular data

105
00:04:18,039 --> 00:04:21,519
structure that's pointed to you by these

106
00:04:19,570 --> 00:04:23,020
fields it's just saying like look here

107
00:04:21,519 --> 00:04:25,330
in memory is going to be the next debug

108
00:04:23,020 --> 00:04:27,460
information it's going to be a type code

109
00:04:25,330 --> 00:04:29,169
view and then once you get there you

110
00:04:27,460 --> 00:04:31,030
have to look at the first D word and

111
00:04:29,169 --> 00:04:33,820
there's this header field to see if

112
00:04:31,030 --> 00:04:35,560
cv header or in this case it's just cv

113
00:04:33,820 --> 00:04:37,870
signature the cv header the first thing

114
00:04:35,560 --> 00:04:39,970
is a cv signature so really it's just at

115
00:04:37,870 --> 00:04:42,070
the very beginning first D word there's

116
00:04:39,970 --> 00:04:43,480
Seavey signature and that's these two

117
00:04:42,070 --> 00:04:47,050
things up here these are the possible

118
00:04:43,480 --> 00:04:48,850
signatures if the the value is 0 1 BN

119
00:04:47,050 --> 00:04:52,360
you treat it like this thing down here

120
00:04:48,850 --> 00:04:53,740
and if the value is SD s are you treat

121
00:04:52,360 --> 00:04:54,790
it like this thing down here so you just

122
00:04:53,740 --> 00:04:57,430
look at the first thing it's like a

123
00:04:54,790 --> 00:05:01,000
magic value it's just saying interpret

124
00:04:57,430 --> 00:05:03,640
the rest of the structure as this or as

125
00:05:01,000 --> 00:05:07,420
this and so what I'm going to show you

126
00:05:03,640 --> 00:05:09,040
is a breakdown of something we're like

127
00:05:07,420 --> 00:05:11,980
looking at the first D word of

128
00:05:09,040 --> 00:05:13,750
information right here we see NB 10 this

129
00:05:11,980 --> 00:05:17,020
is one of those little endian big endian

130
00:05:13,750 --> 00:05:21,430
kind of things so back here there was 0

131
00:05:17,020 --> 00:05:25,030
1 BN and put the other way it's NBD 10

132
00:05:21,430 --> 00:05:26,680
so little endian vs big endian so when

133
00:05:25,030 --> 00:05:28,000
we dig into this structure it's saying

134
00:05:26,680 --> 00:05:29,890
you know here's the first order thing

135
00:05:28,000 --> 00:05:31,360
and it's telling us the address of the

136
00:05:29,890 --> 00:05:33,610
raw data so that's in memory and the

137
00:05:31,360 --> 00:05:39,010
pointer to raw data that's on disk and

138
00:05:33,610 --> 00:05:42,520
so see this RV a 23 to 25 24 using peeve

139
00:05:39,010 --> 00:05:44,770
you using the RV a mode it says 25 24

140
00:05:42,520 --> 00:05:46,600
and so I look at the first D word of

141
00:05:44,770 --> 00:05:49,330
memory and I check that signature it's

142
00:05:46,600 --> 00:05:51,070
NBD 10 so I'm going to be interpreting

143
00:05:49,330 --> 00:05:54,820
it according to this struct right here

144
00:05:51,070 --> 00:05:57,250
and then from there you know I don't

145
00:05:54,820 --> 00:05:58,570
expect you to know the rest of this it's

146
00:05:57,250 --> 00:06:00,430
just this is how the structure would

147
00:05:58,570 --> 00:06:02,440
actually break down there's a signature

148
00:06:00,430 --> 00:06:04,390
and then there's an age and then at the

149
00:06:02,440 --> 00:06:06,400
very end there's a PDB file name this is

150
00:06:04,390 --> 00:06:08,260
what we actually care about out of all

151
00:06:06,400 --> 00:06:09,880
this all we really care about is this PV

152
00:06:08,260 --> 00:06:14,140
file name and you'll see that right

153
00:06:09,880 --> 00:06:18,250
there ACL edit peb so pdb file is a

154
00:06:14,140 --> 00:06:21,340
portable the bugger debug file and so

155
00:06:18,250 --> 00:06:23,140
this is on unlike Linux so maybe on

156
00:06:21,340 --> 00:06:26,230
Linux you're used to your debug symbols

157
00:06:23,140 --> 00:06:28,510
if you use the dash GDB GDB your de bug

158
00:06:26,230 --> 00:06:29,680
symbols get bundled into your executable

159
00:06:28,510 --> 00:06:33,130
and you're carrying them around with you

160
00:06:29,680 --> 00:06:35,530
on windows for all modern windows

161
00:06:33,130 --> 00:06:38,260
systems they compile their debug symbols

162
00:06:35,530 --> 00:06:40,090
off to a PDB file that's stored off to

163
00:06:38,260 --> 00:06:41,710
the side and debuggers know how to read

164
00:06:40,090 --> 00:06:44,810
these pdb files and things like that and

165
00:06:41,710 --> 00:06:46,639
they're they're mostly undocumented even

166
00:06:44,810 --> 00:06:48,169
and this kind of makes sense with you

167
00:06:46,639 --> 00:06:50,480
know a proprietary operating system

168
00:06:48,169 --> 00:06:51,800
these PDB information is you know

169
00:06:50,480 --> 00:06:53,330
extremely helpful for reverse

170
00:06:51,800 --> 00:06:55,160
engineering the files and things like

171
00:06:53,330 --> 00:06:56,630
that so they're pulling that off to a

172
00:06:55,160 --> 00:06:57,830
separate file they'll give you the

173
00:06:56,630 --> 00:07:00,440
binary but they won't give you the

174
00:06:57,830 --> 00:07:02,120
symbols so the debugger is like window

175
00:07:00,440 --> 00:07:03,590
bug behind the scenes there are some

176
00:07:02,120 --> 00:07:04,850
symbols that Microsoft will give you

177
00:07:03,590 --> 00:07:06,590
they'll give you some symbols but they

178
00:07:04,850 --> 00:07:09,320
won't give you other and so behind the

179
00:07:06,590 --> 00:07:11,240
scenes window bug is parsing this data

180
00:07:09,320 --> 00:07:13,430
structure when it's debugging a file

181
00:07:11,240 --> 00:07:15,590
it's looking up the name for a

182
00:07:13,430 --> 00:07:17,389
particular PDB file and it'll go request

183
00:07:15,590 --> 00:07:21,320
from the Microsoft symbol server to say

184
00:07:17,389 --> 00:07:23,450
i need the PDB s4 ACL edit and it'll

185
00:07:21,320 --> 00:07:25,340
pull down the things that's not quite

186
00:07:23,450 --> 00:07:26,720
accurate for the Internet audience don't

187
00:07:25,340 --> 00:07:31,460
you let me know that's not how it works

188
00:07:26,720 --> 00:07:33,229
but anyways notionally the point is this

189
00:07:31,460 --> 00:07:35,180
can either be a relative path like it is

190
00:07:33,229 --> 00:07:37,070
right here so it'll just say ACL edit

191
00:07:35,180 --> 00:07:38,330
PDB and then it can be assumed to be in

192
00:07:37,070 --> 00:07:40,520
the same directory that'll be the first

193
00:07:38,330 --> 00:07:42,590
place you look or it can actually be an

194
00:07:40,520 --> 00:07:43,910
absolute path so it'll give you the full

195
00:07:42,590 --> 00:07:46,310
path to like this is where you should

196
00:07:43,910 --> 00:07:48,260
try the PDB and it's that absolute path

197
00:07:46,310 --> 00:07:49,810
case that we care about for leaking

198
00:07:48,260 --> 00:07:53,360
information about malware authors

199
00:07:49,810 --> 00:07:56,479
development environment so back to that

200
00:07:53,360 --> 00:08:00,440
haugland presentation in 2010 here's

201
00:07:56,479 --> 00:08:03,500
some examples of PDB paths that were

202
00:08:00,440 --> 00:08:06,350
embedded into binaries for some well

203
00:08:03,500 --> 00:08:09,140
known malware so ghost is a rat that's

204
00:08:06,350 --> 00:08:11,180
most often associated with the Chinese

205
00:08:09,140 --> 00:08:14,930
attacks on Tibetan activists and things

206
00:08:11,180 --> 00:08:17,419
like that so ghost rat has a kernel

207
00:08:14,930 --> 00:08:23,060
driver that gets included along with it

208
00:08:17,419 --> 00:08:24,950
and it's called ress DP don't won't tell

209
00:08:23,060 --> 00:08:28,010
you what it does because that rootkit

210
00:08:24,950 --> 00:08:30,919
class material but you can see that when

211
00:08:28,010 --> 00:08:32,900
this file was actually compiled this is

212
00:08:30,919 --> 00:08:34,760
the exact path where they were had their

213
00:08:32,900 --> 00:08:36,650
build environment they had a program

214
00:08:34,760 --> 00:08:38,599
called ghost our directory called ghost

215
00:08:36,650 --> 00:08:40,250
and then server server is the rat

216
00:08:38,599 --> 00:08:44,150
component that gets installed and it

217
00:08:40,250 --> 00:08:47,060
talks back to the point sis I 386 and so

218
00:08:44,150 --> 00:08:49,040
forth and so ghost was probably named

219
00:08:47,060 --> 00:08:50,779
because a malware analyst came along and

220
00:08:49,040 --> 00:08:51,980
said oh I see that they named it ghost

221
00:08:50,779 --> 00:08:54,440
we're going to call a ghost as well

222
00:08:51,980 --> 00:08:57,950
Aurora this was the attack on Google

223
00:08:54,440 --> 00:09:00,500
sort of the apt attack on Google

224
00:08:57,950 --> 00:09:02,270
and the reason the Google attack was

225
00:09:00,500 --> 00:09:04,190
called the Aurora attack is because this

226
00:09:02,270 --> 00:09:07,100
string was found in it and this is from

227
00:09:04,190 --> 00:09:08,930
Stuxnet and this was the murtis string

228
00:09:07,100 --> 00:09:11,240
and the guava where I was like oh is it

229
00:09:08,930 --> 00:09:14,420
murderous is like Myrtle is like you

230
00:09:11,240 --> 00:09:16,190
know Esther from queasy ass tease or

231
00:09:14,420 --> 00:09:18,320
whatever some Old Testament stuff and

232
00:09:16,190 --> 00:09:20,660
their force the Israelis or is this you

233
00:09:18,320 --> 00:09:23,150
know my RT use because it's remote

234
00:09:20,660 --> 00:09:25,460
terminal units and so you can read in

235
00:09:23,150 --> 00:09:28,450
whatever you want to that or murtis is a

236
00:09:25,460 --> 00:09:30,950
you know genus and guava is the

237
00:09:28,450 --> 00:09:32,450
subfamily or whatever it is so you can

238
00:09:30,950 --> 00:09:34,640
read in whatever you want to that but

239
00:09:32,450 --> 00:09:37,340
the people who are making the Stuxnet

240
00:09:34,640 --> 00:09:39,890
thing they had this embedded in one of

241
00:09:37,340 --> 00:09:41,030
their files so they can leak and stuff

242
00:09:39,890 --> 00:09:42,770
about their environment so if you see

243
00:09:41,030 --> 00:09:44,240
this file it's got be equal and

244
00:09:42,770 --> 00:09:46,190
murderous blah blah blah and you've got

245
00:09:44,240 --> 00:09:47,540
this file and you've got be cool and

246
00:09:46,190 --> 00:09:49,280
murderous something else you probably

247
00:09:47,540 --> 00:09:51,440
think it's the same developers working

248
00:09:49,280 --> 00:09:54,080
on the same tools that's the level of

249
00:09:51,440 --> 00:09:55,880
you know attribution that this sort of

250
00:09:54,080 --> 00:09:58,190
malware attribution gets into oh do we

251
00:09:55,880 --> 00:10:00,650
have same groups using same tools using

252
00:09:58,190 --> 00:10:02,270
the same you know chunks of code over

253
00:10:00,650 --> 00:10:03,620
and over as has been found with the

254
00:10:02,270 --> 00:10:05,450
Stuxnet stuff they've said you know

255
00:10:03,620 --> 00:10:07,160
between multiple different tools there's

256
00:10:05,450 --> 00:10:09,280
like this one resource that's embedded

257
00:10:07,160 --> 00:10:11,990
that's reused amongst them and so forth

258
00:10:09,280 --> 00:10:13,760
anyways as I said we really only care

259
00:10:11,990 --> 00:10:15,560
about the debug information to the

260
00:10:13,760 --> 00:10:17,690
degree that we can pull out nice strings

261
00:10:15,560 --> 00:10:20,060
like this in order to to get some leaked

262
00:10:17,690 --> 00:10:22,100
information one of the quiz questions

263
00:10:20,060 --> 00:10:25,070
will be you know this thing is called

264
00:10:22,100 --> 00:10:27,590
round 8 q 0 EXE what was it originally

265
00:10:25,070 --> 00:10:29,720
called when I compiled it and you can

266
00:10:27,590 --> 00:10:32,480
kind of find that by based on looking at

267
00:10:29,720 --> 00:10:33,740
that PDB path that's in there right so

268
00:10:32,480 --> 00:10:35,840
you got to look at these address to

269
00:10:33,740 --> 00:10:37,340
pointer or the point of raw data you got

270
00:10:35,840 --> 00:10:39,800
to go find that data in here it's

271
00:10:37,340 --> 00:10:42,560
probably going to be about there nope

272
00:10:39,800 --> 00:10:45,500
all right well let me do it once pointer

273
00:10:42,560 --> 00:10:49,940
to raw data that's the file offset 1088

274
00:10:45,500 --> 00:10:57,380
I change my thing 1088 so it's above

275
00:10:49,940 --> 00:11:01,300
this 1088 is going to bite it's not

276
00:10:57,380 --> 00:11:01,300
useful oh wait no that's not the strings

277
00:11:02,410 --> 00:11:07,030
there's my string it's not in there

278
00:11:08,460 --> 00:11:16,530
remember kids pump your skill points

279
00:11:10,590 --> 00:11:18,120
into science and heavy weapons okay okay

280
00:11:16,530 --> 00:11:19,680
so it's in there I'm just messing up

281
00:11:18,120 --> 00:11:23,190
something about the offsetting but

282
00:11:19,680 --> 00:11:24,600
here's the actual path where the where

283
00:11:23,190 --> 00:11:27,720
the thing was originally compiled all

284
00:11:24,600 --> 00:11:30,570
right this is the TLS one that's why so

285
00:11:27,720 --> 00:11:32,430
this my this TLS example was originally

286
00:11:30,570 --> 00:11:36,540
called Z colon AV exempt like the

287
00:11:32,430 --> 00:11:40,650
binaries TLS baseline dot exe presumably

288
00:11:36,540 --> 00:11:42,150
rather than PDB right so if you ever see

289
00:11:40,650 --> 00:11:44,160
some malware in the wild that's in Z

290
00:11:42,150 --> 00:11:45,690
colon AV exempt and you know that you

291
00:11:44,160 --> 00:11:47,960
know zeno was the one who put it out

292
00:11:45,690 --> 00:11:47,960
there


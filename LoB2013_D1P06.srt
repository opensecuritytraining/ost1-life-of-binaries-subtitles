1
00:00:03,570 --> 00:00:09,240
all right so welcome to the jungle we

2
00:00:06,270 --> 00:00:11,280
got fun of games time to start playing

3
00:00:09,240 --> 00:00:21,419
and how many predators do you see in

4
00:00:11,280 --> 00:00:28,020
that anyways alright time to get the

5
00:00:21,419 --> 00:00:29,730
game your game on alright so the path is

6
00:00:28,020 --> 00:00:33,870
not set up on these right now so we're

7
00:00:29,730 --> 00:00:36,630
going to do one thing here quicker and

8
00:00:33,870 --> 00:00:38,300
guides online we'll come back to this if

9
00:00:36,630 --> 00:00:40,710
you don't have access to this right

10
00:00:38,300 --> 00:00:41,220
multiple rounds of games so we can do

11
00:00:40,710 --> 00:00:42,900
this later

12
00:00:41,220 --> 00:00:45,750
so first thing I want you to do is

13
00:00:42,900 --> 00:00:46,980
right-click and on your computer and go

14
00:00:45,750 --> 00:00:50,690
to properties you're going to need to

15
00:00:46,980 --> 00:00:50,690
otherwise you can't play the game

16
00:01:04,940 --> 00:01:13,500
all right and then go to the advanced

17
00:01:07,890 --> 00:01:25,860
system settings from there then go to

18
00:01:13,500 --> 00:01:38,910
the environment variables environment

19
00:01:25,860 --> 00:01:52,800
variables cap and a value and at the

20
00:01:38,910 --> 00:01:54,270
very end semi colon c7 so once you're in

21
00:01:52,800 --> 00:01:57,540
environment variables you scroll down a

22
00:01:54,270 --> 00:01:59,070
path double click on the value and at

23
00:01:57,540 --> 00:02:04,760
the very end of the variable value you

24
00:01:59,070 --> 00:02:09,030
put semicolon C colon slash Python to 7

25
00:02:04,760 --> 00:02:13,980
and then next we need to open up two

26
00:02:09,030 --> 00:02:18,150
command-line windows I actually have a

27
00:02:13,980 --> 00:02:21,380
file share that I'm alright so we need

28
00:02:18,150 --> 00:02:24,420
to get the latest version of the code so

29
00:02:21,380 --> 00:02:28,320
what I want you to do is make a folder

30
00:02:24,420 --> 00:02:30,000
on your desktop called lob all right so

31
00:02:28,320 --> 00:02:31,890
then as I said we need to come and live

32
00:02:30,000 --> 00:02:33,239
with us so we're gonna be running one

33
00:02:31,890 --> 00:02:35,010
command line window where it's gonna be

34
00:02:33,239 --> 00:02:37,019
you just playing for your own purposes

35
00:02:35,010 --> 00:02:38,730
to learn the room we're gonna do this is

36
00:02:37,019 --> 00:02:40,200
we're gonna have one you mess around by

37
00:02:38,730 --> 00:02:41,880
yourself for a little bit get familiar

38
00:02:40,200 --> 00:02:45,060
with questions and then we're gonna have

39
00:02:41,880 --> 00:02:47,280
a clasp game where we everybody syncs up

40
00:02:45,060 --> 00:02:49,530
on the same questions and we're all

41
00:02:47,280 --> 00:02:51,180
going to compete against each other so

42
00:02:49,530 --> 00:02:53,010
you're gonna want one that's going to be

43
00:02:51,180 --> 00:02:55,980
your window or just messing around so

44
00:02:53,010 --> 00:03:00,739
you want to go to you know on your

45
00:02:55,980 --> 00:03:00,739
desktop like the binaries Python demo

46
00:03:04,300 --> 00:03:16,690
and take on your second window one of

47
00:03:15,160 --> 00:03:19,420
those windows we basically have to

48
00:03:16,690 --> 00:03:21,600
install the custom TV library that does

49
00:03:19,420 --> 00:03:25,780
a person with key files that I

50
00:03:21,600 --> 00:03:27,700
customized to help big random games so

51
00:03:25,780 --> 00:03:31,380
I'm going to need to change the right

52
00:03:27,700 --> 00:03:31,380
frame to the 0 mod V file

53
00:03:33,160 --> 00:03:37,640
and then so Python and set up top I

54
00:03:35,990 --> 00:03:39,230
install it should say that it's building

55
00:03:37,640 --> 00:03:41,180
and installing it moving it somewhere

56
00:03:39,230 --> 00:03:44,750
else and then once you know that that

57
00:03:41,180 --> 00:04:10,100
you can just go back to the previous

58
00:03:44,750 --> 00:04:13,070
directory it's gonna ask you whether you

59
00:04:10,100 --> 00:04:14,540
want a new single-player mode so we'll

60
00:04:13,070 --> 00:04:16,400
play a replay don't worry about what

61
00:04:14,540 --> 00:04:18,290
that is and then class mode so we'll

62
00:04:16,400 --> 00:04:21,049
just be using a single player that it

63
00:04:18,290 --> 00:04:24,229
seemed clear our sorry zero into easy

64
00:04:21,049 --> 00:04:26,060
single-player class mode so like I said

65
00:04:24,229 --> 00:04:28,190
one of your windows who should run this

66
00:04:26,060 --> 00:04:30,290
as single player and one of them use

67
00:04:28,190 --> 00:04:31,700
you're running the class up and for now

68
00:04:30,290 --> 00:04:38,720
just play around in the single-player

69
00:04:31,700 --> 00:04:40,010
basically spine system no such file all

70
00:04:38,720 --> 00:04:43,400
right but so everyone else just

71
00:04:40,010 --> 00:04:44,600
basically run game like that new 0 and

72
00:04:43,400 --> 00:04:47,840
then start trying to answer the

73
00:04:44,600 --> 00:04:50,090
questions it tells you where the file is

74
00:04:47,840 --> 00:04:51,380
as well so I'll give you if it's just

75
00:04:50,090 --> 00:04:53,419
asking a question like this right now

76
00:04:51,380 --> 00:04:55,910
that you see on screen it's basically a

77
00:04:53,419 --> 00:04:57,860
fact question otherwise for the next

78
00:04:55,910 --> 00:04:59,360
questions it'll ask it'll tell you a

79
00:04:57,860 --> 00:05:01,130
particular file that you should open up

80
00:04:59,360 --> 00:05:03,669
with PU and so forth this is a fact

81
00:05:01,130 --> 00:05:06,110
question what is the magic and asking

82
00:05:03,669 --> 00:05:08,780
yeah I guess here is actually well so

83
00:05:06,110 --> 00:05:10,160
since this randomized this like so when

84
00:05:08,780 --> 00:05:12,260
you're doing it single player just picks

85
00:05:10,160 --> 00:05:14,120
a random it basically takes the time and

86
00:05:12,260 --> 00:05:15,650
uses that as a seed to surround a number

87
00:05:14,120 --> 00:05:17,539
so you will all be getting different

88
00:05:15,650 --> 00:05:21,710
questions right now you will not be able

89
00:05:17,539 --> 00:05:23,030
to copy off of each other but just go

90
00:05:21,710 --> 00:05:27,380
and try to answer the questions

91
00:05:23,030 --> 00:05:29,539
basically well alright and that just

92
00:05:27,380 --> 00:05:33,440
means that you know in Fight Club won

93
00:05:29,539 --> 00:05:35,930
bin's grab one q1 dot exe from any

94
00:05:33,440 --> 00:05:37,700
sections does this fine hair you have so

95
00:05:35,930 --> 00:05:39,180
I'm gonna go a little bit up in the file

96
00:05:37,700 --> 00:05:42,389
header so where it's and have number

97
00:05:39,180 --> 00:05:48,060
section so I can just you know take cff

98
00:05:42,389 --> 00:05:55,380
Explorer I can go to my game directory

99
00:05:48,060 --> 00:05:59,610
like the binaries put my file go to the

100
00:05:55,380 --> 00:06:03,270
file header number of sections right

101
00:05:59,610 --> 00:06:04,620
there so you basically need you go

102
00:06:03,270 --> 00:06:07,979
through that again I have no idea what

103
00:06:04,620 --> 00:06:10,050
where are you with so yeah so this is it

104
00:06:07,979 --> 00:06:12,120
doesn't basically matter which

105
00:06:10,050 --> 00:06:13,889
particular file it is the point is when

106
00:06:12,120 --> 00:06:17,789
you get a question and it says scored

107
00:06:13,889 --> 00:06:19,919
this binary right here right I say for a

108
00:06:17,789 --> 00:06:21,479
particular binary it's going to ask you

109
00:06:19,919 --> 00:06:23,820
particular questions and say how many

110
00:06:21,479 --> 00:06:25,889
sections does it happen and so all you

111
00:06:23,820 --> 00:06:28,949
need to do is either use P view or cff

112
00:06:25,889 --> 00:06:32,610
Explorer doesn't really matter which and

113
00:06:28,949 --> 00:06:36,509
again those are both on your desktop in

114
00:06:32,610 --> 00:06:39,120
the life of binaries folder ESP 44 you

115
00:06:36,509 --> 00:06:41,580
have a few and UCF explorer open up one

116
00:06:39,120 --> 00:07:00,900
of them and then just go find that file

117
00:06:41,580 --> 00:07:02,400
that it asked you a question about and

118
00:07:00,900 --> 00:07:05,570
so feel free to you know this is the

119
00:07:02,400 --> 00:07:05,570
very beginning it's all going to be

120
00:07:07,580 --> 00:07:15,539
mish-mashed but ask any questions you

121
00:07:12,900 --> 00:07:17,909
have them basically we're gonna do this

122
00:07:15,539 --> 00:07:20,220
for you know 5 10 minutes or so you just

123
00:07:17,909 --> 00:07:21,599
play around on your own you make sure

124
00:07:20,220 --> 00:07:23,250
that you're good with all the questions

125
00:07:21,599 --> 00:07:25,349
it's asking and make sure you're good

126
00:07:23,250 --> 00:07:45,960
with opening these different wineries

127
00:07:25,349 --> 00:07:49,620
and what's your good is the first thing

128
00:07:45,960 --> 00:07:52,919
to be careful for when you open up two

129
00:07:49,620 --> 00:07:55,320
files in cff explorer it will open a new

130
00:07:52,919 --> 00:07:57,090
tab for the new file while we're doing

131
00:07:55,320 --> 00:07:59,039
the games and especially during the time

132
00:07:57,090 --> 00:08:00,330
challenge round you'll be rushing and

133
00:07:59,039 --> 00:08:08,130
you won't realize you're not clicking

134
00:08:00,330 --> 00:08:10,199
over to the other tab so I got two tabs

135
00:08:08,130 --> 00:08:11,880
you need to make sure you're on the tab

136
00:08:10,199 --> 00:08:16,130
the right fighting for the right

137
00:08:11,880 --> 00:08:16,130
question otherwise you will get it wrong

138
00:08:17,270 --> 00:08:26,909
you might get lucky and you can drag and

139
00:08:22,560 --> 00:08:28,560
drop into so basically it's going to

140
00:08:26,909 --> 00:08:30,419
keep calling me as the same binary it'll

141
00:08:28,560 --> 00:08:32,610
call them run one question zero round 1

142
00:08:30,419 --> 00:08:34,409
question 1 so forth but we're all

143
00:08:32,610 --> 00:08:36,839
getting different binaries in this stage

144
00:08:34,409 --> 00:08:38,370
so later on when we sync up as the class

145
00:08:36,839 --> 00:08:40,620
we'll get the same things in the same

146
00:08:38,370 --> 00:08:43,820
answers but right now the same question

147
00:08:40,620 --> 00:08:43,820
different answer for everybody

148
00:08:44,990 --> 00:08:49,230
it's basically so that you can go off in

149
00:08:47,550 --> 00:08:51,089
like you know use this six months from

150
00:08:49,230 --> 00:08:52,770
now and you forgot it all you can

151
00:08:51,089 --> 00:08:58,589
quickly refresh yourself on the stuff

152
00:08:52,770 --> 00:09:00,120
right the big thing is if you get a

153
00:08:58,589 --> 00:09:01,860
question wrong and you don't know why

154
00:09:00,120 --> 00:09:03,209
you got involved make sure you ask me

155
00:09:01,860 --> 00:09:03,800
because otherwise don't keep getting it

156
00:09:03,209 --> 00:09:06,410
wrong

157
00:09:03,800 --> 00:09:07,759
today and so you probably want it if you

158
00:09:06,410 --> 00:09:09,679
get a Type A instead of question you

159
00:09:07,759 --> 00:09:11,179
probably want to open it in P view

160
00:09:09,679 --> 00:09:12,470
because that will interpret it for you

161
00:09:11,179 --> 00:09:14,329
if asked you about the year

162
00:09:12,470 --> 00:09:16,970
see if F explorer won't interpret the

163
00:09:14,329 --> 00:09:18,739
year for you so you can take C F F

164
00:09:16,970 --> 00:09:22,069
explores output you can go paste it into

165
00:09:18,739 --> 00:09:24,379
like epoch calculator calm or something

166
00:09:22,069 --> 00:09:27,040
like that and they don't it'll convert

167
00:09:24,379 --> 00:09:27,040
it over for you

168
00:09:34,600 --> 00:09:38,180
well he's looking at I just want to tell

169
00:09:36,890 --> 00:09:40,190
everybody else about the one trick

170
00:09:38,180 --> 00:09:42,950
question in here something you may have

171
00:09:40,190 --> 00:09:46,460
run into this there's one question that

172
00:09:42,950 --> 00:09:48,980
says what is the difference between the

173
00:09:46,460 --> 00:09:53,930
you know what is the what is the exact

174
00:09:48,980 --> 00:09:57,500
word against them what what is the

175
00:09:53,930 --> 00:10:00,230
offset was all shot from the end of the

176
00:09:57,500 --> 00:10:03,050
image das feather yes to the image and

177
00:10:00,230 --> 00:10:07,460
feathers right what is the offset from

178
00:10:03,050 --> 00:10:18,110
the bill can I get the video on the

179
00:10:07,460 --> 00:10:19,880
board all right that's good alright so I

180
00:10:18,110 --> 00:10:21,530
said what is the offset from the end of

181
00:10:19,880 --> 00:10:23,330
the DAW Center to the beginning of the

182
00:10:21,530 --> 00:10:26,180
UN theater so we know that the last

183
00:10:23,330 --> 00:10:28,340
field in the dog Center says what the

184
00:10:26,180 --> 00:10:30,560
offset is from the start of the file to

185
00:10:28,340 --> 00:10:32,960
need an empty header right so in this

186
00:10:30,560 --> 00:10:36,260
case it says 268 so you want to start a

187
00:10:32,960 --> 00:10:37,940
file this is 0 the 268 that's are you

188
00:10:36,260 --> 00:10:39,650
good what I'm saying is what the offset

189
00:10:37,940 --> 00:10:40,910
from the end of this data structure to

190
00:10:39,650 --> 00:10:43,340
the beginning of their stage structure

191
00:10:40,910 --> 00:10:44,960
so you really need to take this offset -

192
00:10:43,340 --> 00:10:47,090
the end of this data structure

193
00:10:44,960 --> 00:10:50,690
elucidation structure ends at X 40

194
00:10:47,090 --> 00:10:53,690
because this last field is received but

195
00:10:50,690 --> 00:10:56,450
it contains 4 bytes so 3 c + 4 bytes

196
00:10:53,690 --> 00:10:59,510
being that the last little bit of data

197
00:10:56,450 --> 00:11:02,870
of this field and that X 40 ends of

198
00:10:59,510 --> 00:11:06,140
thing so you basically always do any

199
00:11:02,870 --> 00:11:11,270
header - X 40 and then you notice of

200
00:11:06,140 --> 00:11:13,640
that question all right so we're gonna

201
00:11:11,270 --> 00:11:15,260
do the class game though basically well

202
00:11:13,640 --> 00:11:16,850
so before we do the class game I'm going

203
00:11:15,260 --> 00:11:20,810
to show you kind of what I expect this

204
00:11:16,850 --> 00:11:25,220
indie game alright so we all got our our

205
00:11:20,810 --> 00:11:27,500
game line so the point is this game was

206
00:11:25,220 --> 00:11:30,980
kind of inspired by this picture over at

207
00:11:27,500 --> 00:11:32,600
the Khan Academy Academy you know they

208
00:11:30,980 --> 00:11:34,040
have many different knowledge nuggets

209
00:11:32,600 --> 00:11:35,450
modules that you can go through you

210
00:11:34,040 --> 00:11:37,940
watch one little video and then you do

211
00:11:35,450 --> 00:11:40,040
like some ways on an arithmetic things

212
00:11:37,940 --> 00:11:42,170
like that it's good for elementary

213
00:11:40,040 --> 00:11:44,810
education this shows basically you know

214
00:11:42,170 --> 00:11:45,610
students on the site how many modules

215
00:11:44,810 --> 00:11:46,959
have a

216
00:11:45,610 --> 00:11:48,310
pleated so they watch this video than

217
00:11:46,959 --> 00:11:50,410
they watch that video and so over time

218
00:11:48,310 --> 00:11:53,860
they just increase the number of things

219
00:11:50,410 --> 00:11:55,450
they well so I want it to basically see

220
00:11:53,860 --> 00:11:57,700
something like this where your scores

221
00:11:55,450 --> 00:11:59,260
you know will go over time go up you

222
00:11:57,700 --> 00:12:01,360
know it'll go down occasionally and then

223
00:11:59,260 --> 00:12:03,610
go down at twice the rate that it goes

224
00:12:01,360 --> 00:12:05,740
up if you get it wrong so what I want to

225
00:12:03,610 --> 00:12:07,630
stress for this next competition amongst

226
00:12:05,740 --> 00:12:10,269
yourselves is accuracy and precision

227
00:12:07,630 --> 00:12:12,970
speed and precision speed into position

228
00:12:10,269 --> 00:12:14,500
so answer the questions fast because you

229
00:12:12,970 --> 00:12:16,930
want to get the highest score and the

230
00:12:14,500 --> 00:12:18,399
least amount of time but you also want

231
00:12:16,930 --> 00:12:21,100
to catch it answer them accurately

232
00:12:18,399 --> 00:12:24,899
otherwise they'll go down by 200 it'll

233
00:12:21,100 --> 00:12:27,700
take two questions at javac right so

234
00:12:24,899 --> 00:12:29,470
this you know is notionally this would

235
00:12:27,700 --> 00:12:31,690
be an example of you know how these

236
00:12:29,470 --> 00:12:33,220
games are good for like just reinforcing

237
00:12:31,690 --> 00:12:34,630
your own knowledge you know you've had

238
00:12:33,220 --> 00:12:37,269
some time now that you can do the

239
00:12:34,630 --> 00:12:39,700
questions over a couple of times so

240
00:12:37,269 --> 00:12:41,440
behind the scenes there's a little CSV

241
00:12:39,700 --> 00:12:42,760
file getting dumped out of your score

242
00:12:41,440 --> 00:12:45,100
over time so it's keeping track of

243
00:12:42,760 --> 00:12:46,930
you've got plus 100 at this time you got

244
00:12:45,100 --> 00:12:48,550
you know now you're at 200 at this time

245
00:12:46,930 --> 00:12:52,149
now you're back down to 0 at this time

246
00:12:48,550 --> 00:12:53,980
and so forth so me after I played my

247
00:12:52,149 --> 00:12:55,420
game enough that I wasn't getting

248
00:12:53,980 --> 00:12:56,949
anything wrong anymore which is

249
00:12:55,420 --> 00:13:00,940
definitely not the case when I first did

250
00:12:56,949 --> 00:13:04,029
it my first round through basically was

251
00:13:00,940 --> 00:13:05,860
this bottom rack so try one was actually

252
00:13:04,029 --> 00:13:07,600
this bottom one so it took me about two

253
00:13:05,860 --> 00:13:09,970
hundred and sixty Seconds in order to

254
00:13:07,600 --> 00:13:12,070
finish the first two rounds for instance

255
00:13:09,970 --> 00:13:14,410
and then the second try it took me about

256
00:13:12,070 --> 00:13:16,149
you know two hundred and thirty seconds

257
00:13:14,410 --> 00:13:18,610
then the third tried took me maybe like

258
00:13:16,149 --> 00:13:20,440
170 seconds some of this is obviously

259
00:13:18,610 --> 00:13:22,420
new to memorization because I was using

260
00:13:20,440 --> 00:13:24,940
the same speed each time I was trying to

261
00:13:22,420 --> 00:13:26,740
reduce that by basically going through

262
00:13:24,940 --> 00:13:29,320
the motions and opening on the file so a

263
00:13:26,740 --> 00:13:30,850
lot of it was really just speed of like

264
00:13:29,320 --> 00:13:32,470
figuring out how to open the file as

265
00:13:30,850 --> 00:13:34,660
much quicker but see if I can explore

266
00:13:32,470 --> 00:13:36,820
and preview and things like that this is

267
00:13:34,660 --> 00:13:40,060
how I improved over time by you know

268
00:13:36,820 --> 00:13:43,060
just doing the same all right now here's

269
00:13:40,060 --> 00:13:44,980
some values from the previous class so

270
00:13:43,060 --> 00:13:46,930
unfortunately there was a data

271
00:13:44,980 --> 00:13:49,079
collection error in the previous game so

272
00:13:46,930 --> 00:13:51,579
I couldn't get like the entire thing but

273
00:13:49,079 --> 00:13:53,470
at the end of the at the beginning of

274
00:13:51,579 --> 00:13:54,880
the second day we basically we've done

275
00:13:53,470 --> 00:13:56,470
five rounds with

276
00:13:54,880 --> 00:14:00,130
material that's why I said like all

277
00:13:56,470 --> 00:14:02,230
right day to review is just see who can

278
00:14:00,130 --> 00:14:03,880
get through round five the fastest right

279
00:14:02,230 --> 00:14:07,240
so unfortunately I can only get the

280
00:14:03,880 --> 00:14:08,620
first two rounds of material but but

281
00:14:07,240 --> 00:14:10,420
basically this showed us kind of you

282
00:14:08,620 --> 00:14:12,160
know there were some people who know

283
00:14:10,420 --> 00:14:13,330
because some of them had some background

284
00:14:12,160 --> 00:14:15,220
knowledge some of them didn't they just

285
00:14:13,330 --> 00:14:16,930
took them in general there's sort of

286
00:14:15,220 --> 00:14:18,880
this first year of people who do you

287
00:14:16,930 --> 00:14:21,130
know relatively fast and there's there's

288
00:14:18,880 --> 00:14:22,960
kind of middle ear and you know

289
00:14:21,130 --> 00:14:24,610
sometimes this long gap between you know

290
00:14:22,960 --> 00:14:25,840
getting stuff along is you know just

291
00:14:24,610 --> 00:14:28,270
because they're taking a lot of time

292
00:14:25,840 --> 00:14:30,490
trying to yeah cure with it sometimes

293
00:14:28,270 --> 00:14:32,020
lot of gaps before getting things right

294
00:14:30,490 --> 00:14:33,340
is because they asked me to come over

295
00:14:32,020 --> 00:14:35,350
and you know answer a question or

296
00:14:33,340 --> 00:14:37,030
something like that but you know there's

297
00:14:35,350 --> 00:14:41,830
sort of a middle tier who gets it

298
00:14:37,030 --> 00:14:43,960
eventually and there's the people we're

299
00:14:41,830 --> 00:14:45,640
not all going to be proficient at all

300
00:14:43,960 --> 00:14:48,010
things in equal measure

301
00:14:45,640 --> 00:14:51,250
but this this is what you need to avoid

302
00:14:48,010 --> 00:14:53,320
don't be this guy right there are people

303
00:14:51,250 --> 00:14:54,970
who don't ask questions because they get

304
00:14:53,320 --> 00:14:56,320
it there are people who don't ask

305
00:14:54,970 --> 00:14:59,140
questions because they don't get it

306
00:14:56,320 --> 00:15:00,880
don't be that guy asking me a question

307
00:14:59,140 --> 00:15:03,640
if you're not getting it

308
00:15:00,880 --> 00:15:06,700
don't stay in negative territory you

309
00:15:03,640 --> 00:15:09,250
know I mean he did good by the end he

310
00:15:06,700 --> 00:15:10,390
was you know asking questions but the

311
00:15:09,250 --> 00:15:12,250
nice thing about this game too is I

312
00:15:10,390 --> 00:15:13,840
cannot look over your shoulders and see

313
00:15:12,250 --> 00:15:15,280
whether you're just you know for five

314
00:15:13,840 --> 00:15:17,170
minutes in and you've still a negative

315
00:15:15,280 --> 00:15:24,610
territory all right you're not asking me

316
00:15:17,170 --> 00:15:26,380
questions you don't get it so again all

317
00:15:24,610 --> 00:15:28,000
right now we're going to do this we're

318
00:15:26,380 --> 00:15:30,310
going to repeat against each other I'm

319
00:15:28,000 --> 00:15:32,710
going to give you a seed so that we all

320
00:15:30,310 --> 00:15:36,100
sync up and we're all on the same page

321
00:15:32,710 --> 00:15:38,470
and so basically we're going to have our

322
00:15:36,100 --> 00:15:40,000
gold silver and bronze medalists written

323
00:15:38,470 --> 00:15:42,400
up on the board will keep track of them

324
00:15:40,000 --> 00:15:43,690
over the day and actually it was

325
00:15:42,400 --> 00:15:45,310
interesting in this class we didn't have

326
00:15:43,690 --> 00:15:48,550
the same people winning over and over

327
00:15:45,310 --> 00:15:50,410
again so among the top like five or six

328
00:15:48,550 --> 00:15:51,910
people they were trading spot so it

329
00:15:50,410 --> 00:15:54,430
wasn't like just the top free trading

330
00:15:51,910 --> 00:15:56,890
spots in order so we can be able to sit

331
00:15:54,430 --> 00:15:59,080
at that second cost we know now yes what

332
00:15:56,890 --> 00:16:01,420
we're gonna do now is we're going to

333
00:15:59,080 --> 00:16:02,890
you've got the second thoughts window so

334
00:16:01,420 --> 00:16:05,740
your first one should have been in

335
00:16:02,890 --> 00:16:07,000
single-player mode and so Mike what

336
00:16:05,740 --> 00:16:08,589
you're going to want to do is everyone

337
00:16:07,000 --> 00:16:11,800
just basically if you

338
00:16:08,589 --> 00:16:14,980
if you don't have a window that's there

339
00:16:11,800 --> 00:16:16,749
actually I guess I don't get there but

340
00:16:14,980 --> 00:16:38,829
basically your second window you should

341
00:16:16,749 --> 00:16:41,559
have all right so into your second dose

342
00:16:38,829 --> 00:16:44,740
window you're going to put it to for

343
00:16:41,559 --> 00:16:46,870
class or no all right so everybody is

344
00:16:44,740 --> 00:16:49,870
waiting so I'm going to give you a seed

345
00:16:46,870 --> 00:16:51,629
and then the timer begins here to go

346
00:16:49,870 --> 00:16:54,490
with the seed of seven eight nine

347
00:16:51,629 --> 00:16:57,009
you made okay here's a possible problem

348
00:16:54,490 --> 00:16:59,829
if you have the other games to loading

349
00:16:57,009 --> 00:17:02,769
your your only game you cancel that out

350
00:16:59,829 --> 00:17:06,900
because that may be holding a file open

351
00:17:02,769 --> 00:17:06,900
and apparently my thing is not around

352
00:17:15,540 --> 00:17:21,220
all right we've done three people done

353
00:17:17,770 --> 00:17:25,600
so basically we're done we've got our

354
00:17:21,220 --> 00:17:29,670
medalists all right so everyone

355
00:17:25,600 --> 00:17:34,870
basically at this point open up Excel

356
00:17:29,670 --> 00:17:38,020
so do run and just Excel we're gonna go

357
00:17:34,870 --> 00:17:42,550
look at your log file for your one you

358
00:17:38,020 --> 00:17:49,800
need to go a file open and then go to

359
00:17:42,550 --> 00:17:53,020
your desktop the binaries Python demo

360
00:17:49,800 --> 00:17:55,870
and make sure Excel is set to see all

361
00:17:53,020 --> 00:17:59,710
files and you want to open a basic log

362
00:17:55,870 --> 00:18:00,280
file that's CSP so what I'm looking for

363
00:17:59,710 --> 00:18:01,810
right now

364
00:18:00,280 --> 00:18:03,490
so you're gonna have seven eight nine

365
00:18:01,810 --> 00:18:05,020
that's your seed which you were using

366
00:18:03,490 --> 00:18:06,520
for your thing you're gonna have a

367
00:18:05,020 --> 00:18:09,610
second column is going to be the number

368
00:18:06,520 --> 00:18:12,070
of seconds it took you to get and is the

369
00:18:09,610 --> 00:18:14,800
score so what I'm looking for is someone

370
00:18:12,070 --> 00:18:19,240
who is meeting orcs or you know faster

371
00:18:14,800 --> 00:18:22,590
than 180 seconds anyone faster than 180

372
00:18:19,240 --> 00:18:22,590
seconds to get to 1,000

373
00:18:28,320 --> 00:18:30,380
you


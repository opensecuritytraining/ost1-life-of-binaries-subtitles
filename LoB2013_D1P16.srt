1
00:00:03,660 --> 00:00:10,709
good next and last import address table

2
00:00:08,370 --> 00:00:14,949
something we're going to talk about the

3
00:00:10,709 --> 00:00:16,000
delay loaded DLLs this is much more like

4
00:00:14,949 --> 00:00:17,110
what you're going to see on the nights

5
00:00:16,000 --> 00:00:18,520
later i'm going to show you a picture

6
00:00:17,110 --> 00:00:20,710
for windows which is going to be the

7
00:00:18,520 --> 00:00:23,920
exact same picture for linux is not the

8
00:00:20,710 --> 00:00:27,130
default of windows the normal imports is

9
00:00:23,920 --> 00:00:29,200
the default but delayed on imports is

10
00:00:27,130 --> 00:00:31,949
basically just saying I don't even want

11
00:00:29,200 --> 00:00:35,770
you OS loader don't even bother loading

12
00:00:31,949 --> 00:00:37,600
dll foo do L don't even bother loading

13
00:00:35,770 --> 00:00:40,750
that into memory until I call a function

14
00:00:37,600 --> 00:00:43,690
in food thiele so it's lazy loading lazy

15
00:00:40,750 --> 00:00:45,340
linking basically what i call that

16
00:00:43,690 --> 00:00:47,079
function now i want you to go out and

17
00:00:45,340 --> 00:00:49,030
get that thing off distant color because

18
00:00:47,079 --> 00:00:51,670
you can think of some cases where you

19
00:00:49,030 --> 00:00:53,079
know maybe it's very expensive to maybe

20
00:00:51,670 --> 00:00:54,880
you've got like a giant thing that takes

21
00:00:53,079 --> 00:00:57,640
up a ton of memory you only call it like

22
00:00:54,880 --> 00:00:59,620
once in some really corner case not a

23
00:00:57,640 --> 00:01:02,200
thing and so why even bother wasting

24
00:00:59,620 --> 00:01:03,910
your memory by loading up a dll unless

25
00:01:02,200 --> 00:01:06,850
you happen to hit that corner case and

26
00:01:03,910 --> 00:01:08,709
you call that particular motion so delay

27
00:01:06,850 --> 00:01:13,420
load of dll says i'm only going to

28
00:01:08,709 --> 00:01:18,130
import it what i needed so that would be

29
00:01:13,420 --> 00:01:20,380
set in the visual studio and we can see

30
00:01:18,130 --> 00:01:22,029
input options for linker specify which

31
00:01:20,380 --> 00:01:24,880
dll's you wanted to hold off on

32
00:01:22,029 --> 00:01:26,679
importing until you call and then you

33
00:01:24,880 --> 00:01:31,090
say whether or not you support it

34
00:01:26,679 --> 00:01:33,459
unloading afterwards and in any PLL be

35
00:01:31,090 --> 00:01:36,639
delay loaded that's a good question i

36
00:01:33,459 --> 00:01:39,880
mean technically i think you could mark

37
00:01:36,639 --> 00:01:41,679
any dll as delay moment but i feel like

38
00:01:39,880 --> 00:01:43,990
well first of all I feel like it open

39
00:01:41,679 --> 00:01:45,189
you colonel three the real person anyone

40
00:01:43,990 --> 00:01:47,679
to know just wondering do you have to

41
00:01:45,189 --> 00:01:50,919
compile the dll to sound like them yeah

42
00:01:47,679 --> 00:01:52,869
so no so there need be no compatibility

43
00:01:50,919 --> 00:01:54,909
with the thing that you're marking is to

44
00:01:52,869 --> 00:01:56,499
lately let's completely independent you

45
00:01:54,909 --> 00:01:59,079
can just say well I don't want to import

46
00:01:56,499 --> 00:02:01,479
that guy until later but the other deal

47
00:01:59,079 --> 00:02:03,729
has to know nothing about because they

48
00:02:01,479 --> 00:02:05,049
do some kind of checking right beginning

49
00:02:03,729 --> 00:02:07,779
that and make sure that it's actually

50
00:02:05,049 --> 00:02:09,759
there or you get an error you try and

51
00:02:07,779 --> 00:02:11,770
call that fun I believe it does do a

52
00:02:09,759 --> 00:02:15,090
little bit of an optimization where it

53
00:02:11,770 --> 00:02:15,090
will check basically

54
00:02:16,120 --> 00:02:20,379
no I don't think so it's like Microsoft

55
00:02:18,310 --> 00:02:22,900
Word it usually closed the equation

56
00:02:20,379 --> 00:02:26,409
editor and it's not you didn't install

57
00:02:22,900 --> 00:02:28,420
it you'll get an error yeah it depends

58
00:02:26,409 --> 00:02:31,180
on what you mean by does it do a check

59
00:02:28,420 --> 00:02:32,739
certainly it won't do a check like at

60
00:02:31,180 --> 00:02:34,209
the times of thing is loaded up into

61
00:02:32,739 --> 00:02:36,280
Robert but I was thinking more in the

62
00:02:34,209 --> 00:02:39,040
context of like if I function one from

63
00:02:36,280 --> 00:02:40,989
that DLL and I'm function to you call

64
00:02:39,040 --> 00:02:43,930
function and then it gets loaded up and

65
00:02:40,989 --> 00:02:45,849
then you call function too I'm trying to

66
00:02:43,930 --> 00:02:47,349
think of whether it resolves and even

67
00:02:45,849 --> 00:02:50,890
though I know it doesn't resolve all of

68
00:02:47,349 --> 00:02:52,750
them and there for Eric so it's

69
00:02:50,890 --> 00:02:55,359
basically a two-stage process there's

70
00:02:52,750 --> 00:02:57,489
four delay load imports each of them is

71
00:02:55,359 --> 00:03:01,870
treated independently so basically it's

72
00:02:57,489 --> 00:03:03,760
to stage when you call function one it

73
00:03:01,870 --> 00:03:06,010
basically jumps to some stub compass

74
00:03:03,760 --> 00:03:08,159
tumkur loads the dll and then it doesn't

75
00:03:06,010 --> 00:03:12,010
look up for the function ones address

76
00:03:08,159 --> 00:03:14,290
when you call function to the stub code

77
00:03:12,010 --> 00:03:16,690
will look and see is the dll already

78
00:03:14,290 --> 00:03:18,549
loaded and if so then it won't load in

79
00:03:16,690 --> 00:03:20,019
the dll and then it will still do the

80
00:03:18,549 --> 00:03:22,359
same process of looking up function

81
00:03:20,019 --> 00:03:24,639
choose dress when that sense there is a

82
00:03:22,359 --> 00:03:26,590
check before it without in the sense he

83
00:03:24,639 --> 00:03:28,540
was talking about like if you load up

84
00:03:26,590 --> 00:03:30,329
the executable and matt dll does not

85
00:03:28,540 --> 00:03:32,799
even exist there's no check like that

86
00:03:30,329 --> 00:03:34,989
will just happen at run time we try to

87
00:03:32,799 --> 00:03:37,629
call the function if the dll isn't there

88
00:03:34,989 --> 00:03:40,510
it will just crash is that aphrotronics

89
00:03:37,629 --> 00:03:41,919
a visual memory it's for saving

90
00:03:40,510 --> 00:03:45,250
additional memory it's for potentially

91
00:03:41,919 --> 00:03:47,709
saving initial one time right I mean

92
00:03:45,250 --> 00:03:49,569
it's not a lot but you know it takes

93
00:03:47,709 --> 00:03:51,340
time for the OS loader to run down the

94
00:03:49,569 --> 00:03:54,190
list of all your different functions and

95
00:03:51,340 --> 00:03:56,349
like resolve them by checking them

96
00:03:54,190 --> 00:03:58,629
through the export thing so i guess i

97
00:03:56,349 --> 00:03:59,949
would see from that sense i would see it

98
00:03:58,629 --> 00:04:01,660
as like let's say you have some small

99
00:03:59,949 --> 00:04:04,060
application that like starts up and

100
00:04:01,660 --> 00:04:05,530
shuts down frequently all right and if

101
00:04:04,060 --> 00:04:07,850
you know that like this [ __ ] this on

102
00:04:05,530 --> 00:04:10,100
occasion let's say that control down

103
00:04:07,850 --> 00:04:11,330
came up before control panel starts up

104
00:04:10,100 --> 00:04:13,790
and it's being called in like one

105
00:04:11,330 --> 00:04:17,480
particular function is being called if

106
00:04:13,790 --> 00:04:19,880
that particular function it requires gll

107
00:04:17,480 --> 00:04:22,760
a function one record vol a function to

108
00:04:19,880 --> 00:04:24,380
rock the party ll be if you're calling a

109
00:04:22,760 --> 00:04:26,210
control panel with some particular

110
00:04:24,380 --> 00:04:27,920
function and that's going to do some

111
00:04:26,210 --> 00:04:30,020
particular thing let's say there's like

112
00:04:27,920 --> 00:04:31,910
you know 50 different functions and

113
00:04:30,020 --> 00:04:33,710
let's say that they have 50 different

114
00:04:31,910 --> 00:04:35,960
dll's you wouldn't want to like load the

115
00:04:33,710 --> 00:04:38,360
thing up load 50d allows you think the

116
00:04:35,960 --> 00:04:40,580
important resolutions call one function

117
00:04:38,360 --> 00:04:42,110
and quit it down right you'd call it up

118
00:04:40,580 --> 00:04:44,630
and then it would just love just the

119
00:04:42,110 --> 00:04:47,750
necessary stuff in shutdown so is that

120
00:04:44,630 --> 00:04:49,850
atomic to the program our site what do

121
00:04:47,750 --> 00:04:51,140
you mean what did they have is there any

122
00:04:49,850 --> 00:04:53,690
concern with the program and they have

123
00:04:51,140 --> 00:04:56,090
to worry about that kind of management

124
00:04:53,690 --> 00:04:57,290
no so this is all basically behind the

125
00:04:56,090 --> 00:04:59,540
scenes as far as the program was

126
00:04:57,290 --> 00:05:01,430
concerned all he has to do is say like

127
00:04:59,540 --> 00:05:04,700
look I don't want to bother with loading

128
00:05:01,430 --> 00:05:06,080
a dll this dll unless i end up using it

129
00:05:04,700 --> 00:05:08,240
now the programmer will know that he

130
00:05:06,080 --> 00:05:10,940
imports will in dll because you know he

131
00:05:08,240 --> 00:05:12,410
went and looked up open open process or

132
00:05:10,940 --> 00:05:14,360
something like that he'll go on with him

133
00:05:12,410 --> 00:05:17,090
STM page and says open process these

134
00:05:14,360 --> 00:05:18,920
escherichia feel up they can say i know

135
00:05:17,090 --> 00:05:21,140
i only called open process in really

136
00:05:18,920 --> 00:05:23,480
rare cases so maybe I wanted to like

137
00:05:21,140 --> 00:05:25,730
that reality right girl for you choose

138
00:05:23,480 --> 00:05:30,290
but especially in law that there's

139
00:05:25,730 --> 00:05:33,380
almost all the time good questions any

140
00:05:30,290 --> 00:05:35,240
other questions alright good so I think

141
00:05:33,380 --> 00:05:37,070
we got the concept of delay load it's

142
00:05:35,240 --> 00:05:39,500
like just don't even bother loading it

143
00:05:37,070 --> 00:05:43,820
just in time all right back to the data

144
00:05:39,500 --> 00:05:48,020
directory looks like index 14 13 said

145
00:05:43,820 --> 00:05:49,520
that I 80 was 12 this must be 13 the

146
00:05:48,020 --> 00:05:50,840
lame-o be apart again just like

147
00:05:49,520 --> 00:05:53,390
everything else it's a virtual

148
00:05:50,840 --> 00:05:55,370
addressing size in the data directory

149
00:05:53,390 --> 00:05:57,290
and what that virtual address is going

150
00:05:55,370 --> 00:05:59,300
to be giving you at that party a is

151
00:05:57,290 --> 00:06:02,090
where you pointing any web is yet

152
00:05:59,300 --> 00:06:05,180
another something something something

153
00:06:02,090 --> 00:06:09,140
something important scripter right so we

154
00:06:05,180 --> 00:06:11,210
had originally we had so we've had

155
00:06:09,140 --> 00:06:12,650
bounded port descriptors we have normal

156
00:06:11,210 --> 00:06:13,590
import the scriptures and our got

157
00:06:12,650 --> 00:06:18,660
delayed

158
00:06:13,590 --> 00:06:20,970
which papers so things were going to

159
00:06:18,660 --> 00:06:23,070
care about this is that we've again got

160
00:06:20,970 --> 00:06:26,190
a name so we're talking one of these

161
00:06:23,070 --> 00:06:29,490
data structures / dll that were delay

162
00:06:26,190 --> 00:06:32,370
loading firm and now at least this time

163
00:06:29,490 --> 00:06:34,920
it has nice clear name's not original

164
00:06:32,370 --> 00:06:37,080
first pumpkin first up right batting

165
00:06:34,920 --> 00:06:39,720
points at the delay loaded cord rested

166
00:06:37,080 --> 00:06:42,360
at the employer to delay looking for

167
00:06:39,720 --> 00:06:45,540
names to these are actually separate

168
00:06:42,360 --> 00:06:48,180
again well so where's the bounding ports

169
00:06:45,540 --> 00:06:50,160
is reusing and pre filling into normal

170
00:06:48,180 --> 00:06:51,780
import dress table this is actually

171
00:06:50,160 --> 00:06:54,240
going to have its own data structures

172
00:06:51,780 --> 00:06:56,430
it's only for us to the only important

173
00:06:54,240 --> 00:07:01,520
names table off to the side completely

174
00:06:56,430 --> 00:07:01,520
separate from me normal imported resting

175
00:07:01,730 --> 00:07:09,390
yeah and so in the original like arrow

176
00:07:06,750 --> 00:07:11,940
carrera slide cause this image delay

177
00:07:09,390 --> 00:07:16,350
import descriptor but there's no such

178
00:07:11,940 --> 00:07:18,090
thing to find in winnt died aged so the

179
00:07:16,350 --> 00:07:20,070
only thing i've seen defined as delay in

180
00:07:18,090 --> 00:07:22,490
that age and it has this particular data

181
00:07:20,070 --> 00:07:26,570
structure we're just going to call it

182
00:07:22,490 --> 00:07:37,230
the lay low to import descriptor anyways

183
00:07:26,570 --> 00:07:41,250
but alright so so this is what i just

184
00:07:37,230 --> 00:07:43,320
said we care about the RBA IEP is

185
00:07:41,250 --> 00:07:45,210
further point that separate ISE again

186
00:07:43,320 --> 00:07:49,110
like the previous ayah t it's just going

187
00:07:45,210 --> 00:07:51,390
to be function pointers but the

188
00:07:49,110 --> 00:07:53,670
interesting thing here is unlike yeah i

189
00:07:51,390 --> 00:07:56,310
guess is right under the picture unlike

190
00:07:53,670 --> 00:07:58,110
the original one that I showed you where

191
00:07:56,310 --> 00:07:59,640
important names table and import address

192
00:07:58,110 --> 00:08:03,300
table are pointing at the same sort of

193
00:07:59,640 --> 00:08:04,830
Kim's names out of data structure here

194
00:08:03,300 --> 00:08:07,130
the important Ames table is going to

195
00:08:04,830 --> 00:08:10,620
point a kink names just like regular

196
00:08:07,130 --> 00:08:13,740
these delay load entries are going to be

197
00:08:10,620 --> 00:08:16,440
pre filled in with pointers to code

198
00:08:13,740 --> 00:08:17,820
inside of the binary itself and this is

199
00:08:16,440 --> 00:08:20,640
sub-code that I was kind of talking

200
00:08:17,820 --> 00:08:22,740
about before stub code which says when

201
00:08:20,640 --> 00:08:24,050
you call this function I'm going to load

202
00:08:22,740 --> 00:08:27,139
the dll and I'm going

203
00:08:24,050 --> 00:08:29,389
look up the address of the function in

204
00:08:27,139 --> 00:08:32,000
that yellow so this works the same way

205
00:08:29,389 --> 00:08:33,560
of linux and windows whenever you do

206
00:08:32,000 --> 00:08:35,360
delay loading like this you've got to

207
00:08:33,560 --> 00:08:38,149
have some sub-code where you call print

208
00:08:35,360 --> 00:08:40,070
up but instead of actually calling

209
00:08:38,149 --> 00:08:42,229
pretty epic calls to some little snippet

210
00:08:40,070 --> 00:08:44,360
of code that says oh someone called

211
00:08:42,229 --> 00:08:46,190
print up I need to do you know load up

212
00:08:44,360 --> 00:08:47,959
the Lib C and then I need to look up the

213
00:08:46,190 --> 00:08:50,120
printf and then I'm going to fill that

214
00:08:47,959 --> 00:08:52,790
back into the table so that's just in

215
00:08:50,120 --> 00:08:54,709
time sort of thinking dynamic ranking

216
00:08:52,790 --> 00:08:58,910
alright so I'm not a little picture like

217
00:08:54,709 --> 00:09:00,740
that for that I believe this is from the

218
00:08:58,910 --> 00:09:03,920
best paint and I'll show you this with a

219
00:09:00,740 --> 00:09:06,399
debugger to a little bit so from the

220
00:09:03,920 --> 00:09:09,950
most pain is my old slide 32 bit system

221
00:09:06,399 --> 00:09:12,440
one ms pain is running a wallet at its

222
00:09:09,950 --> 00:09:14,450
normal code and it hits this call to

223
00:09:12,440 --> 00:09:16,670
import the delay loading port address

224
00:09:14,450 --> 00:09:19,579
table that happens correspond to draw a

225
00:09:16,670 --> 00:09:21,920
theme backer it's going to pull value

226
00:09:19,579 --> 00:09:25,730
out of this delay loading for address

227
00:09:21,920 --> 00:09:27,380
table at 10 3 e 64 to bound here at the

228
00:09:25,730 --> 00:09:30,140
delay loading port address table we've

229
00:09:27,380 --> 00:09:32,000
got 10 3 e 64 it's filled in with a

230
00:09:30,140 --> 00:09:34,459
particular value so this is not putting

231
00:09:32,000 --> 00:09:38,810
out the name in structure it's pointing

232
00:09:34,459 --> 00:09:46,430
at the address of some code so this has

233
00:09:38,810 --> 00:09:49,880
10 35 45 so you pull out 10 35 45 and so

234
00:09:46,430 --> 00:09:56,209
this call pulls that value of that entry

235
00:09:49,880 --> 00:10:00,680
and jumps there right there alright so

236
00:09:56,209 --> 00:10:04,190
this call jumps to hear this right this

237
00:10:00,680 --> 00:10:07,430
stub code takes and move some offset in

238
00:10:04,190 --> 00:10:10,190
ms paint some data value basically turns

239
00:10:07,430 --> 00:10:13,250
out to be this exact data value so 10 3

240
00:10:10,190 --> 00:10:15,649
e 64 happens to be the place that I've

241
00:10:13,250 --> 00:10:17,930
got is thing out of its going to take

242
00:10:15,649 --> 00:10:19,790
this address and put it into the eax

243
00:10:17,930 --> 00:10:22,220
register so it's basically just getting

244
00:10:19,790 --> 00:10:23,779
a copy of the address wherever this

245
00:10:22,220 --> 00:10:24,730
thing is because later on it's going to

246
00:10:23,779 --> 00:10:27,940
fill that in with

247
00:10:24,730 --> 00:10:29,949
different so it's just saying this jumps

248
00:10:27,940 --> 00:10:32,279
to code which gets its own address and

249
00:10:29,949 --> 00:10:35,079
puts it into a holder register and then

250
00:10:32,279 --> 00:10:37,720
it just goes ahead and jumps to a

251
00:10:35,079 --> 00:10:39,699
particular location and this location

252
00:10:37,720 --> 00:10:42,310
will be reused amongst all these scum

253
00:10:39,699 --> 00:10:45,310
entries and this location all it does is

254
00:10:42,310 --> 00:10:54,220
load the dll and try to find draw theme

255
00:10:45,310 --> 00:10:57,970
background so we jumped this stub code

256
00:10:54,220 --> 00:10:59,470
it loaded ux theme vanilla which happens

257
00:10:57,970 --> 00:11:03,040
to be the location where draw theme

258
00:10:59,470 --> 00:11:05,529
background is and so then it does a sort

259
00:11:03,040 --> 00:11:07,180
of look up on what's here offset what's

260
00:11:05,529 --> 00:11:09,639
the absolute virtual address of draw

261
00:11:07,180 --> 00:11:12,610
theme background inside of you XD

262
00:11:09,639 --> 00:11:14,320
mithila so it finds this and it wants to

263
00:11:12,610 --> 00:11:16,060
fill that into this table so that the

264
00:11:14,320 --> 00:11:18,360
next time someone running along and

265
00:11:16,060 --> 00:11:21,430
calling this they get that address

266
00:11:18,360 --> 00:11:23,709
instead of this address all right so

267
00:11:21,430 --> 00:11:25,839
first time through you get stub code

268
00:11:23,709 --> 00:11:31,089
second time through you want to get the

269
00:11:25,839 --> 00:11:34,750
real address of the real function that's

270
00:11:31,089 --> 00:11:36,970
terrible clear that animation awesome

271
00:11:34,750 --> 00:11:39,250
he's starting way up there okay I guess

272
00:11:36,970 --> 00:11:41,709
actually that's right this dll loading

273
00:11:39,250 --> 00:11:45,100
function resolution code out of that

274
00:11:41,709 --> 00:11:54,430
address up here and then it overrode it

275
00:11:45,100 --> 00:11:58,149
into the n3 e6 c4 right so there's a

276
00:11:54,430 --> 00:12:00,190
reason we got this 10 3 p-6 34 and put

277
00:11:58,149 --> 00:12:02,350
into this ax here it's just because we

278
00:12:00,190 --> 00:12:03,579
need to tell this code right here we're

279
00:12:02,350 --> 00:12:06,760
going to fill in the value would it

280
00:12:03,579 --> 00:12:08,260
eventually did the hook up it's going to

281
00:12:06,760 --> 00:12:13,660
work at the exact same way in Linux

282
00:12:08,260 --> 00:12:16,569
later alright so once this stunt code

283
00:12:13,660 --> 00:12:18,250
has filled in the value then it goes

284
00:12:16,569 --> 00:12:19,660
ahead and just all to function because

285
00:12:18,250 --> 00:12:22,870
the original program was really just

286
00:12:19,660 --> 00:12:25,089
trying to call draw me right so it just

287
00:12:22,870 --> 00:12:26,740
goes out and calls drop theme and then

288
00:12:25,089 --> 00:12:31,060
once that's done it's going to return

289
00:12:26,740 --> 00:12:33,189
back to really show it there should be

290
00:12:31,060 --> 00:12:35,380
kind of you call a drama theme and what

291
00:12:33,189 --> 00:12:38,060
it's done it returns back to after the

292
00:12:35,380 --> 00:12:39,500
call and then you continue on

293
00:12:38,060 --> 00:12:42,170
continue on and you eventually hit

294
00:12:39,500 --> 00:12:44,390
another dropping and so the next time

295
00:12:42,170 --> 00:12:46,010
you hit a draw theme you're going to go

296
00:12:44,390 --> 00:12:48,710
direct you don't have to touch the stuff

297
00:12:46,010 --> 00:12:51,260
thrown at all and so basically you can

298
00:12:48,710 --> 00:12:53,750
expect for every entry in the delay load

299
00:12:51,260 --> 00:12:55,910
import address table there's going to be

300
00:12:53,750 --> 00:12:57,680
one of these little stub things that

301
00:12:55,910 --> 00:12:59,570
just takes that entries address and

302
00:12:57,680 --> 00:13:01,910
moves it into some register and then

303
00:12:59,570 --> 00:13:04,640
just call us directly to the same code

304
00:13:01,910 --> 00:13:06,800
so just move this into register call

305
00:13:04,640 --> 00:13:09,230
that down here or move this into

306
00:13:06,800 --> 00:13:11,210
register call that so it's just a

307
00:13:09,230 --> 00:13:14,450
circuitous way of going around and

308
00:13:11,210 --> 00:13:20,800
calling the original function filling in

309
00:13:14,450 --> 00:13:20,800
just in time the delay load more drastic

310
00:13:22,330 --> 00:13:26,630
alright any questions on that

311
00:13:24,860 --> 00:13:29,270
conceptually I'll show you the literal

312
00:13:26,630 --> 00:13:32,900
things you're inside me but I really get

313
00:13:29,270 --> 00:13:35,840
this notion of weight call stub code

314
00:13:32,900 --> 00:13:37,970
that the stuff code fix up this import

315
00:13:35,840 --> 00:13:42,170
dress table that every other time you

316
00:13:37,970 --> 00:13:44,570
call to the original thing yes so after

317
00:13:42,170 --> 00:13:49,160
the first time I call Zach against the

318
00:13:44,570 --> 00:13:51,530
address in the you XD do out now does

319
00:13:49,160 --> 00:13:53,630
that stay yep cached our way it stays

320
00:13:51,530 --> 00:13:57,380
there is able to the rest of the realm

321
00:13:53,630 --> 00:13:59,390
of the program so it's basically because

322
00:13:57,380 --> 00:14:02,360
the call just pulls the value out of the

323
00:13:59,390 --> 00:14:05,630
this dress this address now afterwards

324
00:14:02,360 --> 00:14:10,310
is always only physical so it's pulling

325
00:14:05,630 --> 00:14:12,500
this out every time a new calls its you

326
00:14:10,310 --> 00:14:14,090
feel it in once and then it this remains

327
00:14:12,500 --> 00:14:17,240
for the remainder of the run when the

328
00:14:14,090 --> 00:14:18,980
program closes and reopens again based

329
00:14:17,240 --> 00:14:21,410
on the file data the file data is still

330
00:14:18,980 --> 00:14:23,210
going to be pointing at this done so the

331
00:14:21,410 --> 00:14:25,400
data infile always points at the stub

332
00:14:23,210 --> 00:14:27,710
and then if the stuff ever is called

333
00:14:25,400 --> 00:14:29,090
when it's a long time you'll get this

334
00:14:27,710 --> 00:14:31,070
building and i'll show that with the

335
00:14:29,090 --> 00:14:34,910
debugger basically i'll show that i run

336
00:14:31,070 --> 00:14:37,040
the best paint and you'll see some delay

337
00:14:34,910 --> 00:14:38,360
load entries got filled in already some

338
00:14:37,040 --> 00:14:40,850
of them didn't sometime just don't have

339
00:14:38,360 --> 00:14:42,590
the same thing is on file and then i'll

340
00:14:40,850 --> 00:14:44,300
let go and move my mouse around and that

341
00:14:42,590 --> 00:14:46,370
causes some new function to get called

342
00:14:44,300 --> 00:14:47,089
and you'll see hey look some other way

343
00:14:46,370 --> 00:14:49,550
million

344
00:14:47,089 --> 00:14:51,680
table and she just got affiliated yeah

345
00:14:49,550 --> 00:14:54,019
it's going to be / bone basically it

346
00:14:51,680 --> 00:14:58,519
gets billed in the closed out restarted

347
00:14:54,019 --> 00:15:02,600
us are any other questions yes is I

348
00:14:58,519 --> 00:15:06,559
don't get where the address replacement

349
00:15:02,600 --> 00:15:09,399
occur is this so so it is a line where

350
00:15:06,559 --> 00:15:13,160
you call right so we're everybody know

351
00:15:09,399 --> 00:15:17,629
okay that's kind of business and arrows

352
00:15:13,160 --> 00:15:20,059
how about their she enjoys so back down

353
00:15:17,629 --> 00:15:23,870
at the scub coats and we're starting up

354
00:15:20,059 --> 00:15:26,029
here this guy calls to actually pulls

355
00:15:23,870 --> 00:15:28,249
whatever is in this thing out and call

356
00:15:26,029 --> 00:15:31,639
us to that address so this points here's

357
00:15:28,249 --> 00:15:33,889
this also right here alright we get this

358
00:15:31,639 --> 00:15:35,930
address we put it into the ax and we

359
00:15:33,889 --> 00:15:38,300
just jumped here and now here I'm just

360
00:15:35,930 --> 00:15:40,819
like sort of and waving around all of

361
00:15:38,300 --> 00:15:43,309
this feeling the reality of this is that

362
00:15:40,819 --> 00:15:44,870
there's some code in here that goes and

363
00:15:43,309 --> 00:15:46,220
looks up there's going to be you know

364
00:15:44,870 --> 00:15:47,870
this is the delay looking for a dress

365
00:15:46,220 --> 00:15:49,999
Jane or not even showing it more names

366
00:15:47,870 --> 00:15:51,410
to write the important names table will

367
00:15:49,999 --> 00:15:54,769
be what's showing me that dropping

368
00:15:51,410 --> 00:15:57,980
background is up with you XD not you all

369
00:15:54,769 --> 00:15:59,629
right so somewhere in this hand wavy

370
00:15:57,980 --> 00:16:02,269
code is going to be looking up in four

371
00:15:59,629 --> 00:16:04,999
names table it's going to be calling in

372
00:16:02,269 --> 00:16:06,769
a load library to load up UX team is

373
00:16:04,999 --> 00:16:09,610
going to call you get talk address to

374
00:16:06,769 --> 00:16:13,309
get the address of dropping background

375
00:16:09,610 --> 00:16:15,319
so load the library and then somewhere

376
00:16:13,309 --> 00:16:17,600
in here after in local library it looks

377
00:16:15,319 --> 00:16:19,639
up the address and then it fills in the

378
00:16:17,600 --> 00:16:22,970
address and then it calls to the

379
00:16:19,639 --> 00:16:26,920
original now looking at what yeah x

380
00:16:22,970 --> 00:16:29,329
holes yes it just feels it kills in

381
00:16:26,920 --> 00:16:34,809
whatever it found you a dress up

382
00:16:29,329 --> 00:16:34,809
wherever he acts how can you do this

383
00:16:35,499 --> 00:16:44,360
alright so let's look at this more

384
00:16:37,519 --> 00:16:46,550
generally okay so the data directory

385
00:16:44,360 --> 00:16:49,579
will have have some r-va that point of

386
00:16:46,550 --> 00:16:52,120
that delay mode import things huh I'll

387
00:16:49,579 --> 00:16:52,120
show it for you

388
00:16:52,649 --> 00:16:58,160
good ms paint

389
00:17:12,389 --> 00:17:18,659
it will see if I'm 32 bit and that's

390
00:17:16,449 --> 00:17:18,659
good


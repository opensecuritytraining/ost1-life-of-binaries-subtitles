1
00:00:03,580 --> 00:00:11,389
so pop quiz Hot Shots I need one way

2
00:00:07,700 --> 00:00:13,400
that the PE is similar to elf or

3
00:00:11,389 --> 00:00:14,629
different from elf I don't care what and

4
00:00:13,400 --> 00:00:18,610
we're going to start right here similar

5
00:00:14,629 --> 00:00:18,610
or different you mean one way PDL

6
00:00:25,840 --> 00:00:27,900
you

7
00:00:28,710 --> 00:00:32,489
and you can talk about specific headers

8
00:00:30,689 --> 00:00:33,510
that are like the exact same equivalent

9
00:00:32,489 --> 00:00:42,930
you can talk about where they're

10
00:00:33,510 --> 00:00:45,960
different but give me an example of some

11
00:00:42,930 --> 00:00:59,070
specific instance of information that

12
00:00:45,960 --> 00:01:02,610
they have that's the same yep yeah I did

13
00:00:59,070 --> 00:01:05,880
oh that's like the magic email Eve magic

14
00:01:02,610 --> 00:01:07,110
Kenzi right LMZ they're just little

15
00:01:05,880 --> 00:01:09,659
things tacked at the beginning of the

16
00:01:07,110 --> 00:01:11,490
file saying this is if this isn't here

17
00:01:09,659 --> 00:01:13,020
oh it's loader shouldn't treat this as

18
00:01:11,490 --> 00:01:27,240
an elf file or shouldn't treat this as a

19
00:01:13,020 --> 00:01:28,860
PE address of engine address yeah yeah

20
00:01:27,240 --> 00:01:29,850
so you should all be like seriously

21
00:01:28,860 --> 00:01:31,560
looking up if you're gonna say two

22
00:01:29,850 --> 00:01:34,290
things are the same figure out their

23
00:01:31,560 --> 00:01:42,240
exact name right what's something

24
00:01:34,290 --> 00:01:43,710
similar or different okay so that's one

25
00:01:42,240 --> 00:01:46,050
that's different there's no TLS

26
00:01:43,710 --> 00:01:54,600
callbacks on L the entry point is the

27
00:01:46,050 --> 00:01:56,460
entry yep so well as far as you know

28
00:01:54,600 --> 00:01:59,760
right now you haven't seen sections yeah

29
00:01:56,460 --> 00:02:02,400
but but we see a lot of clear similarity

30
00:01:59,760 --> 00:02:03,900
between segments and L especially the PT

31
00:02:02,400 --> 00:02:06,320
load segment where it's getting mapped

32
00:02:03,900 --> 00:02:16,470
into memory in a section back to the PD

33
00:02:06,320 --> 00:02:19,220
all right yep so file offset is I don't

34
00:02:16,470 --> 00:02:19,220
even remember the name

35
00:02:20,540 --> 00:02:33,050
P offset the offset was offset to raw

36
00:02:26,879 --> 00:02:35,900
data in the LP file all right all right

37
00:02:33,050 --> 00:02:45,470
similarity or difference might've

38
00:02:35,900 --> 00:02:47,420
actually just said they have same right

39
00:02:45,470 --> 00:02:49,550
they don't just well he was talking

40
00:02:47,420 --> 00:02:51,500
about file offsets and so they have this

41
00:02:49,550 --> 00:02:53,480
similar thing in that you know they both

42
00:02:51,500 --> 00:02:55,160
go with start and sighs right they could

43
00:02:53,480 --> 00:02:57,500
both start and end if they wanted but

44
00:02:55,160 --> 00:03:00,800
they go start sighs for files and start

45
00:02:57,500 --> 00:03:06,530
sighs remember right all right similar

46
00:03:00,800 --> 00:03:09,260
or different line and yep exactly

47
00:03:06,530 --> 00:03:12,290
so we've got section alignment which has

48
00:03:09,260 --> 00:03:14,750
to do with when you map a section into

49
00:03:12,290 --> 00:03:18,230
memory in the P file and we've got the

50
00:03:14,750 --> 00:03:21,410
whatever was PA line which is segment

51
00:03:18,230 --> 00:03:40,250
into memory in the L file

52
00:03:21,410 --> 00:03:43,580
all right yep they're different in that

53
00:03:40,250 --> 00:03:46,100
the memory should not be smaller on L so

54
00:03:43,580 --> 00:03:48,320
whereas in PE you could have less memory

55
00:03:46,100 --> 00:03:51,800
because you had padded out file sections

56
00:03:48,320 --> 00:03:54,260
right but on L you've always got equal

57
00:03:51,800 --> 00:03:55,790
or more memory to the file size so

58
00:03:54,260 --> 00:03:57,860
you've got file and either gets mapped

59
00:03:55,790 --> 00:03:59,720
into memory and then you you maybe have

60
00:03:57,860 --> 00:04:02,030
some BSS maybe you don't but you can

61
00:03:59,720 --> 00:04:04,460
never have smaller virtual memory than

62
00:04:02,030 --> 00:04:07,160
you have file size I think it won't even

63
00:04:04,460 --> 00:04:08,720
run if you do that but not 100% sure we

64
00:04:07,160 --> 00:04:10,760
could probably do hex editing later and

65
00:04:08,720 --> 00:04:12,410
modify the header and if it sees that

66
00:04:10,760 --> 00:04:14,270
it'll just say no this isn't about you

67
00:04:12,410 --> 00:04:27,590
can't ask me to map that much filed

68
00:04:14,270 --> 00:04:29,240
either into less space file size is

69
00:04:27,590 --> 00:04:30,680
equivalent to size abroad data

70
00:04:29,240 --> 00:04:32,780
yes that was already saying think up

71
00:04:30,680 --> 00:04:36,130
another one while we go to the phones

72
00:04:32,780 --> 00:04:36,130
and we say

73
00:04:36,669 --> 00:04:43,160
so I might have missed this but what I'm

74
00:04:40,460 --> 00:04:46,880
not noticing in the elf are the

75
00:04:43,160 --> 00:04:48,740
date/time stamps yep there's no sort of

76
00:04:46,880 --> 00:04:50,389
notion of these time date stamps which

77
00:04:48,740 --> 00:04:52,520
are built in and telling us anything

78
00:04:50,389 --> 00:04:54,620
about when it was compiled person so you

79
00:04:52,520 --> 00:04:56,750
don't have that right

80
00:04:54,620 --> 00:05:01,520
what's another way that it's similar are

81
00:04:56,750 --> 00:05:09,380
different is there a what about section

82
00:05:01,520 --> 00:05:10,370
names and constants are the same yeah

83
00:05:09,380 --> 00:05:11,810
that's correct

84
00:05:10,370 --> 00:05:14,180
so we haven't seen the sections

85
00:05:11,810 --> 00:05:16,130
exhaustively yet but he picked up on

86
00:05:14,180 --> 00:05:17,569
earlier when we were showing you know

87
00:05:16,130 --> 00:05:19,130
certainly when it showed segments to

88
00:05:17,569 --> 00:05:21,229
section mappings and stuff like that

89
00:05:19,130 --> 00:05:23,060
with that retail special command you saw

90
00:05:21,229 --> 00:05:25,940
that text you saw that the you saw that

91
00:05:23,060 --> 00:05:27,560
already to saw things like that so yep

92
00:05:25,940 --> 00:05:30,199
that's a way that they're similar the

93
00:05:27,560 --> 00:05:33,520
section names are similar names all

94
00:05:30,199 --> 00:05:33,520
right good good answers all


1
00:00:03,660 --> 00:00:12,790
hey we got a 32-bit and 64-bit others

2
00:00:07,990 --> 00:00:16,170
fail alright so starting from the data

3
00:00:12,790 --> 00:00:18,939
directory up in the optional headers

4
00:00:16,170 --> 00:00:21,939
there's going to be this delay important

5
00:00:18,939 --> 00:00:24,070
entry right it's got a nonzero RBA it's

6
00:00:21,939 --> 00:00:25,570
got a nonzero size so it's saying

7
00:00:24,070 --> 00:00:29,259
there's going to be some data structure

8
00:00:25,570 --> 00:00:29,710
that this is pointing to at RBA 85 si si

9
00:00:29,259 --> 00:00:32,290
si

10
00:00:29,710 --> 00:00:35,250
and that's where I can find the delayed

11
00:00:32,290 --> 00:00:46,420
entry you know one per DLL sort of

12
00:00:35,250 --> 00:00:50,140
structure a stipulated looking for the

13
00:00:46,420 --> 00:00:51,760
delayed load descriptors right so we

14
00:00:50,140 --> 00:00:53,949
always say these things point of the

15
00:00:51,760 --> 00:00:56,800
descriptive cable it's one per DLL in

16
00:00:53,949 --> 00:00:59,320
this case actually here on the 64-bit it

17
00:00:56,800 --> 00:01:02,350
only imports from one back on Windows XP

18
00:00:59,320 --> 00:01:04,390
important for multiple so back on XP it

19
00:01:02,350 --> 00:01:08,950
imported from UNIX being the DLL here

20
00:01:04,390 --> 00:01:11,440
it's only 40 from GTA + yellow but this

21
00:01:08,950 --> 00:01:13,990
delay low descriptors is going to be a 1

22
00:01:11,440 --> 00:01:20,200
per DLL array of things where we've got

23
00:01:13,990 --> 00:01:24,250
a name we got Bob IAT Bob on i-80 and

24
00:01:20,200 --> 00:01:25,390
bound int again this is interpretation

25
00:01:24,250 --> 00:01:28,030
you kind of have to look at this

26
00:01:25,390 --> 00:01:31,870
structure and notice this was PE

27
00:01:28,030 --> 00:01:33,550
underscore ID so this is going to be an

28
00:01:31,870 --> 00:01:36,220
RBA where I can find my import address

29
00:01:33,550 --> 00:01:50,770
table this is going to be an RBA where I

30
00:01:36,220 --> 00:01:56,340
can find my information 0 0 0 and then

31
00:01:50,770 --> 00:01:59,440
this is just like well just like this is

32
00:01:56,340 --> 00:02:01,120
the import address table what we said in

33
00:01:59,440 --> 00:02:02,890
delay loads it's not pointing at that

34
00:02:01,120 --> 00:02:05,080
same data structure with hints and names

35
00:02:02,890 --> 00:02:06,100
although they pretend like it is over

36
00:02:05,080 --> 00:02:08,440
here unfortunately

37
00:02:06,100 --> 00:02:10,509
it delayed a little thing these are all

38
00:02:08,440 --> 00:02:12,160
addresses within myself of my stub

39
00:02:10,509 --> 00:02:12,550
coding these are all those things that

40
00:02:12,160 --> 00:02:15,670
we're

41
00:02:12,550 --> 00:02:17,830
that ETA X so if I were to you know go

42
00:02:15,670 --> 00:02:19,420
figure out what file offsets and then

43
00:02:17,830 --> 00:02:22,180
disassemble the smile offsets

44
00:02:19,420 --> 00:02:26,680
I'd see code which moves values of

45
00:02:22,180 --> 00:02:29,500
themselves into VX so these are a bunch

46
00:02:26,680 --> 00:02:31,120
of addresses within my coat of stucco so

47
00:02:29,500 --> 00:02:32,980
when I first load up the my hearing

48
00:02:31,120 --> 00:02:35,440
they're all going to have those exact

49
00:02:32,980 --> 00:02:37,030
mineral addresses as I call those

50
00:02:35,440 --> 00:02:39,780
functions they're going to fill

51
00:02:37,030 --> 00:02:42,400
themselves in with the real address of

52
00:02:39,780 --> 00:02:47,080
GDI chrome Russian stuff like that

53
00:02:42,400 --> 00:02:48,730
that's and just to confirm on the import

54
00:02:47,080 --> 00:02:53,350
name stable that it kind of looks like a

55
00:02:48,730 --> 00:03:01,180
real regular according to the DLL lay

56
00:02:53,350 --> 00:03:04,330
low enable a loading port names table

57
00:03:01,180 --> 00:03:08,080
eight five f zero zero and as a hint of

58
00:03:04,330 --> 00:03:11,170
zero and the name of geometry so it's

59
00:03:08,080 --> 00:03:14,290
not even bothering to try to help so if

60
00:03:11,170 --> 00:03:18,610
I go to eight five two zero zero which

61
00:03:14,290 --> 00:03:21,040
will leave somewhere zero we see zero

62
00:03:18,610 --> 00:03:21,790
zero that's the hint just happens to not

63
00:03:21,040 --> 00:03:27,489
be doing it again

64
00:03:21,790 --> 00:03:31,080
that's the GED IP so this looks like

65
00:03:27,489 --> 00:03:35,709
just a normal import names table

66
00:03:31,080 --> 00:03:37,780
pointers on pink names and here this is

67
00:03:35,709 --> 00:03:43,810
filled in with absolute virtual

68
00:03:37,780 --> 00:03:49,420
addresses of the actual stuff code we

69
00:03:43,810 --> 00:03:51,489
did itself basically so make sure this

70
00:03:49,420 --> 00:03:52,600
is so if I want to come in notionally

71
00:03:51,489 --> 00:03:54,100
you figure out whether I think these are

72
00:03:52,600 --> 00:03:55,180
really absolute virtual addresses or

73
00:03:54,100 --> 00:03:58,239
whether these developed a virtual

74
00:03:55,180 --> 00:03:59,709
addresses I can kind of go look at the

75
00:03:58,239 --> 00:04:01,209
base just and say does it look like

76
00:03:59,709 --> 00:04:04,180
there's someone within base address

77
00:04:01,209 --> 00:04:05,890
image J's plus you know are they less

78
00:04:04,180 --> 00:04:07,840
than image phase plus size of image

79
00:04:05,890 --> 00:04:09,580
right that's the total memory range if

80
00:04:07,840 --> 00:04:13,110
there's somewhere in there their

81
00:04:09,580 --> 00:04:14,950
absolute virtual addresses over there

82
00:04:13,110 --> 00:04:18,180
there's a relative return addresses

83
00:04:14,950 --> 00:04:24,460
outside so go to the optional header

84
00:04:18,180 --> 00:04:25,680
image base which makes one zero size of

85
00:04:24,460 --> 00:04:29,410
image

86
00:04:25,680 --> 00:04:33,910
6:47 question is on day less than one

87
00:04:29,410 --> 00:04:35,710
six one seven zero zero yes

88
00:04:33,910 --> 00:04:39,310
that's less than one six one seven

89
00:04:35,710 --> 00:04:40,990
rivers so it's absolute virtual just

90
00:04:39,310 --> 00:04:43,800
pointing somewhere within the range of

91
00:04:40,990 --> 00:04:46,330
itself is pointing out stuff

92
00:04:43,800 --> 00:04:49,360
all right I'll show you these things

93
00:04:46,330 --> 00:04:54,819
getting loaded dynamically in bugger and

94
00:04:49,360 --> 00:04:55,690
then we will get on with the game West

95
00:04:54,819 --> 00:04:58,210
yes

96
00:04:55,690 --> 00:05:30,789
so in this tool you can actually drill

97
00:04:58,210 --> 00:05:32,710
down to see where that is all right so

98
00:05:30,789 --> 00:05:34,539
I'm gonna kind of try to cheat they're

99
00:05:32,710 --> 00:05:37,960
just gonna look between them so I want

100
00:05:34,539 --> 00:05:39,580
to drill down at my zero one seven no

101
00:05:37,960 --> 00:05:42,880
this is an absolute virtual address I

102
00:05:39,580 --> 00:05:44,560
know the base address is one services is

103
00:05:42,880 --> 00:05:46,240
zero so I know that was too hard if I

104
00:05:44,560 --> 00:05:48,580
subtract up the base address from this

105
00:05:46,240 --> 00:05:54,940
absolute address and get the RBA one

106
00:05:48,580 --> 00:05:58,560
seven ea9 178 nine I'm going to try to

107
00:05:54,940 --> 00:05:58,560
turn it into the file offset

108
00:06:10,190 --> 00:06:22,800
Chuck you later south 179 I'm looking

109
00:06:20,009 --> 00:06:24,930
for that RBA and pretend that are me

110
00:06:22,800 --> 00:06:26,789
into it I'll offset I'm gonna try to

111
00:06:24,930 --> 00:06:28,289
keep you find like that Harvey and the

112
00:06:26,789 --> 00:06:34,310
army had columns and they just flick it

113
00:06:28,289 --> 00:06:34,310
over so I'm literally just up there

114
00:06:56,900 --> 00:07:14,930
107 ea9 okay turn that pile offset okay

115
00:07:10,800 --> 00:07:20,310
I think all right go over here

116
00:07:14,930 --> 00:07:36,120
disassembler from minutes I can never

117
00:07:20,310 --> 00:07:38,100
remember that's not right because I can

118
00:07:36,120 --> 00:07:41,870
see over here that the first flight

119
00:07:38,100 --> 00:07:41,870
should be 5 6

120
00:07:52,940 --> 00:07:59,880
yeah this is the it's just more

121
00:07:57,330 --> 00:08:02,270
complicated in 64 degrees all right this

122
00:07:59,880 --> 00:08:02,270
is 32

123
00:08:06,830 --> 00:08:15,240
yeah basically it's not at all as easy

124
00:08:12,330 --> 00:08:17,070
to interpret as the XP version that I

125
00:08:15,240 --> 00:08:23,940
had on the slide so I'm not anything

126
00:08:17,070 --> 00:08:25,440
about what she do interpret that but I'm

127
00:08:23,940 --> 00:08:26,970
pretty sure if I like duck downs and

128
00:08:25,440 --> 00:08:39,540
this I would see that this is that stop

129
00:08:26,970 --> 00:08:43,500
code which goes and I would think that

130
00:08:39,540 --> 00:08:48,210
we work on the games all right so I want

131
00:08:43,500 --> 00:08:50,340
sure basically the late load imports

132
00:08:48,210 --> 00:08:51,060
getting resolved in real time has some

133
00:08:50,340 --> 00:09:02,910
actions

134
00:08:51,060 --> 00:09:11,810
okay so we want to mspaint open

135
00:09:02,910 --> 00:09:11,810
executables hold on Windows this pain

136
00:09:12,200 --> 00:09:17,550
alright so what we're gonna do right is

137
00:09:15,990 --> 00:09:18,960
we're gonna find this way load in

138
00:09:17,550 --> 00:09:21,240
progress table we're going to find that

139
00:09:18,960 --> 00:09:23,280
Harvey a in the file based on the file

140
00:09:21,240 --> 00:09:25,800
information we're going to look it up in

141
00:09:23,280 --> 00:09:31,200
memory and we're gonna sort of watch it

142
00:09:25,800 --> 00:09:34,980
change so based on this file I must bank

143
00:09:31,200 --> 00:09:38,250
says the delayed load import is at RB 8

144
00:09:34,980 --> 00:09:39,210
8 8 0 0 0 so I'm just going to take the

145
00:09:38,250 --> 00:09:41,690
baby

146
00:09:39,210 --> 00:09:41,690
nursers

147
00:09:58,149 --> 00:10:01,510
that's the same

148
00:10:40,190 --> 00:10:48,300
with me try to control looking at the

149
00:10:42,840 --> 00:11:19,970
central checking the time date set for

150
00:10:48,300 --> 00:11:22,970
85b cat5e cat6 and the same thing with

151
00:11:19,970 --> 00:11:22,970
interpretation

152
00:11:30,540 --> 00:11:34,949
all right so mspaint is running right

153
00:11:32,220 --> 00:11:37,589
now I'm gonna break into it see if

154
00:11:34,949 --> 00:11:40,800
anything's changed okay that a change so

155
00:11:37,589 --> 00:11:42,360
I do believe these are life you know

156
00:11:40,800 --> 00:11:45,420
these are the absolute virtual addresses

157
00:11:42,360 --> 00:11:46,199
that got filled in a trunk club so I'm

158
00:11:45,420 --> 00:11:47,760
not sure why

159
00:11:46,199 --> 00:11:49,350
quite why I'm seeing not seeing the same

160
00:11:47,760 --> 00:11:53,220
thing but I mean you can kind of tell

161
00:11:49,350 --> 00:12:15,899
these ranges for apps or DS or these are

162
00:11:53,220 --> 00:12:17,370
kind of like things that are I'm going

163
00:12:15,899 --> 00:12:18,750
to flip over to mspaint

164
00:12:17,370 --> 00:12:19,829
I'm going to move the mouse around it's

165
00:12:18,750 --> 00:12:21,420
going to cause it to invoke some

166
00:12:19,829 --> 00:12:36,870
functions and then some more of these

167
00:12:21,420 --> 00:12:40,230
entries are basically we gotta kind of

168
00:12:36,870 --> 00:12:41,870
like watching this range keep your eye

169
00:12:40,230 --> 00:12:44,610
open

170
00:12:41,870 --> 00:12:50,250
okay nothing in that range change did

171
00:12:44,610 --> 00:13:02,130
something in any other say what seemed

172
00:12:50,250 --> 00:13:04,139
like some things changed dresses like

173
00:13:02,130 --> 00:13:05,459
came in but I'll refine this while you

174
00:13:04,139 --> 00:13:15,300
guys are working on the thing I'll show

175
00:13:05,459 --> 00:13:16,589
you clearly anyways the point is you

176
00:13:15,300 --> 00:13:18,300
could clearly see at least at the very

177
00:13:16,589 --> 00:13:33,510
beginning like when I started some of

178
00:13:18,300 --> 00:13:34,940
these things got killed in all right the

179
00:13:33,510 --> 00:13:37,319
only important is that you know

180
00:13:34,940 --> 00:13:37,900
typically you see little values that are

181
00:13:37,319 --> 00:13:39,700
pointing with

182
00:13:37,900 --> 00:13:41,110
it's awesome and when they have filled

183
00:13:39,700 --> 00:13:48,160
in you see the absolute virtual

184
00:13:41,110 --> 00:13:55,140
addresses resolve this is a mess payment

185
00:13:48,160 --> 00:13:55,140
system all 32 yes that's 1800 same


1
00:00:03,530 --> 00:00:07,520
alright so now we're going to move into

2
00:00:05,480 --> 00:00:11,179
the thread-local storage thing this is

3
00:00:07,520 --> 00:00:13,610
going to have relevance to basically an

4
00:00:11,179 --> 00:00:15,589
anti debug technique for an anti analyst

5
00:00:13,610 --> 00:00:18,160
technique the malware trick that people

6
00:00:15,589 --> 00:00:20,630
use in order to get code to execute

7
00:00:18,160 --> 00:00:28,010
before you recognize that code is

8
00:00:20,630 --> 00:00:30,830
executing so just talking generally

9
00:00:28,010 --> 00:00:33,350
about threads actually wish I went and

10
00:00:30,830 --> 00:00:36,769
stole in the first of all that slides in

11
00:00:33,350 --> 00:00:39,109
the right spot I think there's like a

12
00:00:36,769 --> 00:00:41,780
slide that was made for Veronica's class

13
00:00:39,109 --> 00:00:44,659
that talked about threads I maybe should

14
00:00:41,780 --> 00:00:48,050
have stolen but the point of threads is

15
00:00:44,659 --> 00:00:49,519
that you know we've been talking about

16
00:00:48,050 --> 00:00:50,629
you know there's this process here and

17
00:00:49,519 --> 00:00:52,100
there's this process there and the

18
00:00:50,629 --> 00:00:53,679
operating system switches back and forth

19
00:00:52,100 --> 00:00:56,600
in terms of who's running right now

20
00:00:53,679 --> 00:00:58,670
threads are when you go even within a

21
00:00:56,600 --> 00:01:00,739
process and you say I actually want to

22
00:00:58,670 --> 00:01:02,149
allow something I want two things to be

23
00:01:00,739 --> 00:01:04,100
executing in the same process memory

24
00:01:02,149 --> 00:01:05,840
space so it's the same memory it's

25
00:01:04,100 --> 00:01:08,899
isolated task managers threads are

26
00:01:05,840 --> 00:01:12,110
isolated from no pads threads but within

27
00:01:08,899 --> 00:01:13,670
this thread space potentially one thing

28
00:01:12,110 --> 00:01:15,380
you know if it didn't have multiple

29
00:01:13,670 --> 00:01:17,299
threads if it was just a single threaded

30
00:01:15,380 --> 00:01:18,950
kind of thing it would maybe try to go

31
00:01:17,299 --> 00:01:20,810
and read a file off of disk and we know

32
00:01:18,950 --> 00:01:22,610
that reading stuff off of disk is

33
00:01:20,810 --> 00:01:23,899
extremely much slower than reading stuff

34
00:01:22,610 --> 00:01:25,820
from memory right you got to go down to

35
00:01:23,899 --> 00:01:27,979
the physical hardware it takes you know

36
00:01:25,820 --> 00:01:30,439
however many milliseconds or micro

37
00:01:27,979 --> 00:01:31,850
seconds to get the data back but for

38
00:01:30,439 --> 00:01:34,250
those microseconds you could have been

39
00:01:31,850 --> 00:01:35,479
done doing other work right and so what

40
00:01:34,250 --> 00:01:37,430
you want to have is you want to have two

41
00:01:35,479 --> 00:01:40,369
threads where one thread goes and tries

42
00:01:37,430 --> 00:01:41,899
to talk to the hard drive and then it's

43
00:01:40,369 --> 00:01:43,369
sitting there waiting and it says okay I

44
00:01:41,899 --> 00:01:45,560
know I'm going to be waiting when i call

45
00:01:43,369 --> 00:01:47,750
this talk to the hard drive so go ahead

46
00:01:45,560 --> 00:01:49,640
and let this other code run and it has

47
00:01:47,750 --> 00:01:51,170
its own context and it'll continue doing

48
00:01:49,640 --> 00:01:53,960
something like updating the screen or

49
00:01:51,170 --> 00:01:56,240
something like that so the point of

50
00:01:53,960 --> 00:01:59,179
multi-threading is that you don't want a

51
00:01:56,240 --> 00:02:00,530
program overall to get stuck talking to

52
00:01:59,179 --> 00:02:02,030
something that's slow for instance or

53
00:02:00,530 --> 00:02:03,499
waiting for packets on the network or

54
00:02:02,030 --> 00:02:04,850
anything like that so you want to have

55
00:02:03,499 --> 00:02:07,009
break it up the program so that the

56
00:02:04,850 --> 00:02:09,110
operating system says I'm not just going

57
00:02:07,009 --> 00:02:11,390
to say notepad can run and then task

58
00:02:09,110 --> 00:02:13,580
manager run I can say no pad thread one

59
00:02:11,390 --> 00:02:15,530
can run and when notepad thread one is

60
00:02:13,580 --> 00:02:17,480
you know says hey I'm stuck I'm waiting

61
00:02:15,530 --> 00:02:19,159
on something it'll yield and it'll say

62
00:02:17,480 --> 00:02:21,860
the operating system says oh no pad

63
00:02:19,159 --> 00:02:23,989
thread one you're done okay no pads read

64
00:02:21,860 --> 00:02:25,610
to you can run now and then finally one

65
00:02:23,989 --> 00:02:27,440
note all of notepads threads run out of

66
00:02:25,610 --> 00:02:29,810
time minute switches over to process

67
00:02:27,440 --> 00:02:32,060
explorer or whatever alright so that's

68
00:02:29,810 --> 00:02:35,540
the long-winded way of saying why we

69
00:02:32,060 --> 00:02:37,340
need threads beyond that Reds

70
00:02:35,540 --> 00:02:41,239
potentially need their own data storage

71
00:02:37,340 --> 00:02:42,709
so they have their own context and you

72
00:02:41,239 --> 00:02:44,269
again you maybe saw this in the I'll

73
00:02:42,709 --> 00:02:46,129
just keep you know plugging her class

74
00:02:44,269 --> 00:02:47,390
and she'll keep plugging my class maybe

75
00:02:46,129 --> 00:02:48,950
in the malware class you saw something

76
00:02:47,390 --> 00:02:51,410
about a function called set thread

77
00:02:48,950 --> 00:02:53,420
context which malware potentially uses

78
00:02:51,410 --> 00:02:55,730
thread context you can think of it like

79
00:02:53,420 --> 00:02:57,560
a bunch of a list of all the the CPU

80
00:02:55,730 --> 00:02:59,629
registers for that particular thread and

81
00:02:57,560 --> 00:03:00,890
the registers include the instruction

82
00:02:59,629 --> 00:03:02,569
pointer so it says this thread is

83
00:03:00,890 --> 00:03:04,099
running right here right now and this

84
00:03:02,569 --> 00:03:05,930
thread is running down here right now so

85
00:03:04,099 --> 00:03:07,280
they've got their own state like what do

86
00:03:05,930 --> 00:03:08,959
they have in their registers where do

87
00:03:07,280 --> 00:03:11,660
they have their stack and that's an

88
00:03:08,959 --> 00:03:13,220
independent thread context so they've

89
00:03:11,660 --> 00:03:14,959
got their own context but they

90
00:03:13,220 --> 00:03:17,390
potentially want their own sort of

91
00:03:14,959 --> 00:03:18,890
global data as well they've got they

92
00:03:17,390 --> 00:03:20,420
want to basically have some global that

93
00:03:18,890 --> 00:03:22,370
they set when they start running but

94
00:03:20,420 --> 00:03:24,230
they want their global to be separate

95
00:03:22,370 --> 00:03:25,549
from the other for its Global's because

96
00:03:24,230 --> 00:03:27,169
when two threads are sharing the same

97
00:03:25,549 --> 00:03:29,090
global they can be smashing each other's

98
00:03:27,169 --> 00:03:31,180
mobile right that's why we have mutual

99
00:03:29,090 --> 00:03:33,200
exclusion you don't want two threads

100
00:03:31,180 --> 00:03:34,400
accessing the same data at different

101
00:03:33,200 --> 00:03:35,810
times and things like that so they can

102
00:03:34,400 --> 00:03:38,150
say i've got a global but i don't want

103
00:03:35,810 --> 00:03:40,099
the other thread to access it this is

104
00:03:38,150 --> 00:03:41,989
what we call thread-local storage so

105
00:03:40,099 --> 00:03:43,609
it's saying this is something local to

106
00:03:41,989 --> 00:03:46,790
me and my thread this is for my

107
00:03:43,609 --> 00:03:48,370
availability in my thread only and this

108
00:03:46,790 --> 00:03:50,209
is something i'm going to use

109
00:03:48,370 --> 00:03:52,370
independent of other threads and i'll

110
00:03:50,209 --> 00:03:54,139
update it and so forth so thread-local

111
00:03:52,370 --> 00:03:57,230
storage as you see it in windows and

112
00:03:54,139 --> 00:03:58,760
linux it has the concept of like here's

113
00:03:57,230 --> 00:04:01,579
some global data for my thread oming

114
00:03:58,760 --> 00:04:04,519
where we differ is that windows has the

115
00:04:01,579 --> 00:04:06,919
notion of thread-local storage callbacks

116
00:04:04,519 --> 00:04:10,730
and a callback means some function that

117
00:04:06,919 --> 00:04:13,340
will get registered to be called so can

118
00:04:10,730 --> 00:04:14,989
I go into this now so from the data

119
00:04:13,340 --> 00:04:18,019
directory as normal we're going to go to

120
00:04:14,989 --> 00:04:19,789
whatever index this is and we're going

121
00:04:18,019 --> 00:04:22,760
to find the pointer to the thread-local

122
00:04:19,789 --> 00:04:25,190
storage information so from there it

123
00:04:22,760 --> 00:04:26,750
points to this TLS directory so if we're

124
00:04:25,190 --> 00:04:29,360
something about a red local storage

125
00:04:26,750 --> 00:04:31,110
directory or TLS directory we're talking

126
00:04:29,360 --> 00:04:35,700
about the thing pointed to by the data

127
00:04:31,110 --> 00:04:37,290
director so within this what we care

128
00:04:35,700 --> 00:04:38,820
about is that you know there's a start

129
00:04:37,290 --> 00:04:40,560
address raw data and address around

130
00:04:38,820 --> 00:04:42,630
eight that's where the storage is going

131
00:04:40,560 --> 00:04:44,550
to be for the thread-local storage but

132
00:04:42,630 --> 00:04:46,320
what we really are interested in is this

133
00:04:44,550 --> 00:04:48,260
address of callbacks because this is

134
00:04:46,320 --> 00:04:50,640
actually going to be an array of

135
00:04:48,260 --> 00:04:53,190
absolute virtual addresses saying here's

136
00:04:50,640 --> 00:04:55,710
a function that I want to be called when

137
00:04:53,190 --> 00:04:57,090
my thread when a new thread is started

138
00:04:55,710 --> 00:04:59,400
for instance so these are functions that

139
00:04:57,090 --> 00:05:01,110
kind of would happen at thread start in

140
00:04:59,400 --> 00:05:04,080
order to initialize data or something

141
00:05:01,110 --> 00:05:06,120
like that all right so this is what I

142
00:05:04,080 --> 00:05:07,500
was just saying key thing is that all of

143
00:05:06,120 --> 00:05:09,060
these in thread-local storage are

144
00:05:07,500 --> 00:05:12,690
absolute virtual addresses they're not

145
00:05:09,060 --> 00:05:14,190
relative virtual addresses and as I just

146
00:05:12,690 --> 00:05:15,570
said these are the start and end of data

147
00:05:14,190 --> 00:05:17,250
we're less concerned about the data

148
00:05:15,570 --> 00:05:22,400
we're more concerned about the address

149
00:05:17,250 --> 00:05:26,010
of callbacks and so if I have it okay

150
00:05:22,400 --> 00:05:28,260
this is just an example in P view so

151
00:05:26,010 --> 00:05:30,300
we're saying in P view you would see

152
00:05:28,260 --> 00:05:31,830
something like this image TLS directory

153
00:05:30,300 --> 00:05:33,600
i'll show you in cff explorer in a

154
00:05:31,830 --> 00:05:35,490
second this is the first order thing

155
00:05:33,600 --> 00:05:37,950
this address of callbacks is an absolute

156
00:05:35,490 --> 00:05:40,340
virtual address so we can probably guess

157
00:05:37,950 --> 00:05:45,060
that the the base of this thing is X 1

158
00:05:40,340 --> 00:05:46,710
million+ r-va of 1 10 18 this is the

159
00:05:45,060 --> 00:05:48,360
absolute virtual address of just an

160
00:05:46,710 --> 00:05:50,220
array where its function pointer

161
00:05:48,360 --> 00:05:53,130
function pointer function pointer null

162
00:05:50,220 --> 00:05:55,320
and then it's done and so the reason

163
00:05:53,130 --> 00:05:57,660
this matters is because these callbacks

164
00:05:55,320 --> 00:05:59,730
turn out to get executed before the

165
00:05:57,660 --> 00:06:01,680
address of entry point so previously I

166
00:05:59,730 --> 00:06:03,720
told you way back in the optional header

167
00:06:01,680 --> 00:06:06,000
information said address of entry point

168
00:06:03,720 --> 00:06:07,170
is the first place code starts set your

169
00:06:06,000 --> 00:06:09,090
break point there if you're trying to

170
00:06:07,170 --> 00:06:12,600
debug something that's where it's going

171
00:06:09,090 --> 00:06:15,030
to start if there's TLS callbacks those

172
00:06:12,600 --> 00:06:17,430
TLS callbacks those array of function

173
00:06:15,030 --> 00:06:19,740
pointers will get invoked each one after

174
00:06:17,430 --> 00:06:21,300
the other before the OS ever gets around

175
00:06:19,740 --> 00:06:23,250
to calling up to that address of entry

176
00:06:21,300 --> 00:06:24,870
point and so what this means is if

177
00:06:23,250 --> 00:06:27,240
you're you know an analyst and you're

178
00:06:24,870 --> 00:06:28,590
debugging something you set you know a

179
00:06:27,240 --> 00:06:29,610
breakpoint on a dress of entry point

180
00:06:28,590 --> 00:06:31,020
let's say you're kind of doing you know

181
00:06:29,610 --> 00:06:34,140
static analysis you're reading through

182
00:06:31,020 --> 00:06:35,370
the assembly code with ida pro and so

183
00:06:34,140 --> 00:06:36,900
you're looking at the address you're

184
00:06:35,370 --> 00:06:38,130
looking at the code at the address of

185
00:06:36,900 --> 00:06:39,870
entry point you're looking at and you're

186
00:06:38,130 --> 00:06:42,750
saying this doesn't do anything how does

187
00:06:39,870 --> 00:06:44,470
this malware actually run right it turns

188
00:06:42,750 --> 00:06:45,970
out they maybe we'll have move

189
00:06:44,470 --> 00:06:49,060
their functionality to these callbacks

190
00:06:45,970 --> 00:06:53,890
and so if you're not if you're sort of

191
00:06:49,060 --> 00:06:55,210
I'd say you know junior analysts will

192
00:06:53,890 --> 00:06:57,060
generally not know about this medium

193
00:06:55,210 --> 00:06:59,800
analyst may or may not know about this

194
00:06:57,060 --> 00:07:00,640
medium experienced analysts if they

195
00:06:59,800 --> 00:07:02,680
don't know about thread-local storage

196
00:07:00,640 --> 00:07:04,540
they won't be able to properly static

197
00:07:02,680 --> 00:07:06,130
analyze this and they certainly won't be

198
00:07:04,540 --> 00:07:09,370
able to dynamically analyze it correctly

199
00:07:06,130 --> 00:07:11,140
because they'll set a breakpoint they'll

200
00:07:09,370 --> 00:07:12,760
run to there and you know the code will

201
00:07:11,140 --> 00:07:14,200
have already run off and talked over the

202
00:07:12,760 --> 00:07:15,730
network and stuff they're like look it

203
00:07:14,200 --> 00:07:17,410
has you know just hit my break point how

204
00:07:15,730 --> 00:07:19,120
did it talk over the network if they've

205
00:07:17,410 --> 00:07:22,060
got you know dynamic analysis going on

206
00:07:19,120 --> 00:07:23,770
in the background so pls call backs is

207
00:07:22,060 --> 00:07:25,060
very important to be aware of if you're

208
00:07:23,770 --> 00:07:28,840
going to be doing any sort of malware

209
00:07:25,060 --> 00:07:30,400
analysis I also interesting is that you

210
00:07:28,840 --> 00:07:31,900
know so I put a bunch of references that

211
00:07:30,400 --> 00:07:33,310
you should go read more about this

212
00:07:31,900 --> 00:07:35,500
because there's some crazy things that

213
00:07:33,310 --> 00:07:38,440
can happen with this amongst the crazy

214
00:07:35,500 --> 00:07:40,360
things is that if you let's say you have

215
00:07:38,440 --> 00:07:42,580
a TLS callback you go here you look at

216
00:07:40,360 --> 00:07:44,500
the apps address of the at room I mean

217
00:07:42,580 --> 00:07:46,720
let me go to some data first before I go

218
00:07:44,500 --> 00:07:48,700
into that all right so here's what it

219
00:07:46,720 --> 00:07:51,250
looks like in P view show you what it

220
00:07:48,700 --> 00:07:54,550
looks like in cff Explorer went made

221
00:07:51,250 --> 00:07:55,870
separate files or TLS callbacks actually

222
00:07:54,550 --> 00:07:59,169
but I don't think it's going to have any

223
00:07:55,870 --> 00:08:02,500
pls call backs by default I add them in

224
00:07:59,169 --> 00:08:10,180
the code yeah Oh TLS callbacks I gotta

225
00:08:02,500 --> 00:08:12,280
start an example here we're all right

226
00:08:10,180 --> 00:08:14,229
question how many pls call backs does

227
00:08:12,280 --> 00:08:16,930
this binary contain all right I'm going

228
00:08:14,229 --> 00:08:22,419
to go open that in cff Explorer question

229
00:08:16,930 --> 00:08:24,490
eight round eight questions 0xff

230
00:08:22,419 --> 00:08:27,940
explorer this gives you the same sort of

231
00:08:24,490 --> 00:08:29,290
first level cut of here's the you know

232
00:08:27,940 --> 00:08:30,460
things we care about start and then

233
00:08:29,290 --> 00:08:32,289
address well you can see in this

234
00:08:30,460 --> 00:08:34,690
particular thing start and a dresser

235
00:08:32,289 --> 00:08:36,339
zero so that need not necessarily be

236
00:08:34,690 --> 00:08:38,110
there if it's sort of legitimate use

237
00:08:36,339 --> 00:08:39,550
it's going to be using TLS callbacks for

238
00:08:38,110 --> 00:08:41,890
actual global data and stuff like that

239
00:08:39,550 --> 00:08:44,770
but now we're can just say you know jump

240
00:08:41,890 --> 00:08:47,110
straight to the I want callbacks so

241
00:08:44,770 --> 00:08:49,270
question is how many callbacks are in

242
00:08:47,110 --> 00:08:51,670
this thing well you've got an absolute

243
00:08:49,270 --> 00:08:53,800
virtual address so now we're going to

244
00:08:51,670 --> 00:08:56,709
want to convert this basically to a file

245
00:08:53,800 --> 00:08:58,329
offset and we can do that convert a ba

246
00:08:56,709 --> 00:09:01,449
back to virtual address and virtual

247
00:08:58,329 --> 00:09:03,970
address back to file offset i'm going to

248
00:09:01,449 --> 00:09:06,369
let me hope that it's 32 bit yeah 32 bit

249
00:09:03,970 --> 00:09:09,759
so it's easier to do that cff explore

250
00:09:06,369 --> 00:09:12,160
but our sorry peeve you so i'm going to

251
00:09:09,759 --> 00:09:14,889
switch over that but the key point is

252
00:09:12,160 --> 00:09:16,449
that just like PE view this will only

253
00:09:14,889 --> 00:09:18,129
give you this table it won't tell you

254
00:09:16,449 --> 00:09:20,079
here's the array it won't show you

255
00:09:18,129 --> 00:09:21,549
here's the array of callbacks you've got

256
00:09:20,079 --> 00:09:23,739
to go find that yourself this is another

257
00:09:21,549 --> 00:09:27,160
one of those the tools doesn't tell me

258
00:09:23,739 --> 00:09:30,129
so i gotta go find it myself alright so

259
00:09:27,160 --> 00:09:31,839
in p view rather easier way to look at

260
00:09:30,129 --> 00:09:34,739
this just because it's easier to convert

261
00:09:31,839 --> 00:09:39,489
relative addresses to virtual addresses

262
00:09:34,739 --> 00:09:41,739
all right so do have a nice little pls

263
00:09:39,489 --> 00:09:44,730
section because I'm wanted to make easy

264
00:09:41,739 --> 00:09:47,230
code so here's the stuff it says the

265
00:09:44,730 --> 00:09:51,249
absolute virtual address where the array

266
00:09:47,230 --> 00:09:53,350
of callbacks is is 40 50 18 well

267
00:09:51,249 --> 00:09:56,499
conveniently enough I can see that I'm

268
00:09:53,350 --> 00:09:58,360
right up about the 4050 14 right here

269
00:09:56,499 --> 00:09:59,889
with my stuff so probably that callback

270
00:09:58,360 --> 00:10:01,749
array is in the day and a right after

271
00:09:59,889 --> 00:10:05,199
here right the next thing that would be

272
00:10:01,749 --> 00:10:11,259
up is 40 50 14 so if i go back to this

273
00:10:05,199 --> 00:10:13,899
raw view of things 40 50 10 and then

274
00:10:11,259 --> 00:10:15,489
this would be 18 right here and actually

275
00:10:13,899 --> 00:10:18,939
here's a nice thing that i haven't

276
00:10:15,489 --> 00:10:22,179
showed you yet you can actually change

277
00:10:18,939 --> 00:10:23,619
it to do d words instead of lights at a

278
00:10:22,179 --> 00:10:25,360
time that way you can get around some

279
00:10:23,619 --> 00:10:27,790
little endian asur issues if you're

280
00:10:25,360 --> 00:10:30,339
having those issues so under view and

281
00:10:27,790 --> 00:10:35,529
raw data and change between bites words

282
00:10:30,339 --> 00:10:38,699
and you words say again oh yeah you're

283
00:10:35,529 --> 00:10:41,980
right and then it's up here as well yeah

284
00:10:38,699 --> 00:10:44,290
so 40 50 18 is where that array of

285
00:10:41,980 --> 00:10:49,179
callbacks is so I'm going to go 40 50 10

286
00:10:44,290 --> 00:10:50,619
4 250 1440 5018 alright so I've got what

287
00:10:49,179 --> 00:10:52,689
looks like an absolute virtual address

288
00:10:50,619 --> 00:10:54,699
of a callback absolute virtual address

289
00:10:52,689 --> 00:10:57,249
of the callback same one then I got a

290
00:10:54,699 --> 00:11:02,319
null terminator so how many callbacks

291
00:10:57,249 --> 00:11:03,699
are in this binary to yes all right and

292
00:11:02,319 --> 00:11:05,709
so this is how I have to actually figure

293
00:11:03,699 --> 00:11:08,319
that out I have to go find wherever it

294
00:11:05,709 --> 00:11:10,449
was looking for wherever it says in the

295
00:11:08,319 --> 00:11:11,769
headers you know here's the absolute

296
00:11:10,449 --> 00:11:14,619
virtual address of that tape

297
00:11:11,769 --> 00:11:15,970
I got to go find that table and see how

298
00:11:14,619 --> 00:11:18,429
many entries there are before the no

299
00:11:15,970 --> 00:11:22,269
entry all right so the thing I wanted to

300
00:11:18,429 --> 00:11:24,129
tell you that's crazy is you can have

301
00:11:22,269 --> 00:11:26,230
one entry here for instance and then

302
00:11:24,129 --> 00:11:28,449
that one entry can go ahead and like

303
00:11:26,230 --> 00:11:30,879
hack this data in memory so that it adds

304
00:11:28,449 --> 00:11:32,829
a second entry and then when it's done

305
00:11:30,879 --> 00:11:34,269
that next entry will be added so like if

306
00:11:32,829 --> 00:11:36,309
you're statically looking this file and

307
00:11:34,269 --> 00:11:38,170
saying yeah that's got to call backs

308
00:11:36,309 --> 00:11:39,999
right one of those callbacks could get a

309
00:11:38,170 --> 00:11:41,319
third call back if you're not actually

310
00:11:39,999 --> 00:11:43,959
looking at it with the debugger at

311
00:11:41,319 --> 00:11:45,369
runtime you won't realize that there was

312
00:11:43,959 --> 00:11:48,069
some more code over here that got

313
00:11:45,369 --> 00:11:50,350
executed so even just the static view of

314
00:11:48,069 --> 00:11:51,939
things is not the ground truth in terms

315
00:11:50,350 --> 00:11:54,160
of how many callbacks can potentially

316
00:11:51,939 --> 00:11:57,899
get invoked and where all the code will

317
00:11:54,160 --> 00:11:59,679
go the nice thing is Ida pro the comment

318
00:11:57,899 --> 00:12:01,569
reverse-engineer tool for static

319
00:11:59,679 --> 00:12:03,489
analysis does know about TLS callbacks

320
00:12:01,569 --> 00:12:05,499
you can set it to break on TLS call back

321
00:12:03,489 --> 00:12:06,970
instead of entry point so it will be

322
00:12:05,499 --> 00:12:08,949
able to catch these and it should then

323
00:12:06,970 --> 00:12:12,009
catch as you walk through if you're

324
00:12:08,949 --> 00:12:13,779
using Ida's debugger what at the TLS

325
00:12:12,009 --> 00:12:16,059
callbacks well we're going to say about

326
00:12:13,779 --> 00:12:17,439
it pretty much for your randomized game

327
00:12:16,059 --> 00:12:19,089
things it's going to be pretty simple

328
00:12:17,439 --> 00:12:22,509
just because it was to make it easy on

329
00:12:19,089 --> 00:12:24,459
myself i made a new section I put the

330
00:12:22,509 --> 00:12:26,259
header information at the beginning and

331
00:12:24,459 --> 00:12:28,149
then I attacked the array mediately

332
00:12:26,259 --> 00:12:30,069
after it I could randomized this but

333
00:12:28,149 --> 00:12:31,569
padding between them I do need to

334
00:12:30,069 --> 00:12:33,459
randomize these callback addresses

335
00:12:31,569 --> 00:12:35,290
because you can see okay this is the

336
00:12:33,459 --> 00:12:37,299
absolute virtual address I'm going to

337
00:12:35,290 --> 00:12:40,990
guess that this relative address 1000

338
00:12:37,299 --> 00:12:43,899
and so looking at the export address

339
00:12:40,990 --> 00:12:45,249
table I've got a function at 1000 at

340
00:12:43,899 --> 00:12:47,679
1000 playing so I'm going to later on

341
00:12:45,249 --> 00:12:50,049
randomized which callbacks it's using

342
00:12:47,679 --> 00:12:52,779
this is just because this is about

343
00:12:50,049 --> 00:12:57,220
basically after round six that's about

344
00:12:52,779 --> 00:13:00,990
where stuff is not as good as it should

345
00:12:57,220 --> 00:13:04,660
be it so it's going to get sketchy here

346
00:13:00,990 --> 00:13:08,610
alright any questions about TLS

347
00:13:04,660 --> 00:13:11,199
callbacks can like that awesome array

348
00:13:08,610 --> 00:13:15,429
only thing to know about them so going

349
00:13:11,199 --> 00:13:17,249
back just quick debug information right

350
00:13:15,429 --> 00:13:19,779
what do we care about debug information

351
00:13:17,249 --> 00:13:21,040
well I think the bug information we only

352
00:13:19,779 --> 00:13:22,600
think we care about is that there's

353
00:13:21,040 --> 00:13:24,490
going to be an address of rotted or a

354
00:13:22,600 --> 00:13:25,470
pointer to raw data which will get us a

355
00:13:24,490 --> 00:13:27,650
nice string so I

356
00:13:25,470 --> 00:13:30,630
think I'm not actually sure whether I I

357
00:13:27,650 --> 00:13:32,700
yeah maybe I did we see what else is

358
00:13:30,630 --> 00:13:35,730
next because I don't think I think

359
00:13:32,700 --> 00:13:39,590
resources is next and we don't quite

360
00:13:35,730 --> 00:13:43,740
have time to cover this beforehand so

361
00:13:39,590 --> 00:13:47,310
let's say take 10 minutes to go ahead

362
00:13:43,740 --> 00:13:48,810
and play through the game or just go do

363
00:13:47,310 --> 00:13:50,220
lunch and come back 10 minutes and play

364
00:13:48,810 --> 00:13:54,810
for the game but we're going to start

365
00:13:50,220 --> 00:13:59,160
back again at 1250 not one o'clock as we

366
00:13:54,810 --> 00:14:00,810
got raw material to get through so ask

367
00:13:59,160 --> 00:14:05,210
any questions or play through the game

368
00:14:00,810 --> 00:14:09,600
you do round seven and eight so seven is

369
00:14:05,210 --> 00:14:11,040
debug information and relocation

370
00:14:09,600 --> 00:14:15,830
information so seven is the bug and

371
00:14:11,040 --> 00:14:15,830
reload and eight is only TLS


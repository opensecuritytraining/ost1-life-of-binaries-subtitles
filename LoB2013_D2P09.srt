1
00:00:04,590 --> 00:00:10,480
there is one little corner case for

2
00:00:08,020 --> 00:00:13,690
exports that is the reason that we have

3
00:00:10,480 --> 00:00:16,660
more complicated bound imports before so

4
00:00:13,690 --> 00:00:18,220
normally with these normal imports with

5
00:00:16,660 --> 00:00:19,900
these normal exports rather you're

6
00:00:18,220 --> 00:00:21,730
giving a big list of all the strings of

7
00:00:19,900 --> 00:00:23,470
the names of the stuff you export and

8
00:00:21,730 --> 00:00:26,980
you're giving RBA's these are gays are

9
00:00:23,470 --> 00:00:30,100
within your own module space okay turns

10
00:00:26,980 --> 00:00:32,050
out when you have forwarded exports you

11
00:00:30,100 --> 00:00:35,320
can basically give an RBA that's not

12
00:00:32,050 --> 00:00:37,060
within your own module space well it's

13
00:00:35,320 --> 00:00:40,180
going to be within your own module space

14
00:00:37,060 --> 00:00:43,450
but it's going to be outside of the

15
00:00:40,180 --> 00:00:45,130
range of the export information and so

16
00:00:43,450 --> 00:00:47,710
it's going to basically point you have

17
00:00:45,130 --> 00:00:49,239
an RBA here which is going to instead of

18
00:00:47,710 --> 00:00:50,829
pointing out a function it's going to

19
00:00:49,239 --> 00:00:53,350
point out a string and that string is

20
00:00:50,829 --> 00:00:54,760
going to say actually this function is

21
00:00:53,350 --> 00:00:57,340
implemented in some other deal out

22
00:00:54,760 --> 00:01:00,969
they'll say like NT dll dot that

23
00:00:57,340 --> 00:01:04,059
function so the case where it's pointing

24
00:01:00,969 --> 00:01:06,579
is a little complicated so i'll see if i

25
00:01:04,059 --> 00:01:08,789
can all right yeah i think the best way

26
00:01:06,579 --> 00:01:11,789
is just to show an example right here

27
00:01:08,789 --> 00:01:11,789
okay

28
00:01:17,380 --> 00:01:20,380
yes

29
00:01:21,499 --> 00:01:26,999
so this is sort of the trick that

30
00:01:24,450 --> 00:01:28,409
happens with forwarded exports I'm going

31
00:01:26,999 --> 00:01:45,509
to I'm going to go to the board on this

32
00:01:28,409 --> 00:01:50,659
one and think I have to I'll throughout

33
00:01:45,509 --> 00:01:54,509
the process we've seen Dallas header and

34
00:01:50,659 --> 00:01:58,079
T header which has files header and has

35
00:01:54,509 --> 00:02:00,450
an optional header optional header lives

36
00:01:58,079 --> 00:02:03,299
data directory we've got the data

37
00:02:00,450 --> 00:02:10,369
directory and data directory entry zero

38
00:02:03,299 --> 00:02:10,369
here points add some exports information

39
00:02:12,440 --> 00:02:16,170
in directory 0 points and exports

40
00:02:14,819 --> 00:02:18,090
information that starts with that

41
00:02:16,170 --> 00:02:21,290
structure that has you know the few

42
00:02:18,090 --> 00:02:24,870
fields and then it's got the array of

43
00:02:21,290 --> 00:02:30,060
export address table export address

44
00:02:24,870 --> 00:02:32,190
table and then we've got the export

45
00:02:30,060 --> 00:02:35,010
names table we've got the name ordinals

46
00:02:32,190 --> 00:02:37,620
table doesn't matter really what did it

47
00:02:35,010 --> 00:02:39,030
when is all those lists and arrays and

48
00:02:37,620 --> 00:02:42,030
stuff that I was just showing you right

49
00:02:39,030 --> 00:02:43,470
and then finally got like strings all

50
00:02:42,030 --> 00:02:46,290
that stuff that was on that previous

51
00:02:43,470 --> 00:02:49,250
thing all of that is actually specified

52
00:02:46,290 --> 00:02:53,910
by the data directory points to here and

53
00:02:49,250 --> 00:03:01,739
the data directory size data directory

54
00:02:53,910 --> 00:03:04,019
of the directory of 0 dot size is

55
00:03:01,739 --> 00:03:06,630
specifying like here's all of my export

56
00:03:04,019 --> 00:03:08,519
information so whereas previously the

57
00:03:06,630 --> 00:03:09,750
you know sighs and these data directors

58
00:03:08,519 --> 00:03:12,419
would just kind of point out that first

59
00:03:09,750 --> 00:03:14,430
level structure here the data directory

60
00:03:12,419 --> 00:03:17,310
size says I cover all of that

61
00:03:14,430 --> 00:03:19,200
information and what's interesting now

62
00:03:17,310 --> 00:03:21,959
is that in the export address table

63
00:03:19,200 --> 00:03:24,510
whereas normally these export address

64
00:03:21,959 --> 00:03:27,480
table entries are going to you know

65
00:03:24,510 --> 00:03:29,759
point somewhere into your text section

66
00:03:27,480 --> 00:03:31,420
they're going to be pointing our VA's at

67
00:03:29,759 --> 00:03:33,130
the functions where is princess

68
00:03:31,420 --> 00:03:34,810
is you know Malik from things like that

69
00:03:33,130 --> 00:03:38,200
they're pointing somewhere in the code

70
00:03:34,810 --> 00:03:41,319
section if you have an export address

71
00:03:38,200 --> 00:03:44,890
table entry instead points somewhere

72
00:03:41,319 --> 00:03:46,569
within this exports base plus size if

73
00:03:44,890 --> 00:03:49,569
it's somewhere in that pointing at one

74
00:03:46,569 --> 00:03:52,989
string that string will say actually

75
00:03:49,569 --> 00:03:55,209
this export index you know five is over

76
00:03:52,989 --> 00:03:57,459
in some other dll and then it will

77
00:03:55,209 --> 00:04:00,730
specify the dll name right there in the

78
00:03:57,459 --> 00:04:02,830
strings table and so that's called a

79
00:04:00,730 --> 00:04:04,540
forwarded export you're saying I would

80
00:04:02,830 --> 00:04:06,760
like to present you with you know this

81
00:04:04,540 --> 00:04:08,650
name of this function that I export and

82
00:04:06,760 --> 00:04:10,569
then you know it's a bait and switch and

83
00:04:08,650 --> 00:04:14,290
you say oh actually some other person in

84
00:04:10,569 --> 00:04:16,150
tramites that I'm just pretending so

85
00:04:14,290 --> 00:04:18,280
here's an example and this is from

86
00:04:16,150 --> 00:04:20,169
kernel32 and this is the reason why when

87
00:04:18,280 --> 00:04:22,479
we were looking at bound imports you

88
00:04:20,169 --> 00:04:25,389
always thought kernel32 had a number of

89
00:04:22,479 --> 00:04:27,490
forward or refs set to one and it said

90
00:04:25,389 --> 00:04:31,570
NT dll as that next order rough

91
00:04:27,490 --> 00:04:33,880
structure it's because here's the here's

92
00:04:31,570 --> 00:04:36,280
the export address table of kernel32

93
00:04:33,880 --> 00:04:38,590
these are the values that are these are

94
00:04:36,280 --> 00:04:42,340
the RBA's pointing somewhere in kernel32

95
00:04:38,590 --> 00:04:43,930
text section but then down here so you

96
00:04:42,340 --> 00:04:45,880
can see that most of these are VA's are

97
00:04:43,930 --> 00:04:47,770
you know at least well it's not a good

98
00:04:45,880 --> 00:04:51,850
example cuz they're a something or

99
00:04:47,770 --> 00:04:56,620
bigger butt down here we've got this

100
00:04:51,850 --> 00:04:58,690
entry where the RBA is 90 100 so over

101
00:04:56,620 --> 00:05:00,789
here on this parcel table to the side p

102
00:04:58,690 --> 00:05:02,800
view says okay export names this is all

103
00:05:00,789 --> 00:05:05,620
the strings that are associated with

104
00:05:02,800 --> 00:05:09,310
that name stable and so at nine zero one

105
00:05:05,620 --> 00:05:12,940
one we've got this string that says NT

106
00:05:09,310 --> 00:05:14,860
dll dot RTL advective exception handler

107
00:05:12,940 --> 00:05:16,479
and soapy view is trying to just pull

108
00:05:14,860 --> 00:05:19,150
that out for you and give it to you nice

109
00:05:16,479 --> 00:05:20,860
right here it says look I can tell you

110
00:05:19,150 --> 00:05:24,760
that the RV a given right here is

111
00:05:20,860 --> 00:05:26,380
somewhere within that size given in the

112
00:05:24,760 --> 00:05:27,940
data directory we're still pointing

113
00:05:26,380 --> 00:05:29,370
within my exports information you're not

114
00:05:27,940 --> 00:05:31,419
pointing out to some code somewhere

115
00:05:29,370 --> 00:05:33,190
you're pointing within my exports

116
00:05:31,419 --> 00:05:36,400
information here's the string that you

117
00:05:33,190 --> 00:05:39,280
point at the string is empty dll rtl

118
00:05:36,400 --> 00:05:42,250
advected exception in what happens when

119
00:05:39,280 --> 00:05:45,129
the OS loader comes upon this so you're

120
00:05:42,250 --> 00:05:49,419
loading up now a HelloWorld dot exe

121
00:05:45,129 --> 00:05:50,889
imports from kernel32 if you happen to

122
00:05:49,419 --> 00:05:53,080
import the function add vectored

123
00:05:50,889 --> 00:05:55,509
exception either from kernel32 in your

124
00:05:53,080 --> 00:05:58,269
hello world own world plus a vector

125
00:05:55,509 --> 00:06:00,189
exception handler OS loader comes along

126
00:05:58,269 --> 00:06:03,399
and it's trying to resolve your imports

127
00:06:00,189 --> 00:06:05,019
vs colonel 30 Q's exports and it comes

128
00:06:03,399 --> 00:06:06,610
out and it finds this ad vector

129
00:06:05,019 --> 00:06:08,259
exception handler you know went to the

130
00:06:06,610 --> 00:06:09,759
list of names first and i went to the

131
00:06:08,259 --> 00:06:11,889
name ordinal then it got finally to the

132
00:06:09,759 --> 00:06:14,019
export address table and then what it

133
00:06:11,889 --> 00:06:16,089
finds when i get there is oh well let me

134
00:06:14,019 --> 00:06:18,159
do one last check is this Harvey a

135
00:06:16,089 --> 00:06:19,659
pointing with in your code somewhere or

136
00:06:18,159 --> 00:06:21,789
is this still plenty within your export

137
00:06:19,659 --> 00:06:24,369
information turns I disappointing within

138
00:06:21,789 --> 00:06:26,830
your export information so now I need to

139
00:06:24,369 --> 00:06:29,289
parse this string NT dll okay well

140
00:06:26,830 --> 00:06:31,449
that's MP dll dll it's a separate deal

141
00:06:29,289 --> 00:06:35,649
out now I need to go import that thing

142
00:06:31,449 --> 00:06:36,909
and and the thing is I I'm not one

143
00:06:35,649 --> 00:06:38,860
hundred percent sure on this actually

144
00:06:36,909 --> 00:06:42,699
but I believe they won't actually found

145
00:06:38,860 --> 00:06:45,099
on my testing I'm 102 center of this it

146
00:06:42,699 --> 00:06:49,089
will then have to go load up the dll mt

147
00:06:45,099 --> 00:06:50,319
dll dll and so whereas normally your

148
00:06:49,089 --> 00:06:52,329
hello world would just get you know

149
00:06:50,319 --> 00:06:54,939
kernel32 sitting in its memory space

150
00:06:52,329 --> 00:06:57,189
it'll now get kernel32 and it will get

151
00:06:54,939 --> 00:06:59,349
NT dll and your import address table

152
00:06:57,189 --> 00:07:01,389
entry for that particular function will

153
00:06:59,349 --> 00:07:03,039
point down at NPD ll because that's the

154
00:07:01,389 --> 00:07:07,569
guy that actually implements the code

155
00:07:03,039 --> 00:07:09,279
for antique or RTL advected exception

156
00:07:07,569 --> 00:07:12,729
ever which is the behind-the-scenes the

157
00:07:09,279 --> 00:07:14,769
real function which of devices so this

158
00:07:12,729 --> 00:07:16,599
is actually important it's like i said

159
00:07:14,769 --> 00:07:18,309
it's a weird corner case you see it in

160
00:07:16,599 --> 00:07:21,819
kernel32 you don't see it in a lot of

161
00:07:18,309 --> 00:07:26,439
normal applications but where you did

162
00:07:21,819 --> 00:07:28,509
see it was Stuxnet so um Stuxnet had

163
00:07:26,439 --> 00:07:31,029
this Trojan dll that they were trying to

164
00:07:28,509 --> 00:07:34,059
place to basically again kind of met in

165
00:07:31,029 --> 00:07:37,360
the middle the functioning of this step7

166
00:07:34,059 --> 00:07:38,740
software so step 7 was this I think of

167
00:07:37,360 --> 00:07:41,589
it just sort of like a development

168
00:07:38,740 --> 00:07:43,179
environment for for plc code so think of

169
00:07:41,589 --> 00:07:45,699
it like you know Visual Studio you write

170
00:07:43,179 --> 00:07:48,279
up some see cody gets compiled down to

171
00:07:45,699 --> 00:07:51,309
binary code and then you go off and put

172
00:07:48,279 --> 00:07:53,649
that fire somewhere in at once step 7

173
00:07:51,309 --> 00:07:57,680
you're going to reconfiguring PLC's

174
00:07:53,649 --> 00:08:01,610
programmable logic controllers wait

175
00:07:57,680 --> 00:08:03,650
this thing's PLC everyone says CCL but

176
00:08:01,610 --> 00:08:08,240
anyways this is if it's a perfect that's

177
00:08:03,650 --> 00:08:10,520
about that's semantics problem education

178
00:08:08,240 --> 00:08:13,190
in the notes anyways you're compiling

179
00:08:10,520 --> 00:08:15,470
down some plc a code that basically is

180
00:08:13,190 --> 00:08:17,240
going to tell some hardware how it

181
00:08:15,470 --> 00:08:20,330
should actually implement its logic and

182
00:08:17,240 --> 00:08:22,009
so step 7 think of it thinking think of

183
00:08:20,330 --> 00:08:23,449
it like a you know visual studio

184
00:08:22,009 --> 00:08:24,889
environment visual studio is still

185
00:08:23,449 --> 00:08:26,539
providing you all these graphics and

186
00:08:24,889 --> 00:08:28,340
stuff like that so visual studio depends

187
00:08:26,539 --> 00:08:31,789
on a bunch of other dll's to give you

188
00:08:28,340 --> 00:08:35,959
this user interface and so in this case

189
00:08:31,789 --> 00:08:37,580
there's this other dls 70 TV XD XD ll

190
00:08:35,959 --> 00:08:40,279
this is the thing which actually

191
00:08:37,580 --> 00:08:43,550
implements the code to go and talk to

192
00:08:40,279 --> 00:08:45,560
plcs to write data down to them and say

193
00:08:43,550 --> 00:08:47,870
like q plc here's your new no

194
00:08:45,560 --> 00:08:50,510
instruction and it reads back code from

195
00:08:47,870 --> 00:08:51,500
the PLC's so the attackers found that

196
00:08:50,510 --> 00:08:52,640
you know this is the important thing

197
00:08:51,500 --> 00:08:53,899
this is the thing they want to matter

198
00:08:52,640 --> 00:08:56,149
the middle when they want to like hide

199
00:08:53,899 --> 00:08:57,770
stuff they want to get in here so that

200
00:08:56,149 --> 00:09:00,589
when you read stuff back from the

201
00:08:57,770 --> 00:09:03,260
infected plc they give you a clean copy

202
00:09:00,589 --> 00:09:05,150
back right and so this is the ideal man

203
00:09:03,260 --> 00:09:09,170
in the middle case so what they did was

204
00:09:05,150 --> 00:09:14,930
they took the original dll renamed it to

205
00:09:09,170 --> 00:09:18,410
sx dll instead of DX dll and they made a

206
00:09:14,930 --> 00:09:21,110
new dll which contained their own Trojan

207
00:09:18,410 --> 00:09:24,020
codes that when you when you when a step

208
00:09:21,110 --> 00:09:25,310
7 code called s7 block read for instance

209
00:09:24,020 --> 00:09:29,990
so it's trying to read a block out of

210
00:09:25,310 --> 00:09:32,360
the PLC when the original code imported

211
00:09:29,990 --> 00:09:34,670
this dll see so the the import address

212
00:09:32,360 --> 00:09:37,100
table from this step 7 binary is going

213
00:09:34,670 --> 00:09:40,550
to look for the you know import from the

214
00:09:37,100 --> 00:09:42,410
Beinecke from the dll s 70 TV XD X right

215
00:09:40,550 --> 00:09:45,709
that's going to be in the import

216
00:09:42,410 --> 00:09:47,240
directory of step 7 they made a Trojan

217
00:09:45,709 --> 00:09:49,130
dll and they moved the original off to

218
00:09:47,240 --> 00:09:51,950
the side so when the OS lawyer is

219
00:09:49,130 --> 00:09:53,839
looking for the V XD X file it loads of

220
00:09:51,950 --> 00:09:55,790
their file instead and what they did

221
00:09:53,839 --> 00:09:57,950
then is they did an analysis of this

222
00:09:55,790 --> 00:09:59,209
original file and they said here's the

223
00:09:57,950 --> 00:10:00,829
functions which I need two men in the

224
00:09:59,209 --> 00:10:02,300
middle and then here's a bunch of

225
00:10:00,829 --> 00:10:03,770
functions that I don't care about and

226
00:10:02,300 --> 00:10:06,170
I'll just you know let them do their

227
00:10:03,770 --> 00:10:07,970
normal thing the ones that they wanted

228
00:10:06,170 --> 00:10:08,450
to Matta middle they implemented that in

229
00:10:07,970 --> 00:10:10,460
there

230
00:10:08,450 --> 00:10:12,650
dll for the ones they didn't care about

231
00:10:10,460 --> 00:10:16,190
they made their Trojan do values

232
00:10:12,650 --> 00:10:18,620
forwarded exports to forward down to the

233
00:10:16,190 --> 00:10:20,810
original dll so basically their Trojan

234
00:10:18,620 --> 00:10:25,520
dll still gave a big list of like here's

235
00:10:20,810 --> 00:10:27,920
all but whatever it was 107 or 109 so

236
00:10:25,520 --> 00:10:30,860
this original thing had 109 exports

237
00:10:27,920 --> 00:10:33,170
that's here's 109 functions on you the

238
00:10:30,860 --> 00:10:35,360
attackers analyzed it and they saw 93 of

239
00:10:33,170 --> 00:10:38,420
those they don't care about but for the

240
00:10:35,360 --> 00:10:40,060
other 13 of those that they do care

241
00:10:38,420 --> 00:10:42,650
about those 16 that they do care about

242
00:10:40,060 --> 00:10:44,240
they said all right I'm going to

243
00:10:42,650 --> 00:10:45,770
implement those within myself and for

244
00:10:44,240 --> 00:10:49,070
the other 93 I'm just going to do a

245
00:10:45,770 --> 00:10:51,710
forward and export to the original file

246
00:10:49,070 --> 00:10:53,120
which I've copied and renamed and and

247
00:10:51,710 --> 00:10:54,440
then it'll just call the original code

248
00:10:53,120 --> 00:10:56,630
and I don't need to intercept that in

249
00:10:54,440 --> 00:10:59,350
any way and so this is sort of an

250
00:10:56,630 --> 00:11:03,800
interesting use of forwarded exports and

251
00:10:59,350 --> 00:11:05,450
yeah yes so when they would scoop those

252
00:11:03,800 --> 00:11:08,000
reads with it they're just shoot back

253
00:11:05,450 --> 00:11:10,400
like default value like a good running

254
00:11:08,000 --> 00:11:13,400
condition exact I've a bit yep exactly

255
00:11:10,400 --> 00:11:15,230
they just send back clean copies of the

256
00:11:13,400 --> 00:11:16,700
of the you know blocks of coats if

257
00:11:15,230 --> 00:11:18,260
you're trying to read into configure the

258
00:11:16,700 --> 00:11:20,900
the actual code that's going to be

259
00:11:18,260 --> 00:11:22,430
executed on that plc you write it in

260
00:11:20,900 --> 00:11:23,870
there and then it goes in at execute it

261
00:11:22,430 --> 00:11:25,300
you can read it back and see like it's

262
00:11:23,870 --> 00:11:27,440
my thing infected why is this

263
00:11:25,300 --> 00:11:29,900
misbehaving right let's going to read

264
00:11:27,440 --> 00:11:31,400
and analyze our plcs and they send back

265
00:11:29,900 --> 00:11:32,780
a clean copy of the code and say here

266
00:11:31,400 --> 00:11:33,950
you go so it's less like a visual

267
00:11:32,780 --> 00:11:35,750
student thing you can think of it more

268
00:11:33,950 --> 00:11:37,190
like you know it's kind of a middle

269
00:11:35,750 --> 00:11:38,690
students kind of like you're attaching a

270
00:11:37,190 --> 00:11:42,170
debugger you've got this read and write

271
00:11:38,690 --> 00:11:44,000
you know direct access to the PLC so for

272
00:11:42,170 --> 00:11:46,640
it out yes but I was like put on like

273
00:11:44,000 --> 00:11:50,090
the RPM yes so that was to manipulate

274
00:11:46,640 --> 00:11:52,430
the you know presumed presumed

275
00:11:50,090 --> 00:11:53,990
centrifuges attached to the PLC tried to

276
00:11:52,430 --> 00:11:58,700
change the way that they were actually

277
00:11:53,990 --> 00:12:00,230
behaving so that they break so when

278
00:11:58,700 --> 00:12:02,060
people were saying that you know Stuxnet

279
00:12:00,230 --> 00:12:03,830
implemented you know a PLC rootkit and

280
00:12:02,060 --> 00:12:06,260
stuff like that the key thing is that

281
00:12:03,830 --> 00:12:08,630
they weren't actually doing the hiding

282
00:12:06,260 --> 00:12:10,160
and the lying on the piece the PLC

283
00:12:08,630 --> 00:12:13,220
itself but that's sort of more what i

284
00:12:10,160 --> 00:12:15,680
would call plc rootkit yep the code was

285
00:12:13,220 --> 00:12:16,899
on the plc and when you asked the TLC

286
00:12:15,680 --> 00:12:19,540
for the day to even with

287
00:12:16,899 --> 00:12:21,879
clean file environment even with the

288
00:12:19,540 --> 00:12:24,040
clean step 7 if the code to live was

289
00:12:21,879 --> 00:12:25,809
actually in the PLC that would be sort

290
00:12:24,040 --> 00:12:27,519
of more when I think of but you know

291
00:12:25,809 --> 00:12:28,930
it's six of one half dozen any other

292
00:12:27,519 --> 00:12:30,339
there's an attacker he's met in the

293
00:12:28,930 --> 00:12:31,990
middle lane you're trying to read

294
00:12:30,339 --> 00:12:34,269
something he's giving you back clean

295
00:12:31,990 --> 00:12:36,790
copy when it should be dirty this was

296
00:12:34,269 --> 00:12:38,529
implemented at the Windows operating

297
00:12:36,790 --> 00:12:40,480
system level so that's why should be

298
00:12:38,529 --> 00:12:43,119
clear about this is all on windows box

299
00:12:40,480 --> 00:12:44,619
is step 7 kind of stuff and then this is

300
00:12:43,119 --> 00:12:47,019
some hardware that gets attached to the

301
00:12:44,619 --> 00:12:48,790
windows box so the amount of the middle

302
00:12:47,019 --> 00:12:51,490
lane was happening at the interface

303
00:12:48,790 --> 00:12:54,899
between the hardware and the physical

304
00:12:51,490 --> 00:12:57,399
box that are still on the windows box

305
00:12:54,899 --> 00:13:00,490
all right and the interesting thing is

306
00:12:57,399 --> 00:13:05,699
when I was going back and learning about

307
00:13:00,490 --> 00:13:08,199
this function export redirection rather

308
00:13:05,699 --> 00:13:10,179
so I've never dealt with the export

309
00:13:08,199 --> 00:13:12,129
redirection until I saw it in the subnet

310
00:13:10,179 --> 00:13:14,559
paper I was googling around and I found

311
00:13:12,129 --> 00:13:16,179
this nice article on packet storm where

312
00:13:14,559 --> 00:13:18,699
it's basically telling you exactly how

313
00:13:16,179 --> 00:13:20,559
to create a Stuxnet stop Trojan dll and

314
00:13:18,699 --> 00:13:22,629
saying hey you've got some deal out and

315
00:13:20,559 --> 00:13:24,249
you would like to you know make sure

316
00:13:22,629 --> 00:13:26,019
that you implement some functions but

317
00:13:24,249 --> 00:13:28,059
you forward the exports to the original

318
00:13:26,019 --> 00:13:29,740
function for other stuff and that will

319
00:13:28,059 --> 00:13:31,420
tell you how to make the nut nice get

320
00:13:29,740 --> 00:13:32,980
file so you dumped all the export you

321
00:13:31,420 --> 00:13:34,120
put it in a def file you say here's the

322
00:13:32,980 --> 00:13:36,970
ones i'm going to do here's the ones

323
00:13:34,120 --> 00:13:40,660
that are forwarded and so that's an

324
00:13:36,970 --> 00:13:45,309
interesting to tour through alright and

325
00:13:40,660 --> 00:13:47,769
so I already kind of said this the

326
00:13:45,309 --> 00:13:50,230
relevance afforded exports to bound

327
00:13:47,769 --> 00:13:53,319
imports is that you'll see a number of

328
00:13:50,230 --> 00:13:55,059
forwarder reps set to non zero whenever

329
00:13:53,319 --> 00:13:57,699
you import from something that uses

330
00:13:55,059 --> 00:14:00,309
forwarded exports anywhere so if you

331
00:13:57,699 --> 00:14:03,819
import from kernel32 you bind against

332
00:14:00,309 --> 00:14:06,069
kernel32 because it knows that kernel32

333
00:14:03,819 --> 00:14:08,230
does some of this forwarding it knows

334
00:14:06,069 --> 00:14:10,990
that look kernel32 might be the same

335
00:14:08,230 --> 00:14:12,339
about mt dll might change and NT dll is

336
00:14:10,990 --> 00:14:14,170
the real person implementing some of

337
00:14:12,339 --> 00:14:16,420
these functions so i need to know two

338
00:14:14,170 --> 00:14:18,040
versions for both of those because you

339
00:14:16,420 --> 00:14:19,779
know you could be importing add vector

340
00:14:18,040 --> 00:14:21,110
exception either from kernel32 but it's

341
00:14:19,779 --> 00:14:24,900
really coming in from

342
00:14:21,110 --> 00:14:26,580
and so if NT dll changes you got to fix

343
00:14:24,900 --> 00:14:29,640
up the mountain ports or you have to

344
00:14:26,580 --> 00:14:31,950
recalculate them so that's where this

345
00:14:29,640 --> 00:14:34,590
whole pound forward a ref comes from

346
00:14:31,950 --> 00:14:36,120
these are for one dll / thing that this

347
00:14:34,590 --> 00:14:39,020
particular binary happens to be

348
00:14:36,120 --> 00:14:42,060
forwarding and to us colonel / sorry

349
00:14:39,020 --> 00:14:43,770
kernel32.dll pull me forwards to one

350
00:14:42,060 --> 00:14:46,410
thing into dll that's why i always see

351
00:14:43,770 --> 00:14:50,490
one thing immediately after birth ya

352
00:14:46,410 --> 00:14:54,690
gotta keep change the DAT to Alex or it

353
00:14:50,490 --> 00:14:56,910
is rap that's a good question can you

354
00:14:54,690 --> 00:15:01,050
arrange the 82 basically do forwarded

355
00:14:56,910 --> 00:15:04,650
stuff right i would say probably beat

356
00:15:01,050 --> 00:15:09,630
well basically the key thing is right to

357
00:15:04,650 --> 00:15:11,610
use forwarded exports you need to you

358
00:15:09,630 --> 00:15:13,320
could maybe like increase the size of my

359
00:15:11,610 --> 00:15:15,390
package stream there or you'd have the

360
00:15:13,320 --> 00:15:18,090
cactus string over you have to overwrite

361
00:15:15,390 --> 00:15:19,230
an existing string right so so that

362
00:15:18,090 --> 00:15:21,360
would be another way instead of just

363
00:15:19,230 --> 00:15:23,130
changing the ap2 like point way out to

364
00:15:21,360 --> 00:15:24,720
your module to change it to find out a

365
00:15:23,130 --> 00:15:27,480
forwarded thing extending forward to

366
00:15:24,720 --> 00:15:28,710
your body but the main question is you

367
00:15:27,480 --> 00:15:31,230
got to get a string somewhere within

368
00:15:28,710 --> 00:15:32,760
this range applies to your thing you

369
00:15:31,230 --> 00:15:35,100
can't necessarily overwrite the existing

370
00:15:32,760 --> 00:15:37,290
strings otherwise when the OS loner goes

371
00:15:35,100 --> 00:15:40,470
to try to look up you know some normal

372
00:15:37,290 --> 00:15:42,240
export it'll have the completely wrong

373
00:15:40,470 --> 00:15:44,130
string and we'll never find that thing

374
00:15:42,240 --> 00:15:46,910
to fill into somebody else's importune

375
00:15:44,130 --> 00:15:49,290
right if it's looking for edit I did

376
00:15:46,910 --> 00:15:51,180
whatever it's called epic and it opened

377
00:15:49,290 --> 00:15:52,980
in control or whatever it is called if

378
00:15:51,180 --> 00:15:54,390
that string got overwritten you would

379
00:15:52,980 --> 00:15:55,680
never be able to actually import that

380
00:15:54,390 --> 00:15:57,780
for somebody else in that it would just

381
00:15:55,680 --> 00:15:59,370
crash on the programs so i think the

382
00:15:57,780 --> 00:16:02,010
easiest way to go about basically do

383
00:15:59,370 --> 00:16:03,720
expand the size of that data directory

384
00:16:02,010 --> 00:16:06,270
entry and tack a string at the bottom

385
00:16:03,720 --> 00:16:07,860
and then you'd have to add an extra X

386
00:16:06,270 --> 00:16:11,570
for addressing it would be a lot more

387
00:16:07,860 --> 00:16:11,570
work but I think it's due


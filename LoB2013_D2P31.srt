1
00:00:03,700 --> 00:00:08,559
now our terms are always very squishy

2
00:00:06,230 --> 00:00:13,549
what's a virus what's a Trojan what's a

3
00:00:08,559 --> 00:00:16,610
rootkit but for my purposes a virus is a

4
00:00:13,549 --> 00:00:18,289
malware which self replicates and based

5
00:00:16,610 --> 00:00:20,060
on some tax on tomato red long ago I

6
00:00:18,289 --> 00:00:22,310
make the difference between worm and

7
00:00:20,060 --> 00:00:24,130
virus and that a virus requires some

8
00:00:22,310 --> 00:00:28,040
human health to containing propagation

9
00:00:24,130 --> 00:00:30,590
whereas a propagation between systems at

10
00:00:28,040 --> 00:00:32,450
least whereas a worm can like just

11
00:00:30,590 --> 00:00:34,250
automatically keep running around so

12
00:00:32,450 --> 00:00:36,170
like a worm would be using an exploit to

13
00:00:34,250 --> 00:00:38,210
automatically hop between systems and so

14
00:00:36,170 --> 00:00:40,100
forth whereas a virus if you think boot

15
00:00:38,210 --> 00:00:42,109
sector virus is back in the floppy disk

16
00:00:40,100 --> 00:00:43,730
days you've got a human walking around

17
00:00:42,109 --> 00:00:44,809
with the disk plugging it into different

18
00:00:43,730 --> 00:00:47,089
machines and that's how it's spreading

19
00:00:44,809 --> 00:00:50,329
behind the sea yes it can automatically

20
00:00:47,089 --> 00:00:52,129
copy itself the files but in order to

21
00:00:50,329 --> 00:00:55,219
you know jump between machines it's got

22
00:00:52,129 --> 00:00:57,739
some human either you know clicking on

23
00:00:55,219 --> 00:01:00,559
some social engineering email moving

24
00:00:57,739 --> 00:01:05,000
around some file on a lot of floppy

25
00:01:00,559 --> 00:01:07,280
discs etc so it's it's an academic point

26
00:01:05,000 --> 00:01:09,020
but I consider a virus something that

27
00:01:07,280 --> 00:01:13,479
self replicates that still needs humans

28
00:01:09,020 --> 00:01:16,040
to to to move between systems alright so

29
00:01:13,479 --> 00:01:18,350
we're going to show the virus for P

30
00:01:16,040 --> 00:01:19,729
files and the exact same thing that I'm

31
00:01:18,350 --> 00:01:21,950
going to show here would work on the L

32
00:01:19,729 --> 00:01:23,600
files and now you can go look at

33
00:01:21,950 --> 00:01:25,130
tutorials on virus writing and you'll

34
00:01:23,600 --> 00:01:27,350
see the exact same thing you could

35
00:01:25,130 --> 00:01:30,490
probably write basic one yourself if you

36
00:01:27,350 --> 00:01:33,439
really you know spent a week at it or so

37
00:01:30,490 --> 00:01:35,990
all right so conceptually how virus is

38
00:01:33,439 --> 00:01:38,390
doing the self propagation is the viral

39
00:01:35,990 --> 00:01:40,189
code understands the file format that it

40
00:01:38,390 --> 00:01:42,770
wants to infect whether it's executable

41
00:01:40,189 --> 00:01:44,570
whether it's PDF or anything else we're

42
00:01:42,770 --> 00:01:46,850
going to talk executables and so what

43
00:01:44,570 --> 00:01:48,259
happens is there's some executable

44
00:01:46,850 --> 00:01:50,090
sitting around here and somewhere else

45
00:01:48,259 --> 00:01:53,270
there's a virus running it sees the

46
00:01:50,090 --> 00:01:55,399
executable and it taxed the virus onto

47
00:01:53,270 --> 00:01:59,450
the file it can tack it onto the end it

48
00:01:55,399 --> 00:02:00,649
can tack it somewhere in the middle so

49
00:01:59,450 --> 00:02:03,140
for that doesn't really matter where it

50
00:02:00,649 --> 00:02:05,929
goes but somehow the virus is attached

51
00:02:03,140 --> 00:02:07,280
to the file and then there's different

52
00:02:05,929 --> 00:02:10,520
ways that the virus will subsequently

53
00:02:07,280 --> 00:02:12,019
get invocation the simplest way is when

54
00:02:10,520 --> 00:02:14,659
it attacks itself to the end of the file

55
00:02:12,019 --> 00:02:16,450
it changes the address of entry point

56
00:02:14,659 --> 00:02:19,810
field in the optional header

57
00:02:16,450 --> 00:02:21,970
p context to just point down at its code

58
00:02:19,810 --> 00:02:24,220
down there towards the bottom of things

59
00:02:21,970 --> 00:02:25,840
so that should you know that kind of

60
00:02:24,220 --> 00:02:28,030
looks pretty suspicious right you've got

61
00:02:25,840 --> 00:02:29,950
a address of entry point which doesn't

62
00:02:28,030 --> 00:02:31,900
just point at the normal text section it

63
00:02:29,950 --> 00:02:33,640
points that you know maybe there's no

64
00:02:31,900 --> 00:02:36,459
section defined down for that location

65
00:02:33,640 --> 00:02:38,410
maybe it's the resources section as

66
00:02:36,459 --> 00:02:39,880
you'll see the virus today the virus

67
00:02:38,410 --> 00:02:42,610
today it's just going to take whatever

68
00:02:39,880 --> 00:02:44,620
the last section is expand the size in

69
00:02:42,610 --> 00:02:47,019
the section enter and then you know tak

70
00:02:44,620 --> 00:02:50,019
itself down there at the bottom and so

71
00:02:47,019 --> 00:02:51,970
when I see a address of entry point that

72
00:02:50,019 --> 00:02:54,280
points into the dot reload section of

73
00:02:51,970 --> 00:02:56,049
the dot resource section that's not

74
00:02:54,280 --> 00:02:58,329
right we should point into the dot text

75
00:02:56,049 --> 00:03:00,040
section all right and so they know that

76
00:02:58,329 --> 00:03:01,750
and they can potentially get around that

77
00:03:00,040 --> 00:03:03,700
you know certainly some of the early AV

78
00:03:01,750 --> 00:03:05,019
heuristics were just if you've got an

79
00:03:03,700 --> 00:03:06,819
address of entry point that doesn't

80
00:03:05,019 --> 00:03:10,450
point that text section this is a

81
00:03:06,819 --> 00:03:11,890
malware file right and so to get around

82
00:03:10,450 --> 00:03:14,110
that sort of heuristic what they would

83
00:03:11,890 --> 00:03:15,700
do is they would leave the header still

84
00:03:14,110 --> 00:03:17,470
pointing it to the dot text section as

85
00:03:15,700 --> 00:03:20,049
normal but then they just put a jump

86
00:03:17,470 --> 00:03:21,400
instruction as the first thing right at

87
00:03:20,049 --> 00:03:23,290
that address so you point at the address

88
00:03:21,400 --> 00:03:25,000
of entry point and then write the first

89
00:03:23,290 --> 00:03:27,819
instruction at address of entry point is

90
00:03:25,000 --> 00:03:30,190
jump to virus code right so then the

91
00:03:27,819 --> 00:03:32,500
next heuristic that the AV vendors use

92
00:03:30,190 --> 00:03:34,540
is they said if I see address of entry

93
00:03:32,500 --> 00:03:36,370
point points directly to a jump that's

94
00:03:34,540 --> 00:03:39,209
probably my arse code right and so you

95
00:03:36,370 --> 00:03:42,370
can keep playing these games forever

96
00:03:39,209 --> 00:03:43,870
right another way that you could

97
00:03:42,370 --> 00:03:45,190
potentially invoke a virus right as you

98
00:03:43,870 --> 00:03:47,109
could leave the headers pointing at the

99
00:03:45,190 --> 00:03:49,030
tag section and you can add a TLS call

100
00:03:47,109 --> 00:03:51,670
back and then the TLS call back we'll

101
00:03:49,030 --> 00:03:54,880
invite invoke the virus code before you

102
00:03:51,670 --> 00:03:58,060
know the original entry point ever gets

103
00:03:54,880 --> 00:03:59,530
called and like I said before you could

104
00:03:58,060 --> 00:04:01,329
have it be in the text section if there

105
00:03:59,530 --> 00:04:03,430
happens to be a little slack space at

106
00:04:01,329 --> 00:04:07,060
the end of the text section you know

107
00:04:03,430 --> 00:04:09,400
there's maybe some some padding in the

108
00:04:07,060 --> 00:04:11,260
file itself then you could have it be

109
00:04:09,400 --> 00:04:13,630
within the text section in that padding

110
00:04:11,260 --> 00:04:17,049
location this is more common on the elf

111
00:04:13,630 --> 00:04:18,760
viruses but but you could have because

112
00:04:17,049 --> 00:04:21,970
they added as dying essentially but in

113
00:04:18,760 --> 00:04:24,010
the context of PE you could imagine that

114
00:04:21,970 --> 00:04:26,229
you've got that hex 200 by it's worth of

115
00:04:24,010 --> 00:04:28,090
space that will potentially be slack

116
00:04:26,229 --> 00:04:29,919
space on the end of your text section

117
00:04:28,090 --> 00:04:30,249
and so you could put yourself there and

118
00:04:29,919 --> 00:04:33,159
then you

119
00:04:30,249 --> 00:04:36,759
still you know ostensibly be within the

120
00:04:33,159 --> 00:04:39,459
nap text section and again you can play

121
00:04:36,759 --> 00:04:45,099
the same game do virus code in my notes

122
00:04:39,459 --> 00:04:46,599
and so forth alright and so the

123
00:04:45,099 --> 00:04:47,859
important thing is that I told you

124
00:04:46,599 --> 00:04:51,549
there's this technique that's applicable

125
00:04:47,859 --> 00:04:53,709
to viruses and exploits and rootkits

126
00:04:51,549 --> 00:04:55,779
used for the import address table

127
00:04:53,709 --> 00:04:58,959
hooking and stuff like that so you

128
00:04:55,779 --> 00:05:00,939
should probably go check out this win32

129
00:04:58,959 --> 00:05:04,149
shellcode paper it talks about this

130
00:05:00,939 --> 00:05:09,279
technique in the context of shell code

131
00:05:04,149 --> 00:05:11,559
specifically but basically what it is is

132
00:05:09,279 --> 00:05:13,779
it's a small snippet of assembly code

133
00:05:11,559 --> 00:05:15,699
which knows first it knows how to get

134
00:05:13,779 --> 00:05:17,349
the kernel32 that's the key thing that's

135
00:05:15,699 --> 00:05:18,909
the thing which is why this is currently

136
00:05:17,349 --> 00:05:21,849
broken for windows 7 that have it

137
00:05:18,909 --> 00:05:24,519
updated there's some extra windows

138
00:05:21,849 --> 00:05:26,619
information that windows keeps about

139
00:05:24,519 --> 00:05:29,409
what dll's are loaded in memory and

140
00:05:26,619 --> 00:05:31,719
where and so this is linked off of a

141
00:05:29,409 --> 00:05:35,709
data structure called the tab threat

142
00:05:31,719 --> 00:05:37,779
environment block and so this Ted thread

143
00:05:35,709 --> 00:05:39,399
environment block points at the peb

144
00:05:37,779 --> 00:05:41,889
which is the process environment block

145
00:05:39,399 --> 00:05:44,559
points that a linked list that says the

146
00:05:41,889 --> 00:05:48,299
stuff currently mikasa space is you know

147
00:05:44,559 --> 00:05:51,339
notepad.exe and then NT dll and then

148
00:05:48,299 --> 00:05:53,919
kernel32 actually i think it's a TD yeah

149
00:05:51,339 --> 00:05:57,819
that's dope I had Mt dll pretty too

150
00:05:53,919 --> 00:05:59,469
stuff like that and so basically once

151
00:05:57,819 --> 00:06:01,419
you know just this little bit of extra

152
00:05:59,469 --> 00:06:03,879
metadata about how windows arranged is

153
00:06:01,419 --> 00:06:05,349
this list you can always find this

154
00:06:03,879 --> 00:06:07,779
thread environment block because it's

155
00:06:05,349 --> 00:06:09,610
it's linked off of a segment register

156
00:06:07,779 --> 00:06:11,559
it's just a convention on windows that

157
00:06:09,610 --> 00:06:12,939
if you ever see if you have a reverse

158
00:06:11,559 --> 00:06:15,399
engineering stuff and you see something

159
00:06:12,939 --> 00:06:17,769
getting some offset from the FS register

160
00:06:15,399 --> 00:06:20,979
so you see FS colon square brackets 0

161
00:06:17,769 --> 00:06:23,379
it's accessing the first field of the 10

162
00:06:20,979 --> 00:06:25,299
thread environment wire so it's very

163
00:06:23,379 --> 00:06:27,009
common for attackers to start at the tab

164
00:06:25,299 --> 00:06:30,669
walk their way through a bunch of data

165
00:06:27,009 --> 00:06:33,039
structures get to the kernel32.dll and

166
00:06:30,669 --> 00:06:34,360
once you're at kernel32 base then you

167
00:06:33,039 --> 00:06:36,279
start walking your way through the data

168
00:06:34,360 --> 00:06:38,469
structures according to the p energize

169
00:06:36,279 --> 00:06:40,539
you got turtle pretty tues base treating

170
00:06:38,469 --> 00:06:42,429
like a Doss header and then use that

171
00:06:40,539 --> 00:06:43,790
dough center to find the NT other then

172
00:06:42,429 --> 00:06:45,530
use the NT 80 to find the

173
00:06:43,790 --> 00:06:47,780
the letter is the optional header to

174
00:06:45,530 --> 00:06:50,240
find the export address table use the

175
00:06:47,780 --> 00:06:52,850
export address table to find the export

176
00:06:50,240 --> 00:06:56,450
names table is the export names table to

177
00:06:52,850 --> 00:06:58,490
find get proc address or create file or

178
00:06:56,450 --> 00:07:00,140
write file or build library whatever

179
00:06:58,490 --> 00:07:01,400
thing you want a lot of the functions

180
00:07:00,140 --> 00:07:04,130
that you really care about are in

181
00:07:01,400 --> 00:07:05,810
kernel32 so once you can wind your way

182
00:07:04,130 --> 00:07:09,200
down to the export address table

183
00:07:05,810 --> 00:07:10,820
information for kernel32 you can find a

184
00:07:09,200 --> 00:07:12,860
lot of functions that give you the

185
00:07:10,820 --> 00:07:14,540
functionality you need whatever without

186
00:07:12,860 --> 00:07:16,810
ever having to have the operating system

187
00:07:14,540 --> 00:07:21,110
give them to you in the import dress too

188
00:07:16,810 --> 00:07:25,370
so we reminded me of this it's a good

189
00:07:21,110 --> 00:07:27,080
paper by escape that just talks about

190
00:07:25,370 --> 00:07:29,150
the shellcode and gives you the context

191
00:07:27,080 --> 00:07:30,980
about this starting point for the thread

192
00:07:29,150 --> 00:07:32,870
environment block but we're going to use

193
00:07:30,980 --> 00:07:35,240
this in the virus and I'll point it out

194
00:07:32,870 --> 00:07:37,070
when we get to it but or uses it in the

195
00:07:35,240 --> 00:07:39,620
exploits class in the reverse

196
00:07:37,070 --> 00:07:42,800
engineering class they show you some

197
00:07:39,620 --> 00:07:45,530
malware that's using it to do to do

198
00:07:42,800 --> 00:07:49,030
runtime importing likes without to find

199
00:07:45,530 --> 00:07:49,030
load library and get proc address


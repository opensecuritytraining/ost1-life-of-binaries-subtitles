1
00:00:03,860 --> 00:00:10,290
alright so we are going to move on learn

2
00:00:07,680 --> 00:00:11,730
some more information try to knock out

3
00:00:10,290 --> 00:00:14,520
the rest of this looks like we're pretty

4
00:00:11,730 --> 00:00:21,420
close eighty-three only got about 20

5
00:00:14,520 --> 00:00:22,860
slides left and the P information so and

6
00:00:21,420 --> 00:00:25,970
we're going to start with the topic

7
00:00:22,860 --> 00:00:30,359
which I most forget the details up

8
00:00:25,970 --> 00:00:33,449
alright so then F interesting this

9
00:00:30,359 --> 00:00:35,999
animation is broken but starting from

10
00:00:33,449 --> 00:00:39,420
the data directory going to index 0 1 2

11
00:00:35,999 --> 00:00:41,219
we've got the resource directory and I'd

12
00:00:39,420 --> 00:00:42,620
mentioned the resources briefly before

13
00:00:41,219 --> 00:00:45,389
we had shown them briefly before

14
00:00:42,620 --> 00:00:47,879
resources are information like icons

15
00:00:45,389 --> 00:00:49,319
that are embedded into the file in the

16
00:00:47,879 --> 00:00:51,089
case of Stuxnet it's you know the

17
00:00:49,319 --> 00:00:52,530
exploits and dll's that they want to

18
00:00:51,089 --> 00:00:54,420
inject they carry that around in the

19
00:00:52,530 --> 00:00:56,489
resource section you can kind of think

20
00:00:54,420 --> 00:00:58,679
like the resources as its own little

21
00:00:56,489 --> 00:01:01,139
mini file system because it's got a well

22
00:00:58,679 --> 00:01:03,989
structured way of finding your way to

23
00:01:01,139 --> 00:01:06,450
all of these independent sort of files

24
00:01:03,989 --> 00:01:08,970
that are tacked on to the end of end of

25
00:01:06,450 --> 00:01:10,650
the binary itself proper and so there's

26
00:01:08,970 --> 00:01:12,120
programmatic ways to access each of

27
00:01:10,650 --> 00:01:13,860
these files there's AP is that you'll

28
00:01:12,120 --> 00:01:15,780
call to get resource with a particular

29
00:01:13,860 --> 00:01:18,230
ID and things like that so you as a

30
00:01:15,780 --> 00:01:20,820
programmer will put resources into your

31
00:01:18,230 --> 00:01:24,690
executable and then you'll you know read

32
00:01:20,820 --> 00:01:26,160
them up and use them as necessary all

33
00:01:24,690 --> 00:01:27,510
right well so from the data directory

34
00:01:26,160 --> 00:01:29,430
there's just a standard entry and then

35
00:01:27,510 --> 00:01:32,100
that points at the resource directory as

36
00:01:29,430 --> 00:01:34,020
given right here actually I write this

37
00:01:32,100 --> 00:01:36,930
down fix the slides other words on won't

38
00:01:34,020 --> 00:01:38,790
notice so in the resource directory this

39
00:01:36,930 --> 00:01:40,440
first top level structure there's going

40
00:01:38,790 --> 00:01:42,570
to be just one of these and then it's

41
00:01:40,440 --> 00:01:45,090
going to be having an array of other

42
00:01:42,570 --> 00:01:46,320
data structures after it so things we

43
00:01:45,090 --> 00:01:47,760
care about there's a time date stamp

44
00:01:46,320 --> 00:01:50,159
here but that's not actually going to

45
00:01:47,760 --> 00:01:51,570
have anything so that's not going to

46
00:01:50,159 --> 00:01:53,820
matter things we care about is the

47
00:01:51,570 --> 00:01:56,400
number of named entries a number of ID

48
00:01:53,820 --> 00:01:57,960
entries so the resources sometimes

49
00:01:56,400 --> 00:02:00,090
they'll have an actual name and

50
00:01:57,960 --> 00:02:01,680
sometimes they'll just have an ID number

51
00:02:00,090 --> 00:02:03,570
essentially and so you can look up

52
00:02:01,680 --> 00:02:05,250
things by ID or look up things by name

53
00:02:03,570 --> 00:02:10,470
when you're accessing your own resources

54
00:02:05,250 --> 00:02:12,450
programmatically so immediately

55
00:02:10,470 --> 00:02:16,439
following the resource directory there's

56
00:02:12,450 --> 00:02:17,460
going to be an array of structures these

57
00:02:16,439 --> 00:02:19,320
resource directory

58
00:02:17,460 --> 00:02:21,240
entry structures so we start with the

59
00:02:19,320 --> 00:02:23,120
resource directory and then we've got an

60
00:02:21,240 --> 00:02:25,740
array of the resource directory entries

61
00:02:23,120 --> 00:02:27,990
immediately after it where the number of

62
00:02:25,740 --> 00:02:33,090
entries are the number of named entries

63
00:02:27,990 --> 00:02:34,620
plus the number of ID entries and as it

64
00:02:33,090 --> 00:02:39,510
says here the resources can be

65
00:02:34,620 --> 00:02:43,830
identified name or ID but not both so

66
00:02:39,510 --> 00:02:45,950
here's the nice terrible unionized thing

67
00:02:43,830 --> 00:02:49,410
that makes it very hard to just

68
00:02:45,950 --> 00:02:52,830
naturally read the things but basically

69
00:02:49,410 --> 00:02:59,670
how you can think of this is that we've

70
00:02:52,830 --> 00:03:01,200
got so it's reusing this this resource

71
00:02:59,670 --> 00:03:03,240
directory entry it's going to reuse the

72
00:03:01,200 --> 00:03:05,730
same data structure for each of these

73
00:03:03,240 --> 00:03:07,590
things that are after the resource

74
00:03:05,730 --> 00:03:09,240
directory and then it's going to

75
00:03:07,590 --> 00:03:11,670
interpret it differently depending on

76
00:03:09,240 --> 00:03:13,440
whether some weather most significant

77
00:03:11,670 --> 00:03:16,620
bits are set or not so I think the

78
00:03:13,440 --> 00:03:18,750
easiest way is really just to show it so

79
00:03:16,620 --> 00:03:20,970
yeah so it's simpler that looks just

80
00:03:18,750 --> 00:03:23,670
think of it as the first D word in this

81
00:03:20,970 --> 00:03:25,440
entry if the most significant bit is one

82
00:03:23,670 --> 00:03:27,900
which means if you're looking at it in

83
00:03:25,440 --> 00:03:30,840
hex the most you'll have an eight as the

84
00:03:27,900 --> 00:03:33,870
highest nibble it means that the lower

85
00:03:30,840 --> 00:03:37,110
31 bits are an offset to a string which

86
00:03:33,870 --> 00:03:40,170
gives the name of the resource so in

87
00:03:37,110 --> 00:03:41,160
this case you'd say if this so this is

88
00:03:40,170 --> 00:03:43,410
something that you may not necessarily

89
00:03:41,160 --> 00:03:47,820
be familiar with but in see you can have

90
00:03:43,410 --> 00:03:50,190
bit fields so it's saying you know if so

91
00:03:47,820 --> 00:03:52,800
there's 31 bit right here and then

92
00:03:50,190 --> 00:03:54,480
there's a one bit that says name is

93
00:03:52,800 --> 00:03:57,390
string and basically if this name is

94
00:03:54,480 --> 00:04:00,030
string bit is set to 1 then you're going

95
00:03:57,390 --> 00:04:01,980
to have this thing is basically just a

96
00:04:00,030 --> 00:04:04,080
pointer to some other string and this D

97
00:04:01,980 --> 00:04:08,010
word name would be interpreted as an RBA

98
00:04:04,080 --> 00:04:11,220
pointing out to some other string uhhhhh

99
00:04:08,010 --> 00:04:15,090
if the most significant bit is not set

100
00:04:11,220 --> 00:04:17,310
then we're talking about that it's going

101
00:04:15,090 --> 00:04:19,109
to be a word size ID so if you've got a

102
00:04:17,310 --> 00:04:21,330
zero in your most significant bit then

103
00:04:19,109 --> 00:04:24,030
this overall D word should basically be

104
00:04:21,330 --> 00:04:28,260
treated as a ID for a particular

105
00:04:24,030 --> 00:04:30,490
resource so that's for the first D word

106
00:04:28,260 --> 00:04:32,169
of information so we said

107
00:04:30,490 --> 00:04:35,199
is this kind of thing which has these

108
00:04:32,169 --> 00:04:37,599
bit fields and then there's the second D

109
00:04:35,199 --> 00:04:41,199
word of information which is here it's

110
00:04:37,599 --> 00:04:43,300
the name here it's a different Union and

111
00:04:41,199 --> 00:04:44,830
so we can expect that in this different

112
00:04:43,300 --> 00:04:48,580
Union if the most significant bit is set

113
00:04:44,830 --> 00:04:49,930
it's going to be a directory and that's

114
00:04:48,580 --> 00:04:51,340
why i said you can kind of think of it

115
00:04:49,930 --> 00:04:52,389
like a file system it's a directory

116
00:04:51,340 --> 00:04:54,720
which is going to hold more data

117
00:04:52,389 --> 00:04:58,810
structures on anything for instance

118
00:04:54,720 --> 00:05:01,330
right so if the most significant bit of

119
00:04:58,810 --> 00:05:04,690
the second D word is set means the lower

120
00:05:01,330 --> 00:05:07,000
32 the lower 31 bits are offset to

121
00:05:04,690 --> 00:05:09,099
another image resource directory so this

122
00:05:07,000 --> 00:05:12,819
that that's the high level data

123
00:05:09,099 --> 00:05:13,900
structuring so I kind of Miss described

124
00:05:12,819 --> 00:05:15,220
in saying there's only one image

125
00:05:13,900 --> 00:05:16,870
resource directory there's the one

126
00:05:15,220 --> 00:05:18,639
starting one but then there can be other

127
00:05:16,870 --> 00:05:19,900
ones nested lower down but it's not like

128
00:05:18,639 --> 00:05:21,460
you have an array of them or anything

129
00:05:19,900 --> 00:05:22,930
like that it's like you've got a

130
00:05:21,460 --> 00:05:25,599
starting one and then you've got kind of

131
00:05:22,930 --> 00:05:28,810
a tree going on potentially like a

132
00:05:25,599 --> 00:05:30,460
filesystem tree all right so let's just

133
00:05:28,810 --> 00:05:32,050
show an example okay and I don't have

134
00:05:30,460 --> 00:05:34,960
slides for the examples so it's just you

135
00:05:32,050 --> 00:05:36,550
an example for real so this is where cff

136
00:05:34,960 --> 00:05:37,810
explorer really will shine basically

137
00:05:36,550 --> 00:05:39,310
because you don't have to look through

138
00:05:37,810 --> 00:05:41,469
all these bit fields you just have nice

139
00:05:39,310 --> 00:05:43,000
little collapsible resources and things

140
00:05:41,469 --> 00:05:45,840
like that but let's look at it the nasty

141
00:05:43,000 --> 00:05:48,400
way first let's look at it with PE view

142
00:05:45,840 --> 00:05:50,199
all right looks like this thing has some

143
00:05:48,400 --> 00:05:52,539
resources attached to it whatever this

144
00:05:50,199 --> 00:05:55,270
is this round eight thing so something

145
00:05:52,539 --> 00:05:59,310
with TLS all right so the very first

146
00:05:55,270 --> 00:06:02,740
thing is an image resource directory and

147
00:05:59,310 --> 00:06:09,759
so we see here's the information see

148
00:06:02,740 --> 00:06:12,430
time/date stamp is nothing and then if

149
00:06:09,759 --> 00:06:14,139
we wait okay so and then there's the

150
00:06:12,430 --> 00:06:15,550
number of entries named entries in the

151
00:06:14,139 --> 00:06:17,500
number of ID entries there'll be that

152
00:06:15,550 --> 00:06:20,469
many entry structures immediately after

153
00:06:17,500 --> 00:06:24,909
it right so we add up 0 and 1 and we get

154
00:06:20,469 --> 00:06:26,409
one ID entry immediately after and so

155
00:06:24,909 --> 00:06:28,780
the way it's going to work usually is

156
00:06:26,409 --> 00:06:34,210
that you'll have these named entries and

157
00:06:28,780 --> 00:06:35,680
then any ID entries so most so now we

158
00:06:34,210 --> 00:06:38,169
just have like it's a nasty-looking

159
00:06:35,680 --> 00:06:40,089
unionized and bitmap structure but

160
00:06:38,169 --> 00:06:43,449
really each of those entries is just too

161
00:06:40,089 --> 00:06:44,480
d words and so in this case the first D

162
00:06:43,449 --> 00:06:46,520
word the most

163
00:06:44,480 --> 00:06:47,720
you can bit is set to zero which as we

164
00:06:46,520 --> 00:06:49,700
said on the previous slide if it's set

165
00:06:47,720 --> 00:06:52,850
to one it points to a string it's at the

166
00:06:49,700 --> 00:06:54,920
zero just treat it as an ID all right so

167
00:06:52,850 --> 00:06:57,230
that's for the first one and the next

168
00:06:54,920 --> 00:06:58,850
one if the most significant bit is one

169
00:06:57,230 --> 00:07:00,950
treat it as a pointer to another

170
00:06:58,850 --> 00:07:03,530
directory so we're going to have another

171
00:07:00,950 --> 00:07:05,870
one of these data structures otherwise I

172
00:07:03,530 --> 00:07:11,950
don't know what did I say what the case

173
00:07:05,870 --> 00:07:15,080
is if it's mostly agreement not one set

174
00:07:11,950 --> 00:07:16,460
and if it's not set it means it's the

175
00:07:15,080 --> 00:07:18,740
actual data which I'll actually be

176
00:07:16,460 --> 00:07:20,510
following what's given if it is not set

177
00:07:18,740 --> 00:07:23,090
that means the offset to the actual data

178
00:07:20,510 --> 00:07:24,350
so basically on the second D word if the

179
00:07:23,090 --> 00:07:25,880
most significant bit is not set that

180
00:07:24,350 --> 00:07:28,280
means you kind of reach the leaf node

181
00:07:25,880 --> 00:07:30,770
it's saying this second D word points at

182
00:07:28,280 --> 00:07:32,390
to the real data if it doesn't point at

183
00:07:30,770 --> 00:07:33,590
the real data exploits that another data

184
00:07:32,390 --> 00:07:37,190
structure which will give you another

185
00:07:33,590 --> 00:07:38,720
branch of the tree basically all right

186
00:07:37,190 --> 00:07:40,610
so this is kind of how you'd have to dig

187
00:07:38,720 --> 00:07:42,440
down to it so we take the most

188
00:07:40,610 --> 00:07:43,820
significant bit and the other things i

189
00:07:42,440 --> 00:07:46,430
should say is that this is the other

190
00:07:43,820 --> 00:07:48,320
case where we've got our VA's which are

191
00:07:46,430 --> 00:07:50,090
not relative to the base they're

192
00:07:48,320 --> 00:07:52,940
actually relative to the start of the

193
00:07:50,090 --> 00:07:54,590
resource information so when I say this

194
00:07:52,940 --> 00:07:56,570
is the offset to a directory and I say

195
00:07:54,590 --> 00:07:58,790
it's the most significant bit is set so

196
00:07:56,570 --> 00:08:02,510
just chop off the top one bit and treat

197
00:07:58,790 --> 00:08:04,280
the rest as an offset the offset is 18

198
00:08:02,510 --> 00:08:07,100
but it's 18 not from the beginning but

199
00:08:04,280 --> 00:08:09,170
from the start of all of this resource

200
00:08:07,100 --> 00:08:10,880
information so you can see we have 0 and

201
00:08:09,170 --> 00:08:13,100
then we go 14 and so you basically

202
00:08:10,880 --> 00:08:15,950
expect it to be tacked on right now down

203
00:08:13,100 --> 00:08:18,440
here and indeed if we go down here we

204
00:08:15,950 --> 00:08:20,600
see that you know starting at 18 at the

205
00:08:18,440 --> 00:08:21,980
next directory and then we go down and

206
00:08:20,600 --> 00:08:23,450
there's another directory and then we go

207
00:08:21,980 --> 00:08:24,920
down and here's an actual thing where

208
00:08:23,450 --> 00:08:26,960
the most significant bit is not set the

209
00:08:24,920 --> 00:08:30,260
one therefore it's just the opposite to

210
00:08:26,960 --> 00:08:32,840
the real data and so it offset 48 here's

211
00:08:30,260 --> 00:08:34,700
the real data and it's it's saying it's

212
00:08:32,840 --> 00:08:39,710
going to be a manifest type in so it's

213
00:08:34,700 --> 00:08:41,419
interpreted in here so realistically we

214
00:08:39,710 --> 00:08:43,400
don't like tap to deal with it like this

215
00:08:41,419 --> 00:08:45,320
ever if at all possible you really

216
00:08:43,400 --> 00:08:46,940
shouldn't ever have to but I put in

217
00:08:45,320 --> 00:08:48,950
there just sort of for completeness so

218
00:08:46,940 --> 00:08:50,780
that you recognize that as this crazy

219
00:08:48,950 --> 00:08:52,400
Union struct but it's really just two

220
00:08:50,780 --> 00:08:56,330
different ways of looking stuff first

221
00:08:52,400 --> 00:08:58,370
keyword it's either a string or our ID

222
00:08:56,330 --> 00:09:01,420
second D word it's either a point

223
00:08:58,370 --> 00:09:03,860
to a directory or pointer to the data

224
00:09:01,420 --> 00:09:06,200
alright so let's look at it the nice way

225
00:09:03,860 --> 00:09:09,440
the nice way is with cff explorer I

226
00:09:06,200 --> 00:09:10,670
think this is the same thing even and so

227
00:09:09,440 --> 00:09:12,980
and see if I've explore you just click

228
00:09:10,670 --> 00:09:14,300
on the resource directory and you can

229
00:09:12,980 --> 00:09:15,740
see that if you click on the top level

230
00:09:14,300 --> 00:09:17,660
resource directory there you go there's

231
00:09:15,740 --> 00:09:20,870
your data structure that's saying it has

232
00:09:17,660 --> 00:09:23,330
one ID entry next and so that would be

233
00:09:20,870 --> 00:09:25,550
this entry right here and this entry

234
00:09:23,330 --> 00:09:27,110
because the most significant bit on this

235
00:09:25,550 --> 00:09:30,020
first thing is that that means this

236
00:09:27,110 --> 00:09:32,839
entry is going to have a offset 18 to

237
00:09:30,020 --> 00:09:34,460
the next entry and so we click that we

238
00:09:32,839 --> 00:09:36,589
see new directory and that's that also

239
00:09:34,460 --> 00:09:38,690
had 18 and so forth when we go down

240
00:09:36,589 --> 00:09:41,210
right and the thing is we don't even

241
00:09:38,690 --> 00:09:43,460
really want to look at it like this this

242
00:09:41,210 --> 00:09:46,520
is even worse than it has to be we

243
00:09:43,460 --> 00:09:49,310
really want to look at it with is or is

244
00:09:46,520 --> 00:09:51,680
it resource editor this is the nice way

245
00:09:49,310 --> 00:09:53,540
where it'll actually be parsing this

246
00:09:51,680 --> 00:09:54,770
stuff so we said that there's like all

247
00:09:53,540 --> 00:09:56,150
of these directories there's multiple

248
00:09:54,770 --> 00:09:59,420
level directories that actually are only

249
00:09:56,150 --> 00:10:01,730
terminating at a single thing that calls

250
00:09:59,420 --> 00:10:03,670
itself manifest it's actually a manifest

251
00:10:01,730 --> 00:10:06,020
file that just has to do with the

252
00:10:03,670 --> 00:10:09,040
compilation it's a file generated

253
00:10:06,020 --> 00:10:11,810
automatically by visual studio compiler

254
00:10:09,040 --> 00:10:13,070
so this is a very simple case and you

255
00:10:11,810 --> 00:10:14,990
know we can find those basically this

256
00:10:13,070 --> 00:10:16,850
one text file essentially embedded into

257
00:10:14,990 --> 00:10:19,010
it now let's look at the more

258
00:10:16,850 --> 00:10:20,120
interesting case which is the process

259
00:10:19,010 --> 00:10:23,660
explorer that I was telling you about

260
00:10:20,120 --> 00:10:25,820
before so process Explorer right this is

261
00:10:23,660 --> 00:10:27,230
the thing that we run and it will show

262
00:10:25,820 --> 00:10:28,910
us all the different running processes

263
00:10:27,230 --> 00:10:30,080
in a hierarchy view and it will show us

264
00:10:28,910 --> 00:10:32,209
dll's if you're under that's

265
00:10:30,080 --> 00:10:34,520
administrator but what I claim to be

266
00:10:32,209 --> 00:10:36,800
four is that in its resources this exe

267
00:10:34,520 --> 00:10:39,620
actually has a well-formed kernel driver

268
00:10:36,800 --> 00:10:41,900
which it dumps out of its Colonel out of

269
00:10:39,620 --> 00:10:43,640
its resources and then it goes ahead and

270
00:10:41,900 --> 00:10:46,160
loads it up and starts the kernel driver

271
00:10:43,640 --> 00:10:47,959
so that I can talk and get detailed

272
00:10:46,160 --> 00:10:49,790
information from the system another

273
00:10:47,959 --> 00:10:51,740
thing which you may be blink and you'll

274
00:10:49,790 --> 00:10:56,270
miss it but another thing when I double

275
00:10:51,740 --> 00:10:58,910
click on that and I run it it actually

276
00:10:56,270 --> 00:11:01,370
drops out the process explorer 64 dot

277
00:10:58,910 --> 00:11:03,500
exe and then when I close it that goes

278
00:11:01,370 --> 00:11:05,450
away as well so it's actually dropping

279
00:11:03,500 --> 00:11:09,560
out the 64-bit version running it on the

280
00:11:05,450 --> 00:11:10,980
64-bit version and then deleting it when

281
00:11:09,560 --> 00:11:13,350
I close the binary

282
00:11:10,980 --> 00:11:18,449
right so let's go look at its resources

283
00:11:13,350 --> 00:11:20,730
for this interesting little buttery open

284
00:11:18,449 --> 00:11:22,380
up them you don't have process explorer

285
00:11:20,730 --> 00:11:27,420
on your machines I just forgot to put it

286
00:11:22,380 --> 00:11:30,630
as a prerequisite basically if i open up

287
00:11:27,420 --> 00:11:32,670
process explorer and i can go to the

288
00:11:30,630 --> 00:11:34,110
resource directory if i want and you can

289
00:11:32,670 --> 00:11:35,310
see there's a whole bunch of different

290
00:11:34,110 --> 00:11:37,500
things and they've got different names

291
00:11:35,310 --> 00:11:40,410
on some of them so this for one has

292
00:11:37,500 --> 00:11:42,480
named Binh res so you see this first

293
00:11:40,410 --> 00:11:44,250
entry it has most significant bit set to

294
00:11:42,480 --> 00:11:47,399
1 that means the rest of the century 21

295
00:11:44,250 --> 00:11:50,279
d 20 is an offset to this name the name

296
00:11:47,399 --> 00:11:51,870
is bin res I can expand all that I can

297
00:11:50,279 --> 00:11:54,089
go down and find all these particular

298
00:11:51,870 --> 00:11:56,279
resources and all that but the view i

299
00:11:54,089 --> 00:11:58,529
really like is the resource editor so i

300
00:11:56,279 --> 00:12:01,110
can expand that bin res and click right

301
00:11:58,529 --> 00:12:03,720
here and now I've got well that looks

302
00:12:01,110 --> 00:12:07,980
very much like a executable file right

303
00:12:03,720 --> 00:12:09,870
so this binary resource is some

304
00:12:07,980 --> 00:12:12,000
well-formed executables if I really

305
00:12:09,870 --> 00:12:14,639
wanted I can you know figure out where

306
00:12:12,000 --> 00:12:17,339
the efl a new is and I could figure out

307
00:12:14,639 --> 00:12:19,500
what the weather it's 64 and 32 and all

308
00:12:17,339 --> 00:12:21,060
that stuff but easier way is just you

309
00:12:19,500 --> 00:12:23,040
can start right clicking on these

310
00:12:21,060 --> 00:12:25,170
resources and you can click Save

311
00:12:23,040 --> 00:12:28,079
resource raw and this will just dumped

312
00:12:25,170 --> 00:12:31,079
out this raw binary to disk and then I

313
00:12:28,079 --> 00:12:37,019
can just open it back up again in cff

314
00:12:31,079 --> 00:12:42,149
explorer test dot the x e question i

315
00:12:37,019 --> 00:12:50,899
don't think i can use ? but alright so

316
00:12:42,149 --> 00:12:50,899
now i can go open that test folder q

317
00:12:54,060 --> 00:12:58,710
alright so test idea exceeds 32-bit

318
00:12:56,490 --> 00:13:00,810
executable I can go look at its headers

319
00:12:58,710 --> 00:13:02,610
and stuff go see if it as debug

320
00:13:00,810 --> 00:13:04,230
information I could go look up where it

321
00:13:02,610 --> 00:13:05,310
was originally compiled and stuff like

322
00:13:04,230 --> 00:13:07,440
that that would give me probably a

323
00:13:05,310 --> 00:13:09,660
better idea of which particular file

324
00:13:07,440 --> 00:13:11,460
this is so if you want to do that you

325
00:13:09,660 --> 00:13:13,620
can play around with that just go online

326
00:13:11,460 --> 00:13:16,140
and Google process explorer download it

327
00:13:13,620 --> 00:13:17,990
and pop it open but this is pretty much

328
00:13:16,140 --> 00:13:20,760
all I wanted to cover with resources

329
00:13:17,990 --> 00:13:22,410
like I said it's going to be this kind

330
00:13:20,760 --> 00:13:24,120
of tree structure you've got different

331
00:13:22,410 --> 00:13:27,030
branches can hold different resources

332
00:13:24,120 --> 00:13:28,470
it's commonly used for four icons and

333
00:13:27,030 --> 00:13:30,540
stuff like that the most common place

334
00:13:28,470 --> 00:13:32,930
you'll see it for for Microsoft file

335
00:13:30,540 --> 00:13:36,900
certainly and then in the case of

336
00:13:32,930 --> 00:13:38,520
malware that wants to contain extra

337
00:13:36,900 --> 00:13:40,890
functionality and bring it around with

338
00:13:38,520 --> 00:13:42,390
it you know they can obviously just hard

339
00:13:40,890 --> 00:13:44,550
code offsets into their code they're

340
00:13:42,390 --> 00:13:47,490
like I know that I embedded my you know

341
00:13:44,550 --> 00:13:48,780
resource this RZ a within my file and

342
00:13:47,490 --> 00:13:51,000
they can just access it that way as well

343
00:13:48,780 --> 00:13:52,650
but this provides a nice you know clean

344
00:13:51,000 --> 00:13:54,750
interface to use programmatic support

345
00:13:52,650 --> 00:13:57,180
for pulling out stuff and this is what

346
00:13:54,750 --> 00:13:58,710
the suction of people did with embedding

347
00:13:57,180 --> 00:14:00,300
all their dll's that they want to inject

348
00:13:58,710 --> 00:14:01,800
and betting all their exploit code so

349
00:14:00,300 --> 00:14:03,930
they read out a resource now they've got

350
00:14:01,800 --> 00:14:06,630
their exploit code now they send it to

351
00:14:03,930 --> 00:14:09,060
the printer spooler do that buffer

352
00:14:06,630 --> 00:14:12,720
overflow and so forth a quick question

353
00:14:09,060 --> 00:14:17,460
go for it I with those two embedded

354
00:14:12,720 --> 00:14:20,820
executables so if I watched your desktop

355
00:14:17,460 --> 00:14:23,040
closely enough so what it was doing was

356
00:14:20,820 --> 00:14:27,000
actually extracting one of those and

357
00:14:23,040 --> 00:14:29,220
actually writing it as a file out to the

358
00:14:27,000 --> 00:14:33,240
desktop three and then like you know

359
00:14:29,220 --> 00:14:36,270
calling a system execute on that yes

360
00:14:33,240 --> 00:14:39,150
basically yep so you were talking about

361
00:14:36,270 --> 00:14:42,390
how when I run process explorer all of a

362
00:14:39,150 --> 00:14:45,540
sudden the new thing called proc x64bit

363
00:14:42,390 --> 00:14:48,510
shows up all right anyways yeah to your

364
00:14:45,540 --> 00:14:50,510
question Craig process explorer

365
00:14:48,510 --> 00:14:54,330
basically is dropping out one of those

366
00:14:50,510 --> 00:14:55,290
executables and run invoking the 64-bit

367
00:14:54,330 --> 00:14:58,440
because you can see that it's on a

368
00:14:55,290 --> 00:15:00,740
64-bit system right now and this is

369
00:14:58,440 --> 00:15:03,709
going to be a problem freaking team

370
00:15:00,740 --> 00:15:06,660
can talk from the sides here we go

371
00:15:03,709 --> 00:15:09,990
resiliency so that's pretty much it for

372
00:15:06,660 --> 00:15:13,560
resources as far as we want to cover any

373
00:15:09,990 --> 00:15:18,920
other questions on resources okay now

374
00:15:13,560 --> 00:15:21,120
I'll actually have to all this live I

375
00:15:18,920 --> 00:15:24,000
don't know that I would necessarily call

376
00:15:21,120 --> 00:15:25,800
it common because a lot of malware isn't

377
00:15:24,000 --> 00:15:27,570
as big and complicated as Stuxnet and

378
00:15:25,800 --> 00:15:29,040
things like that are so they don't

379
00:15:27,570 --> 00:15:33,240
typically especially in the stocks that

380
00:15:29,040 --> 00:15:34,829
case is very much trying to jump air

381
00:15:33,240 --> 00:15:36,300
gaps and things like that so they knew

382
00:15:34,829 --> 00:15:37,589
that they couldn't you know the more

383
00:15:36,300 --> 00:15:39,240
common technique is that you know you

384
00:15:37,589 --> 00:15:40,800
download the modules that you need as

385
00:15:39,240 --> 00:15:42,240
you need them assuming you have internet

386
00:15:40,800 --> 00:15:44,519
connectivity right when you're trying to

387
00:15:42,240 --> 00:15:45,810
jump air gaps and move around behind the

388
00:15:44,519 --> 00:15:48,570
scenes you got to keep everything with

389
00:15:45,810 --> 00:15:54,029
you so that's one way to do it there is

390
00:15:48,570 --> 00:15:56,040
just regular general you know work

391
00:15:54,029 --> 00:15:58,589
organized crime type mail work where

392
00:15:56,040 --> 00:16:00,529
does this as well it's just I wouldn't

393
00:15:58,589 --> 00:16:03,360
say it's necessarily a common technique

394
00:16:00,529 --> 00:16:04,920
right there is a Game Club there isn't a

395
00:16:03,360 --> 00:16:07,079
game entry for resources right now

396
00:16:04,920 --> 00:16:09,839
actually because I want to get something

397
00:16:07,079 --> 00:16:11,820
decent before I knew I could just slap

398
00:16:09,839 --> 00:16:13,260
down something but I want to find

399
00:16:11,820 --> 00:16:15,570
something that's actually worth looking

400
00:16:13,260 --> 00:16:17,640
at so haven't thought of anything that's

401
00:16:15,570 --> 00:16:20,540
worth looking at yet for resources to

402
00:16:17,640 --> 00:16:20,540
randomize


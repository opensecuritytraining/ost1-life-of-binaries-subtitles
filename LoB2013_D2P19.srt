1
00:00:03,550 --> 00:00:09,370
all right so now we're going to do our

2
00:00:06,310 --> 00:00:11,350
drill though so you are here we're gonna

3
00:00:09,370 --> 00:00:19,000
drill down in the elf hider and it looks

4
00:00:11,350 --> 00:00:22,180
like this this is what the elf header

5
00:00:19,000 --> 00:00:23,770
looks like so we've got some offsets

6
00:00:22,180 --> 00:00:26,980
which are going to be file offsets and

7
00:00:23,770 --> 00:00:29,740
we've got some and we've got some HAP's

8
00:00:26,980 --> 00:00:31,810
which are going to be two biting things

9
00:00:29,740 --> 00:00:32,980
so what do we care about it here quite a

10
00:00:31,810 --> 00:00:36,070
bit that we care about it here but

11
00:00:32,980 --> 00:00:37,600
really you'll see that some of them

12
00:00:36,070 --> 00:00:38,230
there's about three things that are

13
00:00:37,600 --> 00:00:39,940
equivalent

14
00:00:38,230 --> 00:00:41,860
either the dass header or the NT ed ER

15
00:00:39,940 --> 00:00:43,930
and then the other stuff here is really

16
00:00:41,860 --> 00:00:45,790
just saying how do I get to the program

17
00:00:43,930 --> 00:00:49,720
headers or how do I get to these section

18
00:00:45,790 --> 00:00:50,950
headers all right so yeah ident right at

19
00:00:49,720 --> 00:00:53,140
the very beginning this would be the

20
00:00:50,950 --> 00:00:55,390
equivalent of our magic back in the daus

21
00:00:53,140 --> 00:00:56,649
header this is just a string that should

22
00:00:55,390 --> 00:00:58,180
always be at the very beginning of the

23
00:00:56,649 --> 00:01:00,940
file otherwise it's not an elf file

24
00:00:58,180 --> 00:01:04,149
won't be processed by the X by the OS

25
00:01:00,940 --> 00:01:06,280
loader and things like that so this

26
00:01:04,149 --> 00:01:10,179
should start with the 0th entry will be

27
00:01:06,280 --> 00:01:11,619
7f the one thumb tree will be e.l.f so

28
00:01:10,179 --> 00:01:13,450
when you're looking at something in hex

29
00:01:11,619 --> 00:01:15,579
view which you don't have to follow

30
00:01:13,450 --> 00:01:20,579
along for this I'm just gonna show it

31
00:01:15,579 --> 00:01:20,579
quick awesome password for the wind

32
00:01:21,389 --> 00:01:26,939
let's see I'm just looking at a hex view

33
00:01:24,399 --> 00:01:26,939
of a file

34
00:01:38,320 --> 00:01:44,060
all right just looking at the raw hex at

35
00:01:41,270 --> 00:01:46,969
it you should always see 7f followed by

36
00:01:44,060 --> 00:01:52,640
L as the first bytes of a legitimate L

37
00:01:46,969 --> 00:01:57,159
file right moving along the next thing

38
00:01:52,640 --> 00:02:00,890
that we're gonna have is a type and so

39
00:01:57,159 --> 00:02:03,409
this is kind of trying to say you can

40
00:02:00,890 --> 00:02:05,229
think of this type information as to

41
00:02:03,409 --> 00:02:07,850
some degree analogous to the

42
00:02:05,229 --> 00:02:10,549
characteristics on the file header back

43
00:02:07,850 --> 00:02:13,549
in the window stuff right so we've got

44
00:02:10,549 --> 00:02:14,810
is this a relocatable file meaning is it

45
00:02:13,549 --> 00:02:15,590
going to have relocations that need to

46
00:02:14,810 --> 00:02:17,900
be processed

47
00:02:15,590 --> 00:02:19,640
is it an executable file alright you

48
00:02:17,900 --> 00:02:21,650
won't see executable file in like a dot

49
00:02:19,640 --> 00:02:25,940
o file that the linker is using but you

50
00:02:21,650 --> 00:02:27,739
will see it is it a dying or dynamic

51
00:02:25,940 --> 00:02:29,180
file or dynamic linking file it's

52
00:02:27,739 --> 00:02:32,810
basically saying is it a shared object

53
00:02:29,180 --> 00:02:34,430
it's dll so this is saying it's

54
00:02:32,810 --> 00:02:36,799
something that has relocations is it

55
00:02:34,430 --> 00:02:37,069
executable is it a DLL or is it a core

56
00:02:36,799 --> 00:02:38,930
dump

57
00:02:37,069 --> 00:02:40,670
they actually reuse this format for core

58
00:02:38,930 --> 00:02:43,100
dumps if you're familiar with you know

59
00:02:40,670 --> 00:02:45,859
dumping memory out of out of refreshing

60
00:02:43,100 --> 00:02:48,220
programs and stuff like that so you can

61
00:02:45,859 --> 00:02:51,470
kind of think of the type as like the

62
00:02:48,220 --> 00:02:54,380
characteristics from the from the file

63
00:02:51,470 --> 00:02:56,720
header back npe alright now we've got

64
00:02:54,380 --> 00:02:58,880
the entry this is directly analogous to

65
00:02:56,720 --> 00:03:00,950
address of entry point in the peeve

66
00:02:58,880 --> 00:03:02,329
headers it's saying where's the first

67
00:03:00,950 --> 00:03:06,019
place that I'm going to start executing

68
00:03:02,329 --> 00:03:08,450
code there's no pls callbacks in Linux

69
00:03:06,019 --> 00:03:09,980
or in the elf file format so I can tell

70
00:03:08,450 --> 00:03:11,870
you this is going to be the first

71
00:03:09,980 --> 00:03:13,310
address that code is actually executed

72
00:03:11,870 --> 00:03:15,410
so if you put this in a debugger you

73
00:03:13,310 --> 00:03:16,970
look up this header information and you

74
00:03:15,410 --> 00:03:19,639
say I want to set a breakpoint at that

75
00:03:16,970 --> 00:03:23,180
and to be clear this is a virtual

76
00:03:19,639 --> 00:03:24,650
address not an RV a so you should be

77
00:03:23,180 --> 00:03:26,389
able to use it as is unless you're using

78
00:03:24,650 --> 00:03:27,709
a SLR and then you would need to take

79
00:03:26,389 --> 00:03:29,389
this and you'd have to say where did I

80
00:03:27,709 --> 00:03:31,400
actually get loaded figure out the

81
00:03:29,389 --> 00:03:34,880
difference yourself and then add that

82
00:03:31,400 --> 00:03:37,099
difference in so entry just where does

83
00:03:34,880 --> 00:03:38,329
code start alright now we've get into

84
00:03:37,099 --> 00:03:39,739
the stuff that tells us how to get to

85
00:03:38,329 --> 00:03:42,859
the program enters and tells us how to

86
00:03:39,739 --> 00:03:44,510
get to the section first write pH off

87
00:03:42,859 --> 00:03:46,280
program header is offset it's just

88
00:03:44,510 --> 00:03:48,519
saying what's the file offset to get to

89
00:03:46,280 --> 00:03:50,990
the program headers pretty simple

90
00:03:48,519 --> 00:03:52,640
section that are offset

91
00:03:50,990 --> 00:03:56,420
the file offset to get to the section

92
00:03:52,640 --> 00:03:58,550
ITER's all right very simple next you

93
00:03:56,420 --> 00:04:00,710
get information about how many program

94
00:03:58,550 --> 00:04:02,930
headers are there so how many segments

95
00:04:00,710 --> 00:04:05,900
basically are there going to be and so

96
00:04:02,930 --> 00:04:08,480
Eph Nam is saying this is the number of

97
00:04:05,900 --> 00:04:11,420
program headers contiguous starting at

98
00:04:08,480 --> 00:04:14,300
this particular right sorry starting at

99
00:04:11,420 --> 00:04:16,370
the pH offset so you don't have any sort

100
00:04:14,300 --> 00:04:18,290
of like initial metadata thing saying

101
00:04:16,370 --> 00:04:19,580
like here's my program headers and then

102
00:04:18,290 --> 00:04:21,380
there's an array afterwards it's just

103
00:04:19,580 --> 00:04:23,600
you jump straight to an array of program

104
00:04:21,380 --> 00:04:28,190
headers this would be kind of like the

105
00:04:23,600 --> 00:04:30,920
section headers that you have in it's a

106
00:04:28,190 --> 00:04:33,860
little it's a little it'll be a little

107
00:04:30,920 --> 00:04:35,720
confusing because sections and windows

108
00:04:33,860 --> 00:04:38,120
have some characteristics in common with

109
00:04:35,720 --> 00:04:40,940
segments here and some characteristics

110
00:04:38,120 --> 00:04:42,410
in common with sections so just the

111
00:04:40,940 --> 00:04:43,430
important thing is not to think when I

112
00:04:42,410 --> 00:04:48,230
say window section

113
00:04:43,430 --> 00:04:51,860
I mean elf section because they're not

114
00:04:48,230 --> 00:04:53,090
one-to-one all right but basically both

115
00:04:51,860 --> 00:04:54,770
the program headers and the section

116
00:04:53,090 --> 00:04:56,420
headers work the same way you get an

117
00:04:54,770 --> 00:04:59,300
offset in the file and then you've got

118
00:04:56,420 --> 00:05:02,210
an array of pH num programming errors

119
00:04:59,300 --> 00:05:04,130
you've got an array of SH num section

120
00:05:02,210 --> 00:05:05,690
errors all right and each of those is

121
00:05:04,130 --> 00:05:09,740
going to be more or less the same as

122
00:05:05,690 --> 00:05:11,780
with o section all right other stuff

123
00:05:09,740 --> 00:05:13,070
these are just you know the sizes of a

124
00:05:11,780 --> 00:05:14,840
particular thing they really shouldn't

125
00:05:13,070 --> 00:05:16,700
change the size of a program header for

126
00:05:14,840 --> 00:05:20,120
a 64 bit thing should be a fixed size

127
00:05:16,700 --> 00:05:21,470
shouldn't be manipulated but that kind

128
00:05:20,120 --> 00:05:23,450
of would potentially matter if you're

129
00:05:21,470 --> 00:05:25,130
dealing with 32 versus 64-bit thing

130
00:05:23,450 --> 00:05:27,200
maybe your thing uses this as the

131
00:05:25,130 --> 00:05:29,120
initial determination of whether you're

132
00:05:27,200 --> 00:05:31,370
dealing with a 32 or 64 bit headers

133
00:05:29,120 --> 00:05:33,710
right you can say what's the size if

134
00:05:31,370 --> 00:05:40,150
it's small and it's 32 if it's big that

135
00:05:33,710 --> 00:05:45,230
it's 64 and finally the last thing is

136
00:05:40,150 --> 00:05:46,160
this section header string index so

137
00:05:45,230 --> 00:05:47,810
there's going to be one particular

138
00:05:46,160 --> 00:05:53,000
section adder that's going to have a

139
00:05:47,810 --> 00:05:55,250
bunch of strings in it and we need to

140
00:05:53,000 --> 00:05:57,290
know which index into the section header

141
00:05:55,250 --> 00:05:58,790
array is that entry that has much

142
00:05:57,290 --> 00:06:01,340
strings because other stuff is going to

143
00:05:58,790 --> 00:06:03,020
say like I am some offset in from this

144
00:06:01,340 --> 00:06:04,790
string table is what it's called

145
00:06:03,020 --> 00:06:06,170
commonly so you've got the string table

146
00:06:04,790 --> 00:06:08,420
she's gonna hold all your strings and

147
00:06:06,170 --> 00:06:10,370
other things will say I'm you know this

148
00:06:08,420 --> 00:06:11,600
offset in from the string table so you

149
00:06:10,370 --> 00:06:14,360
need to know how to find the string

150
00:06:11,600 --> 00:06:17,090
table and this is what gets it for you

151
00:06:14,360 --> 00:06:18,770
you start at the e section header offset

152
00:06:17,090 --> 00:06:20,930
that'll tell you there's you know

153
00:06:18,770 --> 00:06:22,670
section orders start here then you'll

154
00:06:20,930 --> 00:06:25,430
say there's five section letters and

155
00:06:22,670 --> 00:06:28,240
then this will say at index two

156
00:06:25,430 --> 00:06:34,210
that's the string cable all right so

157
00:06:28,240 --> 00:06:37,340
it's terribly broken that's unfortunate

158
00:06:34,210 --> 00:06:39,410
I'll show that in real time then

159
00:06:37,340 --> 00:06:40,580
alright so retail this is the first

160
00:06:39,410 --> 00:06:43,520
thing we'll use if we want to get the

161
00:06:40,580 --> 00:06:47,320
retail headers we can just use retail -

162
00:06:43,520 --> 00:06:47,320
h-hello so


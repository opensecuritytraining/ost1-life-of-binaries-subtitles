1
00:00:04,210 --> 00:00:11,750
what's the likelihood that s cute look

2
00:00:07,700 --> 00:00:13,879
exponent every place is requesting the

3
00:00:11,750 --> 00:00:15,469
likelihood that executable gets loaded

4
00:00:13,879 --> 00:00:18,350
at the place it requests in memory

5
00:00:15,469 --> 00:00:23,900
depends on whether a Lazar is turned on

6
00:00:18,350 --> 00:00:25,460
overall the LS art it's a SLR SLR is

7
00:00:23,900 --> 00:00:27,890
turned on for the operating system

8
00:00:25,460 --> 00:00:31,999
overall so if it's turned on overall

9
00:00:27,890 --> 00:00:34,070
then then it depends on whether the

10
00:00:31,999 --> 00:00:39,500
flags are set for this particular binary

11
00:00:34,070 --> 00:00:41,270
saying I support ALS our SLR and if

12
00:00:39,500 --> 00:00:44,240
those flags are set so where are those

13
00:00:41,270 --> 00:00:46,700
flags going back to the right so here's

14
00:00:44,240 --> 00:00:49,000
ms paint right and we need to go find

15
00:00:46,700 --> 00:00:51,860
those flags back in the optional header

16
00:00:49,000 --> 00:00:54,860
there's the dynamic bass flag and that

17
00:00:51,860 --> 00:00:56,570
says 0s you can go ahead and move me

18
00:00:54,860 --> 00:00:58,790
around if you want if you're if you

19
00:00:56,570 --> 00:01:01,820
currently using ASL are you can go ahead

20
00:00:58,790 --> 00:01:03,500
and move me around and so then the

21
00:01:01,820 --> 00:01:05,540
likelihood is basically one hundred

22
00:01:03,500 --> 00:01:07,549
percent it's not one hundred percent

23
00:01:05,540 --> 00:01:10,579
codes you know certain number of slots

24
00:01:07,549 --> 00:01:13,090
that the OS can put it into today 64-bit

25
00:01:10,579 --> 00:01:17,140
or 32-bit and the probability that

26
00:01:13,090 --> 00:01:19,329
doesn't get the slot is this one over

27
00:01:17,140 --> 00:01:23,509
many slots there are something like that

28
00:01:19,329 --> 00:01:26,600
yes strata modern exploits wait operate

29
00:01:23,509 --> 00:01:28,670
past as how hard to date I think he

30
00:01:26,600 --> 00:01:32,569
exploits class no do the other place to

31
00:01:28,670 --> 00:01:34,490
change thought that that's setting their

32
00:01:32,569 --> 00:01:36,469
know so they don't actually end up

33
00:01:34,490 --> 00:01:37,759
changing this it's more the most common

34
00:01:36,469 --> 00:01:40,609
way and what you'll see in the exploits

35
00:01:37,759 --> 00:01:42,829
to class does the easy way around of it

36
00:01:40,609 --> 00:01:45,439
is you just find somebody who didn't set

37
00:01:42,829 --> 00:01:48,049
this flag like the flash plugin and then

38
00:01:45,439 --> 00:01:50,630
they are saying to the OS please don't

39
00:01:48,049 --> 00:01:52,520
move me around I'm going to break and

40
00:01:50,630 --> 00:01:54,950
because of that may get the base address

41
00:01:52,520 --> 00:01:58,280
that they asked for and then with high

42
00:01:54,950 --> 00:01:59,929
probability especially if it's an exe he

43
00:01:58,280 --> 00:02:01,429
cuts the Basie dresser wants if it's a

44
00:01:59,929 --> 00:02:04,850
dll it maybe it does maybe it doesn't

45
00:02:01,429 --> 00:02:06,950
but the flash plugins case as you see in

46
00:02:04,850 --> 00:02:10,039
the exploits to class gets the base

47
00:02:06,950 --> 00:02:12,260
address it wants and then they just know

48
00:02:10,039 --> 00:02:14,730
where some code is and then they'll use

49
00:02:12,260 --> 00:02:16,200
snippets of the code and chain

50
00:02:14,730 --> 00:02:17,489
together it's called return oriented

51
00:02:16,200 --> 00:02:19,830
programming you just take little

52
00:02:17,489 --> 00:02:21,659
snippets of the code and you you make it

53
00:02:19,830 --> 00:02:26,849
so that you execute some sequence of

54
00:02:21,659 --> 00:02:28,470
these civets the other way is that the

55
00:02:26,849 --> 00:02:31,160
other way is even if a thing is

56
00:02:28,470 --> 00:02:33,209
randomized if you have an exploit that

57
00:02:31,160 --> 00:02:34,680
doesn't just give you arbitrary right

58
00:02:33,209 --> 00:02:37,349
permission to gives you arbitrary read

59
00:02:34,680 --> 00:02:39,840
permission if you can read some values

60
00:02:37,349 --> 00:02:42,120
from memory you can basically figure out

61
00:02:39,840 --> 00:02:44,370
where stuff ended up getting foot so you

62
00:02:42,120 --> 00:02:46,709
can figure out you know it's randomized

63
00:02:44,370 --> 00:02:48,510
and it's randomized so basically per run

64
00:02:46,709 --> 00:02:50,099
so you load up the executable and stuff

65
00:02:48,510 --> 00:02:52,530
is randomized its put in a particular

66
00:02:50,099 --> 00:02:54,390
place and then you know that kid stays

67
00:02:52,530 --> 00:02:56,129
there forever until the things

68
00:02:54,390 --> 00:02:58,709
terminated and restarted for instance

69
00:02:56,129 --> 00:03:00,420
and so if you can read memory let's say

70
00:02:58,709 --> 00:03:02,519
you have arbitrary read and you can keep

71
00:03:00,420 --> 00:03:08,310
reading without crashing you know you

72
00:03:02,519 --> 00:03:13,140
can read it offset for instance you

73
00:03:08,310 --> 00:03:14,760
would do you know you'd say let you'd be

74
00:03:13,140 --> 00:03:16,470
basically trying to profile like where

75
00:03:14,760 --> 00:03:19,739
are these different dll is loaded in

76
00:03:16,470 --> 00:03:23,280
memory so you'd maybe read at eh 000

77
00:03:19,739 --> 00:03:24,900
than 900 and eat a zero zozo and you can

78
00:03:23,280 --> 00:03:28,200
literally like bounce around trying to

79
00:03:24,900 --> 00:03:29,849
find the particular dll that you're

80
00:03:28,200 --> 00:03:31,639
looking for in memory by like just

81
00:03:29,849 --> 00:03:34,680
trying all the possible addresses or

82
00:03:31,639 --> 00:03:36,630
some relative of us relative to some

83
00:03:34,680 --> 00:03:37,889
function you just if you can find the

84
00:03:36,630 --> 00:03:40,109
address of a function and you can look

85
00:03:37,889 --> 00:03:41,910
it up that way so you break it typically

86
00:03:40,109 --> 00:03:45,209
either by exploiting things that are not

87
00:03:41,910 --> 00:03:48,000
is a lard or by having an arbitrary read

88
00:03:45,209 --> 00:03:49,470
which leaks memory leaks information

89
00:03:48,000 --> 00:03:50,970
about where stuff is in memory and then

90
00:03:49,470 --> 00:03:53,030
once you've got the information and you

91
00:03:50,970 --> 00:03:58,169
code it according to that information

92
00:03:53,030 --> 00:04:03,389
watch out to fire yeah is that I'll give

93
00:03:58,169 --> 00:04:07,590
one burst with a SLR does that move

94
00:04:03,389 --> 00:04:09,389
around the entire module it doesn't mix

95
00:04:07,590 --> 00:04:12,569
things around that's actually a sex

96
00:04:09,389 --> 00:04:13,949
module granularity yep so some sums once

97
00:04:12,569 --> 00:04:16,650
you've once you get one book than

98
00:04:13,949 --> 00:04:18,329
everything else resolved yep exactly and

99
00:04:16,650 --> 00:04:20,920
actually the interesting thing is that I

100
00:04:18,329 --> 00:04:23,620
didn't think this was the case and

101
00:04:20,920 --> 00:04:28,060
so you know let's confirm whether it's

102
00:04:23,620 --> 00:04:31,780
true or not I thought that on Windows 7

103
00:04:28,060 --> 00:04:33,490
by default even it didn't map stuff into

104
00:04:31,780 --> 00:04:35,890
the same virtual memory address across

105
00:04:33,490 --> 00:04:39,460
things so I said before that you know

106
00:04:35,890 --> 00:04:41,050
the OS tries to you know optimize the

107
00:04:39,460 --> 00:04:44,380
use of physical memory by saying I'm

108
00:04:41,050 --> 00:04:46,180
going to copy you know kernel32.dll into

109
00:04:44,380 --> 00:04:47,680
physical memory once and then anybody

110
00:04:46,180 --> 00:04:49,570
who wants to use it i'll point their

111
00:04:47,680 --> 00:04:51,640
virtual memory at that physical memory i

112
00:04:49,570 --> 00:04:53,860
thought that it would potentially use

113
00:04:51,640 --> 00:04:55,780
different virtual memory addresses for

114
00:04:53,860 --> 00:04:58,390
that same physical memory address like

115
00:04:55,780 --> 00:05:01,330
so this guy would get it @ p8 00088

116
00:04:58,390 --> 00:05:03,550
kernel32 that guy would get you 900 for

117
00:05:01,330 --> 00:05:06,670
parent Larry too but I was looking in

118
00:05:03,550 --> 00:05:08,320
process explorer the other day based on

119
00:05:06,670 --> 00:05:10,990
someone asserting that what I was

120
00:05:08,320 --> 00:05:19,930
thinking was not the case actually I

121
00:05:10,990 --> 00:05:22,720
probably and so in process Explorer you

122
00:05:19,930 --> 00:05:25,380
can see the DLLs and secrets and choose

123
00:05:22,720 --> 00:05:25,380
the columns

124
00:05:28,870 --> 00:05:34,840
but columns a lot base address where the

125
00:05:32,680 --> 00:05:36,310
thing is loaded in memory so here's now

126
00:05:34,840 --> 00:05:38,560
the addresses where stuff is actually

127
00:05:36,310 --> 00:05:41,530
loaded in memory and so I was thinking

128
00:05:38,560 --> 00:05:43,510
that for instance like active EDS dll

129
00:05:41,530 --> 00:05:46,210
and this guy's process would be at a

130
00:05:43,510 --> 00:05:47,890
different location than active EDS and a

131
00:05:46,210 --> 00:05:50,530
different gods process so these are two

132
00:05:47,890 --> 00:05:53,850
separate process is the same process but

133
00:05:50,530 --> 00:05:59,770
they're different isolated memory spaces

134
00:05:53,850 --> 00:06:06,990
7f p f a 7 b and instead as an imac the

135
00:05:59,770 --> 00:06:12,100
radius alright advanced api 32p 48

136
00:06:06,990 --> 00:06:21,460
that's it got 31 as they've got 32

137
00:06:12,100 --> 00:06:25,570
that's 32 bit yeah so advanced API 32 is

138
00:06:21,460 --> 00:06:29,860
at seven PG 48 that process and events

139
00:06:25,570 --> 00:06:31,930
API 32 is at seven PG 48 this process so

140
00:06:29,860 --> 00:06:34,300
this is part of why like if you have an

141
00:06:31,930 --> 00:06:35,890
arbitrary read and you know one program

142
00:06:34,300 --> 00:06:38,410
it can even be different programs if you

143
00:06:35,890 --> 00:06:41,620
can inspect the memory addresses range

144
00:06:38,410 --> 00:06:43,210
from one process with a read you even if

145
00:06:41,620 --> 00:06:45,340
you're exploiting something else because

146
00:06:43,210 --> 00:06:47,320
they're randomized the randomized at

147
00:06:45,340 --> 00:06:48,730
load time but then it's reusing this

148
00:06:47,320 --> 00:06:50,620
virtual address across different

149
00:06:48,730 --> 00:06:52,240
processes as well you can make

150
00:06:50,620 --> 00:06:53,740
assumptions that you know if you're

151
00:06:52,240 --> 00:06:55,060
looking for this dll in that process

152
00:06:53,740 --> 00:06:58,660
over there that it's gonna be at the

153
00:06:55,060 --> 00:07:00,460
same base address but yes so some of the

154
00:06:58,660 --> 00:07:03,070
more advanced research in kind of things

155
00:07:00,460 --> 00:07:04,840
like there's a ir book project that's

156
00:07:03,070 --> 00:07:07,420
trying to take a bunch of academic work

157
00:07:04,840 --> 00:07:09,360
and they do sort of more fine-grained

158
00:07:07,420 --> 00:07:11,770
randomization where they will like take

159
00:07:09,360 --> 00:07:14,080
individual functions and randomize the

160
00:07:11,770 --> 00:07:15,940
order of those things take the variables

161
00:07:14,080 --> 00:07:17,950
on the stack and randomized things like

162
00:07:15,940 --> 00:07:20,020
that so there's a lot of academic work

163
00:07:17,950 --> 00:07:24,640
on that trying to do more fine-grained a

164
00:07:20,020 --> 00:07:26,470
Lazar a SLR but you incur performance

165
00:07:24,640 --> 00:07:28,630
overhead obviously so it's one thing to

166
00:07:26,470 --> 00:07:30,490
just like move the blob here versus

167
00:07:28,630 --> 00:07:32,890
there it's another to start you know

168
00:07:30,490 --> 00:07:35,300
randomizing at run time to start you

169
00:07:32,890 --> 00:07:38,240
know slicing up little pieces

170
00:07:35,300 --> 00:07:40,190
in place so it's really Microsoft is

171
00:07:38,240 --> 00:07:42,590
basically the reason why we have this

172
00:07:40,190 --> 00:07:43,970
for instance even just that it's reusing

173
00:07:42,590 --> 00:07:46,250
this virtual address across different

174
00:07:43,970 --> 00:07:47,900
things I believe it's because of the

175
00:07:46,250 --> 00:07:49,159
performance implications of using

176
00:07:47,900 --> 00:07:51,349
different virtual addresses means

177
00:07:49,159 --> 00:07:53,319
different processes would have different

178
00:07:51,349 --> 00:07:55,759
cache misses and stuff like that so

179
00:07:53,319 --> 00:07:58,099
Microsoft's doing the balancing act

180
00:07:55,759 --> 00:08:01,729
between performance and security here

181
00:07:58,099 --> 00:08:03,979
now I think windows 8 definitely has

182
00:08:01,729 --> 00:08:06,129
more randomization so I do think that

183
00:08:03,979 --> 00:08:08,479
windows 8 they're actually doing

184
00:08:06,129 --> 00:08:11,090
interprocess randomization of the base

185
00:08:08,479 --> 00:08:13,759
addresses but don't quote me on that

186
00:08:11,090 --> 00:08:17,960
till I get it in in my hand I just think

187
00:08:13,759 --> 00:08:21,580
I read that somewhere all right so

188
00:08:17,960 --> 00:08:21,580
anyways that was delayed


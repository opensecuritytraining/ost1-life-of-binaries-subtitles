1
00:00:03,540 --> 00:00:10,680
pop quiz hotshots don't need I don't

2
00:00:09,210 --> 00:00:12,410
think there's enough ways here that I

3
00:00:10,680 --> 00:00:14,670
can get answers from everybody but

4
00:00:12,410 --> 00:00:18,090
somebody throw out a way that these

5
00:00:14,670 --> 00:00:23,239
sections in elf are similar or different

6
00:00:18,090 --> 00:00:23,239
to the sections in EE files

7
00:00:29,340 --> 00:00:31,400
you

8
00:00:31,790 --> 00:00:38,280
anybody have any thoughts yes names mice

9
00:00:36,390 --> 00:00:40,290
this is where we find the actual name

10
00:00:38,280 --> 00:00:42,170
segments have no mames they have no

11
00:00:40,290 --> 00:00:45,960
notion of dot txt or anything like that

12
00:00:42,170 --> 00:00:47,879
these sections do have names and how are

13
00:00:45,960 --> 00:00:51,960
those names different someone else then

14
00:00:47,879 --> 00:00:54,720
the names that we have an LP files all

15
00:00:51,960 --> 00:00:56,580
rights training events not just a bite

16
00:00:54,720 --> 00:00:58,530
they're a pointer off to that string

17
00:00:56,580 --> 00:01:00,540
table so they can be as big as you want

18
00:00:58,530 --> 00:01:02,059
don't terminate in straight all right

19
00:01:00,540 --> 00:01:06,450
another way that similar or different

20
00:01:02,059 --> 00:01:09,440
raising your it I was trying to map

21
00:01:06,450 --> 00:01:12,810
between these two so I think SH ad or

22
00:01:09,440 --> 00:01:14,610
similar to virtual address yep SI

23
00:01:12,810 --> 00:01:16,610
chatter is similar to the virtual

24
00:01:14,610 --> 00:01:18,750
address in the section edit and then

25
00:01:16,610 --> 00:01:20,640
continuing on with that thread what's

26
00:01:18,750 --> 00:01:22,830
the other one that's similar there is a

27
00:01:20,640 --> 00:01:25,229
job that would be similar to the one or

28
00:01:22,830 --> 00:01:26,700
two raw data yep exactly s a job set

29
00:01:25,229 --> 00:01:28,950
would be pointed to raw data so you've

30
00:01:26,700 --> 00:01:32,220
got a virtual address and a pointer to

31
00:01:28,950 --> 00:01:35,420
raw data that Samsung applies for what's

32
00:01:32,220 --> 00:01:39,750
right as well as Google yep so although

33
00:01:35,420 --> 00:01:42,390
the flags in these sections here they do

34
00:01:39,750 --> 00:01:44,490
have that one writable flag and there's

35
00:01:42,390 --> 00:01:46,320
this X Cuba which i think is 4x Google

36
00:01:44,490 --> 00:01:48,750
instructions but like this airlock flag

37
00:01:46,320 --> 00:01:50,640
is quite a bit different that's well I

38
00:01:48,750 --> 00:01:53,130
guess that's similar to the discardable

39
00:01:50,640 --> 00:01:54,689
flag so yeah kind of we have we do have

40
00:01:53,130 --> 00:01:56,790
some of the same flags that we were

41
00:01:54,689 --> 00:01:58,530
missing in the segment's segments only

42
00:01:56,790 --> 00:02:00,479
have read write execute right and so

43
00:01:58,530 --> 00:02:03,540
some additional miscellaneous flags show

44
00:02:00,479 --> 00:02:05,100
up here at the section level yeah all

45
00:02:03,540 --> 00:02:13,080
right any other similarities or

46
00:02:05,100 --> 00:02:15,150
differences yep X section held code so I

47
00:02:13,080 --> 00:02:17,220
mean so we don't necessarily know that

48
00:02:15,150 --> 00:02:19,650
the text section is just set to probe

49
00:02:17,220 --> 00:02:20,940
bits but if we went in and we dug into

50
00:02:19,650 --> 00:02:22,860
it we would see they're using the same

51
00:02:20,940 --> 00:02:25,769
conventions for naming that they call

52
00:02:22,860 --> 00:02:28,410
the doc text section the code stuff any

53
00:02:25,769 --> 00:02:30,930
other similarities or differences the

54
00:02:28,410 --> 00:02:33,030
section songs tight we know sections

55
00:02:30,930 --> 00:02:34,620
don't have this type thing right so the

56
00:02:33,030 --> 00:02:36,989
type is kind of saying like is this

57
00:02:34,620 --> 00:02:40,230
probe it's which is generic catch-all or

58
00:02:36,989 --> 00:02:44,680
some very specific section is it dynamic

59
00:02:40,230 --> 00:02:47,709
right is it rel a which is

60
00:02:44,680 --> 00:02:50,680
no relocations actually I believed rally

61
00:02:47,709 --> 00:02:53,079
at the relocation stuff right same sort

62
00:02:50,680 --> 00:02:56,349
of thing as relocations and other things

63
00:02:53,079 --> 00:02:58,689
string table right so there's special

64
00:02:56,349 --> 00:03:00,129
types of sections and then there's just

65
00:02:58,689 --> 00:03:06,060
catch all sections everything is a

66
00:03:00,129 --> 00:03:06,060
catch-all section in in P files right


1
00:00:08,580 --> 00:00:14,370
all right so no compiler drill down

2
00:00:11,730 --> 00:00:16,920
there's tomorrow so we are just going to

3
00:00:14,370 --> 00:00:22,910
kind of skip to what I wanted to show

4
00:00:16,920 --> 00:00:25,410
with this newer form of Trees physical

5
00:00:22,910 --> 00:00:27,720
so I would have told you about all these

6
00:00:25,410 --> 00:00:29,369
different types of trees like so this is

7
00:00:27,720 --> 00:00:31,739
a simple tree that would have been

8
00:00:29,369 --> 00:00:34,620
parsed and it would have said no you've

9
00:00:31,739 --> 00:00:37,440
got some register and you're adding more

10
00:00:34,620 --> 00:00:39,060
to it so here got the frame corner so

11
00:00:37,440 --> 00:00:40,829
we're gonna frame four plus four X

12
00:00:39,060 --> 00:00:48,210
eighty-six this would be like me DP plus

13
00:00:40,829 --> 00:00:49,620
four and so basically how how someone

14
00:00:48,210 --> 00:00:51,870
tried to parse this sort of tree would

15
00:00:49,620 --> 00:00:54,600
go is they new kind of a depth-first

16
00:00:51,870 --> 00:00:56,429
search so they'd go to the to the left

17
00:00:54,600 --> 00:00:58,530
first they'd go down to this entry and

18
00:00:56,429 --> 00:01:01,980
they'd say okay I have this register

19
00:00:58,530 --> 00:01:03,570
register entry and so based on that I'm

20
00:01:01,980 --> 00:01:06,510
going to spit out some particular

21
00:01:03,570 --> 00:01:08,370
assembly for my language and so the

22
00:01:06,510 --> 00:01:11,190
point is the tree is supposed to be more

23
00:01:08,370 --> 00:01:12,660
of an abstract representation and then

24
00:01:11,190 --> 00:01:14,310
from there you spit out whatever is

25
00:01:12,660 --> 00:01:16,710
appropriate for your given assembly

26
00:01:14,310 --> 00:01:19,740
language whether it's x86 arm RPC

27
00:01:16,710 --> 00:01:22,380
whatever so you just say register frame

28
00:01:19,740 --> 00:01:24,960
pointer okay on x86 I'm going to push

29
00:01:22,380 --> 00:01:27,120
EBP and then you go up and then you go

30
00:01:24,960 --> 00:01:30,540
down the other branch and you say

31
00:01:27,120 --> 00:01:33,330
constant for okay I'm going to push this

32
00:01:30,540 --> 00:01:36,780
floor onto the stack and then you go up

33
00:01:33,330 --> 00:01:39,300
and you do plus and so then you you put

34
00:01:36,780 --> 00:01:41,430
some code that's pops EXO it's going to

35
00:01:39,300 --> 00:01:43,020
take this for I was just pushed onto the

36
00:01:41,430 --> 00:01:44,940
stack and it's going to pop it off into

37
00:01:43,020 --> 00:01:47,070
a excellent sort of like just doing a x

38
00:01:44,940 --> 00:01:51,150
equals four and then you're going to do

39
00:01:47,070 --> 00:01:55,350
ad and then square brackets ESP this was

40
00:01:51,150 --> 00:01:57,420
in our in our intel assembly the syntax

41
00:01:55,350 --> 00:01:58,590
square brackets basically meant take

42
00:01:57,420 --> 00:02:01,080
whatever's inside the square brackets

43
00:01:58,590 --> 00:02:02,370
and go to memory treat that as a memory

44
00:02:01,080 --> 00:02:03,240
address and go and remember and pull

45
00:02:02,370 --> 00:02:05,610
something out of there

46
00:02:03,240 --> 00:02:09,330
so we basically would be saying at ESP

47
00:02:05,610 --> 00:02:10,769
this would be the the value of EBP you

48
00:02:09,330 --> 00:02:14,610
to push here and so we're going to say

49
00:02:10,769 --> 00:02:17,310
at ESP we're going to take and pull that

50
00:02:14,610 --> 00:02:19,710
out add that to EI X which is four put

51
00:02:17,310 --> 00:02:20,880
them together and then we're going to

52
00:02:19,710 --> 00:02:24,420
store them back into

53
00:02:20,880 --> 00:02:27,060
ESP the top of the stack so again as I

54
00:02:24,420 --> 00:02:29,640
said I'm not assuming you know assembly

55
00:02:27,060 --> 00:02:32,130
for this class so this is really more to

56
00:02:29,640 --> 00:02:34,740
show that there's some particular way

57
00:02:32,130 --> 00:02:39,660
that the compiler will parse this tree

58
00:02:34,740 --> 00:02:41,580
and spit out a expected assembly in

59
00:02:39,660 --> 00:02:43,650
order to always be able to handle this

60
00:02:41,580 --> 00:02:46,800
in a generic way so you can make these

61
00:02:43,650 --> 00:02:50,600
trees as complicated as you want but

62
00:02:46,800 --> 00:02:53,130
it's still just using the same sort of

63
00:02:50,600 --> 00:02:55,170
Roche method of you know it's another

64
00:02:53,130 --> 00:02:57,780
weird equipment it's just parses the

65
00:02:55,170 --> 00:02:59,940
tree and it spits out mixed assembly and

66
00:02:57,780 --> 00:03:04,670
that will generate and do whatever the

67
00:02:59,940 --> 00:03:06,750
tree was trying to accomplish all right

68
00:03:04,670 --> 00:03:08,780
so that's as far as I'm going to get

69
00:03:06,750 --> 00:03:14,120
into that any questions on that

70
00:03:08,780 --> 00:03:14,120
like you know seconds of talking about


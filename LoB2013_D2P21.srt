1
00:00:03,530 --> 00:00:10,099
all right so drilling down into the

2
00:00:07,189 --> 00:00:13,330
program headers this is what an

3
00:00:10,099 --> 00:00:15,679
individual program header looks like and

4
00:00:13,330 --> 00:00:21,920
we pretty much care about everything in

5
00:00:15,679 --> 00:00:25,820
there so beginning at the very start

6
00:00:21,920 --> 00:00:27,650
there's the p-type so the most important

7
00:00:25,820 --> 00:00:29,870
thing the most important type of program

8
00:00:27,650 --> 00:00:32,840
header that we care about as type PT

9
00:00:29,870 --> 00:00:35,030
load and this is where segments okay so

10
00:00:32,840 --> 00:00:37,219
let me back up one second program

11
00:00:35,030 --> 00:00:39,140
headers are going to define a what we're

12
00:00:37,219 --> 00:00:40,190
gonna call a segment and so they're

13
00:00:39,140 --> 00:00:41,780
gonna have a start they're gonna have

14
00:00:40,190 --> 00:00:47,449
some end it's going to be some chunk of

15
00:00:41,780 --> 00:00:49,909
the of the file itself but only in the

16
00:00:47,449 --> 00:00:52,370
case where a program header defines a PT

17
00:00:49,909 --> 00:00:54,010
load segment is this something which is

18
00:00:52,370 --> 00:00:57,050
going to actually get mapped into memory

19
00:00:54,010 --> 00:00:58,609
so only PT load segments are things

20
00:00:57,050 --> 00:01:01,550
actually going where we're saying start

21
00:00:58,609 --> 00:01:03,229
and OS loader put that into memory and

22
00:01:01,550 --> 00:01:04,879
we're gonna you know execute from there

23
00:01:03,229 --> 00:01:07,220
later so we'll see later on with the

24
00:01:04,879 --> 00:01:08,840
packed code that you know we'll see with

25
00:01:07,220 --> 00:01:11,270
normal code yes they'll have all sorts

26
00:01:08,840 --> 00:01:12,890
of these different types of segments but

27
00:01:11,270 --> 00:01:15,560
with packed code they can get away with

28
00:01:12,890 --> 00:01:17,420
just having two loaded segments and then

29
00:01:15,560 --> 00:01:20,119
they're able to achieve their goals just

30
00:01:17,420 --> 00:01:23,869
fine so everything is really superfluous

31
00:01:20,119 --> 00:01:26,360
except for the for the load segments but

32
00:01:23,869 --> 00:01:29,719
how normal programs use these other

33
00:01:26,360 --> 00:01:30,920
segments is that PT dynamic so if we so

34
00:01:29,719 --> 00:01:32,810
we've got a program header and it can

35
00:01:30,920 --> 00:01:34,759
say like this program header is going to

36
00:01:32,810 --> 00:01:36,820
define some range and that's the stuff

37
00:01:34,759 --> 00:01:39,350
that's going to get loaded into memory

38
00:01:36,820 --> 00:01:42,259
alternatively a program header can say

39
00:01:39,350 --> 00:01:46,609
I'm defining a PT dynamic segment and

40
00:01:42,259 --> 00:01:51,590
this is going to hold dynamic linking

41
00:01:46,609 --> 00:01:58,340
information and yeah dynamic linker

42
00:01:51,590 --> 00:01:59,749
that's right and so what we'll see the

43
00:01:58,340 --> 00:02:03,259
original intent of this and the P

44
00:01:59,749 --> 00:02:05,530
specification it I'm skipping ahead in

45
00:02:03,259 --> 00:02:08,630
my mind to incur okay

46
00:02:05,530 --> 00:02:10,130
yeah PT dynamic holds dynamic linking

47
00:02:08,630 --> 00:02:11,720
information you can think of this as

48
00:02:10,130 --> 00:02:13,430
it's going to hold some information

49
00:02:11,720 --> 00:02:16,680
related to the equivalent of the

50
00:02:13,430 --> 00:02:19,260
imported rest table area

51
00:02:16,680 --> 00:02:22,409
right PT interp is the thing I was

52
00:02:19,260 --> 00:02:24,720
trying to jump forward you this is going

53
00:02:22,409 --> 00:02:27,150
to have this segment is going to have

54
00:02:24,720 --> 00:02:28,739
just a literal string in it and this

55
00:02:27,150 --> 00:02:31,079
literal string is going to be the quote

56
00:02:28,739 --> 00:02:34,109
unquote interpreter for this particular

57
00:02:31,079 --> 00:02:35,099
help file so interpreter could mean like

58
00:02:34,109 --> 00:02:37,019
the kind of thing you're used to

59
00:02:35,099 --> 00:02:38,760
thinking of it could be a purl it could

60
00:02:37,019 --> 00:02:40,950
be a Python interpreter stuff like that

61
00:02:38,760 --> 00:02:44,010
but in the context of normal binary

62
00:02:40,950 --> 00:02:46,439
executables the interpreter is actually

63
00:02:44,010 --> 00:02:48,269
the dynamic linker so what happens is

64
00:02:46,439 --> 00:02:50,220
the OS loader will read this string

65
00:02:48,269 --> 00:02:51,930
it'll load up the dynamic linker and

66
00:02:50,220 --> 00:02:54,510
it'll actually put the dynamic linker

67
00:02:51,930 --> 00:02:56,040
into memory first and then it'll load up

68
00:02:54,510 --> 00:02:58,769
your program according to those load

69
00:02:56,040 --> 00:03:00,299
segments and then it'll say okay let's

70
00:02:58,769 --> 00:03:02,400
go ahead and run through here and oh now

71
00:03:00,299 --> 00:03:04,439
hopefully I have the dynamic linker in

72
00:03:02,400 --> 00:03:06,120
my memory so the dynamic linker can go

73
00:03:04,439 --> 00:03:08,220
fill in the equivalent of the import

74
00:03:06,120 --> 00:03:09,569
address to all of those function

75
00:03:08,220 --> 00:03:11,879
pointers that you need pointing out

76
00:03:09,569 --> 00:03:13,560
external libraries so when you've got an

77
00:03:11,879 --> 00:03:15,239
intersection it's basically just going

78
00:03:13,560 --> 00:03:17,129
to be a string to some binary on disk

79
00:03:15,239 --> 00:03:18,299
somewhere in the file system and it's

80
00:03:17,129 --> 00:03:19,349
going to be something that the OS is

81
00:03:18,299 --> 00:03:21,449
going to say I need to run the

82
00:03:19,349 --> 00:03:23,790
interpreter before I run this binary or

83
00:03:21,449 --> 00:03:25,650
before I run this anything but in the

84
00:03:23,790 --> 00:03:29,730
context of binaries the interpreter is

85
00:03:25,650 --> 00:03:32,549
the dynamic linker alright and then the

86
00:03:29,730 --> 00:03:35,639
last one we actually care about is the P

87
00:03:32,549 --> 00:03:37,019
header thing and this really doesn't

88
00:03:35,639 --> 00:03:39,810
matter but we're just putting it here

89
00:03:37,019 --> 00:03:43,019
because you'll see it on commonly and so

90
00:03:39,810 --> 00:03:45,269
PT P header is saying this is a segment

91
00:03:43,019 --> 00:03:48,989
that has my program headers in it so

92
00:03:45,269 --> 00:03:50,579
that array that we showed right here we

93
00:03:48,989 --> 00:03:53,940
could have a segment that says this

94
00:03:50,579 --> 00:03:56,639
chunk of the file is a segment for the

95
00:03:53,940 --> 00:03:58,139
program header information but since we

96
00:03:56,639 --> 00:04:04,799
already have all that information in the

97
00:03:58,139 --> 00:04:07,290
elf headers it's kind of all right so

98
00:04:04,799 --> 00:04:08,639
that's just the type so again what we

99
00:04:07,290 --> 00:04:10,199
really care about is the load type

100
00:04:08,639 --> 00:04:11,669
that's going to say here's a chunk of

101
00:04:10,199 --> 00:04:14,220
file that's going to get mapped up into

102
00:04:11,669 --> 00:04:15,930
memory dynamic has dynamic linking

103
00:04:14,220 --> 00:04:18,720
information so it's going to cover some

104
00:04:15,930 --> 00:04:19,470
region that has to do with imports and

105
00:04:18,720 --> 00:04:22,770
stuff like that

106
00:04:19,470 --> 00:04:24,240
and Pt interp is the interpreter which

107
00:04:22,770 --> 00:04:26,580
in the context of binaries is the

108
00:04:24,240 --> 00:04:29,639
dynamic linker that gets loaded before

109
00:04:26,580 --> 00:04:30,600
your normal protocol and we're just

110
00:04:29,639 --> 00:04:34,810
going to

111
00:04:30,600 --> 00:04:36,430
all right P offset still we're in

112
00:04:34,810 --> 00:04:39,630
program header still we're talking about

113
00:04:36,430 --> 00:04:42,580
a particular segment of the file on disk

114
00:04:39,630 --> 00:04:44,110
the offset is saying where you want the

115
00:04:42,580 --> 00:04:46,630
segment to start so this is just saying

116
00:04:44,110 --> 00:04:48,730
segment starts at P offset so this could

117
00:04:46,630 --> 00:04:53,740
be offset zero saying start at zero and

118
00:04:48,730 --> 00:04:55,210
go to or something and PV adder is where

119
00:04:53,740 --> 00:04:58,450
this thing is going to be mapped into

120
00:04:55,210 --> 00:04:59,950
memory so based on this what would

121
00:04:58,450 --> 00:05:01,480
people say that this would be equivalent

122
00:04:59,950 --> 00:05:04,420
to back in P files

123
00:05:01,480 --> 00:05:06,400
what do we think the thing that's saying

124
00:05:04,420 --> 00:05:08,320
this is where my stuff that's going to

125
00:05:06,400 --> 00:05:18,430
be mapped into memory which field is

126
00:05:08,320 --> 00:05:22,380
that in a segment header yeah you're on

127
00:05:18,430 --> 00:05:25,980
the right track pointer

128
00:05:22,380 --> 00:05:28,930
something-something pointer to raw data

129
00:05:25,980 --> 00:05:31,240
right you know section headers we had

130
00:05:28,930 --> 00:05:33,160
pointer to raw data that says my raw

131
00:05:31,240 --> 00:05:35,410
data in this section is going to start

132
00:05:33,160 --> 00:05:38,050
right here and I'm gonna map it into

133
00:05:35,410 --> 00:05:40,990
memory so what would the PV address V

134
00:05:38,050 --> 00:05:43,840
adder be right in the segment headers

135
00:05:40,990 --> 00:05:54,700
what would that have been in P files as

136
00:05:43,840 --> 00:06:00,490
where we're gonna map it into memory you

137
00:05:54,700 --> 00:06:02,140
can look okay alright so we've got the

138
00:06:00,490 --> 00:06:03,700
exact same analog going on we've got

139
00:06:02,140 --> 00:06:05,250
some pointer to file data and we got

140
00:06:03,700 --> 00:06:10,240
something that's gonna go in a member

141
00:06:05,250 --> 00:06:11,770
Ivan do you remember it is the r-va what

142
00:06:10,240 --> 00:06:13,560
was the exact name of that particular

143
00:06:11,770 --> 00:06:19,690
field in the section header

144
00:06:13,560 --> 00:06:22,270
it was the virtual address so section

145
00:06:19,690 --> 00:06:24,070
headers virtual address right that would

146
00:06:22,270 --> 00:06:25,720
say where we want to map it it was the

147
00:06:24,070 --> 00:06:27,010
virtual address was not an absolute

148
00:06:25,720 --> 00:06:29,260
virtual address it was a relative

149
00:06:27,010 --> 00:06:32,080
virtual address but field name was just

150
00:06:29,260 --> 00:06:35,350
virtual address all right

151
00:06:32,080 --> 00:06:37,419
so P file size all right we've got where

152
00:06:35,350 --> 00:06:41,720
the file data starts and we've got how

153
00:06:37,419 --> 00:06:46,140
big it is so what do we think the the

154
00:06:41,720 --> 00:06:47,580
that field would be and is the section

155
00:06:46,140 --> 00:06:50,120
headers and P files

156
00:06:47,580 --> 00:06:53,430
what says how big this section is Ross I

157
00:06:50,120 --> 00:06:58,650
close I mean you're right but it's not

158
00:06:53,430 --> 00:07:00,330
the exact word size of raw data I heard

159
00:06:58,650 --> 00:07:01,710
it over there size of raw data was the

160
00:07:00,330 --> 00:07:03,900
section at her field that says here's

161
00:07:01,710 --> 00:07:05,820
how big the section is that I want to

162
00:07:03,900 --> 00:07:08,280
map into mouth same things going on here

163
00:07:05,820 --> 00:07:11,130
in program headers you start out you say

164
00:07:08,280 --> 00:07:13,230
where does it start P offset pointer to

165
00:07:11,130 --> 00:07:17,130
raw data and how big is it

166
00:07:13,230 --> 00:07:20,670
P file size eyes around you it gets

167
00:07:17,130 --> 00:07:24,390
mapped into memory at P V adder virtual

168
00:07:20,670 --> 00:07:27,420
address how big is it in memory P mm

169
00:07:24,390 --> 00:07:32,490
sighs that was misc that virtual size

170
00:07:27,420 --> 00:07:36,150
right we're just virtual size now there

171
00:07:32,490 --> 00:07:37,650
is no notion of file padding in L so we

172
00:07:36,150 --> 00:07:39,960
said before that the virtual science

173
00:07:37,650 --> 00:07:42,840
versus file size 1 could be bigger under

174
00:07:39,960 --> 00:07:45,570
certain circumstances in this case you

175
00:07:42,840 --> 00:07:47,610
can never have a file size which is

176
00:07:45,570 --> 00:07:49,500
bigger than the bird wing file size that

177
00:07:47,610 --> 00:07:51,210
is bigger than the virtual size because

178
00:07:49,500 --> 00:07:54,900
you're never padding out your file data

179
00:07:51,210 --> 00:07:57,510
to the file alignment size basically the

180
00:07:54,900 --> 00:08:00,630
memory size should always be equal to or

181
00:07:57,510 --> 00:08:02,880
greater than the virtual size

182
00:08:00,630 --> 00:08:05,400
grreat equal to or greater than the file

183
00:08:02,880 --> 00:08:08,490
size and why would we think that it

184
00:08:05,400 --> 00:08:10,290
would be greater in memory then that it

185
00:08:08,490 --> 00:08:14,610
was on disk same reason that it's P

186
00:08:10,290 --> 00:08:18,140
files the idea why do we have more

187
00:08:14,610 --> 00:08:20,130
memory than we have file data on disk

188
00:08:18,140 --> 00:08:22,350
because the variables are not

189
00:08:20,130 --> 00:08:24,030
initialized we've got a BSS kind of

190
00:08:22,350 --> 00:08:25,650
situation going on you've got some

191
00:08:24,030 --> 00:08:27,600
global variables they don't have values

192
00:08:25,650 --> 00:08:30,180
you don't need to set them on disk

193
00:08:27,600 --> 00:08:32,370
because you just put them up make space

194
00:08:30,180 --> 00:08:34,740
in memory and they get set as the code

195
00:08:32,370 --> 00:08:37,860
runs so this is you know you can see

196
00:08:34,740 --> 00:08:40,050
this is very analogous to the sections

197
00:08:37,860 --> 00:08:42,540
thus far especially when we have a load

198
00:08:40,050 --> 00:08:46,970
segment now we can have these sizes like

199
00:08:42,540 --> 00:08:49,440
files we can have no p offset and p size

200
00:08:46,970 --> 00:08:50,760
but this thing will not necessarily even

201
00:08:49,440 --> 00:08:52,710
be mapped into memory it's just saying

202
00:08:50,760 --> 00:08:54,110
here's some segment it has the you know

203
00:08:52,710 --> 00:08:56,180
string for the

204
00:08:54,110 --> 00:08:59,480
only one we've got load segments that

205
00:08:56,180 --> 00:09:01,910
were going to go from pea size to a pea

206
00:08:59,480 --> 00:09:05,360
offset two-piece I file size and map

207
00:09:01,910 --> 00:09:07,580
that stuff up into memory all right and

208
00:09:05,360 --> 00:09:10,520
last couple of things I hope last two

209
00:09:07,580 --> 00:09:13,430
fields of the program headers are the P

210
00:09:10,520 --> 00:09:16,130
flags now this would be again very much

211
00:09:13,430 --> 00:09:18,110
like section header characteristics we

212
00:09:16,130 --> 00:09:21,380
have flags like read/write/execute

213
00:09:18,110 --> 00:09:25,670
right so read/write/execute

214
00:09:21,380 --> 00:09:28,040
and they work the same way as you there

215
00:09:25,670 --> 00:09:29,690
bit mask - the same way that you'd have

216
00:09:28,040 --> 00:09:31,460
permissions if you're familiar with

217
00:09:29,690 --> 00:09:34,040
Jomon on the linux system you know you

218
00:09:31,460 --> 00:09:36,200
said Jomon 777 you're setting the bit

219
00:09:34,040 --> 00:09:39,560
for 4 plus the bit for 2 plus the bit

220
00:09:36,200 --> 00:09:41,270
for 1/7 that's rewrite execute so it

221
00:09:39,560 --> 00:09:43,780
works the same way as your typical file

222
00:09:41,270 --> 00:09:46,460
system permissions read write execute

223
00:09:43,780 --> 00:09:49,670
all right so P Flags sets memory

224
00:09:46,460 --> 00:09:52,190
permissions and P align this is segment

225
00:09:49,670 --> 00:09:55,340
alignment and this has to do with

226
00:09:52,190 --> 00:09:57,830
virtual memory what boundaries should it

227
00:09:55,340 --> 00:10:00,830
be aligned on and so what would this be

228
00:09:57,830 --> 00:10:02,840
equivalent to back not in the section

229
00:10:00,830 --> 00:10:05,930
headers but back in the optional header

230
00:10:02,840 --> 00:10:07,460
what did we have for alignment back in

231
00:10:05,930 --> 00:10:08,660
the optional header that has to do with

232
00:10:07,460 --> 00:10:15,200
where sections are supposed to the

233
00:10:08,660 --> 00:10:16,910
matter section alignment yep optional

234
00:10:15,200 --> 00:10:19,430
header or not section alignment says I

235
00:10:16,910 --> 00:10:21,230
want you to map it at X 1000 you'll see

236
00:10:19,430 --> 00:10:25,760
basically the same thing here you know

237
00:10:21,230 --> 00:10:27,740
most often be hex 1000 the same thing I

238
00:10:25,760 --> 00:10:30,380
said it's because behind the scenes the

239
00:10:27,740 --> 00:10:33,020
OS deals and chunks of memory and sizes

240
00:10:30,380 --> 00:10:34,700
of hex 1000 of each other oh this is

241
00:10:33,020 --> 00:10:37,340
just showing the difference between

242
00:10:34,700 --> 00:10:38,840
dynamic link thing and a non dynamic

243
00:10:37,340 --> 00:10:41,960
link thing you'll just have this extra

244
00:10:38,840 --> 00:10:44,330
dynamic segment this will show you where

245
00:10:41,960 --> 00:10:48,650
the dynamic linking information is going

246
00:10:44,330 --> 00:10:52,930
to be within memory essentially so it'll

247
00:10:48,650 --> 00:10:59,390
say we may have a load segment that says

248
00:10:52,930 --> 00:11:01,430
you know right here we've got we've got

249
00:10:59,390 --> 00:11:03,290
the second load segment this has the

250
00:11:01,430 --> 00:11:05,420
same sort of information as this hello

251
00:11:03,290 --> 00:11:06,290
world we were just looking at and it has

252
00:11:05,420 --> 00:11:09,019
the 6

253
00:11:06,290 --> 00:11:11,570
he won eight and so it goes from six

254
00:11:09,019 --> 00:11:13,160
hundred one eight two six one zero two

255
00:11:11,570 --> 00:11:15,350
zero like we said last time

256
00:11:13,160 --> 00:11:18,649
but then the dynamic information is

257
00:11:15,350 --> 00:11:20,360
right here at six hundred e-40 so if you

258
00:11:18,649 --> 00:11:22,070
wanted to know where within that load

259
00:11:20,360 --> 00:11:24,230
segment that a dynamic linking

260
00:11:22,070 --> 00:11:26,029
information was even if you had no

261
00:11:24,230 --> 00:11:27,889
section information and that's the key

262
00:11:26,029 --> 00:11:30,740
thing area said section information is

263
00:11:27,889 --> 00:11:32,660
optional segment information isn't if

264
00:11:30,740 --> 00:11:35,509
you need to find the dynamic linking

265
00:11:32,660 --> 00:11:37,790
information you can use this segment

266
00:11:35,509 --> 00:11:39,290
header or this program header in order

267
00:11:37,790 --> 00:11:42,290
to know that it's going to be a virtual

268
00:11:39,290 --> 00:11:43,940
memory six zero e 4 zero and so the

269
00:11:42,290 --> 00:11:45,709
dynamic linker is going to want to know

270
00:11:43,940 --> 00:11:47,810
where the dynamic linking information is

271
00:11:45,709 --> 00:11:48,980
right we've got the string telling us

272
00:11:47,810 --> 00:11:52,190
the dynamic linker is going to be

273
00:11:48,980 --> 00:11:54,290
processing this one right so we'll get

274
00:11:52,190 --> 00:11:56,569
into that a little bit further so that

275
00:11:54,290 --> 00:11:58,100
was dynamically and when it's got no

276
00:11:56,569 --> 00:12:00,740
dynamic link when it's a statically

277
00:11:58,100 --> 00:12:03,050
linked file so I just did GCC and I

278
00:12:00,740 --> 00:12:06,019
added on the static then we have no

279
00:12:03,050 --> 00:12:07,970
dynamic program header right so that's

280
00:12:06,019 --> 00:12:10,040
saying alright first of all there's no

281
00:12:07,970 --> 00:12:11,720
intersection alright there's nothing

282
00:12:10,040 --> 00:12:13,850
telling us where there's a string to an

283
00:12:11,720 --> 00:12:15,709
interpreter and there's nothing telling

284
00:12:13,850 --> 00:12:18,019
us where the dynamic link information is

285
00:12:15,709 --> 00:12:20,269
got to go we do have a TLS here for no

286
00:12:18,019 --> 00:12:22,010
apparent reason I I'm guessing that's

287
00:12:20,269 --> 00:12:23,779
coming in from the other code that we

288
00:12:22,010 --> 00:12:25,579
just added in right we statically linked

289
00:12:23,779 --> 00:12:27,170
in a bunch of other codes and some of

290
00:12:25,579 --> 00:12:31,279
that other code must be using TLS

291
00:12:27,170 --> 00:12:34,720
storage certainly my hello world is not

292
00:12:31,279 --> 00:12:34,720
using TLS storage


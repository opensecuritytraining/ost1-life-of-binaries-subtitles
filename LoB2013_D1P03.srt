1
00:00:04,170 --> 00:00:11,530
all right so sometimes here the food

2
00:00:08,080 --> 00:00:14,650
format referred to as P slash cup coffee

3
00:00:11,530 --> 00:00:17,619
coming in objects file format UNIX file

4
00:00:14,650 --> 00:00:21,909
format for blacks people derived from

5
00:00:17,619 --> 00:00:24,099
that base equation and so you'll kind of

6
00:00:21,909 --> 00:00:26,679
see that the first couple headers of the

7
00:00:24,099 --> 00:00:28,390
P format are more like COFF headers and

8
00:00:26,679 --> 00:00:30,069
then everything beyond that or all the

9
00:00:28,390 --> 00:00:31,749
real details are those are the stuff

10
00:00:30,069 --> 00:00:35,079
that you know added on to do what they

11
00:00:31,749 --> 00:00:37,059
used to do all right and so then what on

12
00:00:35,079 --> 00:00:40,059
UNIX system so whereas PE is used

13
00:00:37,059 --> 00:00:41,829
primarily by those the interesting side

14
00:00:40,059 --> 00:00:46,539
note is mm-hmm

15
00:00:41,829 --> 00:00:48,699
you may be heard increasingly about UEFI

16
00:00:46,539 --> 00:00:50,980
unified extensible firmware interface

17
00:00:48,699 --> 00:00:54,429
sort of saw the bottles replacement so

18
00:00:50,980 --> 00:00:56,530
much as it is a way to provide new

19
00:00:54,429 --> 00:00:59,769
capabilities to bios's going forward

20
00:00:56,530 --> 00:01:03,640
under Windows 8 and secure that requires

21
00:00:59,769 --> 00:01:05,140
UEFI unify is really just a way to make

22
00:01:03,640 --> 00:01:07,270
it so that there's these services which

23
00:01:05,140 --> 00:01:09,010
are running after BIOS so BIOS usually

24
00:01:07,270 --> 00:01:11,020
wouldn't start up and then it would be

25
00:01:09,010 --> 00:01:13,240
done and initialize the operating system

26
00:01:11,020 --> 00:01:14,740
that would let go of the system UEFI

27
00:01:13,240 --> 00:01:21,390
there's these services which you can

28
00:01:14,740 --> 00:01:23,770
call - that run run post boot anyways

29
00:01:21,390 --> 00:01:26,800
UEFI extensions are actually using the

30
00:01:23,770 --> 00:01:28,360
PU format so basically you compile the

31
00:01:26,800 --> 00:01:29,740
point was - you know it's unified

32
00:01:28,360 --> 00:01:32,620
extensible firmware interface

33
00:01:29,740 --> 00:01:34,980
extensibility here meant that no longer

34
00:01:32,620 --> 00:01:37,150
people had to write their BIOS

35
00:01:34,980 --> 00:01:38,560
extensions using you know hand-coded

36
00:01:37,150 --> 00:01:39,880
assembly things like that you're

37
00:01:38,560 --> 00:01:43,300
basically going to be able to take

38
00:01:39,880 --> 00:01:45,490
normal compiled C code it compiles it to

39
00:01:43,300 --> 00:01:47,110
a peeve Wow and then it's going to be

40
00:01:45,490 --> 00:01:48,970
using that down to how to say you know

41
00:01:47,110 --> 00:01:50,650
kind of think of it like plugging driver

42
00:01:48,970 --> 00:01:52,540
down at the bottom of something that

43
00:01:50,650 --> 00:01:54,430
runs after the fact so anyways

44
00:01:52,540 --> 00:01:55,840
previously I would have said he used

45
00:01:54,430 --> 00:01:58,330
primarily by the way those now it's

46
00:01:55,840 --> 00:01:59,850
actually part of the specification for

47
00:01:58,330 --> 00:02:05,260
unit 5

48
00:01:59,850 --> 00:02:09,039
so P for Windows and u5l for UNIX

49
00:02:05,260 --> 00:02:11,680
systems Linux systems and macho or macho

50
00:02:09,039 --> 00:02:14,410
for Mac OSX

51
00:02:11,680 --> 00:02:16,209
have a playbook macho man and I was

52
00:02:14,410 --> 00:02:19,360
seeing another dance but it doesn't

53
00:02:16,209 --> 00:02:26,849
actually work on doesn't actually work

54
00:02:19,360 --> 00:02:31,599
on this sorry all right

55
00:02:26,849 --> 00:02:34,379
in terms of the the different types of

56
00:02:31,599 --> 00:02:36,760
files which you're going to use these

57
00:02:34,379 --> 00:02:38,260
binary formats you've got your basic

58
00:02:36,760 --> 00:02:39,970
executable that's a standalone thing

59
00:02:38,260 --> 00:02:41,980
that you run this program from command

60
00:02:39,970 --> 00:02:44,410
line or double-click a GUI interface and

61
00:02:41,980 --> 00:02:46,660
that's going to be something which will

62
00:02:44,410 --> 00:02:49,569
execute in one either continuously or

63
00:02:46,660 --> 00:02:52,000
one and terminate whatever so that's you

64
00:02:49,569 --> 00:02:53,140
know I'm not exe in Windows or most

65
00:02:52,000 --> 00:02:55,810
technically advanced enough something

66
00:02:53,140 --> 00:02:58,750
some Linux then you have dynamic link

67
00:02:55,810 --> 00:03:01,239
library or shared object so all this is

68
00:02:58,750 --> 00:03:04,180
is basically a library which is going to

69
00:03:01,239 --> 00:03:06,370
contain some code which will be used by

70
00:03:04,180 --> 00:03:08,530
other programs at one time so before I

71
00:03:06,370 --> 00:03:10,150
showed you the loader imports a bunch of

72
00:03:08,530 --> 00:03:12,640
libraries and then it lets the program

73
00:03:10,150 --> 00:03:17,319
run so what we know is that's a DLL and

74
00:03:12,640 --> 00:03:20,260
Linux that's a ISO file and all it is

75
00:03:17,319 --> 00:03:22,329
basically is some set of code where it's

76
00:03:20,260 --> 00:03:26,709
going to be loaded up by the OS loader

77
00:03:22,329 --> 00:03:29,579
and it will support other programs now

78
00:03:26,709 --> 00:03:32,139
that said you will actually see that and

79
00:03:29,579 --> 00:03:34,569
this matters more for the DLL injection

80
00:03:32,139 --> 00:03:39,430
and the equivalents or LD preload

81
00:03:34,569 --> 00:03:45,819
attacks libraries will still typically

82
00:03:39,430 --> 00:03:47,560
have sort of a main function DLLs it'll

83
00:03:45,819 --> 00:03:50,319
typically be called deal Albanian and so

84
00:03:47,560 --> 00:03:51,940
when the OS loader loads up the DLL it

85
00:03:50,319 --> 00:03:53,739
does have an entry point where it says

86
00:03:51,940 --> 00:03:54,880
okay I'm going to call this function in

87
00:03:53,739 --> 00:03:56,620
you you're going to run some

88
00:03:54,880 --> 00:03:58,900
initialization code you can get all of

89
00:03:56,620 --> 00:04:01,510
your you know global variables or

90
00:03:58,900 --> 00:04:02,859
whatever and then you know I'm gonna run

91
00:04:01,510 --> 00:04:05,109
your initialization code and then I'll

92
00:04:02,859 --> 00:04:07,329
proceed to the next deal out so this is

93
00:04:05,109 --> 00:04:08,709
something that this this capability is

94
00:04:07,329 --> 00:04:10,120
something that attackers take advantage

95
00:04:08,709 --> 00:04:11,980
of when they're doing things like DLL

96
00:04:10,120 --> 00:04:14,230
injection they you know just by virtue

97
00:04:11,980 --> 00:04:16,299
of this DLL being loaded by some other

98
00:04:14,230 --> 00:04:18,400
program they don't have to actually

99
00:04:16,299 --> 00:04:20,049
export functions like a really all we're

100
00:04:18,400 --> 00:04:23,639
gonna do though the point is for it to

101
00:04:20,049 --> 00:04:29,129
export functions and

102
00:04:23,639 --> 00:04:30,629
for the with DLL injection basically you

103
00:04:29,129 --> 00:04:32,219
just don't export functions you load

104
00:04:30,629 --> 00:04:34,319
yourself up and you run all your code in

105
00:04:32,219 --> 00:04:36,029
the main basically so Windows that's

106
00:04:34,319 --> 00:04:42,050
typically deal main and that's it

107
00:04:36,029 --> 00:04:42,050
on Linux shared object box alright

108
00:04:42,800 --> 00:04:50,789
and so static libraries then are things

109
00:04:48,240 --> 00:04:52,919
where you basically not live on Windows

110
00:04:50,789 --> 00:04:54,569
now these are sort of interesting

111
00:04:52,919 --> 00:04:56,520
because oftentimes these don't actually

112
00:04:54,569 --> 00:04:59,039
use the binary format they're usually

113
00:04:56,520 --> 00:05:00,810
more like think of them like just a big

114
00:04:59,039 --> 00:05:02,400
array of object files and they'll maybe

115
00:05:00,810 --> 00:05:05,009
have a little header information that

116
00:05:02,400 --> 00:05:09,090
says you know of your quality may use

117
00:05:05,009 --> 00:05:11,189
your either C is there so static

118
00:05:09,090 --> 00:05:15,000
libraries are basically used where you

119
00:05:11,189 --> 00:05:17,669
want to compile all of the information

120
00:05:15,000 --> 00:05:20,400
down to one file which can be linked

121
00:05:17,669 --> 00:05:23,639
again linked against for static linking

122
00:05:20,400 --> 00:05:27,479
so at compile time you're basically

123
00:05:23,639 --> 00:05:31,860
saying don't use the you know don't dear

124
00:05:27,479 --> 00:05:33,680
linker don't set printf to use this DLL

125
00:05:31,860 --> 00:05:36,449
that's going to load up you know the

126
00:05:33,680 --> 00:05:38,819
runtime the C library so don't use the

127
00:05:36,449 --> 00:05:40,830
printf from the DLL that's a run at load

128
00:05:38,819 --> 00:05:42,569
time use the printout from my particular

129
00:05:40,830 --> 00:05:44,729
implementation you know that could be a

130
00:05:42,569 --> 00:05:46,979
simpler of communication or more complex

131
00:05:44,729 --> 00:05:48,830
one but the static library is basically

132
00:05:46,979 --> 00:05:51,990
something where you're linking against

133
00:05:48,830 --> 00:05:53,069
at linked time and you are providing all

134
00:05:51,990 --> 00:05:55,319
of the code you're saying like just

135
00:05:53,069 --> 00:05:59,430
incorporate this code into the main

136
00:05:55,319 --> 00:06:01,349
binary so that it basically doesn't call

137
00:05:59,430 --> 00:06:03,060
up to some external library at one time

138
00:06:01,349 --> 00:06:06,930
and this is typically you use it for

139
00:06:03,060 --> 00:06:13,680
making your own standalone better okay

140
00:06:06,930 --> 00:06:18,379
so again yeah I I think you know this

141
00:06:13,680 --> 00:06:18,379
kind of just illustrates the two of the

142
00:06:22,639 --> 00:06:33,139
bill pointers is it on the next to

143
00:06:30,149 --> 00:06:33,139
whiteboard and in the rim

144
00:06:33,590 --> 00:06:39,610
you know alright we get the screen over

145
00:06:37,610 --> 00:06:42,650
here

146
00:06:39,610 --> 00:06:44,420
alright so as I said you know we've got

147
00:06:42,650 --> 00:06:46,580
multiple different things using the

148
00:06:44,420 --> 00:06:48,740
binary format we've got an EFC which

149
00:06:46,580 --> 00:06:50,900
stands alone but it only stands alone to

150
00:06:48,740 --> 00:06:53,570
some degree you know oftentimes it's

151
00:06:50,900 --> 00:06:55,220
going to import other libraries and so

152
00:06:53,570 --> 00:06:58,190
the OS loader has to go and find those

153
00:06:55,220 --> 00:07:01,460
libraries within it and so when this

154
00:06:58,190 --> 00:07:03,020
gets loaded in the looker event has to

155
00:07:01,460 --> 00:07:05,030
say like what does it depend on it has

156
00:07:03,020 --> 00:07:08,000
to go find those it was loaded in as

157
00:07:05,030 --> 00:07:10,010
well beyond that each of these as I just

158
00:07:08,000 --> 00:07:11,990
said has you know what DLL made is that

159
00:07:10,010 --> 00:07:14,120
when this gets loaded in the loader

160
00:07:11,990 --> 00:07:15,950
we'll call that the function that sort

161
00:07:14,120 --> 00:07:16,940
of start this thing's code it'll start

162
00:07:15,950 --> 00:07:18,770
that things coach they're all

163
00:07:16,940 --> 00:07:20,630
initialized and then one of everybody

164
00:07:18,770 --> 00:07:22,730
else is initialized it'll go back and

165
00:07:20,630 --> 00:07:24,560
it'll start to again smooth out that exe

166
00:07:22,730 --> 00:07:27,740
so that's why an attacker who's you know

167
00:07:24,560 --> 00:07:28,430
in some way whether he's Trojan to file

168
00:07:27,740 --> 00:07:30,860
on disk

169
00:07:28,430 --> 00:07:34,940
whether he's set some registry entry to

170
00:07:30,860 --> 00:07:36,710
add an extra DLL he can actually get his

171
00:07:34,940 --> 00:07:38,420
code running before the main code and

172
00:07:36,710 --> 00:07:40,490
that he can you know scribble all over

173
00:07:38,420 --> 00:07:46,550
this weakness we have exe and

174
00:07:40,490 --> 00:07:48,410
manipulated in core - all right so these

175
00:07:46,550 --> 00:07:50,210
are just common things this is I put

176
00:07:48,410 --> 00:07:51,710
this on here just kind of to give you a

177
00:07:50,210 --> 00:07:54,500
heads up because some of these you might

178
00:07:51,710 --> 00:07:58,610
not realize dealing with the xes are in

179
00:07:54,500 --> 00:08:00,280
the P files from another of the xes DLLs

180
00:07:58,610 --> 00:08:03,290
and maybe doc sighs - knocks this is

181
00:08:00,280 --> 00:08:05,900
driver ocx then that's an ActiveX

182
00:08:03,290 --> 00:08:08,480
control really that's just a DLL

183
00:08:05,900 --> 00:08:10,130
basically DLL in Internet Explorer loads

184
00:08:08,480 --> 00:08:12,410
up it has a bunch of native code and

185
00:08:10,130 --> 00:08:14,690
that's why is that such a source of

186
00:08:12,410 --> 00:08:16,910
vulnerabilities for a long time because

187
00:08:14,690 --> 00:08:18,380
they weren't jailing these ActiveX

188
00:08:16,910 --> 00:08:20,330
controls in any way you're basically

189
00:08:18,380 --> 00:08:21,860
just loading the allow the attacker can

190
00:08:20,330 --> 00:08:24,650
force it to be loaded and then could

191
00:08:21,860 --> 00:08:28,670
call some wonderful function and do a

192
00:08:24,650 --> 00:08:33,230
buffer overflow control panels as well

193
00:08:28,670 --> 00:08:35,600
or actually px are actually P form files

194
00:08:33,230 --> 00:08:36,770
if you monitor processes when

195
00:08:35,600 --> 00:08:38,900
something's running you'll actually

196
00:08:36,770 --> 00:08:40,550
sometimes see that they'll have exported

197
00:08:38,900 --> 00:08:42,530
functions out of these control panels

198
00:08:40,550 --> 00:08:45,020
like I think power manager is one of

199
00:08:42,530 --> 00:08:46,040
them where you'll see it when using the

200
00:08:45,020 --> 00:08:49,490
XP will run

201
00:08:46,040 --> 00:08:51,770
DLL 32 DHC and the only point of the

202
00:08:49,490 --> 00:08:56,030
executable run vol 3 to the X e is to

203
00:08:51,770 --> 00:08:57,380
call specific exported functions in DLLs

204
00:08:56,030 --> 00:08:59,570
and things like that you'll actually see

205
00:08:57,380 --> 00:09:02,360
it calling functions on control panels

206
00:08:59,570 --> 00:09:12,080
in order to like set some parameters you

207
00:09:02,360 --> 00:09:15,560
know I've seen C run deal I'll calling X

208
00:09:12,080 --> 00:09:18,200
period of things and then screensavers

209
00:09:15,560 --> 00:09:20,090
that you all are nice people so that's a

210
00:09:18,200 --> 00:09:22,220
common thing for social engineering and

211
00:09:20,090 --> 00:09:23,780
stuff like that you say hey download

212
00:09:22,220 --> 00:09:24,980
this cool screensaver and people don't

213
00:09:23,780 --> 00:09:30,110
realize they're downloading and

214
00:09:24,980 --> 00:09:33,320
executing whole wheel code and as I

215
00:09:30,110 --> 00:09:36,350
already mentioned the files and files

216
00:09:33,320 --> 00:09:38,660
are typically not actually will perform

217
00:09:36,350 --> 00:09:43,880
well they have their own format but it's

218
00:09:38,660 --> 00:09:45,650
not just a straight-up about p or l so

219
00:09:43,880 --> 00:09:48,080
you know if we're we're not going to use

220
00:09:45,650 --> 00:09:50,660
visuals to visual studio too much in

221
00:09:48,080 --> 00:09:52,850
this class this time around but this is

222
00:09:50,660 --> 00:09:55,400
just to show that notion lis when you're

223
00:09:52,850 --> 00:09:56,870
compiling in Visual Studio they're just

224
00:09:55,400 --> 00:10:00,020
sort of the drop-down box where you say

225
00:09:56,870 --> 00:10:01,580
I want to compile any executor galo GCC

226
00:10:00,020 --> 00:10:04,270
and Linux should just be you know giving

227
00:10:01,580 --> 00:10:04,270
different options


1
00:00:04,480 --> 00:00:08,639
all right last topic on pes

2
00:00:07,680 --> 00:00:10,320
the

3
00:00:08,639 --> 00:00:12,960
directory entry security and i'm

4
00:00:10,320 --> 00:00:15,759
assuming this yep animation is broken as

5
00:00:12,960 --> 00:00:18,640
well so from the data directory uh

6
00:00:15,759 --> 00:00:19,920
directory entry security

7
00:00:18,640 --> 00:00:21,600
this is

8
00:00:19,920 --> 00:00:25,039
um

9
00:00:21,600 --> 00:00:25,039
not well it's not functioning

10
00:00:25,519 --> 00:00:27,760
this is

11
00:00:26,560 --> 00:00:30,320
technically this can be used for

12
00:00:27,760 --> 00:00:32,160
different things but how it's used these

13
00:00:30,320 --> 00:00:33,840
days is for

14
00:00:32,160 --> 00:00:36,000
certificates so this will actually be

15
00:00:33,840 --> 00:00:38,719
pointing for signed code if it has a

16
00:00:36,000 --> 00:00:40,320
digital signature embedded into the file

17
00:00:38,719 --> 00:00:42,160
that

18
00:00:40,320 --> 00:00:44,320
directory entry security will be

19
00:00:42,160 --> 00:00:46,000
pointing at the digital certificate and

20
00:00:44,320 --> 00:00:48,879
then the os will use that when it's

21
00:00:46,000 --> 00:00:50,879
trying to do certificate verification or

22
00:00:48,879 --> 00:00:53,039
digital signature checking

23
00:00:50,879 --> 00:00:54,480
the correct way to say that

24
00:00:53,039 --> 00:00:56,160
all right so authentic code is

25
00:00:54,480 --> 00:00:59,120
microsoft's keyword for digitally

26
00:00:56,160 --> 00:01:00,719
signing things as you may know

27
00:00:59,120 --> 00:01:02,399
you know kernel drivers starting in

28
00:01:00,719 --> 00:01:03,760
windows vista are supposed to be signed

29
00:01:02,399 --> 00:01:05,199
so you're not allowed to

30
00:01:03,760 --> 00:01:08,199
load kernel drivers unless they're

31
00:01:05,199 --> 00:01:08,199
assigned

32
00:01:12,000 --> 00:01:15,119
so process explorer

33
00:01:15,200 --> 00:01:20,479
if we go to the

34
00:01:17,439 --> 00:01:25,000
optional header and we go down to the

35
00:01:20,479 --> 00:01:25,000
security entry which is where

36
00:01:27,280 --> 00:01:30,799
say what oh it's not process explorer

37
00:01:29,040 --> 00:01:32,240
that

38
00:01:30,799 --> 00:01:35,240
part of the problem but not the entire

39
00:01:32,240 --> 00:01:35,240
problem

40
00:01:40,560 --> 00:01:46,159
process explorer data directory

41
00:01:44,079 --> 00:01:48,240
there we go certificate table

42
00:01:46,159 --> 00:01:50,240
so it's giving you an rva pointing at

43
00:01:48,240 --> 00:01:52,479
this thing which now pe view has parsed

44
00:01:50,240 --> 00:01:54,079
out it's calling it certificate table

45
00:01:52,479 --> 00:01:56,000
and so this is the actual digital

46
00:01:54,079 --> 00:01:57,680
certificate for this file and you can

47
00:01:56,000 --> 00:02:00,240
see in here some stuff washington

48
00:01:57,680 --> 00:02:02,719
redmond microsoft corporation and so

49
00:02:00,240 --> 00:02:05,600
forth so you can see some of the signing

50
00:02:02,719 --> 00:02:05,600
information in here

51
00:02:06,240 --> 00:02:11,680
and let's see i can't remember even if

52
00:02:08,399 --> 00:02:13,200
that shows up in pe view

53
00:02:11,680 --> 00:02:15,440
i mean certainly you can always get to

54
00:02:13,200 --> 00:02:17,360
the same offset but yeah it doesn't look

55
00:02:15,440 --> 00:02:19,680
like it pulls it out so basically you

56
00:02:17,360 --> 00:02:21,280
would have to go to the data directory

57
00:02:19,680 --> 00:02:23,520
here instead of calling it certificate

58
00:02:21,280 --> 00:02:26,160
table it's calling it the

59
00:02:23,520 --> 00:02:27,520
security directory rva

60
00:02:26,160 --> 00:02:30,000
and it's saying that that is in the

61
00:02:27,520 --> 00:02:31,760
resource section so i would go into the

62
00:02:30,000 --> 00:02:33,120
resource somewhere

63
00:02:31,760 --> 00:02:34,879
and

64
00:02:33,120 --> 00:02:37,040
i would basically wander around until i

65
00:02:34,879 --> 00:02:38,560
found it

66
00:02:37,040 --> 00:02:40,640
the easier way i guess in this case

67
00:02:38,560 --> 00:02:44,400
would be

68
00:02:40,640 --> 00:02:46,080
data directory security directory 29 480

69
00:02:44,400 --> 00:02:48,000
and i would like literally just go back

70
00:02:46,080 --> 00:02:51,840
to the

71
00:02:48,000 --> 00:02:51,840
thing like this the raw data

72
00:02:52,400 --> 00:02:59,519
29 480 must be greater than this

73
00:02:56,000 --> 00:02:59,519
somewhere in this range probably

74
00:03:02,879 --> 00:03:09,040
there we go microsoft corporation

75
00:03:06,080 --> 00:03:10,400
microsoft signing pca and so forth

76
00:03:09,040 --> 00:03:12,560
so they've got the actual digital

77
00:03:10,400 --> 00:03:15,599
signature embedded into their file

78
00:03:12,560 --> 00:03:18,080
pointed to by this uh security directory

79
00:03:15,599 --> 00:03:19,519
entry or certificate table entry

80
00:03:18,080 --> 00:03:20,400
whichever

81
00:03:19,519 --> 00:03:23,200
question

82
00:03:20,400 --> 00:03:23,200
okay


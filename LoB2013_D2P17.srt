1
00:00:03,620 --> 00:00:10,740
alright so that's basically it we are

2
00:00:06,450 --> 00:00:12,599
done with PE s as you saw lots of good

3
00:00:10,740 --> 00:00:14,790
stuff to memorize lots of things that

4
00:00:12,599 --> 00:00:16,619
are RVs or absolute virtual addresses a

5
00:00:14,790 --> 00:00:19,110
lot of interaction between export

6
00:00:16,619 --> 00:00:21,210
information and import information three

7
00:00:19,110 --> 00:00:24,000
different types of imports ultra

8
00:00:21,210 --> 00:00:28,500
miscellaneous stuff at the end bug

9
00:00:24,000 --> 00:00:31,640
information resources relocations load

10
00:00:28,500 --> 00:00:33,809
configuration all these sort of things

11
00:00:31,640 --> 00:00:35,399
you know I pointed out where I thought

12
00:00:33,809 --> 00:00:36,930
things were scary relevant things like

13
00:00:35,399 --> 00:00:40,050
import address table how it relates to

14
00:00:36,930 --> 00:00:41,640
i-80 hooking dat hooking that's a common

15
00:00:40,050 --> 00:00:44,100
technique used for hiding stuff I showed

16
00:00:41,640 --> 00:00:46,170
you the simple kind of case where you

17
00:00:44,100 --> 00:00:49,560
hide from task manager the calc da txt

18
00:00:46,170 --> 00:00:52,350
and stuff like that but so we do have

19
00:00:49,560 --> 00:00:54,300
more questions and more game for you to

20
00:00:52,350 --> 00:00:57,030
play to reinforce this last information

21
00:00:54,300 --> 00:01:02,700
we have it only for not resources where

22
00:00:57,030 --> 00:01:04,019
we do have it for load configuration I

23
00:01:02,700 --> 00:01:06,270
guess we don't have a personal kifek a

24
00:01:04,019 --> 00:01:08,100
table yet but we are once again going to

25
00:01:06,270 --> 00:01:11,250
go on from here because I want to get

26
00:01:08,100 --> 00:01:13,710
you at least enough of the elf stuff

27
00:01:11,250 --> 00:01:15,150
that we can come back so what I want to

28
00:01:13,710 --> 00:01:18,510
do is now is we're going to cover the

29
00:01:15,150 --> 00:01:19,890
elf stuff actually I think we'll

30
00:01:18,510 --> 00:01:21,720
probably take a five-minute break quick

31
00:01:19,890 --> 00:01:23,250
just so I can reorient myself to know

32
00:01:21,720 --> 00:01:24,479
what I have coming up but we're going to

33
00:01:23,250 --> 00:01:27,590
cover elf we're going to show how it's

34
00:01:24,479 --> 00:01:30,510
similar and dissimilar to PE and then

35
00:01:27,590 --> 00:01:34,110
whence we get to let's say four o'clock

36
00:01:30,510 --> 00:01:35,670
or so we'll come back and well well

37
00:01:34,110 --> 00:01:37,979
maybe first show a little bit about

38
00:01:35,670 --> 00:01:39,900
packed files on L so let's say here's a

39
00:01:37,979 --> 00:01:41,760
normal file at oh and here's a file

40
00:01:39,900 --> 00:01:43,590
where someone is packing and compressing

41
00:01:41,760 --> 00:01:46,170
it down how does that manipulate the

42
00:01:43,590 --> 00:01:47,189
equivalence of the section headers and

43
00:01:46,170 --> 00:01:48,960
stuff like that how are those

44
00:01:47,189 --> 00:01:51,110
manipulated in order to compress it down

45
00:01:48,960 --> 00:01:54,030
but still decompress it in memory and

46
00:01:51,110 --> 00:01:55,530
then we'll we'll do that and then we'll

47
00:01:54,030 --> 00:01:58,530
come back we'll show a quick thing in

48
00:01:55,530 --> 00:01:59,820
Windows hopefully of packed files and

49
00:01:58,530 --> 00:02:01,290
then we'll come back and we'll walk

50
00:01:59,820 --> 00:02:03,270
through the virus example the virus

51
00:02:01,290 --> 00:02:06,360
example is just very simple proof of

52
00:02:03,270 --> 00:02:09,119
concept code that I made where it is

53
00:02:06,360 --> 00:02:11,280
using inline assembly so it's not going

54
00:02:09,119 --> 00:02:13,499
to be entirely clear for people who

55
00:02:11,280 --> 00:02:14,730
haven't had assembly class but what I'm

56
00:02:13,499 --> 00:02:17,190
going to be pointing out is where its

57
00:02:14,730 --> 00:02:19,770
using assembly and where it's using

58
00:02:17,190 --> 00:02:21,030
in order to parse the data structure I'm

59
00:02:19,770 --> 00:02:23,370
going to be showing its there's one

60
00:02:21,030 --> 00:02:25,920
particular technique that you see the

61
00:02:23,370 --> 00:02:28,350
virus uses it exploit codes uses it

62
00:02:25,920 --> 00:02:30,000
rootkits use it and so there's a

63
00:02:28,350 --> 00:02:33,990
particular way of walking some metadata

64
00:02:30,000 --> 00:02:35,760
in order to find exports in the kernel32

65
00:02:33,990 --> 00:02:37,650
for that load library and get proc

66
00:02:35,760 --> 00:02:39,420
address so that you don't have to like

67
00:02:37,650 --> 00:02:40,950
import them so a virus is not going to

68
00:02:39,420 --> 00:02:43,770
have an import address table that says

69
00:02:40,950 --> 00:02:45,840
dear OS please fill in load library and

70
00:02:43,770 --> 00:02:47,130
get proc address for me and exploit just

71
00:02:45,840 --> 00:02:48,810
a little bit of shell code running in

72
00:02:47,130 --> 00:02:51,180
memory it's not going to have that and

73
00:02:48,810 --> 00:02:52,320
so this one technique you'll see it come

74
00:02:51,180 --> 00:02:53,640
up over and over I think it's in the

75
00:02:52,320 --> 00:02:55,260
reverse engineering class it's in

76
00:02:53,640 --> 00:02:59,070
Corey's exploits classes in this class

77
00:02:55,260 --> 00:03:02,040
it's one particular technique is very

78
00:02:59,070 --> 00:03:03,750
key to a lot of different malware but we

79
00:03:02,040 --> 00:03:05,910
wouldn't be able to cover it without you

80
00:03:03,750 --> 00:03:07,650
having knowledge of the doss header and

81
00:03:05,910 --> 00:03:09,330
the NT header and the data directory and

82
00:03:07,650 --> 00:03:11,610
the import address table and the export

83
00:03:09,330 --> 00:03:12,990
address table and things like that so

84
00:03:11,610 --> 00:03:14,610
we'll come back to that right at the end

85
00:03:12,990 --> 00:03:16,680
to make sure we at least cover from four

86
00:03:14,610 --> 00:03:19,860
to 430 cover that and then beyond that

87
00:03:16,680 --> 00:03:23,370
you know after 4 30 I'm not running off

88
00:03:19,860 --> 00:03:25,080
today I I didn't book my flight so I'll

89
00:03:23,370 --> 00:03:27,480
be here as late as you want you can ask

90
00:03:25,080 --> 00:03:29,130
questions we can do game whatever people

91
00:03:27,480 --> 00:03:31,410
want to do after 4 30 but i'll try to

92
00:03:29,130 --> 00:03:34,940
wrap it up by 4 30 so five minute break

93
00:03:31,410 --> 00:03:34,940
we're going to switch over to help now


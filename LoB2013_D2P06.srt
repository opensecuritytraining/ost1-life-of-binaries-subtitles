1
00:00:03,840 --> 00:00:09,130
just a couple things that I'm going to

2
00:00:06,100 --> 00:00:11,260
show before we move on to exports first

3
00:00:09,130 --> 00:00:13,750
is dependency Walker this is a separate

4
00:00:11,260 --> 00:00:17,289
program that you can use to sort of see

5
00:00:13,750 --> 00:00:19,570
like I import to a cambiar G but what is

6
00:00:17,289 --> 00:00:21,249
comedy LG import I'm the LG important so

7
00:00:19,570 --> 00:00:23,169
important they import their own imports

8
00:00:21,249 --> 00:00:25,749
so actually cff explorer has an

9
00:00:23,169 --> 00:00:29,439
equivalent thing to this down in the

10
00:00:25,749 --> 00:00:30,609
dependency Walker tab so we thin it you

11
00:00:29,439 --> 00:00:32,020
know this would be another way that you

12
00:00:30,609 --> 00:00:34,840
could potentially see how many things it

13
00:00:32,020 --> 00:00:37,239
imports so my around to q0 and points3

14
00:00:34,840 --> 00:00:40,739
dll's use your 32 being the first one

15
00:00:37,239 --> 00:00:43,660
and then you to 32 imports 3 DLLs and

16
00:00:40,739 --> 00:00:45,550
you know the second deal reports much

17
00:00:43,660 --> 00:00:47,739
more do else and that imports many more

18
00:00:45,550 --> 00:00:51,219
deals this is sort of a Windows 7

19
00:00:47,739 --> 00:00:52,839
there's some sub-code dll kind of thing

20
00:00:51,219 --> 00:00:54,670
that does that I don't actually even

21
00:00:52,839 --> 00:00:57,399
know about yet and one of the other

22
00:00:54,670 --> 00:01:00,070
people in my project don't with that but

23
00:00:57,399 --> 00:01:01,719
yeah so the point is you can see all of

24
00:01:00,070 --> 00:01:03,879
the dependencies that are actually

25
00:01:01,719 --> 00:01:07,300
involved and so the OS loader at runtime

26
00:01:03,879 --> 00:01:11,110
has to go through and say ok I'm running

27
00:01:07,300 --> 00:01:12,340
you know round to q0 dll what is that

28
00:01:11,110 --> 00:01:13,870
important that it goes and finds all

29
00:01:12,340 --> 00:01:15,490
those what are they important finds all

30
00:01:13,870 --> 00:01:17,440
those and it has to get this exhaustive

31
00:01:15,490 --> 00:01:20,890
list of all possible dll's that need to

32
00:01:17,440 --> 00:01:23,320
be loaded to make these things work so

33
00:01:20,890 --> 00:01:25,170
in some sense that's another potential

34
00:01:23,320 --> 00:01:28,330
value from something like delay loading

35
00:01:25,170 --> 00:01:30,550
DLLs right so if you delay load a dll

36
00:01:28,330 --> 00:01:33,220
which happens to you know load you know

37
00:01:30,550 --> 00:01:35,500
50 other DLLs by making sure you're not

38
00:01:33,220 --> 00:01:37,540
doing that until you really need to you

39
00:01:35,500 --> 00:01:39,570
potentially put off more than just one

40
00:01:37,540 --> 00:01:42,190
deal I was worth of blowing up overhead

41
00:01:39,570 --> 00:01:46,380
potentially you're putting off 50

42
00:01:42,190 --> 00:01:46,380
overhead 50 DLLs with over


1
00:00:04,560 --> 00:00:07,359
all right

2
00:00:05,359 --> 00:00:10,240
so on to round five you're going to be

3
00:00:07,359 --> 00:00:12,000
asked questions about delay load imports

4
00:00:10,240 --> 00:00:13,440
and bounding points so again boundary

5
00:00:12,000 --> 00:00:15,679
ports those are the pre-filled in

6
00:00:13,440 --> 00:00:17,840
entries delay load it's got its own data

7
00:00:15,679 --> 00:00:18,560
structure off to the side where it's got

8
00:00:17,840 --> 00:00:20,080
you know

9
00:00:18,560 --> 00:00:21,199
one entry per

10
00:00:20,080 --> 00:00:23,920
dll

11
00:00:21,199 --> 00:00:25,600
looks a little something like

12
00:00:23,920 --> 00:00:28,080
this

13
00:00:25,600 --> 00:00:30,320
right on entry per dll pointing at its

14
00:00:28,080 --> 00:00:32,000
own import address table it's own import

15
00:00:30,320 --> 00:00:34,320
address table that's pre-filled in with

16
00:00:32,000 --> 00:00:35,360
stump code addresses and then it's got a

17
00:00:34,320 --> 00:00:37,760
normal

18
00:00:35,360 --> 00:00:39,280
names table itself so

19
00:00:37,760 --> 00:00:41,040
on to round five

20
00:00:39,280 --> 00:00:43,360
and as i said this is probably the

21
00:00:41,040 --> 00:00:45,600
hardest round because

22
00:00:43,360 --> 00:00:47,200
for some of this stuff

23
00:00:45,600 --> 00:00:48,800
i'm pretty sure

24
00:00:47,200 --> 00:00:51,120
none of the tools actually like

25
00:00:48,800 --> 00:00:52,399
interpret the delay load stuff so

26
00:00:51,120 --> 00:00:54,079
whenever basically you're going to run

27
00:00:52,399 --> 00:00:56,239
into you're going to be like asked

28
00:00:54,079 --> 00:00:58,640
questions about delay load imports for

29
00:00:56,239 --> 00:01:01,359
64-bit things right

30
00:00:58,640 --> 00:01:03,359
pvu can't interpret 64-bit

31
00:01:01,359 --> 00:01:05,680
and cff explorer doesn't expose the

32
00:01:03,359 --> 00:01:06,799
64-bit stuff but i still want you to try

33
00:01:05,680 --> 00:01:08,159
it

34
00:01:06,799 --> 00:01:10,159
by

35
00:01:08,159 --> 00:01:12,400
looking at the data description right

36
00:01:10,159 --> 00:01:14,560
looking at the structure description

37
00:01:12,400 --> 00:01:17,840
specifically so that here

38
00:01:14,560 --> 00:01:17,840
here what you should be looking at

39
00:01:18,720 --> 00:01:23,759
i want you specifically looking at the

40
00:01:20,320 --> 00:01:23,759
data structure on

41
00:01:24,320 --> 00:01:30,000
page six and part three

42
00:01:26,400 --> 00:01:30,000
says missing from the picture

43
00:01:30,960 --> 00:01:35,439
all right it's called image bound import

44
00:01:32,960 --> 00:01:38,400
descriptor

45
00:01:35,439 --> 00:01:40,640
says missing from the picture

46
00:01:38,400 --> 00:01:42,240
says image boundary import descriptor

47
00:01:40,640 --> 00:01:43,840
this gives you basically everything you

48
00:01:42,240 --> 00:01:45,920
need to know to interpret something we

49
00:01:43,840 --> 00:01:48,000
know the data directory entry is going

50
00:01:45,920 --> 00:01:49,520
to point at those

51
00:01:48,000 --> 00:01:52,479
and we know that the time is going to be

52
00:01:49,520 --> 00:01:55,280
time date stamp which is four bytes big

53
00:01:52,479 --> 00:01:57,200
offset to module name that's two bytes

54
00:01:55,280 --> 00:01:59,200
big that's an rva where if you go to

55
00:01:57,200 --> 00:02:00,880
that rba you'll see which dll it's

56
00:01:59,200 --> 00:02:02,399
talking about

57
00:02:00,880 --> 00:02:03,439
and then you've got two bytes of you

58
00:02:02,399 --> 00:02:05,040
know either

59
00:02:03,439 --> 00:02:06,320
forward or reps or

60
00:02:05,040 --> 00:02:08,000
not defined

61
00:02:06,320 --> 00:02:10,399
so between those two i might ask you

62
00:02:08,000 --> 00:02:12,800
like you know what's the you know how

63
00:02:10,399 --> 00:02:15,120
many delay load imports are there for

64
00:02:12,800 --> 00:02:18,000
food.dll right you'll have to actually

65
00:02:15,120 --> 00:02:21,040
go to that offset module name rba you'll

66
00:02:18,000 --> 00:02:23,280
have to go that many you know rva bytes

67
00:02:21,040 --> 00:02:24,959
into your file to find which module it's

68
00:02:23,280 --> 00:02:26,319
actually referring to

69
00:02:24,959 --> 00:02:28,720
and then

70
00:02:26,319 --> 00:02:30,239
you'll have to go from that wait

71
00:02:28,720 --> 00:02:32,160
and then no

72
00:02:30,239 --> 00:02:33,680
yeah

73
00:02:32,160 --> 00:02:37,360
all right i'm pointing out the boundary

74
00:02:33,680 --> 00:02:37,360
ports thing not the delay load in place

75
00:02:38,560 --> 00:02:42,000
do you need anything for bonding points

76
00:02:42,400 --> 00:02:45,920
i think you're gonna have to do that for

77
00:02:43,760 --> 00:02:48,800
bounding parts as well because bounding

78
00:02:45,920 --> 00:02:50,800
torch is not exposed by uh

79
00:02:48,800 --> 00:02:52,640
that foundation well i don't know let's

80
00:02:50,800 --> 00:02:54,800
see people ask me questions i'll tell

81
00:02:52,640 --> 00:02:57,200
you the answer

82
00:02:54,800 --> 00:02:58,480
yeah for the delay load that's slide 18

83
00:02:57,200 --> 00:03:00,080
there right

84
00:02:58,480 --> 00:03:02,159
but it's the same notion right you have

85
00:03:00,080 --> 00:03:04,640
to look at the sizes here

86
00:03:02,159 --> 00:03:06,879
if the size says rva

87
00:03:04,640 --> 00:03:06,879
is

88
00:03:07,599 --> 00:03:14,720
32 bits always 64 32 bits binary doesn't

89
00:03:11,519 --> 00:03:15,920
matter it's always 32 bits

90
00:03:14,720 --> 00:03:17,840
and basically you would have to

91
00:03:15,920 --> 00:03:21,120
interpret the notebook you'd have to

92
00:03:17,840 --> 00:03:23,519
find a 64-bit import address table based

93
00:03:21,120 --> 00:03:25,519
on pulling the rva out of the

94
00:03:23,519 --> 00:03:26,640
you know you'd have to go 32 bits in to

95
00:03:25,519 --> 00:03:28,480
get the

96
00:03:26,640 --> 00:03:30,879
dll name and 32-bits indicate the

97
00:03:28,480 --> 00:03:34,159
h-month and 32-bits in to get the

98
00:03:30,879 --> 00:03:37,120
iat and so forth so like i said this

99
00:03:34,159 --> 00:03:37,120
round is going to be nasty

100
00:03:37,360 --> 00:03:41,200
sooner you get started the sooner it'll

101
00:03:39,120 --> 00:03:43,680
be done

102
00:03:41,200 --> 00:03:43,680
go at it

103
00:03:44,480 --> 00:03:47,200
so it's 32

104
00:03:57,360 --> 00:04:00,959
and i would say take your break if you

105
00:03:58,799 --> 00:04:02,480
need it five minutes but

106
00:04:00,959 --> 00:04:04,319
it's all mixed together right now five

107
00:04:02,480 --> 00:04:06,560
minutes break plus

108
00:04:04,319 --> 00:04:08,799
however many minutes we need to

109
00:04:06,560 --> 00:04:09,760
get at least a silver medal winner this

110
00:04:08,799 --> 00:04:11,120
time

111
00:04:09,760 --> 00:04:13,439
i don't think we

112
00:04:11,120 --> 00:04:15,120
said who won last time

113
00:04:13,439 --> 00:04:16,799
i haven't got his gold we'll have to go

114
00:04:15,120 --> 00:04:19,120
back and we'll look through your this is

115
00:04:16,799 --> 00:04:20,799
why we have the csv files we can go

116
00:04:19,120 --> 00:04:22,400
figure out who the number one winner was

117
00:04:20,799 --> 00:04:24,479
from last time

118
00:04:22,400 --> 00:04:26,639
all right i've got one bug

119
00:04:24,479 --> 00:04:28,960
i got a question what is the rva that

120
00:04:26,639 --> 00:04:30,320
points directly at the delay load import

121
00:04:28,960 --> 00:04:32,639
address table

122
00:04:30,320 --> 00:04:34,000
right so how i would find that i want to

123
00:04:32,639 --> 00:04:35,840
find the delay loading port address

124
00:04:34,000 --> 00:04:37,680
table so i need to first start from the

125
00:04:35,840 --> 00:04:40,080
delay load

126
00:04:37,680 --> 00:04:42,000
descriptor table right

127
00:04:40,080 --> 00:04:43,759
problem is the delay load descriptor

128
00:04:42,000 --> 00:04:45,040
table is not even filled in in this

129
00:04:43,759 --> 00:04:46,880
binary

130
00:04:45,040 --> 00:04:49,680
so i would normally go to the delay load

131
00:04:46,880 --> 00:04:51,759
import descriptor table from this and

132
00:04:49,680 --> 00:04:53,680
it's just not there so you get this

133
00:04:51,759 --> 00:04:55,919
question

134
00:04:53,680 --> 00:04:57,520
you may have an error although i'm going

135
00:04:55,919 --> 00:05:00,880
to try just zero

136
00:04:57,520 --> 00:05:03,040
and somehow that's correct

137
00:05:00,880 --> 00:05:04,639
that's not what it's supposed to be but

138
00:05:03,040 --> 00:05:06,720
somehow that zero that's there is

139
00:05:04,639 --> 00:05:09,199
correct

140
00:05:06,720 --> 00:05:11,600
here's my bug list

141
00:05:09,199 --> 00:05:13,680
this is why i have that mode one where i

142
00:05:11,600 --> 00:05:18,440
can like take a seed and i can recreate

143
00:05:13,680 --> 00:05:18,440
the exact sequence that caused it

144
00:05:26,639 --> 00:05:30,639
so how do you find the time stamp for it

145
00:05:31,199 --> 00:05:35,680
how do you find the time steps do you

146
00:05:33,039 --> 00:05:37,440
have a 64-bit one

147
00:05:35,680 --> 00:05:39,919
32

148
00:05:37,440 --> 00:05:42,320
32 it's easy 64 it's hard

149
00:05:39,919 --> 00:05:44,000
i think it's 64.

150
00:05:42,320 --> 00:05:45,360
right now all right

151
00:05:44,000 --> 00:05:47,600
64

152
00:05:45,360 --> 00:05:51,120
thing with bound

153
00:05:47,600 --> 00:05:53,520
i think i have something

154
00:05:51,120 --> 00:05:54,800
because if it was 32 then p-e-v

155
00:05:53,520 --> 00:05:56,560
would just show

156
00:05:54,800 --> 00:05:59,039
all right so i think i have one that has

157
00:05:56,560 --> 00:06:00,800
that that i can kind of show

158
00:05:59,039 --> 00:06:03,280
all right here's one of those nasty

159
00:06:00,800 --> 00:06:03,280
cases

160
00:06:04,080 --> 00:06:07,919
so

161
00:06:05,520 --> 00:06:09,360
64-bit binary that's found

162
00:06:07,919 --> 00:06:10,960
and you know i guess i'm asking you for

163
00:06:09,360 --> 00:06:12,720
the time-date step for some particular

164
00:06:10,960 --> 00:06:15,840
one right

165
00:06:12,720 --> 00:06:17,039
start as always at the data directory

166
00:06:15,840 --> 00:06:21,520
and you need to

167
00:06:17,039 --> 00:06:23,440
find the bound import rba

168
00:06:21,520 --> 00:06:24,560
all right so that gets us to the bound

169
00:06:23,440 --> 00:06:25,360
import

170
00:06:24,560 --> 00:06:27,440
you know

171
00:06:25,360 --> 00:06:30,800
directory for instance

172
00:06:27,440 --> 00:06:32,560
so 2e0 of mine is it it'll potentially

173
00:06:30,800 --> 00:06:36,080
be different in yours is it different in

174
00:06:32,560 --> 00:06:36,080
yours it's actually the same

175
00:06:36,400 --> 00:06:41,280
so 2e0

176
00:06:38,880 --> 00:06:43,199
and then this is where it gets tricky

177
00:06:41,280 --> 00:06:44,400
you now have to go to the actual raw

178
00:06:43,199 --> 00:06:46,720
data

179
00:06:44,400 --> 00:06:48,400
just clicking on section headers

180
00:06:46,720 --> 00:06:50,800
and don't like click into any of the

181
00:06:48,400 --> 00:06:52,319
sections just click on section enters

182
00:06:50,800 --> 00:06:54,160
this will basically be giving you down

183
00:06:52,319 --> 00:06:55,520
here you can see m z so now you're

184
00:06:54,160 --> 00:06:58,160
looking at the beginning of the file

185
00:06:55,520 --> 00:07:00,319
right okay and we know that qpe 0 was

186
00:06:58,160 --> 00:07:01,919
the rva like in memory but it's also

187
00:07:00,319 --> 00:07:04,960
when it's that low it's probably also

188
00:07:01,919 --> 00:07:07,520
the file offset to e0 there's this

189
00:07:04,960 --> 00:07:09,280
little arrow down here that gives us go

190
00:07:07,520 --> 00:07:10,400
to offset that's actually saying file

191
00:07:09,280 --> 00:07:15,240
offsets

192
00:07:10,400 --> 00:07:15,240
so we're going to put in 2e0 there

193
00:07:18,400 --> 00:07:21,919
all right now this doesn't look like

194
00:07:19,759 --> 00:07:24,240
anything this doesn't look like real

195
00:07:21,919 --> 00:07:25,680
data but it is real data

196
00:07:24,240 --> 00:07:28,240
so

197
00:07:25,680 --> 00:07:30,720
what we're seeing here is

198
00:07:28,240 --> 00:07:33,919
this is the time date stamp

199
00:07:30,720 --> 00:07:35,919
four bytes of time date stamp

200
00:07:33,919 --> 00:07:38,479
two bytes of

201
00:07:35,919 --> 00:07:40,240
rba to

202
00:07:38,479 --> 00:07:42,160
oh i think i forgot to talk about this

203
00:07:40,240 --> 00:07:44,479
you know

204
00:07:42,160 --> 00:07:46,240
two bytes of rba to the name of

205
00:07:44,479 --> 00:07:48,720
whichever time date step like which

206
00:07:46,240 --> 00:07:50,800
module are we talking about right now

207
00:07:48,720 --> 00:07:53,120
and then two bytes of

208
00:07:50,800 --> 00:07:54,560
um you know forwarder references or

209
00:07:53,120 --> 00:07:56,639
whatever right so right now we need to

210
00:07:54,560 --> 00:07:58,960
interpret these eight bytes

211
00:07:56,639 --> 00:08:01,440
according to the structure on page six i

212
00:07:58,960 --> 00:08:04,879
believe it was right

213
00:08:01,440 --> 00:08:07,120
slide six says missing from the picture

214
00:08:04,879 --> 00:08:09,680
image bounding port descriptor so right

215
00:08:07,120 --> 00:08:12,000
now at this te0 offset we've got an

216
00:08:09,680 --> 00:08:13,759
array of these things time date stamp

217
00:08:12,000 --> 00:08:17,120
offset module name

218
00:08:13,759 --> 00:08:17,120
and then forward reps

219
00:08:18,639 --> 00:08:21,039
now here's the thing where i have to

220
00:08:20,080 --> 00:08:24,720
actually

221
00:08:21,039 --> 00:08:27,039
correct myself versus what i said before

222
00:08:24,720 --> 00:08:30,160
at least i didn't make it

223
00:08:27,039 --> 00:08:31,280
as clear as i should yeah i mean okay

224
00:08:30,160 --> 00:08:33,200
yeah

225
00:08:31,280 --> 00:08:35,120
i took myself by surprise in the notes

226
00:08:33,200 --> 00:08:37,519
before and i didn't make it as clear as

227
00:08:35,120 --> 00:08:37,519
i should have

228
00:08:38,000 --> 00:08:43,680
back in the bound imports this is one of

229
00:08:40,560 --> 00:08:43,680
these annoying things

230
00:08:44,240 --> 00:08:47,360
right here

231
00:08:46,000 --> 00:08:49,519
this is the data structure we were just

232
00:08:47,360 --> 00:08:52,000
talking about

233
00:08:49,519 --> 00:08:54,160
offset module name as it says on the

234
00:08:52,000 --> 00:08:55,200
next slide and i actually listen to my

235
00:08:54,160 --> 00:08:58,480
slides

236
00:08:55,200 --> 00:09:00,240
offset modulate is not a base relative

237
00:08:58,480 --> 00:09:02,160
rba

238
00:09:00,240 --> 00:09:03,279
it is an offset from the beginning of

239
00:09:02,160 --> 00:09:05,519
the first

240
00:09:03,279 --> 00:09:07,839
import descriptor

241
00:09:05,519 --> 00:09:11,120
that's

242
00:09:07,839 --> 00:09:12,880
let's look at this concretely

243
00:09:11,120 --> 00:09:14,720
we've got a time-date tip and then we've

244
00:09:12,880 --> 00:09:16,880
got this offset to module name but that

245
00:09:14,720 --> 00:09:18,080
is not like 58 bytes from the beginning

246
00:09:16,880 --> 00:09:20,080
of the file

247
00:09:18,080 --> 00:09:23,200
that's 58 bytes from the beginning of

248
00:09:20,080 --> 00:09:25,279
this array so that's just far enough to

249
00:09:23,200 --> 00:09:27,200
like put it right down here somewhere

250
00:09:25,279 --> 00:09:29,120
right after this array

251
00:09:27,200 --> 00:09:31,519
so going back to this example i was just

252
00:09:29,120 --> 00:09:31,519
showing you

253
00:09:32,080 --> 00:09:36,880
i said we're going to treat this as a

254
00:09:33,519 --> 00:09:38,480
time date stamp it's zero and now we've

255
00:09:36,880 --> 00:09:41,040
got this it's going to be a little

256
00:09:38,480 --> 00:09:42,880
endian and that's another tricky thing

257
00:09:41,040 --> 00:09:45,760
it's little endian so it's actually zero

258
00:09:42,880 --> 00:09:47,600
zero two zero and that's the offset from

259
00:09:45,760 --> 00:09:51,120
the beginning of this structure so it's

260
00:09:47,600 --> 00:09:54,480
two e zero plus two zero so we have plus

261
00:09:51,120 --> 00:09:55,920
ten plus twenty so this right here

262
00:09:54,480 --> 00:09:57,600
is going to be

263
00:09:55,920 --> 00:09:59,920
the string

264
00:09:57,600 --> 00:10:02,000
right so this plus

265
00:09:59,920 --> 00:10:04,399
zero zero two zero

266
00:10:02,000 --> 00:10:05,519
is equal to msvcrt

267
00:10:04,399 --> 00:10:07,519
so this

268
00:10:05,519 --> 00:10:11,519
eight bytes right here which is very

269
00:10:07,519 --> 00:10:14,240
hard to interpret is actually msvcrt's

270
00:10:11,519 --> 00:10:16,240
date bound imports directory entry

271
00:10:14,240 --> 00:10:17,519
now we go to the next things right so

272
00:10:16,240 --> 00:10:19,440
two four

273
00:10:17,519 --> 00:10:20,640
four bytes of zero that's our time date

274
00:10:19,440 --> 00:10:23,600
stamp again

275
00:10:20,640 --> 00:10:26,240
like i said i just made this with the

276
00:10:23,600 --> 00:10:27,839
cff explorer so it apparently didn't

277
00:10:26,240 --> 00:10:29,519
really didn't fully

278
00:10:27,839 --> 00:10:31,279
now we've got another little endian

279
00:10:29,519 --> 00:10:32,720
offset 2d

280
00:10:31,279 --> 00:10:34,959
and that's going to be the offset to our

281
00:10:32,720 --> 00:10:38,079
next string we can just see 2 0 was here

282
00:10:34,959 --> 00:10:39,839
2d is going to be here it's kernel 32.

283
00:10:38,079 --> 00:10:42,640
so it's pretty much in order we've got

284
00:10:39,839 --> 00:10:44,079
msp actually no i can't blame cffx4 this

285
00:10:42,640 --> 00:10:47,120
is my fault

286
00:10:44,079 --> 00:10:49,519
i'm pretty sure do i make this photo

287
00:10:47,120 --> 00:10:51,200
thing myself

288
00:10:49,519 --> 00:10:53,440
i can't remember whether i made this

289
00:10:51,200 --> 00:10:54,240
we'll find for culprit i guarantee you

290
00:10:53,440 --> 00:10:55,760
that

291
00:10:54,240 --> 00:10:57,360
but anyways

292
00:10:55,760 --> 00:10:58,800
it's just these eight bytes at a time

293
00:10:57,360 --> 00:11:00,800
which you interpret according to that

294
00:10:58,800 --> 00:11:03,519
structure in there right four bytes time

295
00:11:00,800 --> 00:11:06,160
bait stamp two bytes little indian and

296
00:11:03,519 --> 00:11:08,079
flip it around offset to a string and

297
00:11:06,160 --> 00:11:10,160
then the string array which thankfully

298
00:11:08,079 --> 00:11:13,200
is in the right order so it's msvcr

299
00:11:10,160 --> 00:11:14,880
kernel 32 and tdo and so what was the

300
00:11:13,200 --> 00:11:16,399
specific question i was asking i wanted

301
00:11:14,880 --> 00:11:18,880
to know what year

302
00:11:16,399 --> 00:11:21,120
all right so what year

303
00:11:18,880 --> 00:11:22,880
zero seconds plus you know zero seconds

304
00:11:21,120 --> 00:11:24,880
after nineteen seven nineteen seven

305
00:11:22,880 --> 00:11:26,480
nineteen seven right so you interpret

306
00:11:24,880 --> 00:11:28,880
this as a time based and even though

307
00:11:26,480 --> 00:11:31,519
it's not a nice you know large numbered

308
00:11:28,880 --> 00:11:33,120
timely stamp it's still a second since

309
00:11:31,519 --> 00:11:35,200
1970

310
00:11:33,120 --> 00:11:38,240
and it's zero

311
00:11:35,200 --> 00:11:38,240
and that's what it is

312
00:11:40,399 --> 00:11:44,000
see how annoying it is when the tools

313
00:11:42,160 --> 00:11:45,839
don't actually just interpret things for

314
00:11:44,000 --> 00:11:47,680
you but this is how you would have to do

315
00:11:45,839 --> 00:11:49,360
it if you were writing a program if you

316
00:11:47,680 --> 00:11:50,240
want to write a program that parses this

317
00:11:49,360 --> 00:11:52,480
stuff

318
00:11:50,240 --> 00:11:54,640
you have to go in and just follow the

319
00:11:52,480 --> 00:11:57,120
rbas interpret according to some data

320
00:11:54,640 --> 00:11:58,640
structures and this is just you manually

321
00:11:57,120 --> 00:12:00,160
having to do this data structure

322
00:11:58,640 --> 00:12:02,399
interpretation

323
00:12:00,160 --> 00:12:04,959
you have to figure out which name this

324
00:12:02,399 --> 00:12:07,360
is associated with and that's vcr this

325
00:12:04,959 --> 00:12:09,279
one is associated

326
00:12:07,360 --> 00:12:11,600
that's all it's just

327
00:12:09,279 --> 00:12:14,240
annoying

328
00:12:11,600 --> 00:12:15,680
but it is what it is

329
00:12:14,240 --> 00:12:16,560
so little endian means that every pair

330
00:12:15,680 --> 00:12:18,160
of fighters

331
00:12:16,560 --> 00:12:19,760
means every bite it's not even

332
00:12:18,160 --> 00:12:21,279
necessarily a pair of bites it can be

333
00:12:19,760 --> 00:12:24,760
you know four bites but it's bite bites

334
00:12:21,279 --> 00:12:24,760
not bit wise

335
00:12:28,720 --> 00:12:32,720
right so you can interpret things like

336
00:12:30,959 --> 00:12:34,560
this when you've got a bound import data

337
00:12:32,720 --> 00:12:35,680
structure right

338
00:12:34,560 --> 00:12:37,839
or

339
00:12:35,680 --> 00:12:39,519
you can figure out what the ordering

340
00:12:37,839 --> 00:12:41,360
should be now strictly speaking this

341
00:12:39,519 --> 00:12:43,440
would not be the case but for this

342
00:12:41,360 --> 00:12:45,519
purpose it will work right i can kind of

343
00:12:43,440 --> 00:12:48,160
show you already right here msvcr then

344
00:12:45,519 --> 00:12:49,839
kernel 32 and n2dl right you can kind of

345
00:12:48,160 --> 00:12:51,920
see that in the strings and so what i'm

346
00:12:49,839 --> 00:12:55,120
telling you is that you know this is the

347
00:12:51,920 --> 00:12:57,360
time date stand for msvcr this is the

348
00:12:55,120 --> 00:13:00,000
you know offset to the string which

349
00:12:57,360 --> 00:13:01,680
proves that it's for msvcr and this is

350
00:13:00,000 --> 00:13:03,360
the you know number of forwarder

351
00:13:01,680 --> 00:13:05,040
references

352
00:13:03,360 --> 00:13:06,639
uh what oliver was just doing is he was

353
00:13:05,040 --> 00:13:09,600
just fluffing back to here and going oh

354
00:13:06,639 --> 00:13:12,079
i don't go as msbcr and then kernel 32.

355
00:13:09,600 --> 00:13:14,240
so that mostly gets you the order if you

356
00:13:12,079 --> 00:13:16,800
want to like just say what order if you

357
00:13:14,240 --> 00:13:18,079
ask me a question about kernel 32 i know

358
00:13:16,800 --> 00:13:19,200
it's not going to be the first data

359
00:13:18,079 --> 00:13:21,040
structure i look at right it's going to

360
00:13:19,200 --> 00:13:23,040
be the second data structure i look at

361
00:13:21,040 --> 00:13:24,639
but as you saw when you actually go look

362
00:13:23,040 --> 00:13:26,880
at that offset

363
00:13:24,639 --> 00:13:28,399
you know it had the forwarded rafts

364
00:13:26,880 --> 00:13:30,000
thing kind of thing it had three entries

365
00:13:28,399 --> 00:13:32,639
it didn't have just two that's because

366
00:13:30,000 --> 00:13:34,800
of that forward or rust kind of thing

367
00:13:32,639 --> 00:13:34,800
so

368
00:13:38,079 --> 00:13:41,760
right and why is that if you actually

369
00:13:40,240 --> 00:13:44,639
look at the section entry you see that

370
00:13:41,760 --> 00:13:46,320
it has a non-zero forwarder reps it has

371
00:13:44,639 --> 00:13:48,480
a zero one

372
00:13:46,320 --> 00:13:50,959
entry there as well so that's why this

373
00:13:48,480 --> 00:13:52,079
is the you know real coordinated thing

374
00:13:50,959 --> 00:13:55,600
with you know

375
00:13:52,079 --> 00:13:57,519
found import bound import forwarder refs

376
00:13:55,600 --> 00:13:59,600
null entry

377
00:13:57,519 --> 00:14:01,199
and use clicking back to the import

378
00:13:59,600 --> 00:14:02,480
directory which at least tells you if

379
00:14:01,199 --> 00:14:04,399
it's asking you a question about a

380
00:14:02,480 --> 00:14:06,480
particular thing you can assume the

381
00:14:04,399 --> 00:14:08,240
ordering pretty much in here but if

382
00:14:06,480 --> 00:14:10,560
you're going to assume that ordering you

383
00:14:08,240 --> 00:14:12,240
may as well just assume the ordering

384
00:14:10,560 --> 00:14:16,560
based on those string table that you see

385
00:14:12,240 --> 00:14:18,880
immediately after the stuff anyways

386
00:14:16,560 --> 00:14:21,360
he was asked you know what is the

387
00:14:18,880 --> 00:14:23,760
what year was a you know particular

388
00:14:21,360 --> 00:14:26,639
thing and he was asking for colonel 32

389
00:14:23,760 --> 00:14:29,120
it said what is the year that colonel 32

390
00:14:26,639 --> 00:14:32,320
was compiled and he just assumed it was

391
00:14:29,120 --> 00:14:33,680
the first thing but you know

392
00:14:32,320 --> 00:14:35,680
if he had looked here he would say oh

393
00:14:33,680 --> 00:14:37,120
kermit 32 is the second thing right i

394
00:14:35,680 --> 00:14:39,040
need to go from the first data structure

395
00:14:37,120 --> 00:14:41,040
to the second data structure and take

396
00:14:39,040 --> 00:14:43,199
that thing's time step

397
00:14:41,040 --> 00:14:44,639
chapter b to zero

398
00:14:43,199 --> 00:14:47,279
basically tomorrow what we're going to

399
00:14:44,639 --> 00:14:50,160
come in and do is we're going to do a

400
00:14:47,279 --> 00:14:52,000
full class race two through round five

401
00:14:50,160 --> 00:14:54,399
kind of thing so you probably want to

402
00:14:52,000 --> 00:14:56,320
get this round five step down now if you

403
00:14:54,399 --> 00:14:57,279
expect to get through all five rounds

404
00:14:56,320 --> 00:14:59,600
tomorrow

405
00:14:57,279 --> 00:15:00,639
no i'm gonna test it out make sure i get

406
00:14:59,600 --> 00:15:02,320
us a

407
00:15:00,639 --> 00:15:04,000
an entry which hopefully doesn't have

408
00:15:02,320 --> 00:15:05,519
any crashes or anything in that but

409
00:15:04,000 --> 00:15:07,519
simultaneously if you get certain

410
00:15:05,519 --> 00:15:09,279
questions wrong and i don't follow the

411
00:15:07,519 --> 00:15:10,880
exact same order if you've got the fifth

412
00:15:09,279 --> 00:15:12,399
one wrong and you got the sixth one

413
00:15:10,880 --> 00:15:13,360
wrong you know you kind of diverge in

414
00:15:12,399 --> 00:15:15,360
terms of

415
00:15:13,360 --> 00:15:16,720
getting to the end

416
00:15:15,360 --> 00:15:18,160
in the same thing so i can't guarantee

417
00:15:16,720 --> 00:15:20,240
no crashes but

418
00:15:18,160 --> 00:15:21,760
we'll do as best as we can tomorrow so

419
00:15:20,240 --> 00:15:23,680
first thing tomorrow will basically be

420
00:15:21,760 --> 00:15:25,360
running through all five rounds all in

421
00:15:23,680 --> 00:15:26,480
order and we'll just see you know who

422
00:15:25,360 --> 00:15:28,320
can get there

423
00:15:26,480 --> 00:15:30,480
fastest and basically it's just a good

424
00:15:28,320 --> 00:15:32,880
data collection for me

425
00:15:30,480 --> 00:15:35,199
and then from there we'll continue on

426
00:15:32,880 --> 00:15:35,199
with


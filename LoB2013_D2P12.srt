1
00:00:03,550 --> 00:00:09,730
moving along to the back to the data

2
00:00:06,189 --> 00:00:13,269
directory into this index five or so we

3
00:00:09,730 --> 00:00:15,820
have image directory entry base real oak

4
00:00:13,269 --> 00:00:17,109
and this is going to be the relocation

5
00:00:15,820 --> 00:00:19,449
information so we're finally going to

6
00:00:17,109 --> 00:00:21,519
get at that information where I said if

7
00:00:19,449 --> 00:00:23,169
it wants to be at base address 1 million

8
00:00:21,519 --> 00:00:24,730
but it gets loaded at base address 2

9
00:00:23,169 --> 00:00:27,550
million you got to fix up all the

10
00:00:24,730 --> 00:00:29,679
constants how you find how the OS loader

11
00:00:27,550 --> 00:00:31,569
finds all the information to fix where

12
00:00:29,679 --> 00:00:33,310
all those constants are to fix them up

13
00:00:31,569 --> 00:00:35,320
is it's going to go to the base

14
00:00:33,310 --> 00:00:37,030
relocation information and it's going to

15
00:00:35,320 --> 00:00:39,340
get forwarded on to a list of places

16
00:00:37,030 --> 00:00:43,629
that it needs to fix up if it moves the

17
00:00:39,340 --> 00:00:45,550
thing in memory so relocation so that

18
00:00:43,629 --> 00:00:48,010
base entry real oak basically points at

19
00:00:45,550 --> 00:00:49,750
an array oh well points that one of

20
00:00:48,010 --> 00:00:52,000
these structure no I guess it's an array

21
00:00:49,750 --> 00:00:54,250
of these structures well it's at an

22
00:00:52,000 --> 00:00:55,809
array of these structures base image

23
00:00:54,250 --> 00:00:57,820
based relocation it's got a virtual

24
00:00:55,809 --> 00:01:00,399
address it's got a size and then this

25
00:00:57,820 --> 00:01:02,649
type offset oh that's commented out to

26
00:01:00,399 --> 00:01:04,360
their money but there is going to be

27
00:01:02,649 --> 00:01:07,660
something immediately after it that's a

28
00:01:04,360 --> 00:01:10,270
board size so virtual address is

29
00:01:07,660 --> 00:01:12,730
basically going to say that this data

30
00:01:10,270 --> 00:01:15,370
structure corresponds to a given r-va

31
00:01:12,730 --> 00:01:17,110
range and it'll only ever be hex 1000

32
00:01:15,370 --> 00:01:19,360
range so it'll be virtual address will

33
00:01:17,110 --> 00:01:20,860
say like hex 1000 and what that'll tell

34
00:01:19,360 --> 00:01:23,170
you is that this particular data

35
00:01:20,860 --> 00:01:24,670
structure and all the data associated

36
00:01:23,170 --> 00:01:27,190
with it is going to be relevant from

37
00:01:24,670 --> 00:01:29,260
1000 to 2000 and so the next data

38
00:01:27,190 --> 00:01:31,540
structure will say I am applying to 2000

39
00:01:29,260 --> 00:01:33,490
it'll be from 2000 and 3000 and we'll

40
00:01:31,540 --> 00:01:34,990
see that why that is in a second but the

41
00:01:33,490 --> 00:01:37,390
first order virtual address is just

42
00:01:34,990 --> 00:01:39,730
saying this structure applies to some

43
00:01:37,390 --> 00:01:41,080
chunk of memory range where it's going

44
00:01:39,730 --> 00:01:43,290
to have a list of everything that needs

45
00:01:41,080 --> 00:01:46,090
to be fixed inside of that memory range

46
00:01:43,290 --> 00:01:48,340
sighs a block is then how big is this

47
00:01:46,090 --> 00:01:50,050
list of stuff that has to be fixed in

48
00:01:48,340 --> 00:01:52,270
that memory range and then the next

49
00:01:50,050 --> 00:01:54,460
thing after size the block is going to

50
00:01:52,270 --> 00:01:56,350
be a bunch of words that are the exact

51
00:01:54,460 --> 00:01:59,740
locations that have to be fixed after

52
00:01:56,350 --> 00:02:01,420
you do some calculation so this is a

53
00:01:59,740 --> 00:02:03,820
terrible example right here and I don't

54
00:02:01,420 --> 00:02:05,850
want to use that at all it's to do

55
00:02:03,820 --> 00:02:08,319
change me so I'm going to I'm going to

56
00:02:05,850 --> 00:02:13,720
come up with a better example here on

57
00:02:08,319 --> 00:02:26,210
the fly Eve you still yes

58
00:02:13,720 --> 00:02:28,580
go with the Exies good all right so i

59
00:02:26,210 --> 00:02:30,080
just opened up one of my 32-bit things

60
00:02:28,580 --> 00:02:31,970
so i just told you there's a data

61
00:02:30,080 --> 00:02:34,220
structure that has an RV a and a size

62
00:02:31,970 --> 00:02:36,050
and then it's immediately followed by a

63
00:02:34,220 --> 00:02:39,020
bunch of entries that say here's all the

64
00:02:36,050 --> 00:02:40,820
places you need to fix up now these

65
00:02:39,020 --> 00:02:44,240
entries are of size word so there are

66
00:02:40,820 --> 00:02:46,220
two bites and they're actually broken up

67
00:02:44,240 --> 00:02:48,710
not along bite boundaries it turns out

68
00:02:46,220 --> 00:02:51,110
it's the first four bits the topmost

69
00:02:48,710 --> 00:02:53,330
four bits is specifying what type of

70
00:02:51,110 --> 00:02:55,850
entry it is what kind of fix up the OS

71
00:02:53,330 --> 00:02:59,330
should do the top four bits in this case

72
00:02:55,850 --> 00:03:01,910
number four bits to a hex character so

73
00:02:59,330 --> 00:03:04,970
just this three basically is saying I'm

74
00:03:01,910 --> 00:03:06,560
of Type image rail-based high low so I

75
00:03:04,970 --> 00:03:08,750
don't really talk about the type because

76
00:03:06,560 --> 00:03:10,730
as far as you're concerned everything is

77
00:03:08,750 --> 00:03:12,260
always going to be image-based real low

78
00:03:10,730 --> 00:03:14,090
it's always going to start all these

79
00:03:12,260 --> 00:03:15,500
words are going to start with three so

80
00:03:14,090 --> 00:03:18,530
you can just think of chopped the three

81
00:03:15,500 --> 00:03:20,810
off the top and then what you need to do

82
00:03:18,530 --> 00:03:22,970
to get the location to fix up is you

83
00:03:20,810 --> 00:03:26,989
chop the three off the top and then you

84
00:03:22,970 --> 00:03:29,900
say take the rest 001 and add it to the

85
00:03:26,989 --> 00:03:33,470
1000 that this range applies to and so

86
00:03:29,900 --> 00:03:34,700
it's 1000 one is the RVA where it needs

87
00:03:33,470 --> 00:03:38,150
to fix something up so there's a

88
00:03:34,700 --> 00:03:40,130
constant at RV a 1001 that the OS needs

89
00:03:38,150 --> 00:03:42,200
to fix up if it moves this thing in

90
00:03:40,130 --> 00:03:46,220
memory there's a constant at one

91
00:03:42,200 --> 00:03:47,840
thousand seven is a constant at 1016 so

92
00:03:46,220 --> 00:03:50,390
how I got that is I just chopped off the

93
00:03:47,840 --> 00:03:53,000
first three I add this to the 1000

94
00:03:50,390 --> 00:03:54,590
you'll see over here cff explorer or p

95
00:03:53,000 --> 00:03:56,750
view is just trying to do that for you

96
00:03:54,590 --> 00:03:59,570
all right so it just calculated so now

97
00:03:56,750 --> 00:04:01,100
if I go down so obviously if I'm

98
00:03:59,570 --> 00:04:03,950
chopping off the three and I've only got

99
00:04:01,100 --> 00:04:06,950
these 12 bits right 12 bits can only

100
00:04:03,950 --> 00:04:09,860
specify me you know from 1000 to 2000 12

101
00:04:06,950 --> 00:04:12,980
bits is hex 1000 worth of potential

102
00:04:09,860 --> 00:04:14,900
location so i can go from zero to FFF

103
00:04:12,980 --> 00:04:16,700
all right those are the only ranges that

104
00:04:14,900 --> 00:04:20,030
i can specify so i can go from zero to

105
00:04:16,700 --> 00:04:21,530
FFF plus x 1000 and that will give me

106
00:04:20,030 --> 00:04:24,470
everything in that range as a potential

107
00:04:21,530 --> 00:04:26,120
location how this works is you've got

108
00:04:24,470 --> 00:04:26,820
this first data structure that says I go

109
00:04:26,120 --> 00:04:29,070
from one

110
00:04:26,820 --> 00:04:31,320
a 2000 and then you have to have more

111
00:04:29,070 --> 00:04:34,620
data structures that tell you you know I

112
00:04:31,320 --> 00:04:37,500
go from 2000 and 3000 and I go from 3000

113
00:04:34,620 --> 00:04:40,080
and 4000 right so there's going to be an

114
00:04:37,500 --> 00:04:42,600
array of these data structures that have

115
00:04:40,080 --> 00:04:44,640
what range they apply to and then

116
00:04:42,600 --> 00:04:47,130
beneath that there's an array of within

117
00:04:44,640 --> 00:04:49,740
that range what specific locations does

118
00:04:47,130 --> 00:04:51,750
this apply to so again we just chopped

119
00:04:49,740 --> 00:04:55,830
the three up and we see AC so we go

120
00:04:51,750 --> 00:04:57,780
2000a see that's what see ffpe view

121
00:04:55,830 --> 00:05:00,240
shows I can show the same thing in cff

122
00:04:57,780 --> 00:05:03,360
Explorer it'll probably be trying even

123
00:05:00,240 --> 00:05:05,040
harder to fill that in for you so again

124
00:05:03,360 --> 00:05:07,590
you've got an array of things saying I'm

125
00:05:05,040 --> 00:05:10,620
from 1000 to 2000 I'm from 2000 and 3000

126
00:05:07,590 --> 00:05:13,140
so forth when you click on that so you

127
00:05:10,620 --> 00:05:15,090
got 1000 you've got the size of these

128
00:05:13,140 --> 00:05:18,570
entries afterwards and then these are

129
00:05:15,090 --> 00:05:21,060
the literal values 3005 2000 d3000 two

130
00:05:18,570 --> 00:05:22,680
out top the three off the top and so

131
00:05:21,060 --> 00:05:25,770
when it puts it all together we're in

132
00:05:22,680 --> 00:05:28,380
the 1000s right you know and so 1000

133
00:05:25,770 --> 00:05:37,380
plus 5 is 1005 we go down here we're in

134
00:05:28,380 --> 00:05:41,340
the 2000s 2090 is does money yes does it

135
00:05:37,380 --> 00:05:43,110
only exact words ah basically the type

136
00:05:41,340 --> 00:05:45,180
yep that's a good question basically the

137
00:05:43,110 --> 00:05:46,710
type is going to specify how big of a

138
00:05:45,180 --> 00:05:49,230
size it's fixing up and things like that

139
00:05:46,710 --> 00:05:51,600
and so yes in this case it's fixing up a

140
00:05:49,230 --> 00:05:54,810
D word right so it's saying there's a

141
00:05:51,600 --> 00:05:56,940
32-bit constant at this location and so

142
00:05:54,810 --> 00:05:59,130
you moved me in memory figure out what

143
00:05:56,940 --> 00:06:00,510
the diff is and add that dip in to this

144
00:05:59,130 --> 00:06:02,490
constant and so you're just applying

145
00:06:00,510 --> 00:06:03,810
that diff from where you want it to get

146
00:06:02,490 --> 00:06:06,630
loaded versus where you really got

147
00:06:03,810 --> 00:06:08,220
loaded apply that to 32-bit chunks it

148
00:06:06,630 --> 00:06:09,270
can definitely be different it's just

149
00:06:08,220 --> 00:06:12,210
then you'd have a different type

150
00:06:09,270 --> 00:06:14,340
essentially yep the type would say

151
00:06:12,210 --> 00:06:15,720
alright no this is actually a 64-bit

152
00:06:14,340 --> 00:06:20,580
thing that needs to be fixed up like

153
00:06:15,720 --> 00:06:22,920
part of it great yes the top nibble is a

154
00:06:20,580 --> 00:06:25,620
type specifying essentially what size

155
00:06:22,920 --> 00:06:27,300
it's going to be fixing out prices but

156
00:06:25,620 --> 00:06:29,130
far as you're concerned it's basically

157
00:06:27,300 --> 00:06:32,040
always going to be 32 bit that's that

158
00:06:29,130 --> 00:06:34,410
let me go double check if I can confirm

159
00:06:32,040 --> 00:06:36,230
on a 64-bit thing that I see a different

160
00:06:34,410 --> 00:06:40,670
type

161
00:06:36,230 --> 00:06:49,190
p never a good idea to go off the beaten

162
00:06:40,670 --> 00:06:52,670
path yeah sweet a instead of three we've

163
00:06:49,190 --> 00:06:56,210
got a different type so a is instead of

164
00:06:52,670 --> 00:06:59,030
threes type is der change those no okay

165
00:06:56,210 --> 00:07:01,700
doesn't matter so anyways A's instead of

166
00:06:59,030 --> 00:07:03,980
threes 64bit relocations instead of

167
00:07:01,700 --> 00:07:08,740
32-bit relocations and apply that

168
00:07:03,980 --> 00:07:12,260
relocation at the address 2000 plus 1 48

169
00:07:08,740 --> 00:07:19,190
2148 by relocation of size whatever the

170
00:07:12,260 --> 00:07:21,230
type is good question okeydoke that's

171
00:07:19,190 --> 00:07:25,340
pretty much it free locations any other

172
00:07:21,230 --> 00:07:27,500
questions on relocations we have some

173
00:07:25,340 --> 00:07:30,290
quizzes on this but i'm probably going

174
00:07:27,500 --> 00:07:32,570
to keep going up through 12 when I ask

175
00:07:30,290 --> 00:07:34,550
you to you know come back and play

176
00:07:32,570 --> 00:07:35,990
through these rounds just like I said we

177
00:07:34,550 --> 00:07:39,050
want to make sure we we get through all

178
00:07:35,990 --> 00:07:41,720
the PE material I want to try to get

179
00:07:39,050 --> 00:07:45,770
through all the material by very one

180
00:07:41,720 --> 00:07:49,220
o'clock at the latest alright so that's

181
00:07:45,770 --> 00:07:53,450
relocations let's see if I have anything

182
00:07:49,220 --> 00:07:55,940
else to say nope okay one last thing on

183
00:07:53,450 --> 00:07:57,380
this just for my belly wick since I said

184
00:07:55,940 --> 00:07:58,970
that you know some of the research and

185
00:07:57,380 --> 00:08:00,680
work I do is on memory integrity

186
00:07:58,970 --> 00:08:01,880
checking which has to do with you know

187
00:08:00,680 --> 00:08:04,100
when I was showing you over here we've

188
00:08:01,880 --> 00:08:06,500
got attackers changing IITs and you've

189
00:08:04,100 --> 00:08:08,780
got attackers changing V 80s and things

190
00:08:06,500 --> 00:08:11,060
like that so the stuff I work on is like

191
00:08:08,780 --> 00:08:12,230
look at it in memory to measure it and

192
00:08:11,060 --> 00:08:14,960
figure out whether it's being

193
00:08:12,230 --> 00:08:16,250
manipulated or not and so we do things

194
00:08:14,960 --> 00:08:17,900
like you know checking the data

195
00:08:16,250 --> 00:08:19,670
structures and the headers and stuff

196
00:08:17,900 --> 00:08:22,010
like that but also we do things like

197
00:08:19,670 --> 00:08:23,660
checking the dot text section right and

198
00:08:22,010 --> 00:08:25,310
so I said we've got the dot text section

199
00:08:23,660 --> 00:08:27,050
it's got all the code in there that code

200
00:08:25,310 --> 00:08:28,610
potentially has hard-coded constants

201
00:08:27,050 --> 00:08:31,310
which got changed when the thing got

202
00:08:28,610 --> 00:08:32,960
moved around in memory so if I just do a

203
00:08:31,310 --> 00:08:34,460
hash of the dot text section of memory

204
00:08:32,960 --> 00:08:35,960
and I do a hash of the dot text section

205
00:08:34,460 --> 00:08:38,180
on disk they're probably going to be

206
00:08:35,960 --> 00:08:39,680
different if the thing got relocated in

207
00:08:38,180 --> 00:08:40,760
memory right because when you relocated

208
00:08:39,680 --> 00:08:43,250
a memory you're changing a bunch of

209
00:08:40,760 --> 00:08:45,980
constants the hash is going to differ so

210
00:08:43,250 --> 00:08:47,870
when we do memory verification of you

211
00:08:45,980 --> 00:08:49,380
know code in memory for instance we have

212
00:08:47,870 --> 00:08:51,870
to back on our

213
00:08:49,380 --> 00:08:53,610
and we have to say you know where did it

214
00:08:51,870 --> 00:08:55,470
get located in memory and then we have

215
00:08:53,610 --> 00:08:58,200
to apply all of the relocations to a

216
00:08:55,470 --> 00:09:00,720
copy based on what we think the OS would

217
00:08:58,200 --> 00:09:02,340
have done we take a copy off disk apply

218
00:09:00,720 --> 00:09:04,080
all the relocations like we think it

219
00:09:02,340 --> 00:09:05,730
would have based at basing it at that

220
00:09:04,080 --> 00:09:08,130
address in memory and then we hash our

221
00:09:05,730 --> 00:09:09,720
we constructed copy instead of hashing

222
00:09:08,130 --> 00:09:12,570
you know just whatever happened to be in

223
00:09:09,720 --> 00:09:13,920
memory and sending it back well we dash

224
00:09:12,570 --> 00:09:16,620
whatever's in memory and we send it back

225
00:09:13,920 --> 00:09:17,790
but then we reconstruct based on where

226
00:09:16,620 --> 00:09:20,640
we think it should have been a memory

227
00:09:17,790 --> 00:09:25,790
for instance all right so anyways that's

228
00:09:20,640 --> 00:09:25,790
the only point about that due to do Reds


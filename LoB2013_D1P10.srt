1
00:00:03,570 --> 00:00:18,830
alright I'll put a side shot

2
00:00:06,930 --> 00:00:18,830
okay all right before you leave for

3
00:00:18,860 --> 00:00:27,840
lunch which meals do we care about here

4
00:00:22,590 --> 00:00:31,649
and why okay see a machine indeed time

5
00:00:27,840 --> 00:00:36,150
stamp to begin okay so why do we care

6
00:00:31,649 --> 00:00:40,410
about machine like yeah remember whether

7
00:00:36,150 --> 00:00:44,280
that has to do with 32 versus 64-bit

8
00:00:40,410 --> 00:00:47,580
memory it does have to do with 32 versus

9
00:00:44,280 --> 00:00:48,870
64 but not in terms of memory it's sort

10
00:00:47,580 --> 00:00:51,150
of an implication but it's not

11
00:00:48,870 --> 00:00:53,160
authoritative in the file enter machine

12
00:00:51,150 --> 00:00:55,440
means whether you're running 32-bit

13
00:00:53,160 --> 00:00:58,739
assembly or 64-bit assembly or arm

14
00:00:55,440 --> 00:01:00,540
assembly or MIPS assembly so this is

15
00:00:58,739 --> 00:01:02,940
telling us sort of what CPU but we can

16
00:01:00,540 --> 00:01:04,799
kind of infer based on that if it says

17
00:01:02,940 --> 00:01:06,570
it's running a 64 bit CPU that was

18
00:01:04,799 --> 00:01:10,050
probably set by a compiler that new is

19
00:01:06,570 --> 00:01:13,200
generating 64-bit binary yeah that's

20
00:01:10,050 --> 00:01:22,140
correct so anyone else time/date to do

21
00:01:13,200 --> 00:01:23,670
what do we care about that yeah when the

22
00:01:22,140 --> 00:01:27,350
file is compiled we can use that for

23
00:01:23,670 --> 00:01:27,350
some sort of our forensics and you know

24
00:01:33,950 --> 00:01:38,300
number of sections yeah why don't we

25
00:01:36,510 --> 00:01:48,520
care about that

26
00:01:38,300 --> 00:01:53,120
a case to you honey all right Jesse no

27
00:01:48,520 --> 00:01:54,950
other characteristics yep you may be one

28
00:01:53,120 --> 00:01:56,480
characteristic I promise like I said

29
00:01:54,950 --> 00:02:02,690
even though roses began too many

30
00:01:56,480 --> 00:02:04,520
characters but yes you know sometimes

31
00:02:02,690 --> 00:02:06,830
the DLL characteristics in the optional

32
00:02:04,520 --> 00:02:08,890
header okay so thinking back further

33
00:02:06,830 --> 00:02:17,930
from that or some earlier

34
00:02:08,890 --> 00:02:20,270
characteristics like there can execute

35
00:02:17,930 --> 00:02:22,160
in greater than two gigabyte greater

36
00:02:20,270 --> 00:02:24,590
than tree view I'd sort of range whether

37
00:02:22,160 --> 00:02:26,090
it's a PLL unsafe personally I consider

38
00:02:24,590 --> 00:02:27,500
like if you have to remember only one

39
00:02:26,090 --> 00:02:29,510
thing about that characteristic scale

40
00:02:27,500 --> 00:02:31,700
over then it's got the flag that says

41
00:02:29,510 --> 00:02:33,290
whether it's a deal or not the sections

42
00:02:31,700 --> 00:02:34,670
do actually have to be explicitly

43
00:02:33,290 --> 00:02:37,430
ordered so they could never be out of

44
00:02:34,670 --> 00:02:39,350
order like one virtual address as like a

45
00:02:37,430 --> 00:03:06,530
little has a higher value than the next

46
00:02:39,350 --> 00:03:08,660
section all right yeah so basically get

47
00:03:06,530 --> 00:03:10,459
started on round three I'm gonna do you

48
00:03:08,660 --> 00:03:11,900
no good five minutes or so ask me any

49
00:03:10,459 --> 00:03:13,610
questions if you're not clear on how the

50
00:03:11,900 --> 00:03:15,620
tools how you can use the tools to get

51
00:03:13,610 --> 00:03:17,930
the information out of things like I

52
00:03:15,620 --> 00:03:20,360
said it's always intuitive but again the

53
00:03:17,930 --> 00:03:22,280
point is in real life you go read a

54
00:03:20,360 --> 00:03:23,930
specification file it tells you what

55
00:03:22,280 --> 00:03:25,940
these headers and data structures are

56
00:03:23,930 --> 00:03:27,890
and then you fool around with tools

57
00:03:25,940 --> 00:03:30,800
until you think you figured out what

58
00:03:27,890 --> 00:03:34,090
you're actually I just gave you the

59
00:03:30,800 --> 00:03:34,090
slightly faster version of that


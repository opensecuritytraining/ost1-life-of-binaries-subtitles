1
00:00:03,550 --> 00:00:10,799
all right now you are here and now we

2
00:00:06,760 --> 00:00:10,799
zoom in of the doubt set look at that

3
00:00:12,030 --> 00:00:16,480
alright so this is the Dahle Center this

4
00:00:14,889 --> 00:00:19,060
is the very first structure this is that

5
00:00:16,480 --> 00:00:20,860
offset zero means the file and there's

6
00:00:19,060 --> 00:00:22,090
turns out there's only two things that

7
00:00:20,860 --> 00:00:24,130
we actually care about in the

8
00:00:22,090 --> 00:00:26,800
destruction all the rest is da stuff

9
00:00:24,130 --> 00:00:29,260
right really care about you care about

10
00:00:26,800 --> 00:00:31,420
the magic value at the beginning common

11
00:00:29,260 --> 00:00:33,010
thing of file formats right you need to

12
00:00:31,420 --> 00:00:35,079
say what kind of file you are so that

13
00:00:33,010 --> 00:00:37,390
people can parse you you care about the

14
00:00:35,079 --> 00:00:39,970
magic at the beginning and we care about

15
00:00:37,390 --> 00:00:42,100
this VLF a new and this is basically an

16
00:00:39,970 --> 00:00:43,420
offset to the next structure so just

17
00:00:42,100 --> 00:00:48,100
like how do I get to the next structure

18
00:00:43,420 --> 00:00:50,440
which is what I really care about so the

19
00:00:48,100 --> 00:00:54,190
magic is always going to be sent to the

20
00:00:50,440 --> 00:00:57,670
ASCII mg4 marks a toasty person who is

21
00:00:54,190 --> 00:00:59,079
developing dots and so basically this is

22
00:00:57,670 --> 00:01:00,520
a very common thing if you're looking at

23
00:00:59,079 --> 00:01:01,870
memory don't so things like that if

24
00:01:00,520 --> 00:01:03,219
you're looking at just raw memory and

25
00:01:01,870 --> 00:01:06,520
trying to get your bearings and you see

26
00:01:03,219 --> 00:01:08,590
an MZ pops out then you say aha maybe

27
00:01:06,520 --> 00:01:11,530
there is a PE file right here maybe

28
00:01:08,590 --> 00:01:13,590
there's a DA spot right here so that MZ

29
00:01:11,530 --> 00:01:16,630
is definitely a thing too you know

30
00:01:13,590 --> 00:01:18,670
an interesting thing is that the most

31
00:01:16,630 --> 00:01:21,130
Windows programs you've got actually a

32
00:01:18,670 --> 00:01:23,560
DOS program built into the header which

33
00:01:21,130 --> 00:01:26,590
basically runs and Dawson says I can't

34
00:01:23,560 --> 00:01:29,829
be running dots so the only point of it

35
00:01:26,590 --> 00:01:31,209
is just to have a minimal program so

36
00:01:29,829 --> 00:01:34,240
that if someone tries to run a Windows

37
00:01:31,209 --> 00:01:36,369
program like notepad in Doss it'll say I

38
00:01:34,240 --> 00:01:37,899
cannot be running dogs and you'll

39
00:01:36,369 --> 00:01:39,520
actually see that as a string as well

40
00:01:37,899 --> 00:01:44,590
you see this program cannot be run in

41
00:01:39,520 --> 00:01:48,310
DOS mode and going back to my key view

42
00:01:44,590 --> 00:01:52,479
view of things we can see that right

43
00:01:48,310 --> 00:01:54,520
here this this program cannot be run in

44
00:01:52,479 --> 00:01:56,859
DOS mode so that's another string you

45
00:01:54,520 --> 00:01:59,739
kind of so this would be looking at the

46
00:01:56,859 --> 00:02:01,840
raw file right here and so we see an MZ

47
00:01:59,739 --> 00:02:03,159
at the beginning and we see this kind of

48
00:02:01,840 --> 00:02:05,109
this program cannot be run in DOS mode

49
00:02:03,159 --> 00:02:06,819
so that's a pretty good indication that

50
00:02:05,109 --> 00:02:09,640
we're dealing with you know a Windows

51
00:02:06,819 --> 00:02:13,900
program just based on this raw human

52
00:02:09,640 --> 00:02:15,550
things now keep in mind that an attacker

53
00:02:13,900 --> 00:02:17,260
can always remove that

54
00:02:15,550 --> 00:02:18,850
thing once he's loaded into memory you

55
00:02:17,260 --> 00:02:20,260
know what you're loaded doesn't need to

56
00:02:18,850 --> 00:02:23,020
be there anymore you can just wipe it

57
00:02:20,260 --> 00:02:25,570
out so don't do like some commercial

58
00:02:23,020 --> 00:02:27,280
products do and use that as the end-all

59
00:02:25,570 --> 00:02:33,700
be-all determination of whether there's

60
00:02:27,280 --> 00:02:36,000
something like that all right so but

61
00:02:33,700 --> 00:02:38,170
then so MZ is what you'll be looking at

62
00:02:36,000 --> 00:02:41,320
this is program cannot be run in DOS

63
00:02:38,170 --> 00:02:42,640
mode for that as well and then you'll

64
00:02:41,320 --> 00:02:44,530
know if any user gives us some file

65
00:02:42,640 --> 00:02:47,500
offset so this is saying how far into

66
00:02:44,530 --> 00:02:52,290
this file is the next header the P

67
00:02:47,500 --> 00:02:52,290
header or NT so


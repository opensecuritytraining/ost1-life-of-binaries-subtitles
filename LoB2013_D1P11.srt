1
00:00:03,550 --> 00:00:14,910
all right let's do the competition so

2
00:00:09,960 --> 00:00:14,910
cancel out of pure existing games

3
00:00:15,240 --> 00:00:26,320
I'm Python bin host

4
00:00:17,650 --> 00:00:33,340
high early at the end enter - so that's

5
00:00:26,320 --> 00:00:40,420
your using this last mode and the way to

6
00:00:33,340 --> 00:00:44,550
proceed when is to the timing start when

7
00:00:40,420 --> 00:00:44,550
he returned and time starts after this

8
00:00:52,530 --> 00:00:56,530
yeah I think there's an off by one error

9
00:00:54,940 --> 00:00:58,870
in the coming off of the number of

10
00:00:56,530 --> 00:01:01,210
flights that are said right now so about

11
00:00:58,870 --> 00:01:03,070
two people show it some if it has to you

12
00:01:01,210 --> 00:01:05,920
know how many flags are set up here at 3

13
00:01:03,070 --> 00:01:20,200
right but I think it says - so like

14
00:01:05,920 --> 00:01:23,549
somewhere I'm missing the game I think

15
00:01:20,200 --> 00:01:23,549
it's just an OP I wonder

16
00:01:29,450 --> 00:01:34,460
so some people may have stumbled when

17
00:01:31,970 --> 00:01:37,070
asked like a virtual address and you may

18
00:01:34,460 --> 00:01:39,500
be put into the RBA but if it's saying

19
00:01:37,070 --> 00:01:41,060
like what is the VA it's saying take

20
00:01:39,500 --> 00:01:43,220
whatever RDA you would have calculated

21
00:01:41,060 --> 00:01:45,369
it at the base address such as a lot of

22
00:01:43,220 --> 00:01:45,369
the

23
00:01:49,680 --> 00:01:54,060
yeah explain Kelly I think I need to

24
00:01:52,290 --> 00:01:56,270
kind of draw a picture for this kind of

25
00:01:54,060 --> 00:01:56,270
thing

26
00:02:14,450 --> 00:02:23,010
all right so signs of image back in the

27
00:02:16,980 --> 00:02:24,690
optional headers is trying to tell you

28
00:02:23,010 --> 00:02:27,870
this takes up all this space in memory

29
00:02:24,690 --> 00:02:29,760
and what I'm asking you know is the size

30
00:02:27,870 --> 00:02:31,560
of image correct or what should the size

31
00:02:29,760 --> 00:02:37,130
of image being what I'm basically saying

32
00:02:31,560 --> 00:02:39,660
is that you know in color within a given

33
00:02:37,130 --> 00:02:41,010
file it's got some number of sections

34
00:02:39,660 --> 00:02:42,810
and the sections are really what

35
00:02:41,010 --> 00:02:53,160
determine what gets mapped from file

36
00:02:42,810 --> 00:02:56,040
into memory you see that I read the

37
00:02:53,160 --> 00:02:59,670
names all right and each of these has

38
00:02:56,040 --> 00:03:02,190
some Harvey a associated with it this

39
00:02:59,670 --> 00:03:04,620
this virtual address that it's got some

40
00:03:02,190 --> 00:03:08,310
size then this has an RBA and then it

41
00:03:04,620 --> 00:03:09,900
has the size size and so what I'm really

42
00:03:08,310 --> 00:03:11,880
just trying to say with these questions

43
00:03:09,900 --> 00:03:14,459
about don't meet the size of image break

44
00:03:11,880 --> 00:03:16,290
so I'm saying when you look at you know

45
00:03:14,459 --> 00:03:19,040
where these sections get placed in

46
00:03:16,290 --> 00:03:22,320
memory and how much space they take up

47
00:03:19,040 --> 00:03:26,640
when you go down and and this is a point

48
00:03:22,320 --> 00:03:28,260
that sort of they've made you know these

49
00:03:26,640 --> 00:03:30,570
things don't necessarily take up all the

50
00:03:28,260 --> 00:03:33,840
space right so this thing could have you

51
00:03:30,570 --> 00:03:35,340
know an RV in any of x3000 for instance

52
00:03:33,840 --> 00:03:38,070
but then the size of image could be

53
00:03:35,340 --> 00:03:40,530
equal to like hex to letter and then

54
00:03:38,070 --> 00:03:42,870
this guy could have an RBA of 4,000 and

55
00:03:40,530 --> 00:03:45,540
then it could have a photo size of the

56
00:03:42,870 --> 00:03:47,370
next 100 or something like that right so

57
00:03:45,540 --> 00:03:50,870
what that would really more accurately

58
00:03:47,370 --> 00:03:54,600
look like is that you would have this

59
00:03:50,870 --> 00:03:56,640
size of 200 being like this much space

60
00:03:54,600 --> 00:03:59,730
and then this would all be just sort of

61
00:03:56,640 --> 00:04:01,560
like a gap of space that you know

62
00:03:59,730 --> 00:04:02,550
there's nothing mapped in from file

63
00:04:01,560 --> 00:04:07,790
right here so it's

64
00:04:02,550 --> 00:04:09,870
uninitialized memory basically so

65
00:04:07,790 --> 00:04:11,670
they'll see this with basically all of

66
00:04:09,870 --> 00:04:14,580
those very two sections are going to

67
00:04:11,670 --> 00:04:16,470
take up exactly you know Tex 1000 worth

68
00:04:14,580 --> 00:04:18,660
of space whereas you'll see that most

69
00:04:16,470 --> 00:04:20,670
commonly these sections themselves will

70
00:04:18,660 --> 00:04:23,520
be aligned on that section alignment of

71
00:04:20,670 --> 00:04:25,050
X 1000 right way back in the optional

72
00:04:23,520 --> 00:04:26,940
header we talked about the section

73
00:04:25,050 --> 00:04:29,220
alignment and you said most most of the

74
00:04:26,940 --> 00:04:31,110
time that means basically that these

75
00:04:29,220 --> 00:04:36,420
things will be mapped into memory at X

76
00:04:31,110 --> 00:04:38,220
1000 so again odds of image should

77
00:04:36,420 --> 00:04:40,740
basically be you know take all of your

78
00:04:38,220 --> 00:04:43,260
sections and you know if you just add

79
00:04:40,740 --> 00:04:44,910
them up the thing is you're not

80
00:04:43,260 --> 00:04:47,400
accounting for the fact that you know

81
00:04:44,910 --> 00:04:49,650
continuously for doing all of this

82
00:04:47,400 --> 00:04:51,810
memory there's going to be these these

83
00:04:49,650 --> 00:04:53,430
gaps in there and so the simple way to

84
00:04:51,810 --> 00:04:55,680
do it is basically you just go to the

85
00:04:53,430 --> 00:04:57,390
last thing and you recognize that look

86
00:04:55,680 --> 00:05:01,140
there are going to be you know spaces in

87
00:04:57,390 --> 00:05:03,660
here we can initialize having in memory

88
00:05:01,140 --> 00:05:05,910
between these sections and you just go

89
00:05:03,660 --> 00:05:08,730
to the last one and you add this plus

90
00:05:05,910 --> 00:05:11,850
that you say that's my total size and so

91
00:05:08,730 --> 00:05:13,800
it's it's kind of you know arguable and

92
00:05:11,850 --> 00:05:16,290
things like that you know they would say

93
00:05:13,800 --> 00:05:18,030
well besides really is this because this

94
00:05:16,290 --> 00:05:20,490
is the stuff that's actually used right

95
00:05:18,030 --> 00:05:22,919
it never actually touches this gap

96
00:05:20,490 --> 00:05:24,720
between there no code in the program

97
00:05:22,919 --> 00:05:27,090
should be touching that gap in there so

98
00:05:24,720 --> 00:05:30,300
you can say well is it using this memory

99
00:05:27,090 --> 00:05:32,640
or is it using all of this memory it's

100
00:05:30,300 --> 00:05:35,010
definitely ambiguous but for purposes of

101
00:05:32,640 --> 00:05:37,410
this game it's using all of this memory

102
00:05:35,010 --> 00:05:39,510
because the onus loader has to actually

103
00:05:37,410 --> 00:05:41,130
allocate all of that memory even though

104
00:05:39,510 --> 00:05:43,260
there's gaps and padding's and things

105
00:05:41,130 --> 00:05:45,300
like that nobody else can use it it

106
00:05:43,260 --> 00:05:48,030
still just you know us is allocating

107
00:05:45,300 --> 00:05:51,780
chunks of X 1000 at a time except the

108
00:05:48,030 --> 00:05:55,290
smallest unit it can deal with it's

109
00:05:51,780 --> 00:05:58,800
using all of this memory any other

110
00:05:55,290 --> 00:06:01,310
questions about this it makes sense

111
00:05:58,800 --> 00:06:01,310
everybody


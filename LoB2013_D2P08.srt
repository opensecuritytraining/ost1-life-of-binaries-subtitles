1
00:00:03,690 --> 00:00:07,660
so now let's talk about exports I've

2
00:00:06,069 --> 00:00:09,540
made reference to them multiple times

3
00:00:07,660 --> 00:00:12,520
there has to be this pairing between

4
00:00:09,540 --> 00:00:15,219
this binary says I want to import those

5
00:00:12,520 --> 00:00:17,529
functions and this other binary says I

6
00:00:15,219 --> 00:00:23,080
am exporting these functions for you to

7
00:00:17,529 --> 00:00:26,770
import so export is basically I just

8
00:00:23,080 --> 00:00:28,180
said it's going to be the export table

9
00:00:26,770 --> 00:00:29,650
just at a high level oregan into the

10
00:00:28,180 --> 00:00:31,060
pictures you can think of an export

11
00:00:29,650 --> 00:00:33,910
table we've said that there's lots of

12
00:00:31,060 --> 00:00:36,430
RBA's all over the place within the

13
00:00:33,910 --> 00:00:38,079
binary format right the export table you

14
00:00:36,430 --> 00:00:41,079
can think of it as just a big list that

15
00:00:38,079 --> 00:00:42,730
maps an RBA to a particular string so

16
00:00:41,079 --> 00:00:44,620
this is your lip c library and it says

17
00:00:42,730 --> 00:00:47,680
here's the string print up and here's

18
00:00:44,620 --> 00:00:49,960
the RVA within myself so its relative to

19
00:00:47,680 --> 00:00:51,489
my own module where you can find print

20
00:00:49,960 --> 00:00:54,430
up so if you want to find the absolute

21
00:00:51,489 --> 00:00:57,219
virtual address of printf first you find

22
00:00:54,430 --> 00:00:59,350
the base address of the lipsy module in

23
00:00:57,219 --> 00:01:01,809
memory and then you go look up in this

24
00:00:59,350 --> 00:01:03,940
table printf has this RBA as you take

25
00:01:01,809 --> 00:01:06,369
the base address plus that are VA to get

26
00:01:03,940 --> 00:01:07,869
the absolute address print up and you go

27
00:01:06,369 --> 00:01:09,969
fill it into your own import a dress

28
00:01:07,869 --> 00:01:12,189
code that's the I'm the scenes what the

29
00:01:09,969 --> 00:01:14,229
OS loader is doing to fill in an import

30
00:01:12,189 --> 00:01:15,969
dress too it uses those hints for

31
00:01:14,229 --> 00:01:19,029
instance to say Oh printf maybe that's

32
00:01:15,969 --> 00:01:20,229
index you know 123 in the export table

33
00:01:19,029 --> 00:01:22,119
and it tries and it says he is that

34
00:01:20,229 --> 00:01:23,920
print up it looks at its import names

35
00:01:22,119 --> 00:01:27,130
table says I'm looking for print it is

36
00:01:23,920 --> 00:01:28,869
in X 1 2 3 print up if yes just take

37
00:01:27,130 --> 00:01:31,149
base plus that are be a fill it in to

38
00:01:28,869 --> 00:01:32,549
the important drastic so that's a high

39
00:01:31,149 --> 00:01:37,179
level you can just think of it like

40
00:01:32,549 --> 00:01:38,939
names and RBA's the reality is that the

41
00:01:37,179 --> 00:01:43,420
data structure would be a little more

42
00:01:38,939 --> 00:01:46,329
all right so how do we get to the export

43
00:01:43,420 --> 00:01:48,579
address table we do that through the

44
00:01:46,329 --> 00:01:50,200
data directory as normal and the 0s

45
00:01:48,579 --> 00:01:53,079
entry in the data directory is the

46
00:01:50,200 --> 00:01:55,959
export is the pointer to the export

47
00:01:53,079 --> 00:02:01,020
address table so from there we point

48
00:01:55,959 --> 00:02:03,520
over to these data structures so now

49
00:02:01,020 --> 00:02:05,979
we're talking about a particular module

50
00:02:03,520 --> 00:02:08,380
exporting a bunch of functions so

51
00:02:05,979 --> 00:02:10,300
there's not going to be many of this

52
00:02:08,380 --> 00:02:11,860
export directory structure there's only

53
00:02:10,300 --> 00:02:14,170
going to be one of that structure right

54
00:02:11,860 --> 00:02:16,910
there that structure is going to be

55
00:02:14,170 --> 00:02:18,500
pointing to three lists and

56
00:02:16,910 --> 00:02:20,000
three lists one of the lists is

57
00:02:18,500 --> 00:02:21,880
basically those are bas thought I was

58
00:02:20,000 --> 00:02:25,520
telling you about one of those lists is

59
00:02:21,880 --> 00:02:27,530
basically pointers to strings right so

60
00:02:25,520 --> 00:02:29,600
here's the pointers to strings here's

61
00:02:27,530 --> 00:02:31,040
the list of our bas so these are the

62
00:02:29,600 --> 00:02:34,400
first two things that I emotionally said

63
00:02:31,040 --> 00:02:36,500
first list is like here's the RBA second

64
00:02:34,400 --> 00:02:38,210
list is here is the string and then

65
00:02:36,500 --> 00:02:40,070
there's this other thing called ordinals

66
00:02:38,210 --> 00:02:42,650
and that kind of you can think of that

67
00:02:40,070 --> 00:02:44,150
having to do with those hints it kind of

68
00:02:42,650 --> 00:02:46,070
has to do with like a particular

69
00:02:44,150 --> 00:02:50,450
function is that a particular index in

70
00:02:46,070 --> 00:02:52,760
these other tables but basically it's

71
00:02:50,450 --> 00:02:55,250
because thus far we've been looking at

72
00:02:52,760 --> 00:02:57,410
imports that import by name so they say

73
00:02:55,250 --> 00:02:58,760
I want print up you give a string and

74
00:02:57,410 --> 00:03:00,830
that's the function which you want to

75
00:02:58,760 --> 00:03:02,600
import it turns out there's a second way

76
00:03:00,830 --> 00:03:04,580
to import things that we haven't seen at

77
00:03:02,600 --> 00:03:06,740
all it's called import by ordinal where

78
00:03:04,580 --> 00:03:11,150
instead of saying I want printf you say

79
00:03:06,740 --> 00:03:13,070
i want function 39 in dll food all right

80
00:03:11,150 --> 00:03:15,740
so instead of saying a strained you say

81
00:03:13,070 --> 00:03:19,010
just I want a number in a dll and that's

82
00:03:15,740 --> 00:03:20,420
called in 40 by ordinal the benefit of

83
00:03:19,010 --> 00:03:22,970
importing by ordinal is kind of

84
00:03:20,420 --> 00:03:24,500
optimization because there's no string

85
00:03:22,970 --> 00:03:26,420
searching that has to go on in the

86
00:03:24,500 --> 00:03:29,630
background it's like if this deal isn't

87
00:03:26,420 --> 00:03:31,430
changing this de la hasn't changed in 10

88
00:03:29,630 --> 00:03:34,370
years you can just say I know that

89
00:03:31,430 --> 00:03:36,560
printf is index 1 2 3 in the lipsy

90
00:03:34,370 --> 00:03:38,870
library so I can just ask for an X 1 2 3

91
00:03:36,560 --> 00:03:40,700
I don't need to take this extra time to

92
00:03:38,870 --> 00:03:43,100
go search string table search ins and

93
00:03:40,700 --> 00:03:45,200
things like that so that's the second

94
00:03:43,100 --> 00:03:47,690
way it's basically again just sort of a

95
00:03:45,200 --> 00:03:49,220
speed optimization mechanism if you want

96
00:03:47,690 --> 00:03:51,320
to go directly to function and you're

97
00:03:49,220 --> 00:03:53,239
really confident that you know the index

98
00:03:51,320 --> 00:03:56,780
of that function you can import

99
00:03:53,239 --> 00:03:58,520
specifically by index or I ordinal index

100
00:03:56,780 --> 00:04:02,930
and ordinal basically means the same

101
00:03:58,520 --> 00:04:04,520
thing alright so again I'm starting from

102
00:04:02,930 --> 00:04:07,640
the single structure that points out

103
00:04:04,520 --> 00:04:10,700
three lists so if we care about first of

104
00:04:07,640 --> 00:04:13,280
all time date stamp right so he for when

105
00:04:10,700 --> 00:04:15,609
we saw bound imports we said there's a

106
00:04:13,280 --> 00:04:18,350
time date stamp that specifies which

107
00:04:15,609 --> 00:04:20,299
binary this corresponds to and I said

108
00:04:18,350 --> 00:04:21,890
I'd like to tell you that that's the

109
00:04:20,299 --> 00:04:23,750
time/date stamp that you learn about in

110
00:04:21,890 --> 00:04:25,130
the file header but in reality it's the

111
00:04:23,750 --> 00:04:27,770
time/date stamp in the export

112
00:04:25,130 --> 00:04:29,510
information so when you're doing binding

113
00:04:27,770 --> 00:04:30,249
and your pre filling in the import

114
00:04:29,510 --> 00:04:32,109
address table

115
00:04:30,249 --> 00:04:35,409
you're taking the time date-stamped from

116
00:04:32,109 --> 00:04:37,869
here in this data structure the exports

117
00:04:35,409 --> 00:04:39,099
data structure taking that time bends

118
00:04:37,869 --> 00:04:40,899
down and you're filling it into that

119
00:04:39,099 --> 00:04:43,209
bound import information saying this is

120
00:04:40,899 --> 00:04:45,009
the version of this particular dll which

121
00:04:43,209 --> 00:04:48,159
I use when I fills in your imported

122
00:04:45,009 --> 00:04:49,839
resting alright and so just like the

123
00:04:48,159 --> 00:04:51,669
other time date-stamped though unlike

124
00:04:49,839 --> 00:04:53,649
the one that was either zero or negative

125
00:04:51,669 --> 00:04:57,159
1 this is a real time date-stamped this

126
00:04:53,649 --> 00:04:59,829
is second sense epoch and so this is the

127
00:04:57,159 --> 00:05:01,329
sort of second place that a person

128
00:04:59,829 --> 00:05:03,069
trying to do sort of malware forensics

129
00:05:01,329 --> 00:05:05,049
and tracking of when people have

130
00:05:03,069 --> 00:05:06,879
compiled stuff this is the second place

131
00:05:05,049 --> 00:05:09,789
they might look so the interesting thing

132
00:05:06,879 --> 00:05:12,189
here is right that first time day except

133
00:05:09,789 --> 00:05:14,499
we saw that gets set every time you

134
00:05:12,189 --> 00:05:16,299
compile the thing right this time

135
00:05:14,499 --> 00:05:18,789
date-stamped should basically be only

136
00:05:16,299 --> 00:05:20,949
changing any time that the exports

137
00:05:18,789 --> 00:05:24,039
information is changing in some way so

138
00:05:20,949 --> 00:05:27,459
it's like if the RVA to function one is

139
00:05:24,039 --> 00:05:29,169
still double x 1000 and you just

140
00:05:27,459 --> 00:05:30,969
recompile the thing and nothing about

141
00:05:29,169 --> 00:05:33,309
the r-va changed nothing about the name

142
00:05:30,969 --> 00:05:34,989
change then this time date-stamped can

143
00:05:33,309 --> 00:05:37,239
stay the same and so this should

144
00:05:34,989 --> 00:05:39,339
basically only change when there's been

145
00:05:37,239 --> 00:05:41,349
a meaningful change to the export saying

146
00:05:39,339 --> 00:05:42,969
it's at a new offset its [ __ ] at least

147
00:05:41,349 --> 00:05:45,339
one function is at least one byte

148
00:05:42,969 --> 00:05:46,839
different than it used to be because you

149
00:05:45,339 --> 00:05:48,759
basically want to keep the exports

150
00:05:46,839 --> 00:05:50,589
information listed if the exports

151
00:05:48,759 --> 00:05:52,360
information is the same then you don't

152
00:05:50,589 --> 00:05:54,549
need to go changing in about import

153
00:05:52,360 --> 00:05:56,469
stuff right you can keep recompiling a

154
00:05:54,549 --> 00:05:58,539
file and maybe you're just playing with

155
00:05:56,469 --> 00:05:59,949
strings and you're just you know editing

156
00:05:58,539 --> 00:06:02,499
strings and stuff like that the function

157
00:05:59,949 --> 00:06:03,879
information all stays the same if the

158
00:06:02,499 --> 00:06:05,379
function information stays on the same

159
00:06:03,879 --> 00:06:07,779
you want to keep this time in the same

160
00:06:05,379 --> 00:06:09,759
so that anybody that bound against it

161
00:06:07,779 --> 00:06:11,889
knows that this function information is

162
00:06:09,759 --> 00:06:13,929
still the same this should only really

163
00:06:11,889 --> 00:06:15,759
change when the function information

164
00:06:13,929 --> 00:06:17,499
changes your first one up in the file

165
00:06:15,759 --> 00:06:19,239
header that changes every time you

166
00:06:17,499 --> 00:06:22,209
recompile the thing this one should be

167
00:06:19,239 --> 00:06:25,599
more static and in that sense whereas

168
00:06:22,209 --> 00:06:27,789
the first file header time/date stamp

169
00:06:25,599 --> 00:06:29,679
can kind of say like oh well you know

170
00:06:27,789 --> 00:06:32,469
Bob compiled this thing you know last

171
00:06:29,679 --> 00:06:35,469
Tuesday this thing could say oh but

172
00:06:32,469 --> 00:06:37,179
really it turns out the last time any

173
00:06:35,469 --> 00:06:39,189
dysfunction information changed was last

174
00:06:37,179 --> 00:06:41,199
year so maybe Bob made this file last

175
00:06:39,189 --> 00:06:42,169
year and then he's just been changing it

176
00:06:41,199 --> 00:06:43,909
since then but not

177
00:06:42,169 --> 00:06:45,169
about functions is changing so this kind

178
00:06:43,909 --> 00:06:47,870
of gives you a farther back date

179
00:06:45,169 --> 00:06:51,289
potentially for the original creation

180
00:06:47,870 --> 00:06:53,150
time of this particular buyer and when I

181
00:06:51,289 --> 00:06:55,939
was saying before about burski article

182
00:06:53,150 --> 00:06:58,669
is about the Stuxnet crew changing the

183
00:06:55,939 --> 00:07:00,379
time/date stamp on that file header they

184
00:06:58,669 --> 00:07:02,569
weren't trying to do any manipulation of

185
00:07:00,379 --> 00:07:05,060
the the time/date stamp at this level

186
00:07:02,569 --> 00:07:06,650
right so they were like messing with the

187
00:07:05,060 --> 00:07:08,270
file header one to make it look like it

188
00:07:06,650 --> 00:07:11,150
was in nineteen ninety or something like

189
00:07:08,270 --> 00:07:12,499
that some fun unreasonable value but

190
00:07:11,150 --> 00:07:15,370
they were not manipulating this so you

191
00:07:12,499 --> 00:07:17,870
can still say okay because to delta not

192
00:07:15,370 --> 00:07:19,159
so this is the second of three time

193
00:07:17,870 --> 00:07:23,229
date-stamped i would kind of talk about

194
00:07:19,159 --> 00:07:26,509
for that sort of malware attribution and

195
00:07:23,229 --> 00:07:28,339
intelligence-gathering kind of thing so

196
00:07:26,509 --> 00:07:29,990
that's pretty simple it just relates to

197
00:07:28,339 --> 00:07:32,240
get anything about the exports

198
00:07:29,990 --> 00:07:39,979
information or and change it does update

199
00:07:32,240 --> 00:07:43,849
that time beads all right yes these are

200
00:07:39,979 --> 00:07:46,099
my updated slides so number of functions

201
00:07:43,849 --> 00:07:47,569
a number of names will differ when

202
00:07:46,099 --> 00:07:49,490
you're doing this import by ordinal

203
00:07:47,569 --> 00:07:50,960
right so the number of names first of

204
00:07:49,490 --> 00:07:53,479
all is just we said there's there's

205
00:07:50,960 --> 00:07:57,469
tables let's say here's the RBA's and

206
00:07:53,479 --> 00:07:59,389
here's the names okay typically if your

207
00:07:57,469 --> 00:08:01,099
typical binary is going to have the

208
00:07:59,389 --> 00:08:03,050
exact same number of functions as number

209
00:08:01,099 --> 00:08:06,349
of names because it's exporting all of

210
00:08:03,050 --> 00:08:09,110
its functions by name but if it allows

211
00:08:06,349 --> 00:08:10,819
for importing by ordinal then you can

212
00:08:09,110 --> 00:08:12,740
actually have more functions than you

213
00:08:10,819 --> 00:08:14,240
have names so there will be some

214
00:08:12,740 --> 00:08:16,669
function where you can't call this

215
00:08:14,240 --> 00:08:18,499
function by name you have to call it by

216
00:08:16,669 --> 00:08:23,360
ordinal and that's when you'll have more

217
00:08:18,499 --> 00:08:26,210
functions than you have names yes you

218
00:08:23,360 --> 00:08:29,210
can't be mentioned about doing a binary

219
00:08:26,210 --> 00:08:31,279
search yes and flies into the names or

220
00:08:29,210 --> 00:08:33,380
sort alphabetically yes actually very

221
00:08:31,279 --> 00:08:35,479
very good very good in prints I'll show

222
00:08:33,380 --> 00:08:38,060
that in the picture and like right about

223
00:08:35,479 --> 00:08:40,219
here so we'll see that actually the

224
00:08:38,060 --> 00:08:44,079
names are scorned alphabetically to

225
00:08:40,219 --> 00:08:44,079
allow for binary search enormities names

226
00:08:44,769 --> 00:08:51,890
alright so

227
00:08:47,990 --> 00:08:55,160
bass is another field in the export

228
00:08:51,890 --> 00:08:56,990
information base is sort of weird I'm

229
00:08:55,160 --> 00:08:59,750
not really sure why they have this

230
00:08:56,990 --> 00:09:02,589
capability but it's there and we have to

231
00:08:59,750 --> 00:09:05,390
account for it bass is basically saying

232
00:09:02,589 --> 00:09:08,300
when you're exporting things by ordinal

233
00:09:05,390 --> 00:09:09,980
you can start at some value greater than

234
00:09:08,300 --> 00:09:11,959
one so you doesn't have to be like

235
00:09:09,980 --> 00:09:15,620
import or no one two three four you can

236
00:09:11,959 --> 00:09:17,330
say import ordinal 501 502 503 and

237
00:09:15,620 --> 00:09:18,709
therefore you're saying anyone who wants

238
00:09:17,330 --> 00:09:20,510
to import these functions for me they

239
00:09:18,709 --> 00:09:24,140
better know that they need to ask for

240
00:09:20,510 --> 00:09:25,970
500 to some degree I think part of the

241
00:09:24,140 --> 00:09:27,890
point of this might be to break

242
00:09:25,970 --> 00:09:30,020
backwards compatibility like if you want

243
00:09:27,890 --> 00:09:31,580
to make it to that stuff is no longer

244
00:09:30,020 --> 00:09:33,709
compatible people used to be importing

245
00:09:31,580 --> 00:09:35,839
function one two three and now you say

246
00:09:33,709 --> 00:09:39,230
set the base to 10 now they need to

247
00:09:35,839 --> 00:09:42,740
import function you know 10 11 12 or 11

248
00:09:39,230 --> 00:09:44,270
12 13 this I think could be used to

249
00:09:42,740 --> 00:09:45,470
actually explicitly break backwards

250
00:09:44,270 --> 00:09:47,450
compatibility so that they need to

251
00:09:45,470 --> 00:09:50,120
update their code in order to call your

252
00:09:47,450 --> 00:09:51,740
functions but I'm not entirely sure

253
00:09:50,120 --> 00:09:53,240
whether that's the original intention

254
00:09:51,740 --> 00:09:57,470
but all you have to know for base is

255
00:09:53,240 --> 00:09:59,720
basically it's the starting index that

256
00:09:57,470 --> 00:10:01,579
will be used in to this ordinal table

257
00:09:59,720 --> 00:10:05,120
and I'll show a picture basically a

258
00:10:01,579 --> 00:10:08,240
little bit well that's okay all right so

259
00:10:05,120 --> 00:10:10,190
when we're dealing with base we've got

260
00:10:08,240 --> 00:10:13,399
this export address table this is the

261
00:10:10,190 --> 00:10:15,950
actual table of our va's this is the

262
00:10:13,399 --> 00:10:18,800
thing saying this is the RVA for index

263
00:10:15,950 --> 00:10:20,360
zero index 1 index 2 and so forth and so

264
00:10:18,800 --> 00:10:21,529
we don't without knowing anything else

265
00:10:20,360 --> 00:10:22,880
we don't know which functions these

266
00:10:21,529 --> 00:10:24,529
actually correspond to we don't know

267
00:10:22,880 --> 00:10:26,899
which name is that correspond to they

268
00:10:24,529 --> 00:10:28,579
may not correspond to any names because

269
00:10:26,899 --> 00:10:31,399
the import dress table can have more

270
00:10:28,579 --> 00:10:33,170
things than the importation or dress

271
00:10:31,399 --> 00:10:35,839
cables could have five things if we're

272
00:10:33,170 --> 00:10:37,730
named in SF 0 things they're all

273
00:10:35,839 --> 00:10:39,829
exported by ordinal and so when we have

274
00:10:37,730 --> 00:10:42,260
a base of one what it's basically

275
00:10:39,829 --> 00:10:45,110
telling you is that if someone imports

276
00:10:42,260 --> 00:10:47,660
ordinal one you need to subtract off one

277
00:10:45,110 --> 00:10:49,130
and then you'll get to index zero in

278
00:10:47,660 --> 00:10:51,020
this particular table so it's basically

279
00:10:49,130 --> 00:10:53,300
just a thing where they asked for

280
00:10:51,020 --> 00:10:55,339
ordinal foo and you subtract out the

281
00:10:53,300 --> 00:10:59,350
base to get the correct index in this

282
00:10:55,339 --> 00:11:01,790
table of our case so correspondingly if

283
00:10:59,350 --> 00:11:03,800
you decided to break backwards compat

284
00:11:01,790 --> 00:11:05,570
ability you changed the base you said

285
00:11:03,800 --> 00:11:09,110
from now on all of my exported by

286
00:11:05,570 --> 00:11:13,120
ordinal things are starting at 37 if

287
00:11:09,110 --> 00:11:15,470
someone wants to import 37 then the

288
00:11:13,120 --> 00:11:17,930
loader will basically say okay you're

289
00:11:15,470 --> 00:11:19,910
trying to import 37 I'm going to look at

290
00:11:17,930 --> 00:11:22,730
the base field i'm going to subtract off

291
00:11:19,910 --> 00:11:25,430
37 and i'll go to index zero in the

292
00:11:22,730 --> 00:11:28,310
export address table so basically it's

293
00:11:25,430 --> 00:11:30,470
just used as a subtractor to get a

294
00:11:28,310 --> 00:11:37,970
particular index in the import drastic

295
00:11:30,470 --> 00:11:40,790
question all right so that's all the

296
00:11:37,970 --> 00:11:43,040
point of base and going back then we

297
00:11:40,790 --> 00:11:48,740
have the actual three pointers to those

298
00:11:43,040 --> 00:11:50,420
rate so address of functions we're going

299
00:11:48,740 --> 00:11:53,450
to be calling address of functions the

300
00:11:50,420 --> 00:11:57,950
thing which points at the export address

301
00:11:53,450 --> 00:12:00,320
table o EI t means export address table

302
00:11:57,950 --> 00:12:05,630
and that is given by the field address

303
00:12:00,320 --> 00:12:07,130
of functions address of names this is

304
00:12:05,630 --> 00:12:09,020
going to go in at the names table so

305
00:12:07,130 --> 00:12:12,380
again just back to the notional there's

306
00:12:09,020 --> 00:12:13,850
a map of functions are bas and names so

307
00:12:12,380 --> 00:12:16,190
the address of functions is the RV eight

308
00:12:13,850 --> 00:12:19,010
address of names is the names and you

309
00:12:16,190 --> 00:12:22,760
can think of it like export dress table

310
00:12:19,010 --> 00:12:24,890
EA t export name stable EMT it's just

311
00:12:22,760 --> 00:12:27,230
the equivalent of backing imports we had

312
00:12:24,890 --> 00:12:29,360
a poor name stable import address too

313
00:12:27,230 --> 00:12:32,810
here we have export address table

314
00:12:29,360 --> 00:12:35,360
explore name stable and then finally

315
00:12:32,810 --> 00:12:37,240
name of ordinals and this has to do with

316
00:12:35,360 --> 00:12:40,700
the fact that we're doing this

317
00:12:37,240 --> 00:12:43,370
alphabetic we're doing outcome I don't

318
00:12:40,700 --> 00:12:46,850
want that alphabetical lexical ordering

319
00:12:43,370 --> 00:12:48,890
of those strings and so the explore

320
00:12:46,850 --> 00:12:51,770
names table is basically going to be a

321
00:12:48,890 --> 00:12:54,110
thing which allows us to map from the

322
00:12:51,770 --> 00:12:56,870
strings table potentially into a

323
00:12:54,110 --> 00:12:58,610
different index in the export address

324
00:12:56,870 --> 00:13:01,420
table or show that with a picture in

325
00:12:58,610 --> 00:13:01,420
here in a second

326
00:13:04,480 --> 00:13:11,199
okay so I think this is my only picture

327
00:13:09,250 --> 00:13:16,170
so this is kind of taken from the mat I

328
00:13:11,199 --> 00:13:21,360
Trek articles and remade a little bit so

329
00:13:16,170 --> 00:13:27,940
get pointy feel can you follow me to the

330
00:13:21,360 --> 00:13:29,680
screen so we've got single one of these

331
00:13:27,940 --> 00:13:33,300
data structures for all of your

332
00:13:29,680 --> 00:13:35,350
exporting information and then we've got

333
00:13:33,300 --> 00:13:37,389
primarily free table isn't then you've

334
00:13:35,350 --> 00:13:41,320
got a string table off to the side kind

335
00:13:37,389 --> 00:13:44,079
of like we had for imports so we've got

336
00:13:41,320 --> 00:13:47,949
the EIT that's pointed to by address of

337
00:13:44,079 --> 00:13:50,529
functions we got the ENT that's pointed

338
00:13:47,949 --> 00:13:52,570
by address of names and we've got this

339
00:13:50,529 --> 00:13:54,610
name ordinals table which I'm going to

340
00:13:52,570 --> 00:13:57,310
claim as sort of a translation table

341
00:13:54,610 --> 00:13:59,740
between the export name stable and the

342
00:13:57,310 --> 00:14:02,139
export address table so first of all

343
00:13:59,740 --> 00:14:05,470
let's say that we're looking for a

344
00:14:02,139 --> 00:14:07,120
particular function to import by name so

345
00:14:05,470 --> 00:14:08,980
we don't have like import by ordinal we

346
00:14:07,120 --> 00:14:11,199
can't just skip directly to the RVA

347
00:14:08,980 --> 00:14:14,050
we're importing by name so we're going

348
00:14:11,199 --> 00:14:17,740
to say we're going to import edit odor

349
00:14:14,050 --> 00:14:20,740
information editorial so the OS loader

350
00:14:17,740 --> 00:14:24,819
is trying to find the RBA or edit owner

351
00:14:20,740 --> 00:14:27,610
info oh it does this is it goes to the

352
00:14:24,819 --> 00:14:29,620
it has edit owner info off the

353
00:14:27,610 --> 00:14:31,449
assignment a local variable and it's

354
00:14:29,620 --> 00:14:33,790
when you go to this table that's going

355
00:14:31,449 --> 00:14:35,260
to do a binary search so this table is

356
00:14:33,790 --> 00:14:37,510
six big we're going to assume that a

357
00:14:35,260 --> 00:14:40,089
binary searches to you know the middle

358
00:14:37,510 --> 00:14:42,370
and that entry right there so it goes to

359
00:14:40,089 --> 00:14:45,699
the middle and it goes to this entry

360
00:14:42,370 --> 00:14:47,199
this entry has an RV a of a string it

361
00:14:45,699 --> 00:14:49,660
goes and looks up that string that

362
00:14:47,199 --> 00:14:52,240
string it's edit permission info and it

363
00:14:49,660 --> 00:14:53,860
says edit owner info and a permission

364
00:14:52,240 --> 00:14:56,440
info is this you know less than or

365
00:14:53,860 --> 00:15:00,160
greater than at less than greater than

366
00:14:56,440 --> 00:15:04,420
or equal to write as edit owner info

367
00:15:00,160 --> 00:15:07,089
because it's ed I t 0 vs ed I PP they

368
00:15:04,420 --> 00:15:08,800
owe is less than P so I know that ok

369
00:15:07,089 --> 00:15:11,440
I've done my binary search it's not on

370
00:15:08,800 --> 00:15:13,630
that half of the array it's got to be

371
00:15:11,440 --> 00:15:15,570
back on this half of the array so then

372
00:15:13,630 --> 00:15:17,130
they you know go back to you know

373
00:15:15,570 --> 00:15:19,170
well half is here we're going to say

374
00:15:17,130 --> 00:15:21,690
they rub down and then they say okay

375
00:15:19,170 --> 00:15:24,570
this entry right here they say this

376
00:15:21,690 --> 00:15:28,320
entry has an RBA that points at edit

377
00:15:24,570 --> 00:15:31,740
audit info alright and so ebit a versus

378
00:15:28,320 --> 00:15:34,110
EDI tio a is less than 0 so I know it's

379
00:15:31,740 --> 00:15:36,060
not on this part of the table and now

380
00:15:34,110 --> 00:15:39,360
you know I go halfway the only thing

381
00:15:36,060 --> 00:15:41,730
left is this right here I found edit

382
00:15:39,360 --> 00:15:45,360
owner info at a donor info okay that

383
00:15:41,730 --> 00:15:47,700
right there is what I care about no it's

384
00:15:45,360 --> 00:15:52,500
not so simple as just all right go in

385
00:15:47,700 --> 00:15:55,140
dec 01 to it goes 0 1 2 up to here we

386
00:15:52,500 --> 00:15:57,750
have this name ordinals table which is

387
00:15:55,140 --> 00:15:59,310
basically the translation because these

388
00:15:57,750 --> 00:16:04,110
things can actually be in different

389
00:15:59,310 --> 00:16:06,300
order so we go 0 1 2 into this array so

390
00:16:04,110 --> 00:16:08,910
we got index 2 is the function that we

391
00:16:06,300 --> 00:16:12,900
care about at an owner info and then we

392
00:16:08,910 --> 00:16:15,480
go 0 1 2 into this array and this now is

393
00:16:12,900 --> 00:16:16,950
going to be an ordinal that we're

394
00:16:15,480 --> 00:16:19,410
pulling out well it's not even an

395
00:16:16,950 --> 00:16:22,380
ordinal it's just an index up into this

396
00:16:19,410 --> 00:16:24,690
table so we go 012 in this week's pull

397
00:16:22,380 --> 00:16:27,570
out this index in each one it goes 0 1

398
00:16:24,690 --> 00:16:30,960
and now this is going to be the RDA for

399
00:16:27,570 --> 00:16:32,520
edit owner info alright and so this is

400
00:16:30,960 --> 00:16:34,290
the sort of process that it goes through

401
00:16:32,520 --> 00:16:35,880
what it looks up by name so obviously

402
00:16:34,290 --> 00:16:38,340
you can see that you know in port by

403
00:16:35,880 --> 00:16:40,590
ordinal it's faster right and just go

404
00:16:38,340 --> 00:16:42,960
right there figure the binary search you

405
00:16:40,590 --> 00:16:45,210
know so log in but it's still a big

406
00:16:42,960 --> 00:16:46,920
search right so import by ordinal you

407
00:16:45,210 --> 00:16:49,200
just go straight to what you want but

408
00:16:46,920 --> 00:16:50,790
this end you know change around so when

409
00:16:49,200 --> 00:16:53,130
you fall back to import by name you do

410
00:16:50,790 --> 00:16:54,630
binary search find this and you get this

411
00:16:53,130 --> 00:16:56,970
translation table and that tells you

412
00:16:54,630 --> 00:17:00,180
where you actually go in this I believe

413
00:16:56,970 --> 00:17:02,820
originally this was not alphabetically

414
00:17:00,180 --> 00:17:04,620
sorted so I think that was part of the

415
00:17:02,820 --> 00:17:06,780
reason why we had this in the first

416
00:17:04,620 --> 00:17:09,750
place when this was not alphabetically

417
00:17:06,780 --> 00:17:12,329
sorted you needed to basically you know

418
00:17:09,750 --> 00:17:13,829
you do linear search throughout this and

419
00:17:12,329 --> 00:17:16,500
then you know you'd find a particular

420
00:17:13,829 --> 00:17:18,180
entry and then you'd find it up here and

421
00:17:16,500 --> 00:17:20,339
then you find it up there so I believe

422
00:17:18,180 --> 00:17:22,850
that's the reason for this because when

423
00:17:20,339 --> 00:17:25,800
they were originally like circa an IV

424
00:17:22,850 --> 00:17:28,000
windows 95 or something like that they

425
00:17:25,800 --> 00:17:29,620
weren't in sorting this and so

426
00:17:28,000 --> 00:17:33,250
just kind of dessert to that find that

427
00:17:29,620 --> 00:17:35,800
can go on to that but that's why I

428
00:17:33,250 --> 00:17:37,960
believe that extra layer of interaction

429
00:17:35,800 --> 00:17:39,270
and their kids in there but I'm not ever

430
00:17:37,960 --> 00:17:41,560
sensor and on that that's just

431
00:17:39,270 --> 00:17:44,890
historical stuff I've seen we're looking

432
00:17:41,560 --> 00:17:47,230
into it alright any questions on this

433
00:17:44,890 --> 00:17:49,330
looking up basically the trend the

434
00:17:47,230 --> 00:17:51,250
process by which you import my name you

435
00:17:49,330 --> 00:17:54,340
find something how you ultimately find

436
00:17:51,250 --> 00:17:57,990
the RBA associated with in it what do

437
00:17:54,340 --> 00:18:05,620
you have any questions on that alright

438
00:17:57,990 --> 00:18:08,770
so moving on okay and then just the last

439
00:18:05,620 --> 00:18:10,810
thing again number of functions is the

440
00:18:08,770 --> 00:18:13,060
size of this array right here number of

441
00:18:10,810 --> 00:18:18,030
names is the size of this array right

442
00:18:13,060 --> 00:18:26,620
there and this need not be as big as

443
00:18:18,030 --> 00:18:27,940
translation all right so just as a

444
00:18:26,620 --> 00:18:29,020
miscellaneous thing you know there's the

445
00:18:27,940 --> 00:18:31,960
question of if you're writing code

446
00:18:29,020 --> 00:18:33,760
you're actually export a function ID Jim

447
00:18:31,960 --> 00:18:36,460
how do you make the compiler generate on

448
00:18:33,760 --> 00:18:38,380
a batch data structure for you well the

449
00:18:36,460 --> 00:18:40,390
simplest way and the way that was used

450
00:18:38,380 --> 00:18:43,000
to make your dll is that you're using

451
00:18:40,390 --> 00:18:45,280
the templates is that you just happen to

452
00:18:43,000 --> 00:18:48,730
be sterile spec export at the beginning

453
00:18:45,280 --> 00:18:51,190
of the of a function key throw this deco

454
00:18:48,730 --> 00:18:52,750
spec dll export at the beginning of the

455
00:18:51,190 --> 00:18:54,640
function and then the compiler just does

456
00:18:52,750 --> 00:18:56,200
the right thing and just make something

457
00:18:54,640 --> 00:18:58,210
where there's going to be an import

458
00:18:56,200 --> 00:19:03,610
names table that has a reference to a

459
00:18:58,210 --> 00:19:05,770
string say hello all right the other way

460
00:19:03,610 --> 00:19:08,830
you can do it is that there's a module

461
00:19:05,770 --> 00:19:11,110
definition file or a depth file and deb

462
00:19:08,830 --> 00:19:14,340
file is basically the more i believe its

463
00:19:11,110 --> 00:19:16,630
more common way if you want to do actual

464
00:19:14,340 --> 00:19:18,760
export by ordinal it's basically that

465
00:19:16,630 --> 00:19:20,470
def file can just be a big like here's

466
00:19:18,760 --> 00:19:22,240
my function and here's what I want it to

467
00:19:20,470 --> 00:19:23,890
be exported as and like here's my

468
00:19:22,240 --> 00:19:26,860
function and I want to call that ordinal

469
00:19:23,890 --> 00:19:29,260
five and six and seven so typically

470
00:19:26,860 --> 00:19:31,030
export bio you make this test pilot it's

471
00:19:29,260 --> 00:19:33,690
just a big list of here's the functions

472
00:19:31,030 --> 00:19:33,690
I want to export


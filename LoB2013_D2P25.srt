1
00:00:03,580 --> 00:00:08,799
all right down to section editors down

2
00:00:07,120 --> 00:00:10,209
most commonly at the bottom they don't

3
00:00:08,799 --> 00:00:11,860
strictly have to be at the bottom they

4
00:00:10,209 --> 00:00:14,500
could be literally right after the

5
00:00:11,860 --> 00:00:16,869
program headers but most commonly is see

6
00:00:14,500 --> 00:00:22,539
them at the bottom so drill down on that

7
00:00:16,869 --> 00:00:23,949
oh it's not you mean okay all right so

8
00:00:22,539 --> 00:00:29,170
here's our section header information

9
00:00:23,949 --> 00:00:32,830
and we care about at all awesome okay so

10
00:00:29,170 --> 00:00:34,870
first thing SH name this is an offset

11
00:00:32,830 --> 00:00:37,059
into the string table that I mentioned

12
00:00:34,870 --> 00:00:44,070
before where you can find the name of

13
00:00:37,059 --> 00:00:46,780
this section so it is going to be a n

14
00:00:44,070 --> 00:00:48,699
okay so if there's just straight-up null

15
00:00:46,780 --> 00:00:50,140
table it can point so section doesn't

16
00:00:48,699 --> 00:00:52,600
have to necessarily have a name it could

17
00:00:50,140 --> 00:00:54,540
just point directly at a a null night

18
00:00:52,600 --> 00:00:56,890
and it would be a zero size thing but

19
00:00:54,540 --> 00:01:02,019
but basically what this is saying is

20
00:00:56,890 --> 00:01:03,970
that whereas on PE files you only have

21
00:01:02,019 --> 00:01:05,710
eight bytes and you got to fit your name

22
00:01:03,970 --> 00:01:07,390
in that right there's a fixed eight bike

23
00:01:05,710 --> 00:01:09,730
and it may or may not have a null

24
00:01:07,390 --> 00:01:11,170
terminator hear the name is just a

25
00:01:09,730 --> 00:01:12,910
pointer into that string table so you

26
00:01:11,170 --> 00:01:14,920
can have a long of a name as you want

27
00:01:12,910 --> 00:01:21,820
for your section and you kind of see

28
00:01:14,920 --> 00:01:25,950
that in our files I wish I would have

29
00:01:21,820 --> 00:01:29,290
set the lock screen lock time the higher

30
00:01:25,950 --> 00:01:37,060
all right so in our files when we did

31
00:01:29,290 --> 00:01:38,680
that what's it that we did read alpesh L

32
00:01:37,060 --> 00:01:40,450
on this thing and we got this mapping to

33
00:01:38,680 --> 00:01:42,070
these other things you can see we've got

34
00:01:40,450 --> 00:01:45,190
some longer names right doc a new

35
00:01:42,070 --> 00:01:49,810
version underscore our we've got dot

36
00:01:45,190 --> 00:01:51,990
Rella p ltd h exception frame and header

37
00:01:49,810 --> 00:01:54,370
that sort of thing right so your psyche

38
00:01:51,990 --> 00:01:57,700
section names can be as long as you want

39
00:01:54,370 --> 00:01:59,170
because this had this name field in the

40
00:01:57,700 --> 00:02:00,940
section editors is just a pointer into

41
00:01:59,170 --> 00:02:02,820
that string table and it's just

42
00:02:00,940 --> 00:02:04,780
expecting a null-terminated strength

43
00:02:02,820 --> 00:02:07,750
alright so that's one way that it's

44
00:02:04,780 --> 00:02:10,959
different again so then there's the SH

45
00:02:07,750 --> 00:02:12,830
type field and there's a ton of

46
00:02:10,959 --> 00:02:17,060
different types I think I have

47
00:02:12,830 --> 00:02:23,930
if they okay so I talked about the types

48
00:02:17,060 --> 00:02:25,820
here so the thing is basically with the

49
00:02:23,930 --> 00:02:28,730
type field there's going to be a whole

50
00:02:25,820 --> 00:02:31,970
bunch of special-purpose type types for

51
00:02:28,730 --> 00:02:34,190
sections and so the whole point of the

52
00:02:31,970 --> 00:02:36,110
type for a section header is it's

53
00:02:34,190 --> 00:02:38,210
basically going to say to the linker

54
00:02:36,110 --> 00:02:40,430
this is where you should store you know

55
00:02:38,210 --> 00:02:41,930
your import address table information

56
00:02:40,430 --> 00:02:44,030
this is where you should store your

57
00:02:41,930 --> 00:02:46,610
exception and anyway information and so

58
00:02:44,030 --> 00:02:47,960
forth so the the specification for elf

59
00:02:46,610 --> 00:02:50,270
is going to lay out a bunch of different

60
00:02:47,960 --> 00:02:51,740
section types which can be reused you

61
00:02:50,270 --> 00:02:53,750
can have a generic section type like

62
00:02:51,740 --> 00:02:55,510
this is just you know generic data but

63
00:02:53,750 --> 00:02:58,100
then there's also a bunch of little

64
00:02:55,510 --> 00:03:00,610
specific kind of section types that will

65
00:02:58,100 --> 00:03:03,920
use for specific things so what kind of

66
00:03:00,610 --> 00:03:06,410
what kind of show a few of those in a

67
00:03:03,920 --> 00:03:11,660
little bit but but just wanted to say

68
00:03:06,410 --> 00:03:15,260
that this is different from the sections

69
00:03:11,660 --> 00:03:16,730
that we had back in PE because whereas

70
00:03:15,260 --> 00:03:18,260
previously sections were just sort of

71
00:03:16,730 --> 00:03:20,390
like it's got some data and it's going

72
00:03:18,260 --> 00:03:22,810
to be mapped in and if it was anything

73
00:03:20,390 --> 00:03:26,060
special it was special because it was

74
00:03:22,810 --> 00:03:28,850
because of the something pointed to by

75
00:03:26,060 --> 00:03:30,440
the data directory here the linker is

76
00:03:28,850 --> 00:03:32,360
going to have specific types that it

77
00:03:30,440 --> 00:03:35,780
uses four different sections to keep

78
00:03:32,360 --> 00:03:37,430
track of where information set oh I

79
00:03:35,780 --> 00:03:41,930
guess I did talk immediately lots of

80
00:03:37,430 --> 00:03:43,970
types okay up this is a type probe it's

81
00:03:41,930 --> 00:03:45,560
this is the catch-all basically so if

82
00:03:43,970 --> 00:03:48,050
it's just called probe it's it's you

83
00:03:45,560 --> 00:03:49,760
know it can be code it can be data it's

84
00:03:48,050 --> 00:03:51,560
just whatever its probe it's is

85
00:03:49,760 --> 00:03:54,350
basically saying this is not a special

86
00:03:51,560 --> 00:03:56,120
section everything besides probe it's

87
00:03:54,350 --> 00:03:57,640
means it is some special section that's

88
00:03:56,120 --> 00:04:02,360
predefined in this classification

89
00:03:57,640 --> 00:04:04,280
alright so on that special section that

90
00:04:02,360 --> 00:04:06,560
we call the string table it's got to

91
00:04:04,280 --> 00:04:08,030
have a type of string table that has to

92
00:04:06,560 --> 00:04:10,519
match up so that when you have that help

93
00:04:08,030 --> 00:04:12,430
header pointing at particular entry it

94
00:04:10,519 --> 00:04:14,720
better have a string team inside right

95
00:04:12,430 --> 00:04:16,760
dynamic as we already sort of mentioned

96
00:04:14,720 --> 00:04:18,620
you got dynamic up at the program header

97
00:04:16,760 --> 00:04:21,400
information and that's because as I said

98
00:04:18,620 --> 00:04:24,080
you don't need section headers to run

99
00:04:21,400 --> 00:04:25,970
the dynamic linker needs to just depend

100
00:04:24,080 --> 00:04:27,800
on the program matters but down

101
00:04:25,970 --> 00:04:29,300
this level you do have you know when

102
00:04:27,800 --> 00:04:31,670
you're linking across multiple different

103
00:04:29,300 --> 00:04:34,370
things you know if this dot C file

104
00:04:31,670 --> 00:04:36,380
imports functions from somewhere and

105
00:04:34,370 --> 00:04:38,690
this dot C file in sports functions from

106
00:04:36,380 --> 00:04:39,740
somewhere you can get 20 files and

107
00:04:38,690 --> 00:04:41,330
they're each going to have sections

108
00:04:39,740 --> 00:04:43,520
they're each going to have you know a

109
00:04:41,330 --> 00:04:45,320
need for an equivalent of an importer

110
00:04:43,520 --> 00:04:46,730
dress table and then eventually when you

111
00:04:45,320 --> 00:04:48,440
link everything together there's going

112
00:04:46,730 --> 00:04:50,390
to be you know one major importer

113
00:04:48,440 --> 00:04:54,260
dressed table and that's going to be the

114
00:04:50,390 --> 00:04:55,790
dynamic linking section Vince's it so

115
00:04:54,260 --> 00:04:59,990
that's for keeping that sort of

116
00:04:55,790 --> 00:05:02,150
information no bits is for BSS

117
00:04:59,990 --> 00:05:04,730
essentially and I think that's pretty

118
00:05:02,150 --> 00:05:06,620
much okay so then I threw in some other

119
00:05:04,730 --> 00:05:08,540
things but we really don't care about

120
00:05:06,620 --> 00:05:11,960
these simple table that's more like

121
00:05:08,540 --> 00:05:17,270
debugging information dynamics and bowls

122
00:05:11,960 --> 00:05:20,150
yep so I'm pretty much gonna skip it but

123
00:05:17,270 --> 00:05:23,530
you can go back and read those or read

124
00:05:20,150 --> 00:05:26,060
the specification if you care all right

125
00:05:23,530 --> 00:05:29,919
sections can have flags as well and

126
00:05:26,060 --> 00:05:32,540
these flags are again along the lines of

127
00:05:29,919 --> 00:05:35,210
things like permission so you've got

128
00:05:32,540 --> 00:05:38,120
right ability or not but then there's

129
00:05:35,210 --> 00:05:39,950
things like allocation and X exec

130
00:05:38,120 --> 00:05:46,640
instructions I believe that's execute

131
00:05:39,950 --> 00:05:48,919
instructions um and to be honest I don't

132
00:05:46,640 --> 00:05:50,540
remember what exactly instructions does

133
00:05:48,919 --> 00:05:53,360
but I'm thinking that's probably going

134
00:05:50,540 --> 00:05:56,030
to be used or the four sections that

135
00:05:53,360 --> 00:06:00,590
contain code definitely don't remember

136
00:05:56,030 --> 00:06:02,479
what a lock does there's whether the

137
00:06:00,590 --> 00:06:05,510
section is going to occupy memory during

138
00:06:02,479 --> 00:06:07,340
program execution I guess it's saying if

139
00:06:05,510 --> 00:06:08,810
it doesn't have the a lock flag set you

140
00:06:07,340 --> 00:06:12,229
can kind of think of it as a discard

141
00:06:08,810 --> 00:06:13,640
Amell thing and that section will be you

142
00:06:12,229 --> 00:06:15,260
know potentially mapped into memory but

143
00:06:13,640 --> 00:06:17,210
if you've got section editor information

144
00:06:15,260 --> 00:06:18,710
and you don't set the olive flag then

145
00:06:17,210 --> 00:06:21,620
that can be something that could

146
00:06:18,710 --> 00:06:24,169
actually be thrown away if I'm

147
00:06:21,620 --> 00:06:25,669
interpreting my own slides correctly so

148
00:06:24,169 --> 00:06:27,800
we didn't get to section errors with the

149
00:06:25,669 --> 00:06:29,360
last class and I wasn't sure if I'd get

150
00:06:27,800 --> 00:06:34,130
to them with this glass what we're doing

151
00:06:29,360 --> 00:06:36,050
a push on through alright so se address

152
00:06:34,130 --> 00:06:37,680
this is the virtual address where the

153
00:06:36,050 --> 00:06:40,650
section is going to start if

154
00:06:37,680 --> 00:06:43,110
gets mapped into memory offset is again

155
00:06:40,650 --> 00:06:45,960
the file offset its that's back just

156
00:06:43,110 --> 00:06:49,860
like the the data sizes and things like

157
00:06:45,960 --> 00:06:53,370
that so size is then section size and

158
00:06:49,860 --> 00:06:54,810
bites so we only have this is another

159
00:06:53,370 --> 00:06:57,570
one of those cases and trying to think

160
00:06:54,810 --> 00:07:01,110
where did we see this last it was you

161
00:06:57,570 --> 00:07:03,240
debug information in the PE files debug

162
00:07:01,110 --> 00:07:05,699
information in P files we had a memory

163
00:07:03,240 --> 00:07:08,550
address and we had a file address and we

164
00:07:05,699 --> 00:07:10,020
had a single size saying that these two

165
00:07:08,550 --> 00:07:11,580
cannot different right so that's what we

166
00:07:10,020 --> 00:07:14,130
have the same situation going on here

167
00:07:11,580 --> 00:07:16,440
for the section aters so other things

168
00:07:14,130 --> 00:07:18,180
like the type will tell us whether or

169
00:07:16,440 --> 00:07:21,419
not you know it's no bits and so it's a

170
00:07:18,180 --> 00:07:23,699
BSS section right so but if it's a BSS

171
00:07:21,419 --> 00:07:25,530
section it'll still have the size that's

172
00:07:23,699 --> 00:07:27,270
equal to the total BSS eyes we won't say

173
00:07:25,530 --> 00:07:29,610
like here's my memory size versus money

174
00:07:27,270 --> 00:07:33,240
by file size but it'll say it's no bit

175
00:07:29,610 --> 00:07:35,490
so implicitly it will not this the size

176
00:07:33,240 --> 00:07:39,680
the single size will not be used as file

177
00:07:35,490 --> 00:07:42,240
size because it's got type new business

178
00:07:39,680 --> 00:07:44,430
alright so again we've got a virtual

179
00:07:42,240 --> 00:07:46,500
memory start to a section we've got a

180
00:07:44,430 --> 00:07:49,500
file start to a section we've got a

181
00:07:46,500 --> 00:07:53,639
single size for the section and bites

182
00:07:49,500 --> 00:07:57,210
unless it's like a no bits section don't

183
00:07:53,639 --> 00:07:58,889
know why I put this in blue but this is

184
00:07:57,210 --> 00:08:01,349
just straight out of the specification

185
00:07:58,889 --> 00:08:02,760
basically just explaining to you I think

186
00:08:01,349 --> 00:08:04,650
all I'm trying to say right here is that

187
00:08:02,760 --> 00:08:07,770
if you want to go find out for all of

188
00:08:04,650 --> 00:08:10,320
these specific different types you know

189
00:08:07,770 --> 00:08:12,360
what does it mean then you can go ahead

190
00:08:10,320 --> 00:08:18,659
and go to the specification and read

191
00:08:12,360 --> 00:08:22,229
this link and info fields all right now

192
00:08:18,659 --> 00:08:25,860
we have SH adder a line so this is

193
00:08:22,229 --> 00:08:28,409
alignment constraints on the section but

194
00:08:25,860 --> 00:08:31,430
this is typically not going to be

195
00:08:28,409 --> 00:08:35,190
something like x 1000 this is more like

196
00:08:31,430 --> 00:08:39,180
let's say you've got some code section

197
00:08:35,190 --> 00:08:41,490
right and you know on x86 that x86 likes

198
00:08:39,180 --> 00:08:44,279
to be sixteen byte aligned or something

199
00:08:41,490 --> 00:08:46,529
like that and so this would say i want

200
00:08:44,279 --> 00:08:49,500
this section to be started at a multiple

201
00:08:46,529 --> 00:08:50,570
of Sutekh pub 16 basically so you'd want

202
00:08:49,500 --> 00:08:52,990
to make sure that this

203
00:08:50,570 --> 00:08:55,850
doesn't like get mapped into memory at

204
00:08:52,990 --> 00:08:57,350
four hundred thousand and one for

205
00:08:55,850 --> 00:08:58,910
instance you want to make sure it's four

206
00:08:57,350 --> 00:09:00,590
hundred thousand and ten because you

207
00:08:58,910 --> 00:09:03,470
need to have some section alignment and

208
00:09:00,590 --> 00:09:04,910
so this is just used so that on file you

209
00:09:03,470 --> 00:09:05,990
put all the section headers together

210
00:09:04,910 --> 00:09:08,060
with each other and they're going to

211
00:09:05,990 --> 00:09:09,980
specify where the file date is but in

212
00:09:08,060 --> 00:09:11,630
memory you want to make sure that

213
00:09:09,980 --> 00:09:14,140
they're still on appropriate alignments

214
00:09:11,630 --> 00:09:17,240
if they have any extra constraints so

215
00:09:14,140 --> 00:09:19,400
most the most thing that comes to mind

216
00:09:17,240 --> 00:09:21,470
with that is you know code will often

217
00:09:19,400 --> 00:09:23,960
have a line in thanks for optimization

218
00:09:21,470 --> 00:09:26,540
with the citizen so I would use that to

219
00:09:23,960 --> 00:09:28,910
to set it to sixteen byte aligned

220
00:09:26,540 --> 00:09:32,630
basically over all this has to be a

221
00:09:28,910 --> 00:09:36,230
multiple of two alright so if the

222
00:09:32,630 --> 00:09:39,700
particular section has within it an

223
00:09:36,230 --> 00:09:41,990
array of other structures basically then

224
00:09:39,700 --> 00:09:43,610
so basically if you see that the type

225
00:09:41,990 --> 00:09:45,800
says that it's one of these special

226
00:09:43,610 --> 00:09:47,330
things like a symbol table or it's going

227
00:09:45,800 --> 00:09:51,440
to have all your debugging structures

228
00:09:47,330 --> 00:09:52,790
built in then this entry type is going

229
00:09:51,440 --> 00:09:54,860
to say what the size of each of those

230
00:09:52,790 --> 00:09:56,630
debugging things is so basically just

231
00:09:54,860 --> 00:09:59,420
knowing that it's a simple table section

232
00:09:56,630 --> 00:10:01,250
is not enough to tell you like what the

233
00:09:59,420 --> 00:10:03,440
size of each and the subsequent symbol

234
00:10:01,250 --> 00:10:05,630
attainable data structures is going to

235
00:10:03,440 --> 00:10:07,730
be so that's what this thing is for so

236
00:10:05,630 --> 00:10:10,810
as you can see it's just used with some

237
00:10:07,730 --> 00:10:13,670
particular special types of sections

238
00:10:10,810 --> 00:10:15,950
okay so here's some of the the table

239
00:10:13,670 --> 00:10:18,520
showing all of the different by

240
00:10:15,950 --> 00:10:21,080
convention things that they're sent to

241
00:10:18,520 --> 00:10:26,060
special sections here's some of the

242
00:10:21,080 --> 00:10:28,340
names mmm kind of already I mean these

243
00:10:26,060 --> 00:10:31,040
are basically the same as what geez well

244
00:10:28,340 --> 00:10:33,380
things like that text BSS data and our

245
00:10:31,040 --> 00:10:37,670
data those are the same oh I think the

246
00:10:33,380 --> 00:10:48,520
sugar Caro data you should like our data

247
00:10:37,670 --> 00:10:48,520
I don't think they used see that eroded

248
00:10:49,540 --> 00:10:54,890
all right debug is where debug

249
00:10:51,650 --> 00:10:57,910
information would go comment is just

250
00:10:54,890 --> 00:10:59,999
version information typically a note

251
00:10:57,910 --> 00:11:03,009
whatever that says

252
00:10:59,999 --> 00:11:04,899
really matter but these are some of the

253
00:11:03,009 --> 00:11:08,049
more important ones they're things like

254
00:11:04,899 --> 00:11:12,879
the interpreter is the equivalent of the

255
00:11:08,049 --> 00:11:15,670
intersegmental anammox segment so it's

256
00:11:12,879 --> 00:11:18,129
either the dynamic linker itself and the

257
00:11:15,670 --> 00:11:20,470
dynamic linking information dine string

258
00:11:18,129 --> 00:11:22,269
this is so it says string table for

259
00:11:20,470 --> 00:11:26,290
dynamic linking this would be sort of

260
00:11:22,269 --> 00:11:30,339
like back way back in IIT we had int and

261
00:11:26,290 --> 00:11:33,910
i-80 right and the int pointed at some

262
00:11:30,339 --> 00:11:36,279
other thing int point of that hint name

263
00:11:33,910 --> 00:11:38,919
things right so if you remove the hint

264
00:11:36,279 --> 00:11:41,889
you can think of this dine string as

265
00:11:38,919 --> 00:11:43,989
sort of the names table not the import

266
00:11:41,889 --> 00:11:45,609
names table that has our va's with the

267
00:11:43,989 --> 00:11:51,369
actual names that are pointed to you by

268
00:11:45,609 --> 00:11:53,769
the by those are VA s and dine soon

269
00:11:51,369 --> 00:11:56,499
dynamic linking simple table that's just

270
00:11:53,769 --> 00:12:01,179
if you have symbols built in the table

271
00:11:56,499 --> 00:12:04,629
again great to them yeah went overboard

272
00:12:01,179 --> 00:12:06,339
with the writing of sections here but I

273
00:12:04,629 --> 00:12:12,249
guess this is like I said where we start

274
00:12:06,339 --> 00:12:14,709
to care global offset table god this is

275
00:12:12,249 --> 00:12:16,869
as it says it's used for position

276
00:12:14,709 --> 00:12:20,129
independent code so if you do the F pick

277
00:12:16,869 --> 00:12:23,169
option this will be used to find

278
00:12:20,129 --> 00:12:26,290
relative offsets to other Global's and

279
00:12:23,169 --> 00:12:29,649
things like that the PLT is used for

280
00:12:26,290 --> 00:12:34,239
your delay load imports or dynamic

281
00:12:29,649 --> 00:12:36,660
imports things like that but the PLT is

282
00:12:34,239 --> 00:12:39,939
the actual you can think of it like the

283
00:12:36,660 --> 00:12:42,879
delay load import address table whereas

284
00:12:39,939 --> 00:12:44,889
the got PLT i believe is going to be

285
00:12:42,879 --> 00:12:47,259
sort of the location where there's stubs

286
00:12:44,889 --> 00:12:48,999
but don't quote me on that because i got

287
00:12:47,259 --> 00:12:52,629
a picture coming up which will correct

288
00:12:48,999 --> 00:12:55,989
me if I'm you're right rel star stuff is

289
00:12:52,629 --> 00:12:58,149
relocation sections constructors and

290
00:12:55,989 --> 00:13:00,309
destructors are typically used for see

291
00:12:58,149 --> 00:13:03,129
stuff but actually it's a weird sort of

292
00:13:00,309 --> 00:13:04,539
thing as Corey said in the me exploits

293
00:13:03,129 --> 00:13:08,949
one class that even when you're not

294
00:13:04,539 --> 00:13:10,509
writing C++ code the GCC will actually

295
00:13:08,949 --> 00:13:13,379
tack on these constructor destructor

296
00:13:10,509 --> 00:13:14,789
stuff and you may not be using it at all

297
00:13:13,379 --> 00:13:16,619
but that means that there's this thing

298
00:13:14,789 --> 00:13:18,329
that exploits can we utilize because

299
00:13:16,619 --> 00:13:19,799
there's some function pointer that's

300
00:13:18,329 --> 00:13:21,720
getting called when you're starting and

301
00:13:19,799 --> 00:13:22,949
more importantly there's a function

302
00:13:21,720 --> 00:13:25,139
pointer that gets called when you're

303
00:13:22,949 --> 00:13:27,720
quitting the program so basically if you

304
00:13:25,139 --> 00:13:29,279
buffer overflow overwrite this function

305
00:13:27,720 --> 00:13:31,379
pointer and then crash the program and

306
00:13:29,279 --> 00:13:33,359
it's trying to quit down it's calling

307
00:13:31,379 --> 00:13:35,039
the destruct ER it will invoke the

308
00:13:33,359 --> 00:13:37,459
attackers code before it as a chance to

309
00:13:35,039 --> 00:13:37,459
exit


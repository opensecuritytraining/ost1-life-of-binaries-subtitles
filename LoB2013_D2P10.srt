1
00:00:03,520 --> 00:00:12,070
so on to Round six do your automated

2
00:00:10,210 --> 00:00:14,260
thing player on around six ask me

3
00:00:12,070 --> 00:00:22,240
questions and did you a question before

4
00:00:14,260 --> 00:00:25,320
go all right sku round 6i own been hurt

5
00:00:22,240 --> 00:00:25,320
by six

6
00:00:35,060 --> 00:00:39,290
I think I'm going to go through a couple

7
00:00:36,650 --> 00:00:42,020
of these up here just to give some hints

8
00:00:39,290 --> 00:00:43,550
make sure who resents a page you can

9
00:00:42,020 --> 00:00:45,710
follow along or just keep going into

10
00:00:43,550 --> 00:00:48,020
feta if you want alright so a virtual

11
00:00:45,710 --> 00:00:51,680
address of export address table well I

12
00:00:48,020 --> 00:00:54,710
know that the X bar us table is within

13
00:00:51,680 --> 00:00:56,270
the export information so i got let's

14
00:00:54,710 --> 00:01:02,960
try with PE view which gives us the

15
00:00:56,270 --> 00:01:09,590
simplest look at it so what are we on

16
00:01:02,960 --> 00:01:13,240
around six on six ends up 64 bit all

17
00:01:09,590 --> 00:01:13,240
right let's explore it

18
00:01:24,770 --> 00:01:30,450
six well there just happens to me a nice

19
00:01:28,799 --> 00:01:32,609
little export directory off here to the

20
00:01:30,450 --> 00:01:34,619
side and then we said there's three

21
00:01:32,609 --> 00:01:37,049
arrays that's export address table

22
00:01:34,619 --> 00:01:39,149
there's export aims table and there's

23
00:01:37,049 --> 00:01:41,369
that ordinals table set address some

24
00:01:39,149 --> 00:01:43,049
functions is the exported resting said

25
00:01:41,369 --> 00:01:45,359
address some names is the export names

26
00:01:43,049 --> 00:01:47,130
table and addresses name ordinals is

27
00:01:45,359 --> 00:01:51,390
that anymore no scandal for lack of a

28
00:01:47,130 --> 00:01:56,340
better name ok so again back to this

29
00:01:51,390 --> 00:02:01,020
smog so the question asks us is the VA

30
00:01:56,340 --> 00:02:02,849
of the export dress table so first of

31
00:02:01,020 --> 00:02:04,649
all we need to find the RBA to it the

32
00:02:02,849 --> 00:02:07,200
RBA to it is in this address of

33
00:02:04,649 --> 00:02:12,959
functions thing so that's what I just

34
00:02:07,200 --> 00:02:15,750
found right here I address the functions

35
00:02:12,959 --> 00:02:20,489
of prva so now I just convert it into a

36
00:02:15,750 --> 00:02:33,750
VA by adding a basic 268 plus on the

37
00:02:20,489 --> 00:02:35,489
basis that is that 2 3 4 260 right all

38
00:02:33,750 --> 00:02:36,959
right which of the following points that

39
00:02:35,489 --> 00:02:38,970
the export address table that's just

40
00:02:36,959 --> 00:02:40,440
pretty much that the slide was saying is

41
00:02:38,970 --> 00:02:43,109
it address of functions aggressive aim

42
00:02:40,440 --> 00:02:45,569
to address in a mortal's well again from

43
00:02:43,109 --> 00:02:48,599
this slide which one points at the

44
00:02:45,569 --> 00:02:51,630
export dressed table address some

45
00:02:48,599 --> 00:03:00,180
functions little arrow dress of

46
00:02:51,630 --> 00:03:04,620
functions i take their uncle till every

47
00:03:00,180 --> 00:03:06,989
way I know all right what is the value

48
00:03:04,620 --> 00:03:09,150
of the export directly number of

49
00:03:06,989 --> 00:03:15,569
functions well again there's only one

50
00:03:09,150 --> 00:03:19,980
export directory Oh export director what

51
00:03:15,569 --> 00:03:22,290
is the literal value of number of

52
00:03:19,980 --> 00:03:28,290
functions well number of functions right

53
00:03:22,290 --> 00:03:34,470
there too what is the literal value of

54
00:03:28,290 --> 00:03:37,239
address of functions file or the export

55
00:03:34,470 --> 00:03:44,389
directory dress-up function

56
00:03:37,239 --> 00:03:47,749
five or eight what is the ordinal of

57
00:03:44,389 --> 00:03:49,700
none this is totally a bug that I forgot

58
00:03:47,749 --> 00:03:51,109
from last five or they were going done

59
00:03:49,700 --> 00:03:53,629
it should be giving you like a screen

60
00:03:51,109 --> 00:03:57,400
name what is the word mole of particular

61
00:03:53,629 --> 00:04:03,590
string but let's see what we can find

62
00:03:57,400 --> 00:04:09,079
question for all right we've got an

63
00:04:03,590 --> 00:04:11,000
export address table and so now here's

64
00:04:09,079 --> 00:04:12,950
one thing so we've got we said we had

65
00:04:11,000 --> 00:04:18,739
three separate little tables right we

66
00:04:12,950 --> 00:04:22,970
have these three tables explore address

67
00:04:18,739 --> 00:04:24,500
names ordinals an DNT c FF explorer is

68
00:04:22,970 --> 00:04:26,840
just going to merge those all into this

69
00:04:24,500 --> 00:04:30,289
sort of columns and Mike previously a

70
00:04:26,840 --> 00:04:31,910
sort of implicitly is that great columns

71
00:04:30,289 --> 00:04:33,770
are calculated values they're not just

72
00:04:31,910 --> 00:04:35,360
the data structure values there's

73
00:04:33,770 --> 00:04:37,310
something that cff Explorer is trying to

74
00:04:35,360 --> 00:04:40,729
help you and go pull out the value for

75
00:04:37,310 --> 00:04:43,610
you and other values are real values so

76
00:04:40,729 --> 00:04:44,990
function r-va we can probably expect

77
00:04:43,610 --> 00:04:46,669
that that's going to be explored dressed

78
00:04:44,990 --> 00:04:49,820
table you said export address table is

79
00:04:46,669 --> 00:04:52,340
just a bunch of RBA's functions name

80
00:04:49,820 --> 00:04:56,090
r-va export names table that's just a

81
00:04:52,340 --> 00:04:58,280
bunch of RBA's two names and name here I

82
00:04:56,090 --> 00:05:01,009
believe actually is going to be the

83
00:04:58,280 --> 00:05:02,870
string table so it's it's looking up

84
00:05:01,009 --> 00:05:05,120
that's interesting it's not doing gray

85
00:05:02,870 --> 00:05:06,770
like it should all right but anyways

86
00:05:05,120 --> 00:05:09,590
it's asking you what is the ordinal of

87
00:05:06,770 --> 00:05:11,300
none well none is probably one of these

88
00:05:09,590 --> 00:05:13,009
error entries right here that doesn't

89
00:05:11,300 --> 00:05:15,830
have an actual name like python is

90
00:05:13,009 --> 00:05:17,419
saying it has so it should be asking you

91
00:05:15,830 --> 00:05:18,949
like what's the ordinal of devastate and

92
00:05:17,419 --> 00:05:21,919
then you just go over here you say to

93
00:05:18,949 --> 00:05:24,469
say what's the app ordinal inculcate

94
00:05:21,919 --> 00:05:26,180
you'd say four well I'm going to take a

95
00:05:24,469 --> 00:05:28,460
shot and i'm going to say no maybe it

96
00:05:26,180 --> 00:05:31,820
cook none right here and i'll say 60

97
00:05:28,460 --> 00:05:36,460
maybe it's six minutes seven either way

98
00:05:31,820 --> 00:05:40,610
it's a bug in correct answer is five ok

99
00:05:36,460 --> 00:05:42,500
what would be maybe that minus one it

100
00:05:40,610 --> 00:05:43,390
looks like this is actually the field

101
00:05:42,500 --> 00:05:47,410
that something

102
00:05:43,390 --> 00:05:50,500
that question but likewise there was a

103
00:05:47,410 --> 00:05:54,460
base set well yeah you're correct

104
00:05:50,500 --> 00:05:59,950
basically should be basically bat- the

105
00:05:54,460 --> 00:06:02,560
base good call all right as the RVA of

106
00:05:59,950 --> 00:06:04,570
the export names table well that's just

107
00:06:02,560 --> 00:06:08,230
going to be this address of names thing

108
00:06:04,570 --> 00:06:09,280
if i open up the next fine area but go

109
00:06:08,230 --> 00:06:13,300
ahead on your own for a little bit

110
00:06:09,280 --> 00:06:14,800
player arms that and as you can see

111
00:06:13,300 --> 00:06:16,300
basically everything you could ever want

112
00:06:14,800 --> 00:06:18,190
to know is going to be in this exports

113
00:06:16,300 --> 00:06:20,290
directory thing over here on the side

114
00:06:18,190 --> 00:06:22,060
you may get asked some questions that

115
00:06:20,290 --> 00:06:25,450
require you to go to the data directory

116
00:06:22,060 --> 00:06:29,170
and go to the zero our country otherwise

117
00:06:25,450 --> 00:06:30,730
it's pretty much all they're making a

118
00:06:29,170 --> 00:06:33,130
bug if you get a question where it's

119
00:06:30,730 --> 00:06:35,020
saying you know is the export file

120
00:06:33,130 --> 00:06:36,880
time/date stamp the same as the file

121
00:06:35,020 --> 00:06:38,740
header one so to answer that you'd go

122
00:06:36,880 --> 00:06:40,600
like okay let me look at this time date

123
00:06:38,740 --> 00:06:41,830
stamp here and then we go back to the

124
00:06:40,600 --> 00:06:43,630
file header and look at the time it's

125
00:06:41,830 --> 00:06:45,400
not there you just had a case where they

126
00:06:43,630 --> 00:06:47,440
were the same but the program said they

127
00:06:45,400 --> 00:06:49,860
weren't so there may be a bug on that if

128
00:06:47,440 --> 00:06:49,860
you get that


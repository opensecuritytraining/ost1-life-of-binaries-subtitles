1
00:00:10,080 --> 00:00:14,639
all right um we'll do this quick

2
00:00:12,639 --> 00:00:17,440
so we need some fields here that we care

3
00:00:14,639 --> 00:00:20,000
about let's see joe how much field do we

4
00:00:17,440 --> 00:00:20,000
care about here

5
00:00:20,720 --> 00:00:23,199
still don't care about that magic there

6
00:00:22,240 --> 00:00:24,800
you go

7
00:00:23,199 --> 00:00:27,439
you care about the magic why do we care

8
00:00:24,800 --> 00:00:30,359
about it this time

9
00:00:27,439 --> 00:00:33,760
so we can

10
00:00:30,359 --> 00:00:35,360
64-32 yep this optional header magic i'm

11
00:00:33,760 --> 00:00:37,120
showing the 32-bit version right here

12
00:00:35,360 --> 00:00:39,040
but this magic is saying

13
00:00:37,120 --> 00:00:41,840
interpret this optional header as a

14
00:00:39,040 --> 00:00:43,120
32-bit or 64-bit one where we've got you

15
00:00:41,840 --> 00:00:45,200
know

16
00:00:43,120 --> 00:00:47,440
or 64-32-bit values in for some of this

17
00:00:45,200 --> 00:00:48,239
stuff based on what the magic value is

18
00:00:47,440 --> 00:00:51,360
right

19
00:00:48,239 --> 00:00:51,360
what else do we care about

20
00:00:52,719 --> 00:00:56,000
what's any field in here that we care

21
00:00:54,079 --> 00:00:57,760
about

22
00:00:56,000 --> 00:00:59,440
the image base image base why don't we

23
00:00:57,760 --> 00:01:02,079
care about it

24
00:00:59,440 --> 00:01:02,079
that's uh

25
00:01:03,359 --> 00:01:08,159
is that the start address it's where the

26
00:01:05,840 --> 00:01:10,240
thing is asking to be loaded into memory

27
00:01:08,159 --> 00:01:12,240
it's saying this is the start location

28
00:01:10,240 --> 00:01:13,760
where i want my file to be mapped into

29
00:01:12,240 --> 00:01:15,680
memory basically

30
00:01:13,760 --> 00:01:18,479
yeah what else

31
00:01:15,680 --> 00:01:20,159
yes you want that uh data directory here

32
00:01:18,479 --> 00:01:22,080
certainly data directory we're going to

33
00:01:20,159 --> 00:01:24,240
see over and over again right so we care

34
00:01:22,080 --> 00:01:26,080
about that reality is that it's embedded

35
00:01:24,240 --> 00:01:27,360
at the end of the thing your different

36
00:01:26,080 --> 00:01:29,280
tools may show that it's kind of

37
00:01:27,360 --> 00:01:30,799
separate or not but it really is just

38
00:01:29,280 --> 00:01:32,720
embedded at the end

39
00:01:30,799 --> 00:01:36,680
and what do each of those entries in the

40
00:01:32,720 --> 00:01:36,680
data directory look like

41
00:01:41,600 --> 00:01:45,600
no

42
00:01:43,119 --> 00:01:48,640
name you're thinking about imports

43
00:01:45,600 --> 00:01:51,600
right there it's an array and so there's

44
00:01:48,640 --> 00:01:53,360
going to be two entries per

45
00:01:51,600 --> 00:01:55,680
entry in this thing so this is basically

46
00:01:53,360 --> 00:01:57,200
an array of data structures called image

47
00:01:55,680 --> 00:01:58,799
data directory

48
00:01:57,200 --> 00:02:00,560
what does one of those look like say

49
00:01:58,799 --> 00:02:03,360
again it's the union that was that music

50
00:02:00,560 --> 00:02:05,759
string it's not a union no it's not rba

51
00:02:03,360 --> 00:02:07,360
size rv in size is sort of it's a

52
00:02:05,759 --> 00:02:08,959
structure it's not a union structure

53
00:02:07,360 --> 00:02:11,039
it's just a plane structure where the

54
00:02:08,959 --> 00:02:13,840
first field of the structure is the rba

55
00:02:11,039 --> 00:02:16,319
and the second field is the size so for

56
00:02:13,840 --> 00:02:19,360
every entry in this it's two two two two

57
00:02:16,319 --> 00:02:22,000
rva size rba size rda size

58
00:02:19,360 --> 00:02:23,280
all there at the end of this uh thing

59
00:02:22,000 --> 00:02:24,400
all right what's another thing we care

60
00:02:23,280 --> 00:02:25,440
about

61
00:02:24,400 --> 00:02:27,840
john

62
00:02:25,440 --> 00:02:29,760
the dll characteristics yup dll

63
00:02:27,840 --> 00:02:33,360
characteristics what's an example dml

64
00:02:29,760 --> 00:02:36,000
characteristic uh is that like mx

65
00:02:33,360 --> 00:02:38,560
yes nx right non-executable memory

66
00:02:36,000 --> 00:02:40,879
what's another pll characteristic

67
00:02:38,560 --> 00:02:40,879
dave

68
00:02:43,280 --> 00:02:47,280
what's another yellow characteristic

69
00:02:45,040 --> 00:02:49,760
algorithm is it catchable

70
00:02:47,280 --> 00:02:50,879
sagan cachable

71
00:02:49,760 --> 00:02:51,920
uh

72
00:02:50,879 --> 00:02:53,440
yes

73
00:02:51,920 --> 00:02:56,160
yes

74
00:02:53,440 --> 00:02:59,920
not cash right because one of them not

75
00:02:56,160 --> 00:03:02,080
paged is another one aslr aosr

76
00:02:59,920 --> 00:03:04,800
right wait not cash now page no that's a

77
00:03:02,080 --> 00:03:06,480
section characters yeah

78
00:03:04,800 --> 00:03:08,560
i'm trying to just picture the slide in

79
00:03:06,480 --> 00:03:10,080
my mind but yes those are see we've got

80
00:03:08,560 --> 00:03:11,440
characteristics of the file letter we've

81
00:03:10,080 --> 00:03:13,760
got characteristics down here at the

82
00:03:11,440 --> 00:03:15,200
option letter

83
00:03:13,760 --> 00:03:16,879
section headers

84
00:03:15,200 --> 00:03:18,239
it's all good

85
00:03:16,879 --> 00:03:20,159
all right what's another thing we

86
00:03:18,239 --> 00:03:22,560
actually care about out here paul in

87
00:03:20,159 --> 00:03:24,239
this section alignment section alignment

88
00:03:22,560 --> 00:03:25,840
and so why do we care about that how is

89
00:03:24,239 --> 00:03:28,000
that used

90
00:03:25,840 --> 00:03:30,159
that gives you the

91
00:03:28,000 --> 00:03:32,319
basically page size where each section

92
00:03:30,159 --> 00:03:33,840
starts yep so that's supposed to be used

93
00:03:32,319 --> 00:03:35,200
to basically say for each of those

94
00:03:33,840 --> 00:03:36,640
sections when they get mapped into

95
00:03:35,200 --> 00:03:39,120
memory from disk

96
00:03:36,640 --> 00:03:40,560
they should go on alignments of whatever

97
00:03:39,120 --> 00:03:42,560
section alignment is that's another

98
00:03:40,560 --> 00:03:44,239
thing we care about jesse yeah file

99
00:03:42,560 --> 00:03:45,519
alignment all right file alignment how

100
00:03:44,239 --> 00:03:47,760
do we use that

101
00:03:45,519 --> 00:03:49,920
does the same job for the physical file

102
00:03:47,760 --> 00:03:52,000
yep same job for the file section in

103
00:03:49,920 --> 00:03:53,760
file here's the alignment if we don't

104
00:03:52,000 --> 00:03:56,640
have enough space in our section we pad

105
00:03:53,760 --> 00:03:58,319
it out to the size of alignment

106
00:03:56,640 --> 00:03:59,840
are there any other things we haven't

107
00:03:58,319 --> 00:04:01,680
covered in here

108
00:03:59,840 --> 00:04:02,959
yes one other important one we haven't

109
00:04:01,680 --> 00:04:05,040
really dealt with it i think you might

110
00:04:02,959 --> 00:04:07,680
have got a few quiz questions if you

111
00:04:05,040 --> 00:04:09,599
happen to run into it but yes address

112
00:04:07,680 --> 00:04:12,159
dress venture point absolutely why do

113
00:04:09,599 --> 00:04:12,159
you care about it

114
00:04:13,120 --> 00:04:17,040
sure it is the first place where we

115
00:04:14,799 --> 00:04:18,560
execute code so if you want to set a

116
00:04:17,040 --> 00:04:19,680
breakpoint with your debugger you just

117
00:04:18,560 --> 00:04:20,720
look at the header and you'd say i'm

118
00:04:19,680 --> 00:04:23,680
going to set a breakpoint on that

119
00:04:20,720 --> 00:04:25,040
particular guy now that's an rva so

120
00:04:23,680 --> 00:04:26,400
that's another thing where you if you

121
00:04:25,040 --> 00:04:28,160
want to set a breakpoint

122
00:04:26,400 --> 00:04:32,080
you got to say either you know hello

123
00:04:28,160 --> 00:04:34,560
world plus whatever that is or you do

124
00:04:32,080 --> 00:04:36,160
image based plus address of entry point

125
00:04:34,560 --> 00:04:37,440
to get an absolute virtual address and

126
00:04:36,160 --> 00:04:39,040
then you set a breakpoint on that

127
00:04:37,440 --> 00:04:41,360
address

128
00:04:39,040 --> 00:04:42,800
i think that's everything

129
00:04:41,360 --> 00:04:44,639
good

130
00:04:42,800 --> 00:04:48,639
size of image yes

131
00:04:44,639 --> 00:04:49,840
last one and why do we care about that

132
00:04:48,639 --> 00:04:53,520
that's the size of memory you have to

133
00:04:49,840 --> 00:04:55,360
allocate thickness size of memory the os

134
00:04:53,520 --> 00:04:57,440
loader basically has to allocate to

135
00:04:55,360 --> 00:04:58,960
cover this entire range here you know

136
00:04:57,440 --> 00:05:00,479
there can be gaps within there because

137
00:04:58,960 --> 00:05:03,280
the sections aren't actually using all

138
00:05:00,479 --> 00:05:04,960
of it but you know i for purposes of

139
00:05:03,280 --> 00:05:06,479
this class and purposes of the quiz you

140
00:05:04,960 --> 00:05:08,479
can think of it as the equivalence of

141
00:05:06,479 --> 00:05:11,520
like start at the beginning go to the

142
00:05:08,479 --> 00:05:13,759
rva of the last section plus however big

143
00:05:11,520 --> 00:05:15,680
the section is and that's what the

144
00:05:13,759 --> 00:05:17,280
president should be i should say part of

145
00:05:15,680 --> 00:05:19,039
the reason i say this is for the purpose

146
00:05:17,280 --> 00:05:21,520
of this class is when i was doing my

147
00:05:19,039 --> 00:05:22,960
randomizations i wasn't updating the

148
00:05:21,520 --> 00:05:24,960
size of an image and the executables

149
00:05:22,960 --> 00:05:27,039
weren't working so if you don't if this

150
00:05:24,960 --> 00:05:29,520
is not accurate your code will just not

151
00:05:27,039 --> 00:05:30,720
run this is too small basically

152
00:05:29,520 --> 00:05:32,320
so if that's

153
00:05:30,720 --> 00:05:34,320
smaller than you know all of your

154
00:05:32,320 --> 00:05:35,680
sections added up you know the reviews

155
00:05:34,320 --> 00:05:37,759
of your sections got it up so that's

156
00:05:35,680 --> 00:05:39,280
kind of why you know while i agree with

157
00:05:37,759 --> 00:05:41,199
your interpretation of how much memory

158
00:05:39,280 --> 00:05:42,960
is this using you can add how much

159
00:05:41,199 --> 00:05:45,199
they're actually using

160
00:05:42,960 --> 00:05:47,520
from the perspective of the os loader if

161
00:05:45,199 --> 00:05:49,440
you don't actually have that being the

162
00:05:47,520 --> 00:05:52,479
last virtual address plus whatever size

163
00:05:49,440 --> 00:05:55,560
it is

164
00:05:52,479 --> 00:05:55,560
all right


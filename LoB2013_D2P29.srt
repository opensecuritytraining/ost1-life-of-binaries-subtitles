1
00:00:03,560 --> 00:00:11,670
the rarely used section for all right it

2
00:00:09,809 --> 00:00:17,279
looks like I covered viruses first for

3
00:00:11,670 --> 00:00:19,040
the stucco hackers first all right so so

4
00:00:17,279 --> 00:00:21,750
packing was pretty common back when

5
00:00:19,040 --> 00:00:24,810
common for legitimate purposes back when

6
00:00:21,750 --> 00:00:27,149
you had you know hard drive space as a

7
00:00:24,810 --> 00:00:29,880
premium floppy disk space in space at a

8
00:00:27,149 --> 00:00:32,070
premium right and so the point of

9
00:00:29,880 --> 00:00:34,230
Packers is basically just you take some

10
00:00:32,070 --> 00:00:36,060
executable and you're going to want to

11
00:00:34,230 --> 00:00:37,440
compressed on the code and the data and

12
00:00:36,060 --> 00:00:39,420
you're going to tack a little bit of

13
00:00:37,440 --> 00:00:41,280
code on the front that says you know

14
00:00:39,420 --> 00:00:43,740
take this blob of compressed code and

15
00:00:41,280 --> 00:00:45,870
data and decompress it into the exact

16
00:00:43,740 --> 00:00:47,250
same locations in memory where it would

17
00:00:45,870 --> 00:00:49,320
have ended up if you just ran the

18
00:00:47,250 --> 00:00:51,840
uncompressed program so make sure it

19
00:00:49,320 --> 00:00:56,250
decompresses and shows up the exact same

20
00:00:51,840 --> 00:00:58,310
in memory but then okay i just lost me

21
00:00:56,250 --> 00:01:00,510
25 do you compress it into memory and

22
00:00:58,310 --> 00:01:02,010
then just go ahead and jump to the

23
00:01:00,510 --> 00:01:04,170
original entry point and let the thing

24
00:01:02,010 --> 00:01:05,729
run well so that's notionally it's not

25
00:01:04,170 --> 00:01:08,159
quite that simple in practice right now

26
00:01:05,729 --> 00:01:11,190
we know all about all of the nitty

27
00:01:08,159 --> 00:01:13,530
gritty details that the OS performs at

28
00:01:11,190 --> 00:01:15,510
load time and so a packer has to

29
00:01:13,530 --> 00:01:17,430
actually replicate these details in

30
00:01:15,510 --> 00:01:20,580
order to be able to jump back to the

31
00:01:17,430 --> 00:01:23,100
original program so for instance you can

32
00:01:20,580 --> 00:01:24,810
decompress the program into memory but

33
00:01:23,100 --> 00:01:27,120
then it'll basically just look like a

34
00:01:24,810 --> 00:01:29,340
memory mapped copy of the original file

35
00:01:27,120 --> 00:01:31,470
right you don't have the OS loader

36
00:01:29,340 --> 00:01:33,210
filling in the import address table you

37
00:01:31,470 --> 00:01:35,430
don't have the OS loader doing the

38
00:01:33,210 --> 00:01:37,619
relocations and so this is the kind of

39
00:01:35,430 --> 00:01:40,200
thing that the packer has to do itself

40
00:01:37,619 --> 00:01:41,820
it has to go and fix up anything you

41
00:01:40,200 --> 00:01:44,970
know if it d compressed into a random

42
00:01:41,820 --> 00:01:47,280
location like let's say that aslr is

43
00:01:44,970 --> 00:01:49,409
being used and it can't get the virtual

44
00:01:47,280 --> 00:01:51,650
memory address space that it wanted then

45
00:01:49,409 --> 00:01:54,840
you know it has to now apply relocation

46
00:01:51,650 --> 00:01:56,520
in order for this program to run it has

47
00:01:54,840 --> 00:01:57,810
to have you know the import dress table

48
00:01:56,520 --> 00:02:00,299
because this program that your

49
00:01:57,810 --> 00:02:01,920
compressed and decompressing it doesn't

50
00:02:00,299 --> 00:02:03,540
know anything about upx or any other

51
00:02:01,920 --> 00:02:05,759
Packer it just thinks that it's going to

52
00:02:03,540 --> 00:02:07,259
be run like a normal executable loaded

53
00:02:05,759 --> 00:02:09,330
into memory the OS will do whatever it

54
00:02:07,259 --> 00:02:11,489
has to do and then the OS will call it

55
00:02:09,330 --> 00:02:14,340
at its entry point and then it'll go

56
00:02:11,489 --> 00:02:17,130
ahead and start running right so packers

57
00:02:14,340 --> 00:02:18,630
are basically this has been called

58
00:02:17,130 --> 00:02:21,390
in the context of Linux it's been called

59
00:02:18,630 --> 00:02:23,010
virtual exec or user space exact sorry

60
00:02:21,390 --> 00:02:24,690
that's the correct term if you google

61
00:02:23,010 --> 00:02:27,330
for userspace exact you'll hear people

62
00:02:24,690 --> 00:02:28,860
talking about this notion of normally

63
00:02:27,330 --> 00:02:31,560
when you call the exact function on

64
00:02:28,860 --> 00:02:34,110
Linux you're invoking a process you call

65
00:02:31,560 --> 00:02:35,820
exec and then some process names and

66
00:02:34,110 --> 00:02:37,440
then the OS handles all the things

67
00:02:35,820 --> 00:02:40,020
that's necessary to load that thing into

68
00:02:37,440 --> 00:02:42,960
memory do relocations doing port address

69
00:02:40,020 --> 00:02:44,730
tables stuff things like that when a

70
00:02:42,960 --> 00:02:47,520
packers doing in itself it's kind of

71
00:02:44,730 --> 00:02:49,530
like it has to do the OSS job that the

72
00:02:47,520 --> 00:02:52,350
OS would do with exec so it's been

73
00:02:49,530 --> 00:02:55,020
called user space exec called Nevin

74
00:02:52,350 --> 00:02:57,330
shuttle that's a different paper that's

75
00:02:55,020 --> 00:02:59,670
still talking about the same thing but

76
00:02:57,330 --> 00:03:02,490
at the end of the day it's just this

77
00:02:59,670 --> 00:03:04,350
unpacking program has to replicate the

78
00:03:02,490 --> 00:03:06,090
behavior of the operating system motor

79
00:03:04,350 --> 00:03:07,710
in order to make sure that it puts

80
00:03:06,090 --> 00:03:09,930
everything back in place the same way

81
00:03:07,710 --> 00:03:11,370
the OS loader would have put it and then

82
00:03:09,930 --> 00:03:13,560
once it's done doing all that all that

83
00:03:11,370 --> 00:03:16,260
has to do is jump to the original entry

84
00:03:13,560 --> 00:03:17,700
point of that code and that's another

85
00:03:16,260 --> 00:03:18,840
thing you'll hear frequently mentioned

86
00:03:17,700 --> 00:03:21,090
in the context of malware analysis

87
00:03:18,840 --> 00:03:22,800
you'll hear you know we're searching for

88
00:03:21,090 --> 00:03:24,180
the original entry point we've got some

89
00:03:22,800 --> 00:03:26,850
malware it's packed it's compressed

90
00:03:24,180 --> 00:03:28,830
what's the original entry point and what

91
00:03:26,850 --> 00:03:30,990
they're saying there is may want to know

92
00:03:28,830 --> 00:03:33,180
what the decompressed program look like

93
00:03:30,990 --> 00:03:35,580
and where code started in the original

94
00:03:33,180 --> 00:03:38,340
and so there's this ambiguity where does

95
00:03:35,580 --> 00:03:41,040
the unpacking stop and the original code

96
00:03:38,340 --> 00:03:42,300
start right with things like upx it's

97
00:03:41,040 --> 00:03:44,430
nice and clear because when the

98
00:03:42,300 --> 00:03:47,459
unpacking code is done you'll see this

99
00:03:44,430 --> 00:03:50,010
like direct jump back into memory that

100
00:03:47,459 --> 00:03:52,470
upx was just scribbling all over upx

101
00:03:50,010 --> 00:03:54,630
just from decompressed this big blob to

102
00:03:52,470 --> 00:03:56,580
like right here and then you see a jump

103
00:03:54,630 --> 00:04:00,480
to right there and that's kind of a

104
00:03:56,580 --> 00:04:02,910
common notional way to to have a sense

105
00:04:00,480 --> 00:04:05,550
when unpacking is done if the packer

106
00:04:02,910 --> 00:04:08,010
wrote a bunch of data to some location

107
00:04:05,550 --> 00:04:09,720
and then jumped into that data that's

108
00:04:08,010 --> 00:04:11,100
probably the original data and that's

109
00:04:09,720 --> 00:04:12,480
probably the transition point but

110
00:04:11,100 --> 00:04:14,760
obviously they can make it quite a bit

111
00:04:12,480 --> 00:04:17,330
more sophisticated than just like

112
00:04:14,760 --> 00:04:20,340
writing a blob and jumping into it right

113
00:04:17,330 --> 00:04:22,020
so when people talk about original entry

114
00:04:20,340 --> 00:04:24,450
point what they mean is address of entry

115
00:04:22,020 --> 00:04:26,280
point in the original file before it was

116
00:04:24,450 --> 00:04:28,890
compressed because when you compress it

117
00:04:26,280 --> 00:04:30,129
down the file on disk is going to have

118
00:04:28,890 --> 00:04:31,959
the entry point for the

119
00:04:30,129 --> 00:04:33,580
hacker code and that'll start kicking

120
00:04:31,959 --> 00:04:35,379
off it'll decompress the thing into

121
00:04:33,580 --> 00:04:38,379
memory but what you really want is you

122
00:04:35,379 --> 00:04:40,869
want the exact point where the Packer is

123
00:04:38,379 --> 00:04:43,239
done it jumps to the original code and

124
00:04:40,869 --> 00:04:44,619
the thing in memory right now looks just

125
00:04:43,239 --> 00:04:46,239
like the thing would have looked like in

126
00:04:44,619 --> 00:04:48,459
memory have the OS just loaded the

127
00:04:46,239 --> 00:04:51,099
original program now you can take that

128
00:04:48,459 --> 00:04:53,889
dump it from memory and start doing

129
00:04:51,099 --> 00:04:56,319
static analysis on that so that's one of

130
00:04:53,889 --> 00:04:58,599
the goals often with unpacking is find

131
00:04:56,319 --> 00:05:00,219
the original entry point dump the thing

132
00:04:58,599 --> 00:05:02,319
once you see the jump to the original

133
00:05:00,219 --> 00:05:04,029
entry point dump it from memory and then

134
00:05:02,319 --> 00:05:06,789
you know you can reconstruct it on disk

135
00:05:04,029 --> 00:05:08,349
like you know based on the header

136
00:05:06,789 --> 00:05:09,939
information which may or may not be in

137
00:05:08,349 --> 00:05:11,439
memory you can try to reconstruct it

138
00:05:09,939 --> 00:05:16,479
back to what the file would have looked

139
00:05:11,439 --> 00:05:18,909
like based on the memorandum so anyways

140
00:05:16,479 --> 00:05:20,830
riginal context was it was originally

141
00:05:18,909 --> 00:05:23,349
used just to decompress because you

142
00:05:20,830 --> 00:05:26,259
didn't have enough file space and I

143
00:05:23,349 --> 00:05:28,179
should say that this technique of like

144
00:05:26,259 --> 00:05:29,979
compressing and then decompressing

145
00:05:28,179 --> 00:05:32,019
yourself once you get into memory it's

146
00:05:29,979 --> 00:05:35,589
still used commonly in embedded systems

147
00:05:32,019 --> 00:05:37,869
if you go read the talks on like Cisco

148
00:05:35,589 --> 00:05:39,610
IOS hacking and things like that they

149
00:05:37,869 --> 00:05:42,550
talk about how you know iOS the

150
00:05:39,610 --> 00:05:45,099
operating system for cisco's is you know

151
00:05:42,550 --> 00:05:48,909
it's compressed it's got this image in

152
00:05:45,099 --> 00:05:50,499
the in the null in the I don't remember

153
00:05:48,909 --> 00:05:54,459
whether it's flash chips but i think

154
00:05:50,499 --> 00:05:56,709
it's flash chips I think it's flash

155
00:05:54,459 --> 00:05:59,259
chips in the in the routers themselves

156
00:05:56,709 --> 00:06:01,659
and so you've got this eeprom that holds

157
00:05:59,259 --> 00:06:04,089
the iOS image and then at runtime it

158
00:06:01,659 --> 00:06:05,679
loads itself up in a decompresses the of

159
00:06:04,089 --> 00:06:08,409
the operating system that runs on the

160
00:06:05,679 --> 00:06:11,319
router same thing for bios you have the

161
00:06:08,409 --> 00:06:16,240
bios written into flash chips in the

162
00:06:11,319 --> 00:06:18,279
system and it'll have some initial boot

163
00:06:16,240 --> 00:06:20,740
store bootstrapping code it's called the

164
00:06:18,279 --> 00:06:22,869
boot block where the booth block will

165
00:06:20,740 --> 00:06:25,209
kick off decompression of a bunch of

166
00:06:22,869 --> 00:06:26,889
subsequent blocks of code neil

167
00:06:25,209 --> 00:06:29,409
decompress amount of memory and invoke

168
00:06:26,889 --> 00:06:30,759
them so whenever you have space at a

169
00:06:29,409 --> 00:06:34,119
premium you're going to see the same

170
00:06:30,759 --> 00:06:35,769
sort of expanded to memory technique and

171
00:06:34,119 --> 00:06:38,259
it's still used on embedded systems

172
00:06:35,769 --> 00:06:42,009
where you have less space and routers

173
00:06:38,259 --> 00:06:43,660
and bios and things like that but on all

174
00:06:42,009 --> 00:06:45,550
own operating systems

175
00:06:43,660 --> 00:06:48,760
i just bought a 3 terabyte drive the

176
00:06:45,550 --> 00:06:50,530
other day for 140 bucks so space is not

177
00:06:48,760 --> 00:06:52,240
at a premium there's not really good

178
00:06:50,530 --> 00:06:54,610
reasons to be using Packers on

179
00:06:52,240 --> 00:06:56,710
legitimate things the most common use

180
00:06:54,610 --> 00:06:59,710
for them these days is to explicitly

181
00:06:56,710 --> 00:07:01,950
obfuscated code so because we take the

182
00:06:59,710 --> 00:07:04,240
original program we compress it down

183
00:07:01,950 --> 00:07:06,700
right we've got the original program

184
00:07:04,240 --> 00:07:08,500
where an analyst like yourselves would

185
00:07:06,700 --> 00:07:09,850
be able to understand that oh I can look

186
00:07:08,500 --> 00:07:11,560
for the strings in the import name

187
00:07:09,850 --> 00:07:12,820
stable and figure out you know what

188
00:07:11,560 --> 00:07:15,280
functions it's probably going to call

189
00:07:12,820 --> 00:07:17,110
well if i take my import names table and

190
00:07:15,280 --> 00:07:19,450
all the rest of my data compress it down

191
00:07:17,110 --> 00:07:22,060
and make it into a blob and then i just

192
00:07:19,450 --> 00:07:23,650
tack on some unpacking code now you're

193
00:07:22,060 --> 00:07:25,900
going to not really understand what was

194
00:07:23,650 --> 00:07:30,060
going on in that original thing until

195
00:07:25,900 --> 00:07:33,280
you run it and let it decompress itself

196
00:07:30,060 --> 00:07:35,500
all right so at runtime all right so

197
00:07:33,280 --> 00:07:37,690
this is the packing process on disk you

198
00:07:35,500 --> 00:07:40,180
get this compressed version and at

199
00:07:37,690 --> 00:07:42,430
runtime and the header information was

200
00:07:40,180 --> 00:07:44,820
updated to start at the unpacking code

201
00:07:42,430 --> 00:07:47,370
and then the unpacking code just

202
00:07:44,820 --> 00:07:49,890
decompresses the information into memory

203
00:07:47,370 --> 00:07:53,230
so the first thing is it'll do is it'll

204
00:07:49,890 --> 00:07:55,300
sort of it'll map enough space so that

205
00:07:53,230 --> 00:07:56,680
the thing can be decompressed so that's

206
00:07:55,300 --> 00:07:58,690
a common feature that you'll see the

207
00:07:56,680 --> 00:08:01,180
file data is going to be much smaller

208
00:07:58,690 --> 00:08:03,280
than the virtual size so you're going to

209
00:08:01,180 --> 00:08:04,510
have a lot of virtual size for a small

210
00:08:03,280 --> 00:08:06,370
amount of file data because you got

211
00:08:04,510 --> 00:08:08,950
compressed file data but it's going to

212
00:08:06,370 --> 00:08:11,350
want to map it into memory compressed

213
00:08:08,950 --> 00:08:12,669
and then decompress it into memory so

214
00:08:11,350 --> 00:08:14,500
you're going to need lots more space

215
00:08:12,669 --> 00:08:16,870
you're going to need the equivalent

216
00:08:14,500 --> 00:08:18,010
amount of space as whatever the original

217
00:08:16,870 --> 00:08:19,960
file was right because you need

218
00:08:18,010 --> 00:08:22,300
everything to just look exactly like the

219
00:08:19,960 --> 00:08:25,060
original file so then you've got the

220
00:08:22,300 --> 00:08:27,310
compressed blob gets decompressed into

221
00:08:25,060 --> 00:08:30,160
memory so that everything looks the same

222
00:08:27,310 --> 00:08:31,720
way as it originally did and then

223
00:08:30,160 --> 00:08:33,520
typically the unpacking code will still

224
00:08:31,720 --> 00:08:35,380
have to be even further beyond that so

225
00:08:33,520 --> 00:08:36,820
it'll require even extra memory the

226
00:08:35,380 --> 00:08:38,979
unpacking code starts to sit there

227
00:08:36,820 --> 00:08:40,630
running the unpacking loop and then when

228
00:08:38,979 --> 00:08:42,550
game packing code is finally done in

229
00:08:40,630 --> 00:08:45,070
something like upx you'll have just a

230
00:08:42,550 --> 00:08:47,410
very simple jump back to it would have

231
00:08:45,070 --> 00:08:49,450
taken a copy of the address of entry

232
00:08:47,410 --> 00:08:51,610
point in the original code and it'll

233
00:08:49,450 --> 00:08:53,710
just jump back to that address of entry

234
00:08:51,610 --> 00:08:55,540
quick as the original headers would have

235
00:08:53,710 --> 00:08:56,529
specified that's what we call the

236
00:08:55,540 --> 00:08:58,589
original entry point

237
00:08:56,529 --> 00:08:58,589
you


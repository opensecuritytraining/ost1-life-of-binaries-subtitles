1
00:00:03,569 --> 00:00:15,879
all right so L 2012 so originally elf

2
00:00:13,030 --> 00:00:22,450
was released with the UNIX system V

3
00:00:15,879 --> 00:00:24,430
stuff on time ago when the ease and well

4
00:00:22,450 --> 00:00:27,700
I don't know I just copied and pasted

5
00:00:24,430 --> 00:00:31,510
directly from this history lesson and so

6
00:00:27,700 --> 00:00:35,379
just saying basically that elf started

7
00:00:31,510 --> 00:00:37,660
out with you know system v which son was

8
00:00:35,379 --> 00:00:39,579
working on and this basically came to

9
00:00:37,660 --> 00:00:42,790
eventually be the dominant format for

10
00:00:39,579 --> 00:00:44,890
Linux's and unix's and all variants and

11
00:00:42,790 --> 00:00:47,980
all variants of UNIX basically including

12
00:00:44,890 --> 00:00:51,160
Linux so L stands for executable and

13
00:00:47,980 --> 00:00:55,809
linkable format and we're going to focus

14
00:00:51,160 --> 00:00:58,480
on x86 64 version of it so you can check

15
00:00:55,809 --> 00:01:01,750
out the previous class slides in the

16
00:00:58,480 --> 00:01:06,310
previous class video or for the 32-bit

17
00:01:01,750 --> 00:01:10,439
version but it's basically the same the

18
00:01:06,310 --> 00:01:14,139
official documentation for the 32-bit

19
00:01:10,439 --> 00:01:17,530
ABI how occasion binary interface is

20
00:01:14,139 --> 00:01:20,139
hosted that score calm let's go of the

21
00:01:17,530 --> 00:01:22,630
people who started trying to you know

22
00:01:20,139 --> 00:01:25,149
say claim that they had copyrights on on

23
00:01:22,630 --> 00:01:27,789
UNIX and they owned UNIX and anybody

24
00:01:25,149 --> 00:01:31,529
like the Linux's were using things had

25
00:01:27,789 --> 00:01:34,149
to pay up so they were generally

26
00:01:31,529 --> 00:01:37,420
disliked became a sort of copyright

27
00:01:34,149 --> 00:01:41,049
troll but originally they were just sort

28
00:01:37,420 --> 00:01:43,990
of a company alright so previously your

29
00:01:41,049 --> 00:01:47,169
best friends were peeve you and cff

30
00:01:43,990 --> 00:01:50,919
explore your new best friends are reno

31
00:01:47,169 --> 00:01:56,349
ldd and object o but in reality read elf

32
00:01:50,919 --> 00:01:58,119
is going to be the major so read elf

33
00:01:56,349 --> 00:02:00,009
will be able to basically it's going to

34
00:01:58,119 --> 00:02:02,859
be a command line program which is going

35
00:02:00,009 --> 00:02:04,359
to slice apart the elf files it'll print

36
00:02:02,859 --> 00:02:07,389
out the first level headers which would

37
00:02:04,359 --> 00:02:09,280
be sort of analogous to the boss header

38
00:02:07,389 --> 00:02:11,260
or kind of a little bit of the daus

39
00:02:09,280 --> 00:02:14,740
header and a little bit of the optional

40
00:02:11,260 --> 00:02:15,180
header I don't wanna give that look up

41
00:02:14,740 --> 00:02:17,350
there

42
00:02:15,180 --> 00:02:18,670
Reno will slice apart the header

43
00:02:17,350 --> 00:02:23,740
it'll show you the various information

44
00:02:18,670 --> 00:02:26,350
about sections and basic headers and ldd

45
00:02:23,740 --> 00:02:27,160
can show you which libraries the excute

46
00:02:26,350 --> 00:02:28,840
will depends on

47
00:02:27,160 --> 00:02:30,730
so that means sort of like what is it

48
00:02:28,840 --> 00:02:33,370
import right what shared libraries does

49
00:02:30,730 --> 00:02:35,080
it important and I've stopped you

50
00:02:33,370 --> 00:02:37,390
haven't had to try x86 but I've still

51
00:02:35,080 --> 00:02:39,250
can be useful for disassembling the

52
00:02:37,390 --> 00:02:44,890
thing and you know just getting a flat

53
00:02:39,250 --> 00:02:46,330
view of the assembly so I kind of showed

54
00:02:44,890 --> 00:02:47,980
before at the very beginning if you

55
00:02:46,330 --> 00:02:49,600
remember way back at the beginning of

56
00:02:47,980 --> 00:02:52,570
the piece stuff I showed where there's

57
00:02:49,600 --> 00:02:54,010
like a single option that you would pull

58
00:02:52,570 --> 00:02:55,300
down box that you'd have in the visual

59
00:02:54,010 --> 00:02:57,460
studio where it says I would like to

60
00:02:55,300 --> 00:02:59,020
compile this source code to an exe I

61
00:02:57,460 --> 00:03:04,180
would like to compile it to a DLL I

62
00:02:59,020 --> 00:03:07,510
would all right so you're on Linux as

63
00:03:04,180 --> 00:03:09,910
we'll be using today you'll use GCC as

64
00:03:07,510 --> 00:03:12,820
your primary compiler and so if you just

65
00:03:09,910 --> 00:03:15,160
do a default GCC - oh you're out file

66
00:03:12,820 --> 00:03:16,600
and then whatever source files default

67
00:03:15,160 --> 00:03:18,640
you're going to get a dynamic linked

68
00:03:16,600 --> 00:03:20,050
executable so that means it's not going

69
00:03:18,640 --> 00:03:22,000
to put all the code in it's like a

70
00:03:20,050 --> 00:03:24,760
default Windows executable you're going

71
00:03:22,000 --> 00:03:25,480
to import from other libraries as we'll

72
00:03:24,760 --> 00:03:27,700
see you later

73
00:03:25,480 --> 00:03:29,230
the imports do not work like the windows

74
00:03:27,700 --> 00:03:30,760
imports they work like the delay load

75
00:03:29,230 --> 00:03:33,580
imports so whereas Linux

76
00:03:30,760 --> 00:03:35,380
whereas Windows resolves all imports

77
00:03:33,580 --> 00:03:37,780
that start up and then we'll call stuff

78
00:03:35,380 --> 00:03:39,970
as necessary Linux will use this delay

79
00:03:37,780 --> 00:03:42,670
loaded import basically exactly the same

80
00:03:39,970 --> 00:03:45,010
way as Windows uses till a loading point

81
00:03:42,670 --> 00:03:47,350
to a stub code and then the stuff code

82
00:03:45,010 --> 00:03:50,290
calls the equivalent of load library and

83
00:03:47,350 --> 00:03:52,120
get proc address so that's the kind of

84
00:03:50,290 --> 00:03:55,570
binary you'll get if you just do it all

85
00:03:52,120 --> 00:03:56,800
GCC compiler we do - static you're now

86
00:03:55,570 --> 00:03:58,300
going to get something where it's going

87
00:03:56,800 --> 00:03:59,740
to gather up all of the different

88
00:03:58,300 --> 00:04:01,990
libraries that you're using and it's

89
00:03:59,740 --> 00:04:03,430
going to plop all of that code into your

90
00:04:01,990 --> 00:04:05,740
binary and make sure that if you know

91
00:04:03,430 --> 00:04:08,740
your binary knows where to call all of

92
00:04:05,740 --> 00:04:11,380
the functions within yourself so this

93
00:04:08,740 --> 00:04:14,290
obviously leads to extremely large code

94
00:04:11,380 --> 00:04:18,370
and if you see some code size

95
00:04:14,290 --> 00:04:20,830
comparisons later then if you're

96
00:04:18,370 --> 00:04:23,440
compiling a shared library you're

97
00:04:20,830 --> 00:04:26,680
generally going to want to use the - F

98
00:04:23,440 --> 00:04:29,800
pick option so pick stands for position

99
00:04:26,680 --> 00:04:30,420
independent code and so if you don't

100
00:04:29,800 --> 00:04:33,840
have to

101
00:04:30,420 --> 00:04:35,610
use the pick option but if you use the

102
00:04:33,840 --> 00:04:38,130
pick option well it's it's trade off so

103
00:04:35,610 --> 00:04:40,050
I maybe should have showed both ways on

104
00:04:38,130 --> 00:04:41,910
the slide but there's trade off you can

105
00:04:40,050 --> 00:04:43,680
make a library that's going to be sort

106
00:04:41,910 --> 00:04:44,850
of like a normal DLL on Windows where

107
00:04:43,680 --> 00:04:48,090
you're gonna have relocation information

108
00:04:44,850 --> 00:04:49,950
and so it'll again compile code that

109
00:04:48,090 --> 00:04:51,870
will have assumptions about I'm going to

110
00:04:49,950 --> 00:04:53,340
load at this base address and I'm going

111
00:04:51,870 --> 00:04:56,160
to hard-code those constants into my

112
00:04:53,340 --> 00:04:58,440
assembly and into my headers if you use

113
00:04:56,160 --> 00:05:00,780
the dash F pick they won't compile any

114
00:04:58,440 --> 00:05:02,850
of those hard-coded constants into the

115
00:05:00,780 --> 00:05:04,950
assembly it'll generate code which

116
00:05:02,850 --> 00:05:07,740
dynamically looks up offsets to things

117
00:05:04,950 --> 00:05:10,350
so this code can take can run a little

118
00:05:07,740 --> 00:05:12,630
bit slower because it's always having to

119
00:05:10,350 --> 00:05:14,310
calculate stuff at runtime

120
00:05:12,630 --> 00:05:16,050
you'll always say like okay let me get

121
00:05:14,310 --> 00:05:17,430
the base address of this and put it into

122
00:05:16,050 --> 00:05:20,160
this register and now I know its base

123
00:05:17,430 --> 00:05:23,310
address plus something so it'll always

124
00:05:20,160 --> 00:05:28,890
be resolving its things as it runs along

125
00:05:23,310 --> 00:05:32,190
in the code so but anyways with GCC this

126
00:05:28,890 --> 00:05:34,650
is kind of a couple stage process first

127
00:05:32,190 --> 00:05:35,910
you're going to use the - C option which

128
00:05:34,650 --> 00:05:38,010
tells you compile it

129
00:05:35,910 --> 00:05:39,480
but don't link it so if you go to the

130
00:05:38,010 --> 00:05:41,370
very beginning or class where I said

131
00:05:39,480 --> 00:05:43,820
bunch of dot C files go into the

132
00:05:41,370 --> 00:05:47,460
compiler a bunch of old files come out

133
00:05:43,820 --> 00:05:50,880
here we're talking about you use the - C

134
00:05:47,460 --> 00:05:54,630
and you're going to have this particular

135
00:05:50,880 --> 00:05:56,730
C file turn into this particular - oh-oh

136
00:05:54,630 --> 00:05:58,860
file so Lib Fudo

137
00:05:56,730 --> 00:06:01,140
if you're selling it foo just by

138
00:05:58,860 --> 00:06:03,630
convention Linux will typically call

139
00:06:01,140 --> 00:06:04,250
your library live whatever Lib pcap loop

140
00:06:03,630 --> 00:06:06,900
needs

141
00:06:04,250 --> 00:06:09,840
all right so live whatever dot o gets

142
00:06:06,900 --> 00:06:12,120
output and then you use LD to link this

143
00:06:09,840 --> 00:06:13,860
this thing is a shared library and you

144
00:06:12,120 --> 00:06:17,400
give it the Esso name so saying short

145
00:06:13,860 --> 00:06:19,740
shared object name is live food s o dot

146
00:06:17,400 --> 00:06:21,090
one and this is going to be the output

147
00:06:19,740 --> 00:06:24,410
file so it's just kind of following

148
00:06:21,090 --> 00:06:26,430
particular conventions set up by Linux

149
00:06:24,410 --> 00:06:27,840
all right and so if you want to build a

150
00:06:26,430 --> 00:06:29,310
static library where you've got a bunch

151
00:06:27,840 --> 00:06:31,140
of stuff this is kind of the case that's

152
00:06:29,310 --> 00:06:33,390
more like the dot Lib file I was talking

153
00:06:31,140 --> 00:06:35,280
about before the static library is where

154
00:06:33,390 --> 00:06:37,260
it's not an actual elf file it's more

155
00:06:35,280 --> 00:06:38,670
like a collection of dot o files with a

156
00:06:37,260 --> 00:06:41,160
little bit of header information at the

157
00:06:38,670 --> 00:06:42,680
beginning saying you know I've got an

158
00:06:41,160 --> 00:06:45,199
archive this

159
00:06:42,680 --> 00:06:47,030
we use the AR command you're typically

160
00:06:45,199 --> 00:06:48,289
going to call it a dot a file and you're

161
00:06:47,030 --> 00:06:50,690
going to use the archive thing it's an

162
00:06:48,289 --> 00:06:52,669
archive of dot o files so that if

163
00:06:50,690 --> 00:06:54,289
someone wants to do static linking you

164
00:06:52,669 --> 00:06:56,090
can say here's all of the dot o files

165
00:06:54,289 --> 00:06:57,580
that you need to link against in order

166
00:06:56,090 --> 00:07:00,740
to put stuff together

167
00:06:57,580 --> 00:07:02,419
so again two-stage process first you

168
00:07:00,740 --> 00:07:05,360
compile all of your files to get a bunch

169
00:07:02,419 --> 00:07:09,289
of - o files using this - C and then you

170
00:07:05,360 --> 00:07:12,560
use archive to combine create and

171
00:07:09,289 --> 00:07:14,060
replace existing old files with this - a

172
00:07:12,560 --> 00:07:15,830
file so you're just taking a bunch of

173
00:07:14,060 --> 00:07:16,880
old files pretty much putting them into

174
00:07:15,830 --> 00:07:19,099
an array with a little header

175
00:07:16,880 --> 00:07:21,919
information on the front and then later

176
00:07:19,099 --> 00:07:24,789
on if you're wanting to if you're

177
00:07:21,919 --> 00:07:27,830
wanting to link against this you'll use

178
00:07:24,789 --> 00:07:30,410
GCC and you used a shell and that will

179
00:07:27,830 --> 00:07:32,180
include this the code from this and your

180
00:07:30,410 --> 00:07:38,210
thing even if you're not using the

181
00:07:32,180 --> 00:07:39,949
static linking alright so as with

182
00:07:38,210 --> 00:07:41,990
Windows where we had to get on the same

183
00:07:39,949 --> 00:07:44,630
page in terms of a D word equals four by

184
00:07:41,990 --> 00:07:46,760
its Q word equals eight bytes these are

185
00:07:44,630 --> 00:07:51,620
the Linux these are the elf sizes rather

186
00:07:46,760 --> 00:07:55,789
and so when elf says word it does mean

187
00:07:51,620 --> 00:08:00,620
it so whereas on Windows word meant

188
00:07:55,789 --> 00:08:03,710
16 bytes and a D word meant 32 on elf

189
00:08:00,620 --> 00:08:07,130
word means four bytes so what was a D

190
00:08:03,710 --> 00:08:09,979
word before is a word now what was the

191
00:08:07,130 --> 00:08:11,690
word before was a half now so

192
00:08:09,979 --> 00:08:14,180
unfortunately these are just the kind of

193
00:08:11,690 --> 00:08:15,830
things where hopefully you get to work

194
00:08:14,180 --> 00:08:17,180
in a situation where you're not having

195
00:08:15,830 --> 00:08:18,650
to flip back and forth but you know if

196
00:08:17,180 --> 00:08:21,759
you're having to port code from Windows

197
00:08:18,650 --> 00:08:24,409
to Linux you have to go back and forth

198
00:08:21,759 --> 00:08:27,199
but anyways we're talking about 64 bit

199
00:08:24,409 --> 00:08:28,310
and so addresses are always eight bytes

200
00:08:27,199 --> 00:08:31,280
large because you know we're dealing

201
00:08:28,310 --> 00:08:36,560
with 64-bit addresses and offsets or

202
00:08:31,280 --> 00:08:39,320
64-bit as well yep and so just as a

203
00:08:36,560 --> 00:08:41,419
notional thing for yourself if it's

204
00:08:39,320 --> 00:08:43,190
calling itself an underscore adder it's

205
00:08:41,419 --> 00:08:44,899
saying it's a virtual address kind of

206
00:08:43,190 --> 00:08:46,970
thing and if it's saying underscore

207
00:08:44,899 --> 00:08:50,300
offset that's more like a file offset as

208
00:08:46,970 --> 00:08:54,590
it says right over here program address

209
00:08:50,300 --> 00:08:57,630
address mean virtual address file offset

210
00:08:54,590 --> 00:08:59,550
all right so these are the two kind of

211
00:08:57,630 --> 00:09:01,950
views that we're going to be looking at

212
00:08:59,550 --> 00:09:04,350
an elf file from so there's a linking

213
00:09:01,950 --> 00:09:06,510
view and there is an execution view and

214
00:09:04,350 --> 00:09:09,870
so typically this linking view is what

215
00:09:06,510 --> 00:09:11,370
you'll have in dot o files and so the

216
00:09:09,870 --> 00:09:13,890
linker wants this kind of information

217
00:09:11,370 --> 00:09:16,110
this section information whereas an

218
00:09:13,890 --> 00:09:17,910
executable will typically have this

219
00:09:16,110 --> 00:09:21,540
information but the interesting thing is

220
00:09:17,910 --> 00:09:23,130
that okay so whereas when you're linking

221
00:09:21,540 --> 00:09:24,780
you don't necessarily need this program

222
00:09:23,130 --> 00:09:26,130
header at the beginning but you need

223
00:09:24,780 --> 00:09:27,900
these section editors we talked about

224
00:09:26,130 --> 00:09:29,610
each of these sections or about

225
00:09:27,900 --> 00:09:31,320
executable you have this program header

226
00:09:29,610 --> 00:09:34,320
that points at all these segments and

227
00:09:31,320 --> 00:09:36,030
this section adder is optional but what

228
00:09:34,320 --> 00:09:37,620
we'll see is that typically it's an

229
00:09:36,030 --> 00:09:39,030
executable we'll have built the program

230
00:09:37,620 --> 00:09:40,800
header and the section header so that

231
00:09:39,030 --> 00:09:42,600
you kind of have these two views merged

232
00:09:40,800 --> 00:09:44,550
together you've got information about

233
00:09:42,600 --> 00:09:46,530
segments you've got information about

234
00:09:44,550 --> 00:09:48,390
sections and I'll have to kind of

235
00:09:46,530 --> 00:09:49,980
clarify what a section is versus what a

236
00:09:48,390 --> 00:09:54,450
segment is versus what we were talking

237
00:09:49,980 --> 00:09:56,010
about in Windows all right oh I think I

238
00:09:54,450 --> 00:09:58,080
started to have some decent better

239
00:09:56,010 --> 00:09:59,580
sometimes known for this all right so

240
00:09:58,080 --> 00:10:01,260
back to this kind of linking view of

241
00:09:59,580 --> 00:10:03,960
thinking of a linker as a splicing

242
00:10:01,260 --> 00:10:07,320
engine splicing together all these

243
00:10:03,960 --> 00:10:10,020
different object files this object file

244
00:10:07,320 --> 00:10:12,240
means own file we've got some headers on

245
00:10:10,020 --> 00:10:13,470
here this would be actually I guess in

246
00:10:12,240 --> 00:10:15,570
this sense it should be at the bottom

247
00:10:13,470 --> 00:10:19,080
it's more the section header information

248
00:10:15,570 --> 00:10:21,240
and section headers talk about section

249
00:10:19,080 --> 00:10:22,560
headers talk about sections and sections

250
00:10:21,240 --> 00:10:25,920
here are still things that are named

251
00:10:22,560 --> 00:10:27,510
like text ro data and so forth and the

252
00:10:25,920 --> 00:10:29,580
linker is basically trying to take these

253
00:10:27,510 --> 00:10:31,230
and put them together so that the dot

254
00:10:29,580 --> 00:10:35,370
text up goes together and the dot ro

255
00:10:31,230 --> 00:10:36,600
stuff goes together so how we're

256
00:10:35,370 --> 00:10:38,040
basically going to think about this what

257
00:10:36,600 --> 00:10:40,170
we're going to see in there's these two

258
00:10:38,040 --> 00:10:41,910
views but when we look at our actual

259
00:10:40,170 --> 00:10:44,280
binaries that are compiled we're gonna

260
00:10:41,910 --> 00:10:46,050
be seeing these views together and the

261
00:10:44,280 --> 00:10:48,750
read alpha stuff will often show us like

262
00:10:46,050 --> 00:10:52,230
try to show us how sections map to

263
00:10:48,750 --> 00:10:53,850
segments and things like that so this is

264
00:10:52,230 --> 00:10:54,990
more concretely what we're going to see

265
00:10:53,850 --> 00:10:56,460
in our files when we break them apart

266
00:10:54,990 --> 00:10:58,440
there's going to be some elf headers at

267
00:10:56,460 --> 00:10:59,760
the very beginning and then within the

268
00:10:58,440 --> 00:11:01,620
elf headers there's going to be pointer

269
00:10:59,760 --> 00:11:03,290
to the program headers there's going to

270
00:11:01,620 --> 00:11:06,080
be a pointer to the section interest

271
00:11:03,290 --> 00:11:07,640
because it is much simpler in this sense

272
00:11:06,080 --> 00:11:09,410
that you've got the first-order headers

273
00:11:07,640 --> 00:11:11,090
and then there's two other kinds of

274
00:11:09,410 --> 00:11:13,460
headers beyond that and each of those

275
00:11:11,090 --> 00:11:16,550
headers then point at segments or add

276
00:11:13,460 --> 00:11:18,380
sections so once you get to the program

277
00:11:16,550 --> 00:11:23,030
headers it's basically going to say I

278
00:11:18,380 --> 00:11:24,890
have this many segments after my program

279
00:11:23,030 --> 00:11:26,540
headers and segments are what's going to

280
00:11:24,890 --> 00:11:30,170
be actually mapped into memory later on

281
00:11:26,540 --> 00:11:32,570
as we'll see whereas the section headers

282
00:11:30,170 --> 00:11:34,340
are basically saying here are all of the

283
00:11:32,570 --> 00:11:36,050
different types of information here are

284
00:11:34,340 --> 00:11:37,670
the different things like dot text which

285
00:11:36,050 --> 00:11:39,830
is trying to tell you that this is code

286
00:11:37,670 --> 00:11:43,970
and ro data that's trying to tell you

287
00:11:39,830 --> 00:11:45,740
this is this is read-only data as far as

288
00:11:43,970 --> 00:11:47,390
the OS loader is concerned it only cares

289
00:11:45,740 --> 00:11:49,100
about these segments segments either say

290
00:11:47,390 --> 00:11:51,740
like take this chunk of file and put it

291
00:11:49,100 --> 00:11:53,030
in memory and things like that whereas

292
00:11:51,740 --> 00:11:56,180
like I said the linker cares more about

293
00:11:53,030 --> 00:11:59,690
sections but also some other programs

294
00:11:56,180 --> 00:12:01,340
that may want to - may want to interact

295
00:11:59,690 --> 00:12:03,650
with the elf file may care about

296
00:12:01,340 --> 00:12:06,400
sections things debuggers will walk and

297
00:12:03,650 --> 00:12:06,400
care about sections


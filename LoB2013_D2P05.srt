1
00:00:03,600 --> 00:00:08,980
okay that I'm going to do a quick five

2
00:00:05,920 --> 00:00:10,540
minutes like when through all of the

3
00:00:08,980 --> 00:00:14,140
material we covered yesterday then we're

4
00:00:10,540 --> 00:00:15,550
going to do rounds 1 through 5 as the

5
00:00:14,140 --> 00:00:19,929
competition you can ask me questions

6
00:00:15,550 --> 00:00:21,820
during its fine and and yeah that'll be

7
00:00:19,929 --> 00:00:23,650
sort of our review and making sure that

8
00:00:21,820 --> 00:00:25,630
we're still remembering the stuff from

9
00:00:23,650 --> 00:00:27,880
yesterday actually when I forgot to use

10
00:00:25,630 --> 00:00:31,210
I wanted to go find a clip like uhf

11
00:00:27,880 --> 00:00:33,250
that's weird al movie where there was my

12
00:00:31,210 --> 00:00:34,960
goal of this or not like it bevels

13
00:00:33,250 --> 00:00:38,230
whatever the guy who's played Kramer and

14
00:00:34,960 --> 00:00:39,760
in Seinfeld there's a great little clip

15
00:00:38,230 --> 00:00:41,440
in there where they put to the screen

16
00:00:39,760 --> 00:00:44,260
and he says who wants to drink from the

17
00:00:41,440 --> 00:00:46,690
firehose a little bit opens up our hose

18
00:00:44,260 --> 00:00:50,969
that's the kid straight from the final

19
00:00:46,690 --> 00:00:50,969
you get to drink from the fire

20
00:00:56,160 --> 00:00:58,190
Oh

21
00:01:01,950 --> 00:01:08,470
so yes prepare to drink from a fire hose

22
00:01:06,400 --> 00:01:14,680
for all the stuff that we discovered

23
00:01:08,470 --> 00:01:24,070
yesterday all right go through weekly

24
00:01:14,680 --> 00:01:25,869
fast all right good all right so we

25
00:01:24,070 --> 00:01:27,340
started we talked about the DA cider the

26
00:01:25,869 --> 00:01:29,410
only things we care about in the DA

27
00:01:27,340 --> 00:01:31,840
sadder is the magic which is always MZ

28
00:01:29,410 --> 00:01:33,220
and the ELP new which gets us to the

29
00:01:31,840 --> 00:01:36,399
next data structure that we care about

30
00:01:33,220 --> 00:01:39,190
which is the antenna all right that's

31
00:01:36,399 --> 00:01:40,780
the NP header and that doesn't

32
00:01:39,190 --> 00:01:42,100
necessarily have to be immediately after

33
00:01:40,780 --> 00:01:43,360
the DA cider and most of the time it

34
00:01:42,100 --> 00:01:45,130
isn't because we said there's that

35
00:01:43,360 --> 00:01:46,930
little program at das program right

36
00:01:45,130 --> 00:01:51,280
after the das header that's prints out

37
00:01:46,930 --> 00:01:52,960
this is not about program right so last

38
00:01:51,280 --> 00:01:54,789
field of the DA center gets you to the

39
00:01:52,960 --> 00:01:56,440
start of the NT header the NT enter it's

40
00:01:54,789 --> 00:01:58,660
two structures embedded in it not

41
00:01:56,440 --> 00:02:01,420
pointed to but embedded in it it's got

42
00:01:58,660 --> 00:02:04,660
the signature which is just PE right

43
00:02:01,420 --> 00:02:07,929
right their signature is just this which

44
00:02:04,660 --> 00:02:10,450
is PE backwards little-endian some zero

45
00:02:07,929 --> 00:02:12,910
padding and then embedded in it it has

46
00:02:10,450 --> 00:02:14,380
the file header and we had the machine

47
00:02:12,910 --> 00:02:16,990
that tells us the sort of cpu

48
00:02:14,380 --> 00:02:21,280
architecture like just x86 code x86 64

49
00:02:16,990 --> 00:02:22,959
arm risk whatever we had the time/date

50
00:02:21,280 --> 00:02:24,670
stamp that tells us basically the

51
00:02:22,959 --> 00:02:28,000
compiled pod it's really the link time

52
00:02:24,670 --> 00:02:29,620
but that tells us when the person was

53
00:02:28,000 --> 00:02:32,349
sitting a visual studio compiling the

54
00:02:29,620 --> 00:02:34,300
thing we have the number of sections

55
00:02:32,349 --> 00:02:36,730
that tells us because there's this array

56
00:02:34,300 --> 00:02:38,920
of section headers immediately after the

57
00:02:36,730 --> 00:02:41,260
optional header need to know how many of

58
00:02:38,920 --> 00:02:42,220
those data structures there are all

59
00:02:41,260 --> 00:02:45,190
right so we've got the number of

60
00:02:42,220 --> 00:02:46,900
sections you've got the size of option

61
00:02:45,190 --> 00:02:48,670
letter technically that can change later

62
00:02:46,900 --> 00:02:52,330
on we'll probably have some even more

63
00:02:48,670 --> 00:02:54,790
interesting malformed malware or just

64
00:02:52,330 --> 00:02:56,830
malformed p files that will be sort of

65
00:02:54,790 --> 00:02:59,260
like a harder level pack that's added on

66
00:02:56,830 --> 00:03:01,480
at the end of the game but for now don't

67
00:02:59,260 --> 00:03:02,860
have that but in that case someone would

68
00:03:01,480 --> 00:03:04,390
potentially manipulate the size of

69
00:03:02,860 --> 00:03:05,620
optional header to basically if you

70
00:03:04,390 --> 00:03:07,750
shrink the size of optional header

71
00:03:05,620 --> 00:03:09,670
you're like cutting things off the end

72
00:03:07,750 --> 00:03:11,230
of the data directory right so you still

73
00:03:09,670 --> 00:03:13,630
will have to import maybe because that's

74
00:03:11,230 --> 00:03:16,260
index one maybe you won't have the delay

75
00:03:13,630 --> 00:03:19,000
little entries or something like that

76
00:03:16,260 --> 00:03:22,150
all right and characteristics and so

77
00:03:19,000 --> 00:03:25,300
what did we have four characteristics we

78
00:03:22,150 --> 00:03:27,250
had things like like I said if you

79
00:03:25,300 --> 00:03:28,750
remember only one thing about the op the

80
00:03:27,250 --> 00:03:30,760
file header characteristics I want you

81
00:03:28,750 --> 00:03:32,950
to remember that it's as the flag saying

82
00:03:30,760 --> 00:03:35,170
this is the DOL as other things like

83
00:03:32,950 --> 00:03:37,780
executable that's on exe zor DLLs as

84
00:03:35,170 --> 00:03:40,270
other things like whether or not it

85
00:03:37,780 --> 00:03:45,780
supports large memory space and whether

86
00:03:40,270 --> 00:03:49,920
it's a 32-bit machine for the jungle

87
00:03:45,780 --> 00:03:53,140
don't be that guy ask me questions all

88
00:03:49,920 --> 00:03:55,570
right optional header what do we care

89
00:03:53,140 --> 00:03:57,340
about here magic this tells us whether

90
00:03:55,570 --> 00:04:00,810
this is a 32-bit optional header or

91
00:03:57,340 --> 00:04:03,520
64-bit optional header there's 32 or 64

92
00:04:00,810 --> 00:04:06,400
all right address of entry point that's

93
00:04:03,520 --> 00:04:07,480
where the code starts image base that's

94
00:04:06,400 --> 00:04:08,860
where this thing would like to be

95
00:04:07,480 --> 00:04:10,600
located in memory that's where it

96
00:04:08,860 --> 00:04:12,790
assumes it's located in memory as far as

97
00:04:10,600 --> 00:04:15,870
all those constants in the delay loading

98
00:04:12,790 --> 00:04:18,010
ports and constants in the assembly go

99
00:04:15,870 --> 00:04:20,350
section alignment this says we've got

100
00:04:18,010 --> 00:04:21,850
those sections and if they're going to

101
00:04:20,350 --> 00:04:23,050
be mapped into memory it's saying you

102
00:04:21,850 --> 00:04:25,000
would like them to be mapped at

103
00:04:23,050 --> 00:04:28,210
multiples of whatever this is it's

104
00:04:25,000 --> 00:04:30,880
typically 1000 x 1000 all I'm at same

105
00:04:28,210 --> 00:04:32,530
thing we've got these sections on file

106
00:04:30,880 --> 00:04:34,420
and we'd like them to be mapped we'd

107
00:04:32,530 --> 00:04:38,350
like them to get offsets in the file of

108
00:04:34,420 --> 00:04:40,270
multiples up for instance 200 size of

109
00:04:38,350 --> 00:04:42,070
image this is the total size that this

110
00:04:40,270 --> 00:04:44,710
member this binary takes when it's

111
00:04:42,070 --> 00:04:46,210
loaded into memory and because there can

112
00:04:44,710 --> 00:04:47,830
be sections you know map that some

113
00:04:46,210 --> 00:04:49,750
offset and because there can be gaps

114
00:04:47,830 --> 00:04:52,510
between sections you should really just

115
00:04:49,750 --> 00:04:54,910
think of that as over the last section

116
00:04:52,510 --> 00:04:56,740
whatever offset it it says whatever RBA

117
00:04:54,910 --> 00:05:00,100
of the last section plus the size of the

118
00:04:56,740 --> 00:05:03,070
section that's the total size that it's

119
00:05:00,100 --> 00:05:04,960
going to have the allow characteristics

120
00:05:03,070 --> 00:05:07,810
this has the things like the weather

121
00:05:04,960 --> 00:05:10,720
it's a SLR using the dynamic page flag

122
00:05:07,810 --> 00:05:13,420
whether it supports depth or NX not

123
00:05:10,720 --> 00:05:15,730
executable data whether it has a

124
00:05:13,420 --> 00:05:18,100
structured exception entering etc and

125
00:05:15,730 --> 00:05:21,100
then data directory which recovered all

126
00:05:18,100 --> 00:05:22,870
sorts of ways these are some of the

127
00:05:21,100 --> 00:05:24,760
characteristics we said the missing

128
00:05:22,870 --> 00:05:27,300
characteristic was that that i quiz you

129
00:05:24,760 --> 00:05:30,460
on is that

130
00:05:27,300 --> 00:05:32,080
terminal server aware all right so

131
00:05:30,460 --> 00:05:34,870
terminal server where means that it

132
00:05:32,080 --> 00:05:36,220
works with rdp well so you typically not

133
00:05:34,870 --> 00:05:38,699
see this on things that are like

134
00:05:36,220 --> 00:05:41,380
command-line tools and stuff like that

135
00:05:38,699 --> 00:05:45,280
can't remember whether command a txt has

136
00:05:41,380 --> 00:05:48,810
that or not you can go check alright so

137
00:05:45,280 --> 00:05:51,910
data directory is just this big array of

138
00:05:48,810 --> 00:05:55,650
it's technically 16 things I think only

139
00:05:51,910 --> 00:05:57,970
15 of them are actually filmed in and

140
00:05:55,650 --> 00:05:59,560
you know data directory is big because

141
00:05:57,970 --> 00:06:01,750
all these other topics that were

142
00:05:59,560 --> 00:06:04,030
covering imports will talk about exports

143
00:06:01,750 --> 00:06:06,220
we saw delayed in for its founding ports

144
00:06:04,030 --> 00:06:07,990
those are all reference from the data

145
00:06:06,220 --> 00:06:10,570
directory right so it's just an array of

146
00:06:07,990 --> 00:06:12,580
these structures at the end of the

147
00:06:10,570 --> 00:06:16,030
optional heterostructure has two fields

148
00:06:12,580 --> 00:06:18,280
each entry that's that important thing

149
00:06:16,030 --> 00:06:20,260
work I'm asking you some questions about

150
00:06:18,280 --> 00:06:21,639
this or that index in the data directory

151
00:06:20,260 --> 00:06:27,669
this is where you figure out what

152
00:06:21,639 --> 00:06:29,110
connects it is quiz show sections you

153
00:06:27,669 --> 00:06:31,270
said you know there's some particular

154
00:06:29,110 --> 00:06:33,099
names one that I forgot to tell you

155
00:06:31,270 --> 00:06:34,389
yesterday that I'll reiterate again that

156
00:06:33,099 --> 00:06:37,180
you should have written in your notes is

157
00:06:34,389 --> 00:06:40,090
that dot P data is 64-bit exception

158
00:06:37,180 --> 00:06:41,620
handling by convention all right so

159
00:06:40,090 --> 00:06:42,970
we've got an array of these section

160
00:06:41,620 --> 00:06:46,090
headers that immediately after the

161
00:06:42,970 --> 00:06:47,409
optional header and in the section

162
00:06:46,090 --> 00:06:49,510
headers the things we care about are the

163
00:06:47,409 --> 00:06:51,430
name it's just eight bytes it can be

164
00:06:49,510 --> 00:06:55,889
whatever as we saw yesterday it can be

165
00:06:51,430 --> 00:06:58,300
you know left arrow zeno whatever

166
00:06:55,889 --> 00:07:00,340
virtual sighs we're always going to

167
00:06:58,300 --> 00:07:02,860
think of this as miss virtual sighs will

168
00:07:00,340 --> 00:07:04,570
never think of it as physical address so

169
00:07:02,860 --> 00:07:05,830
you've got virtual address that's saying

170
00:07:04,570 --> 00:07:08,320
where the section is mount the memory

171
00:07:05,830 --> 00:07:10,960
virtual size is the total size it takes

172
00:07:08,320 --> 00:07:12,639
a memory you've got pointer to raw data

173
00:07:10,960 --> 00:07:14,919
that's where the thing starts in file

174
00:07:12,639 --> 00:07:16,990
and you've got size of raw data that's

175
00:07:14,919 --> 00:07:19,300
the total size that it takes in five we

176
00:07:16,990 --> 00:07:21,159
said you know size of raw data can be

177
00:07:19,300 --> 00:07:23,080
bigger than virtual signs virtual size

178
00:07:21,159 --> 00:07:24,370
can be bigger than Sun rotated Mel just

179
00:07:23,080 --> 00:07:26,349
has to do with whether you're patting

180
00:07:24,370 --> 00:07:27,870
out the file or whether you're patting

181
00:07:26,349 --> 00:07:30,250
out memory because you need some

182
00:07:27,870 --> 00:07:32,380
uninitialized variable space basically

183
00:07:30,250 --> 00:07:34,030
global very good space and

184
00:07:32,380 --> 00:07:36,840
characteristics that's the stuff like

185
00:07:34,030 --> 00:07:38,030
read write execute page' ball or not

186
00:07:36,840 --> 00:07:40,010
this

187
00:07:38,030 --> 00:07:44,870
armel or not and whether it contains

188
00:07:40,010 --> 00:07:47,990
code whether it's shared all right

189
00:07:44,870 --> 00:07:50,330
sections we need whatever all right we

190
00:07:47,990 --> 00:07:52,040
went deep into imports but the important

191
00:07:50,330 --> 00:07:53,600
thing is that the main import

192
00:07:52,040 --> 00:07:55,220
information starts at the directory

193
00:07:53,600 --> 00:07:58,160
injure import that's the next one and

194
00:07:55,220 --> 00:08:00,320
that data directory entry points out an

195
00:07:58,160 --> 00:08:04,040
array which we call the import

196
00:08:00,320 --> 00:08:06,560
descriptor table or the import directory

197
00:08:04,040 --> 00:08:09,110
and this is an array of these data

198
00:08:06,560 --> 00:08:13,820
structures one per dll that you're

199
00:08:09,110 --> 00:08:16,250
importing from all right so I've had two

200
00:08:13,820 --> 00:08:18,260
things we cared about basically the

201
00:08:16,250 --> 00:08:22,070
original first club first entry in this

202
00:08:18,260 --> 00:08:24,560
data structure points at the int that's

203
00:08:22,070 --> 00:08:27,710
all right there I mt the last entry

204
00:08:24,560 --> 00:08:29,630
points at the iat so when you've got

205
00:08:27,710 --> 00:08:32,030
that one pretty ll structure just take

206
00:08:29,630 --> 00:08:35,270
the first field points at the int last

207
00:08:32,030 --> 00:08:38,540
field points at BFI AP and so when we're

208
00:08:35,270 --> 00:08:40,070
thinking about int and I 80 and stuff

209
00:08:38,540 --> 00:08:42,620
like that I prefer to just go straight

210
00:08:40,070 --> 00:08:45,020
to this sort of picture I empties and

211
00:08:42,620 --> 00:08:47,450
IITs on disk point at these other

212
00:08:45,020 --> 00:08:49,940
structures that are going to be a hint

213
00:08:47,450 --> 00:08:51,830
and again this hint is an index in the

214
00:08:49,940 --> 00:08:53,240
export table which we still haven't

215
00:08:51,830 --> 00:08:54,860
learned about but there's going to be an

216
00:08:53,240 --> 00:08:57,320
export table somewhere and the other

217
00:08:54,860 --> 00:08:59,840
binary you're importing from that will

218
00:08:57,320 --> 00:09:01,400
have will say like here's the phone

219
00:08:59,840 --> 00:09:03,770
here's the address of the function io

220
00:09:01,400 --> 00:09:05,690
delete symbolically and the hint is just

221
00:09:03,770 --> 00:09:08,210
trying to let the OS loader figure it

222
00:09:05,690 --> 00:09:11,150
out fast rate saying dear OS lower try

223
00:09:08,210 --> 00:09:13,070
skipping down to entry 14 be any export

224
00:09:11,150 --> 00:09:15,890
table of the other guy and see if that's

225
00:09:13,070 --> 00:09:16,940
I Oh delete symbolically if it's not so

226
00:09:15,890 --> 00:09:19,190
me it you're just going to have to

227
00:09:16,940 --> 00:09:22,180
search the strings until you find the

228
00:09:19,190 --> 00:09:24,860
appropriate one file delete symbolically

229
00:09:22,180 --> 00:09:28,130
basically IMT that first entry in the

230
00:09:24,860 --> 00:09:32,320
data directory the first field in the

231
00:09:28,130 --> 00:09:35,920
data directory entry sorry import

232
00:09:32,320 --> 00:09:40,010
descriptor table or import directory

233
00:09:35,920 --> 00:09:43,010
entry the first field points up the int

234
00:09:40,010 --> 00:09:45,290
the last field points at the I issue on

235
00:09:43,010 --> 00:09:47,840
this they both point it the same thing

236
00:09:45,290 --> 00:09:50,680
in memory it gets flipped around that

237
00:09:47,840 --> 00:09:52,600
the IAT will get filled in with the real

238
00:09:50,680 --> 00:09:57,520
at run times those will be the real

239
00:09:52,600 --> 00:10:03,070
values of those functions it's a

240
00:09:57,520 --> 00:10:04,750
madhouse all right there is a shortcut

241
00:10:03,070 --> 00:10:07,000
instead of having to go through that

242
00:10:04,750 --> 00:10:09,310
import directory table set up going up

243
00:10:07,000 --> 00:10:10,540
to here over here and down to here you

244
00:10:09,310 --> 00:10:12,490
know try to figure out where the very

245
00:10:10,540 --> 00:10:15,100
base of the import address table is you

246
00:10:12,490 --> 00:10:18,520
can just skip directly there by going to

247
00:10:15,100 --> 00:10:19,839
index 12 in the in the data directory

248
00:10:18,520 --> 00:10:24,700
that will get you right to the start of

249
00:10:19,839 --> 00:10:26,620
the int dr. what is a [ __ ] just now

250
00:10:24,700 --> 00:10:28,180
IT hooking is basically a

251
00:10:26,620 --> 00:10:31,120
man-in-the-middle attack work someone

252
00:10:28,180 --> 00:10:35,050
fills in the IIT with their own pointer

253
00:10:31,120 --> 00:10:36,670
instead of a legitimate pointer all

254
00:10:35,050 --> 00:10:38,350
right and then the last two things we

255
00:10:36,670 --> 00:10:44,110
talked about where to found imports and

256
00:10:38,350 --> 00:10:47,250
delayed load imports see I couldn't even

257
00:10:44,110 --> 00:10:47,250
get it through that in five minutes

258
00:10:52,270 --> 00:10:56,350
alright so first thing is that your

259
00:10:54,340 --> 00:10:58,720
normal imports when you have bound

260
00:10:56,350 --> 00:11:00,220
imports this time date stamp which was

261
00:10:58,720 --> 00:11:01,930
zero before it's going to be negative

262
00:11:00,220 --> 00:11:04,330
one that's kind of a tip-off that you've

263
00:11:01,930 --> 00:11:07,000
got found imports going on and all bound

264
00:11:04,330 --> 00:11:09,070
imports are is that on disk your pre

265
00:11:07,000 --> 00:11:11,770
filling in the address that you think

266
00:11:09,070 --> 00:11:14,760
would be filled in at runtime if it

267
00:11:11,770 --> 00:11:16,900
turns out those ASL are involved then

268
00:11:14,760 --> 00:11:18,340
it'll be inaccurate it'll have to be

269
00:11:16,900 --> 00:11:20,490
filled in again by the u.s. letter

270
00:11:18,340 --> 00:11:22,630
anyways but you're basically just saying

271
00:11:20,490 --> 00:11:25,620
I'm going to pre fill it in with what I

272
00:11:22,630 --> 00:11:28,450
think it's going to be we found we find

273
00:11:25,620 --> 00:11:29,710
there is a separate data structure

274
00:11:28,450 --> 00:11:32,110
that's pointed to you by this data

275
00:11:29,710 --> 00:11:34,450
directory entry the entry boundary

276
00:11:32,110 --> 00:11:36,340
import and this is going to point out an

277
00:11:34,450 --> 00:11:38,530
array of these things right image bounty

278
00:11:36,340 --> 00:11:41,170
for descriptors and so we showed that

279
00:11:38,530 --> 00:11:43,240
like this you've got this array these

280
00:11:41,170 --> 00:11:45,310
things pointed to by the data directory

281
00:11:43,240 --> 00:11:46,960
and you have one of them per deal out

282
00:11:45,310 --> 00:11:49,030
it's basically just trying to say this

283
00:11:46,960 --> 00:11:51,400
is the version of the dll that I

284
00:11:49,030 --> 00:11:54,130
prefilled stuff anyway and the version

285
00:11:51,400 --> 00:11:55,930
is given by this time date step right so

286
00:11:54,130 --> 00:11:58,300
the time/date stamp is pulled out of the

287
00:11:55,930 --> 00:12:00,880
other file and it says comedy LG and

288
00:11:58,300 --> 00:12:02,890
this time date stamp if you're trying to

289
00:12:00,880 --> 00:12:05,080
load this up right now and you don't see

290
00:12:02,890 --> 00:12:06,760
that hon date stamp and come dlg that

291
00:12:05,080 --> 00:12:07,870
means the pre-filled end stuff is

292
00:12:06,760 --> 00:12:10,300
probably going to be wrong there's been

293
00:12:07,870 --> 00:12:11,620
a patch to that DLL that if the function

294
00:12:10,300 --> 00:12:13,210
is going to moved around so the

295
00:12:11,620 --> 00:12:15,340
pre-filled in stuff should not do some

296
00:12:13,210 --> 00:12:19,090
cute wreck just do the normal thing that

297
00:12:15,340 --> 00:12:20,800
you do to resolve imports all right and

298
00:12:19,090 --> 00:12:22,450
then within this because I asked

299
00:12:20,800 --> 00:12:24,430
questions about how many of these

300
00:12:22,450 --> 00:12:26,680
structures are there when you have that

301
00:12:24,430 --> 00:12:30,100
sort of question you just add up all the

302
00:12:26,680 --> 00:12:31,780
entries you get out of all the entries

303
00:12:30,100 --> 00:12:33,400
accept the null in true and subtract out

304
00:12:31,780 --> 00:12:35,890
however many things there are that have

305
00:12:33,400 --> 00:12:37,480
nonzero for the number of orbiters or if

306
00:12:35,890 --> 00:12:39,610
I ask you how many forwarders there are

307
00:12:37,480 --> 00:12:41,980
you just sum up all these nonzero

308
00:12:39,610 --> 00:12:44,290
entries because this is basically a mix

309
00:12:41,980 --> 00:12:46,150
of to data structures right now these

310
00:12:44,290 --> 00:12:48,820
things that have zeros in them their

311
00:12:46,150 --> 00:12:50,680
image bound import descriptors and the

312
00:12:48,820 --> 00:12:53,020
things immediately after something that

313
00:12:50,680 --> 00:12:55,390
has a nonzero entry if this says one

314
00:12:53,020 --> 00:12:58,510
that means there's going to be one of

315
00:12:55,390 --> 00:13:00,100
these forward or rock structures next it

316
00:12:58,510 --> 00:13:00,800
said to there would be too forward or

317
00:13:00,100 --> 00:13:02,779
structures from

318
00:13:00,800 --> 00:13:07,070
and then you go back to just regular

319
00:13:02,779 --> 00:13:08,899
bound import descriptive all right again

320
00:13:07,070 --> 00:13:11,630
see if up explore this is kind of what

321
00:13:08,899 --> 00:13:13,459
it looks like you have apps for the time

322
00:13:11,630 --> 00:13:15,500
made stamp if you're dealing with bound

323
00:13:13,459 --> 00:13:17,089
imports you can see that these things

324
00:13:15,500 --> 00:13:22,930
are kind of filled in with absolute

325
00:13:17,089 --> 00:13:25,490
virtual addresses all right aslr and

326
00:13:22,930 --> 00:13:29,810
binding are kind of a condo at odds with

327
00:13:25,490 --> 00:13:31,670
each other because if your pre filling

328
00:13:29,810 --> 00:13:33,560
it in based on the assumption that a

329
00:13:31,670 --> 00:13:36,470
given module gets loaded up the image

330
00:13:33,560 --> 00:13:38,600
base in its headers right and with a SLR

331
00:13:36,470 --> 00:13:40,940
it's most likely not going to be loaded

332
00:13:38,600 --> 00:13:43,370
at that interface so things will most

333
00:13:40,940 --> 00:13:45,589
likely get a new drug and delay load

334
00:13:43,370 --> 00:13:48,529
imports again data directory entry

335
00:13:45,589 --> 00:13:51,529
points at an array of these structures

336
00:13:48,529 --> 00:13:54,350
one per dll these delay import

337
00:13:51,529 --> 00:13:56,390
descriptors or more correctly imaging

338
00:13:54,350 --> 00:13:59,420
importas image delay descriptors and

339
00:13:56,390 --> 00:14:02,029
this has again fleet things my name and

340
00:13:59,420 --> 00:14:08,060
just a pointer to the IIT pointer to the

341
00:14:02,029 --> 00:14:10,850
int and so they'll a load as we just

342
00:14:08,060 --> 00:14:13,279
talked about is all about filling in the

343
00:14:10,850 --> 00:14:17,290
stub code with the absolute coast at one

344
00:14:13,279 --> 00:14:21,520
time right when the function gets called

345
00:14:17,290 --> 00:14:21,520
all right that is it


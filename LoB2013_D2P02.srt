1
00:00:03,580 --> 00:00:08,260
alright one thing that I messed up

2
00:00:05,470 --> 00:00:10,680
yesterday was we were talking about

3
00:00:08,260 --> 00:00:13,660
delay load imports right and we said

4
00:00:10,680 --> 00:00:16,240
delay load imports get filled in on

5
00:00:13,660 --> 00:00:26,079
demand we were basically dealing with

6
00:00:16,240 --> 00:00:28,509
this situation right here right we were

7
00:00:26,079 --> 00:00:31,239
talking about how the label of imports

8
00:00:28,509 --> 00:00:35,410
they start out with the address of a sub

9
00:00:31,239 --> 00:00:37,629
code in the in the delay loading port

10
00:00:35,410 --> 00:00:40,300
address table and it eventually gets

11
00:00:37,629 --> 00:00:43,329
filled in with the address of the real

12
00:00:40,300 --> 00:00:44,980
code alright so yesterday I showed this

13
00:00:43,329 --> 00:00:47,260
for mspaint but it's like man those

14
00:00:44,980 --> 00:00:49,510
values don't make sense I wasn't I

15
00:00:47,260 --> 00:00:52,030
wasn't taking you into crime address

16
00:00:49,510 --> 00:00:54,579
space layout randomization realizing

17
00:00:52,030 --> 00:00:56,620
that these values that start out right

18
00:00:54,579 --> 00:00:58,719
here they're absolute virtual addresses

19
00:00:56,620 --> 00:01:01,179
and therefore if this thing gets moved

20
00:00:58,719 --> 00:01:03,550
around in memory before anything even

21
00:01:01,179 --> 00:01:06,550
starts those will get fixed up if the

22
00:01:03,550 --> 00:01:08,170
Salus are they got moves around so we

23
00:01:06,550 --> 00:01:22,360
talked about relocations that'll make

24
00:01:08,170 --> 00:01:25,990
more sense but I was just making sure we

25
00:01:22,360 --> 00:01:30,000
remember this paint again this is gonna

26
00:01:25,990 --> 00:01:30,000
be the 32-bit version of mspaint

27
00:01:41,780 --> 00:01:45,410
you know you'd went to install do I try

28
00:01:44,570 --> 00:01:49,790
to set up again

29
00:01:45,410 --> 00:01:53,180
shouldn't it all right so here's the

30
00:01:49,790 --> 00:01:56,180
first thing to keep in mind so I miss

31
00:01:53,180 --> 00:01:57,800
paint on disk if we go up to it's

32
00:01:56,180 --> 00:02:02,000
optional header and we look at its image

33
00:01:57,800 --> 00:02:05,260
base I miss paint on disk has whatever

34
00:02:02,000 --> 00:02:08,240
this is 1 million for its image base

35
00:02:05,260 --> 00:02:09,709
right and so inside the delay load

36
00:02:08,240 --> 00:02:12,020
import address table

37
00:02:09,709 --> 00:02:22,459
it's got hard-coded values that are like

38
00:02:12,020 --> 00:02:24,350
1 million 17 anyways it's got hard coded

39
00:02:22,459 --> 00:02:27,670
addresses which are obviously have the

40
00:02:24,350 --> 00:02:30,530
base address built into them so this is

41
00:02:27,670 --> 00:02:32,420
if we were to interpret this as an RBA

42
00:02:30,530 --> 00:02:34,070
although it does have absolute virtual

43
00:02:32,420 --> 00:02:36,080
addresses built in here if we were to

44
00:02:34,070 --> 00:02:39,440
interpret this as an RBA so we subtract

45
00:02:36,080 --> 00:02:42,080
out X you know 1 million from it we

46
00:02:39,440 --> 00:02:44,510
would say that this is at 17 ei not and

47
00:02:42,080 --> 00:02:46,820
so that matters because what if we don't

48
00:02:44,510 --> 00:02:48,739
get loaded at X 1 million what if we get

49
00:02:46,820 --> 00:02:52,670
loaded at X 2 million will get loaded at

50
00:02:48,739 --> 00:02:53,959
X E 5 0 0 0 and so that's exactly what

51
00:02:52,670 --> 00:02:56,989
we're seeing when we actually are

52
00:02:53,959 --> 00:02:59,000
looking in memory when we actually get

53
00:02:56,989 --> 00:03:04,160
loaded in memory mspaint gets loaded at

54
00:02:59,000 --> 00:03:08,720
base address e 8 0 0 0 instead of X 1

55
00:03:04,160 --> 00:03:10,340
million right and so I'll get your

56
00:03:08,720 --> 00:03:14,269
question and just stuck my kind of

57
00:03:10,340 --> 00:03:18,799
finish this example first so because it

58
00:03:14,269 --> 00:03:20,900
gets loaded at 3 8 0 0 0 0 the entries

59
00:03:18,799 --> 00:03:22,970
in the delay load import address table

60
00:03:20,900 --> 00:03:24,860
are going to be based at he age 0 0

61
00:03:22,970 --> 00:03:33,769
because the US actually ends up fixing

62
00:03:24,860 --> 00:03:36,049
them up so this is the delay load import

63
00:03:33,769 --> 00:03:41,390
address table and the entries are

64
00:03:36,049 --> 00:03:43,549
basically e 8 0 0 0 plus whatever the

65
00:03:41,390 --> 00:03:48,380
r-va would have been so the RBA would

66
00:03:43,549 --> 00:03:52,209
have been 1 7 EI 9 no we do the math for

67
00:03:48,380 --> 00:03:52,209
that one 7 EI 9

68
00:03:56,469 --> 00:04:05,140
this one 7e a nine-plus but the base

69
00:04:01,280 --> 00:04:11,000
address is e8 zero zero zero zero

70
00:04:05,140 --> 00:04:13,159
that equals e 9 7 e 8 9 and that's

71
00:04:11,000 --> 00:04:15,019
exactly what we see in the delay import

72
00:04:13,159 --> 00:04:16,579
address table so basically all I'm

73
00:04:15,019 --> 00:04:18,310
trying to justify here is that you've

74
00:04:16,579 --> 00:04:21,650
got absolute virtual addresses

75
00:04:18,310 --> 00:04:23,060
hard-coded into the binary on disk which

76
00:04:21,650 --> 00:04:26,210
assume that it's going to be based on

77
00:04:23,060 --> 00:04:29,120
hex 1 million at load time when it

78
00:04:26,210 --> 00:04:29,900
doesn't get hex 1 million against xe8 0

79
00:04:29,120 --> 00:04:32,240
0 0 0

80
00:04:29,900 --> 00:04:33,830
vos has to go through and fix up all of

81
00:04:32,240 --> 00:04:35,330
these constants just like I was talking

82
00:04:33,830 --> 00:04:37,430
about the OS fixing up constants that

83
00:04:35,330 --> 00:04:38,750
are hard-coded into assembly it has to

84
00:04:37,430 --> 00:04:41,449
fix up constants that are hard-coded

85
00:04:38,750 --> 00:04:43,580
into the headers and it just relocates

86
00:04:41,449 --> 00:04:46,699
them it sighs you wanted X 1 million you

87
00:04:43,580 --> 00:04:48,169
got EI 0 0 0 you know you know take

88
00:04:46,699 --> 00:04:50,300
whatever the difference is between those

89
00:04:48,169 --> 00:04:54,259
two and added to this constant so that

90
00:04:50,300 --> 00:04:55,820
everything still works out right so for

91
00:04:54,259 --> 00:04:58,099
instance I could go to this address that

92
00:04:55,820 --> 00:04:59,419
I could see that this is assembly for

93
00:04:58,099 --> 00:05:03,289
some code and like I showed yesterday

94
00:04:59,419 --> 00:05:06,050
it's not as pretty as back in oh okay I

95
00:05:03,289 --> 00:05:07,820
must've did it wrong yesterday then so

96
00:05:06,050 --> 00:05:09,380
this is the stub code at this address

97
00:05:07,820 --> 00:05:11,690
right it's just like we kind of showing

98
00:05:09,380 --> 00:05:14,210
the slides yesterday it's a move some

99
00:05:11,690 --> 00:05:15,889
constant to EAX and then jump and all

100
00:05:14,210 --> 00:05:21,099
these other stuff codes are just moved

101
00:05:15,889 --> 00:05:23,479
some constant EAX and then jump right so

102
00:05:21,099 --> 00:05:25,340
anyways I haven't actually run the code

103
00:05:23,479 --> 00:05:26,960
yet so this is the delay load here at

104
00:05:25,340 --> 00:05:29,570
address stable before anything is

105
00:05:26,960 --> 00:05:31,090
actually run let's go ahead and run

106
00:05:29,570 --> 00:05:33,680
mspaint

107
00:05:31,090 --> 00:05:35,930
all right so now mspaint is actually

108
00:05:33,680 --> 00:05:37,669
running I'm going to stop it and you can

109
00:05:35,930 --> 00:05:40,370
see that some stuff already got filled

110
00:05:37,669 --> 00:05:42,050
in and some stuff has not been filled in

111
00:05:40,370 --> 00:05:48,349
right some stuff still has stuff in the

112
00:05:42,050 --> 00:05:51,020
range of he 8 so the key thing is just

113
00:05:48,349 --> 00:05:53,599
to show you know that I can see even

114
00:05:51,020 --> 00:05:56,509
more things actually get loaded into on

115
00:05:53,599 --> 00:05:59,360
demand and in response to my acting with

116
00:05:56,509 --> 00:06:03,620
the GUI I'm just going to dump some of

117
00:05:59,360 --> 00:06:06,950
this memory so this is dumping D words

118
00:06:03,620 --> 00:06:07,990
but that address that Laila implored us

119
00:06:06,950 --> 00:06:11,780
table

120
00:06:07,990 --> 00:06:13,160
don't X 24th of bytes so I want you to

121
00:06:11,780 --> 00:06:16,220
basically watch this thing at this

122
00:06:13,160 --> 00:06:18,230
offset 16 this is gonna be something

123
00:06:16,220 --> 00:06:20,630
that when I go ahead and I switch over

124
00:06:18,230 --> 00:06:22,790
to right now it's not filled in so it's

125
00:06:20,630 --> 00:06:25,190
a delay load import that hasn't been

126
00:06:22,790 --> 00:06:26,720
called the program has never called this

127
00:06:25,190 --> 00:06:29,030
function so it still points that stub

128
00:06:26,720 --> 00:06:30,440
code but once I go ahead and move around

129
00:06:29,030 --> 00:06:33,170
in the thing it'll call whatever

130
00:06:30,440 --> 00:06:37,550
graphics repeated needed and it'll get

131
00:06:33,170 --> 00:06:38,750
filled in so I'm gonna let that go you

132
00:06:37,550 --> 00:06:44,410
go over to mspaint

133
00:06:38,750 --> 00:06:47,870
this is right one and then break back in

134
00:06:44,410 --> 00:06:50,510
you have the same command and there we

135
00:06:47,870 --> 00:06:55,490
go the one at sixty whereas it used to

136
00:06:50,510 --> 00:06:58,250
be EAD seven one three no it's 7-6 7-4

137
00:06:55,490 --> 00:07:00,080
cetera right so this is just showing on

138
00:06:58,250 --> 00:07:02,660
demand you took some action it caused

139
00:07:00,080 --> 00:07:05,240
the function to get called it fills in

140
00:07:02,660 --> 00:07:07,670
the sub code pointer with the real code

141
00:07:05,240 --> 00:07:09,700
point so just if we want to see what

142
00:07:07,670 --> 00:07:14,510
actual function that was that was called

143
00:07:09,700 --> 00:07:18,470
enlist in nearby symbols and it looks

144
00:07:14,510 --> 00:07:21,200
like it called GDI plus DLL and it

145
00:07:18,470 --> 00:07:23,180
called the GDI pset smoothing mode

146
00:07:21,200 --> 00:07:24,710
function so I didn't need to call that

147
00:07:23,180 --> 00:07:28,190
until I actually interacted with the

148
00:07:24,710 --> 00:07:32,480
program so never done any programming

149
00:07:28,190 --> 00:07:34,910
guess windows but area saying there's

150
00:07:32,480 --> 00:07:37,550
these stuff methods that are generated

151
00:07:34,910 --> 00:07:39,770
but he's done by the music supposedly

152
00:07:37,550 --> 00:07:41,870
created by I think they're done by the

153
00:07:39,770 --> 00:07:43,130
linker actually but notionally they're

154
00:07:41,870 --> 00:07:45,080
done by the compiler it's not the

155
00:07:43,130 --> 00:07:47,420
programmer it's just when you select

156
00:07:45,080 --> 00:08:01,640
delay load this DLL yeah I can father a

157
00:07:47,420 --> 00:08:03,410
linker what's code in there for this so

158
00:08:01,640 --> 00:08:06,530
I know you can't find them it's the

159
00:08:03,410 --> 00:08:08,480
right basically right the question is

160
00:08:06,530 --> 00:08:11,990
how do you find the delayed import

161
00:08:08,480 --> 00:08:15,889
descriptor entries right and in over in

162
00:08:11,990 --> 00:08:17,750
something like give you

163
00:08:15,889 --> 00:08:19,639
right they're all nicely lined up right

164
00:08:17,750 --> 00:08:22,159
here but how you really find them is

165
00:08:19,639 --> 00:08:25,370
that you go back to the data directory

166
00:08:22,159 --> 00:08:27,259
right the data directory I will bet you

167
00:08:25,370 --> 00:08:30,710
dollars to doughnuts that the data

168
00:08:27,259 --> 00:08:33,979
directory says eight five CCC is where

169
00:08:30,710 --> 00:08:39,440
the pointer to this table is right so on

170
00:08:33,979 --> 00:08:41,240
back here data directory eight five CCC

171
00:08:39,440 --> 00:08:43,669
all right so how do you find it in the c

172
00:08:41,240 --> 00:08:45,410
FF Explorer well the only way you can do

173
00:08:43,669 --> 00:08:47,959
it is you can you know go to the data

174
00:08:45,410 --> 00:08:49,910
directory look for the delay import

175
00:08:47,959 --> 00:08:55,130
whatever is in there you don't treat

176
00:08:49,910 --> 00:08:57,350
that as a that r-va can be a just a-- do

177
00:08:55,130 --> 00:08:58,790
this process of converting RBA's to file

178
00:08:57,350 --> 00:09:00,529
offsets based on where it is in the

179
00:08:58,790 --> 00:09:02,500
section and if it's not in the section

180
00:09:00,529 --> 00:09:05,800
we'll just treat it like the basis there

181
00:09:02,500 --> 00:09:05,800
but yeah


1
00:00:04,230 --> 00:00:15,870
all right so upx is available from

2
00:00:07,109 --> 00:00:18,050
SourceForge it's actually you know it's

3
00:00:15,870 --> 00:00:21,210
the hello world of packers basically

4
00:00:18,050 --> 00:00:23,160
it's very easy to understand because it

5
00:00:21,210 --> 00:00:26,040
isn't explicitly malicious it really is

6
00:00:23,160 --> 00:00:27,840
just a thing for compressing files and

7
00:00:26,040 --> 00:00:30,029
letting them decompress they have an

8
00:00:27,840 --> 00:00:32,219
autumn packing functionality so if you

9
00:00:30,029 --> 00:00:34,559
pack something with upx you just use upx

10
00:00:32,219 --> 00:00:36,960
dash d to decompress it and it'll do

11
00:00:34,559 --> 00:00:38,190
that nicely so it's very simple to

12
00:00:36,960 --> 00:00:40,320
understand because it's not trying to

13
00:00:38,190 --> 00:00:41,790
obfuscate it's not trying to use TLS to

14
00:00:40,320 --> 00:00:44,790
play any tricks on the analyst or

15
00:00:41,790 --> 00:00:47,460
anything like that and so the basic

16
00:00:44,790 --> 00:00:48,870
usage of it is you just say upx and the

17
00:00:47,460 --> 00:00:50,730
file you want to pack and the output

18
00:00:48,870 --> 00:00:52,980
file that you want to write it to and if

19
00:00:50,730 --> 00:00:56,910
you use - deep you can go ahead and

20
00:00:52,980 --> 00:00:59,370
unpack the package file so what I want

21
00:00:56,910 --> 00:01:01,440
to show now is the header changes that

22
00:00:59,370 --> 00:01:02,550
are made on elf specifically so we're

23
00:01:01,440 --> 00:01:09,320
going to start with out and we'll go

24
00:01:02,550 --> 00:01:09,320
back to will go back to PE files later

25
00:01:15,229 --> 00:01:20,039
alright so I said before I've got hello

26
00:01:17,999 --> 00:01:23,999
static it's a big giant hello world that

27
00:01:20,039 --> 00:01:28,439
takes up 725 K all right and so when I

28
00:01:23,999 --> 00:01:31,999
run hello static through upx EPX - oh

29
00:01:28,439 --> 00:01:31,999
hello static -

30
00:01:40,320 --> 00:01:48,180
I already exists let's pretend that it

31
00:01:45,750 --> 00:01:50,790
just created that fob oh hey I've got

32
00:01:48,180 --> 00:01:56,820
upx statics I'll just call it fun just

33
00:01:50,790 --> 00:01:57,870
to show you call it hello static vxq all

34
00:01:56,820 --> 00:02:01,440
right so it goes through its process

35
00:01:57,870 --> 00:02:05,100
compressing it down whatever algorithm I

36
00:02:01,440 --> 00:02:06,150
think it may use lzma these days see I

37
00:02:05,100 --> 00:02:13,500
think they have their own custom

38
00:02:06,150 --> 00:02:15,090
compression all right so kind of funny

39
00:02:13,500 --> 00:02:16,770
actually compressed two different sizes

40
00:02:15,090 --> 00:02:18,780
different times I may have used a

41
00:02:16,770 --> 00:02:20,310
different option last time but the point

42
00:02:18,780 --> 00:02:21,810
is whereas something took around you

43
00:02:20,310 --> 00:02:24,630
know four hundred seven hundred and

44
00:02:21,810 --> 00:02:29,430
forty K before after we use upx it takes

45
00:02:24,630 --> 00:02:31,500
around two hundred and eighty K 256 K so

46
00:02:29,430 --> 00:02:33,030
you've got significant savings you know

47
00:02:31,500 --> 00:02:34,770
it's less than half the size that it

48
00:02:33,030 --> 00:02:36,120
used to be so this is a good thing if

49
00:02:34,770 --> 00:02:41,730
you're in a space constrained

50
00:02:36,120 --> 00:02:43,680
environment well let's try to understand

51
00:02:41,730 --> 00:02:45,120
what the changes were that it made to

52
00:02:43,680 --> 00:02:47,550
the headers that have manipulated in

53
00:02:45,120 --> 00:02:50,940
order to you know have this compressed

54
00:02:47,550 --> 00:02:55,050
version so if we look at with read L

55
00:02:50,940 --> 00:02:56,430
dash L on the original hello static the

56
00:02:55,050 --> 00:02:59,700
things that we really care about are

57
00:02:56,430 --> 00:03:03,150
those load segments right the original

58
00:02:59,700 --> 00:03:07,080
thing had a file offset going from 0 to

59
00:03:03,150 --> 00:03:09,709
a 0 BD 9 that's going to be the first

60
00:03:07,080 --> 00:03:12,690
segment it's gonna map that segment to

61
00:03:09,709 --> 00:03:15,720
400,000 all right the second thing

62
00:03:12,690 --> 00:03:19,140
starts at a 0 F 0 and it's a much

63
00:03:15,720 --> 00:03:22,290
smaller segment it's only DB 0 big and

64
00:03:19,140 --> 00:03:24,060
it's gonna map that at 6 a 0 F 0 so kind

65
00:03:22,290 --> 00:03:25,890
of like we've been seeing we've got one

66
00:03:24,060 --> 00:03:28,380
segment around 400,000 one segment

67
00:03:25,890 --> 00:03:30,900
around 600,000 the first ones big second

68
00:03:28,380 --> 00:03:34,170
ones small this is more just a function

69
00:03:30,900 --> 00:03:36,019
of this particular hello world so the

70
00:03:34,170 --> 00:03:38,280
first segment is readable and executable

71
00:03:36,019 --> 00:03:40,410
second segment is readable and writeable

72
00:03:38,280 --> 00:03:41,940
so you expect that that first big thing

73
00:03:40,410 --> 00:03:44,040
has all the code and stuff in there

74
00:03:41,940 --> 00:03:45,989
because it's executable and the second

75
00:03:44,040 --> 00:03:48,090
thing is going to have you know data the

76
00:03:45,989 --> 00:03:50,099
data section that our o data

77
00:03:48,090 --> 00:03:53,790
and stuff like that it's readable and

78
00:03:50,099 --> 00:03:55,709
writeable but not executable alright so

79
00:03:53,790 --> 00:03:57,959
that's the original hello static big

80
00:03:55,709 --> 00:04:03,269
giant hello world now let's look at the

81
00:03:57,959 --> 00:04:05,069
hello static upx alright first of all we

82
00:04:03,269 --> 00:04:06,930
have no section information right so

83
00:04:05,069 --> 00:04:07,560
there's no mapping between segments and

84
00:04:06,930 --> 00:04:11,580
sections

85
00:04:07,560 --> 00:04:19,199
oh let's even look back at the header

86
00:04:11,580 --> 00:04:22,190
information commit right what is the you

87
00:04:19,199 --> 00:04:24,660
know number of section headers zero

88
00:04:22,190 --> 00:04:27,660
right what is there should be an offset

89
00:04:24,660 --> 00:04:29,880
section headers which 0 as well right

90
00:04:27,660 --> 00:04:31,410
startup section headers size of section

91
00:04:29,880 --> 00:04:32,130
headers there's no section editors on

92
00:04:31,410 --> 00:04:33,660
this binary

93
00:04:32,130 --> 00:04:35,220
there's no mapping there's no more

94
00:04:33,660 --> 00:04:37,410
granular view where you can figure out

95
00:04:35,220 --> 00:04:40,500
here's the top text section here's the

96
00:04:37,410 --> 00:04:42,479
you know dot dot dot PLT here's the

97
00:04:40,500 --> 00:04:44,639
symbol table there's nothing like that

98
00:04:42,479 --> 00:04:48,199
you just got two big blobs of file data

99
00:04:44,639 --> 00:04:51,449
that are going to get mapped into memory

100
00:04:48,199 --> 00:04:53,310
alright so here's our two big blobs how

101
00:04:51,449 --> 00:04:59,250
do they differ well the first one goes

102
00:04:53,310 --> 00:05:03,930
from 0 to 3 f-85 it's mapped to 40000

103
00:04:59,250 --> 00:05:09,180
again and the second one goes from a 4d

104
00:05:03,930 --> 00:05:12,810
a8 but it's got a file size of 0 and

105
00:05:09,180 --> 00:05:16,800
it's got a Memphis of 0 this is kind of

106
00:05:12,810 --> 00:05:18,990
weird it's got a segment that is going

107
00:05:16,800 --> 00:05:21,090
to start at some particular file offset

108
00:05:18,990 --> 00:05:24,450
it's not going to read anything in from

109
00:05:21,090 --> 00:05:26,849
file but it's going to map itself to a

110
00:05:24,450 --> 00:05:29,729
particular virtual address and so the

111
00:05:26,849 --> 00:05:33,330
reason behind this is to some degree I

112
00:05:29,729 --> 00:05:34,979
believe it's because if I'm remembering

113
00:05:33,330 --> 00:05:37,110
correctly from back when I was playing

114
00:05:34,979 --> 00:05:39,750
around with this sort of thing if you

115
00:05:37,110 --> 00:05:41,400
don't have two segments the program

116
00:05:39,750 --> 00:05:42,840
simply won't work I don't think you can

117
00:05:41,400 --> 00:05:45,810
actually get the program to run with a

118
00:05:42,840 --> 00:05:48,330
single segment so it's doing this

119
00:05:45,810 --> 00:05:51,210
partially because it needs two segments

120
00:05:48,330 --> 00:05:53,430
but partially it's doing this because

121
00:05:51,210 --> 00:05:55,169
even though it's asked for 0 file size

122
00:05:53,430 --> 00:05:58,320
and even though it's asked for 0 memory

123
00:05:55,169 --> 00:06:00,240
size it's still the OS loader because

124
00:05:58,320 --> 00:06:01,020
it's all load segment is going to

125
00:06:00,240 --> 00:06:04,500
actually

126
00:06:01,020 --> 00:06:06,509
map some is going to allocate some space

127
00:06:04,500 --> 00:06:07,800
at that particular location so that

128
00:06:06,509 --> 00:06:09,569
memory is going to be mapped there's not

129
00:06:07,800 --> 00:06:11,520
going to be any file data there it's not

130
00:06:09,569 --> 00:06:12,960
gonna be using any virtual data but like

131
00:06:11,520 --> 00:06:14,819
in terms of the back end memory

132
00:06:12,960 --> 00:06:16,830
management that's going to be some

133
00:06:14,819 --> 00:06:18,419
memory space that's allocated so you

134
00:06:16,830 --> 00:06:20,009
could touch that data you could read

135
00:06:18,419 --> 00:06:22,949
data you could write data right there

136
00:06:20,009 --> 00:06:24,870
and so it's kind of like it's allocating

137
00:06:22,949 --> 00:06:26,789
hex 1,000 worth of memory there in

138
00:06:24,870 --> 00:06:28,349
reality behind the scenes it's not

139
00:06:26,789 --> 00:06:31,259
taking anything from disk and putting it

140
00:06:28,349 --> 00:06:33,780
there but when the Packer starts running

141
00:06:31,259 --> 00:06:35,520
you'll actually see that it'll copy data

142
00:06:33,780 --> 00:06:36,690
into this region too even though there's

143
00:06:35,520 --> 00:06:39,240
nothing there because it knows

144
00:06:36,690 --> 00:06:41,699
implicitly behind the scenes the OS

145
00:06:39,240 --> 00:06:45,470
loader still made that memory valid at

146
00:06:41,699 --> 00:06:47,550
least X 1000 that memory is valid so

147
00:06:45,470 --> 00:06:49,500
it's basically gonna have one big

148
00:06:47,550 --> 00:06:51,000
segment where it has all of its stuff

149
00:06:49,500 --> 00:06:53,520
and then one segment that's ostensibly

150
00:06:51,000 --> 00:06:55,979
of zero size but when you actually watch

151
00:06:53,520 --> 00:06:58,020
the thing run you'll see it copy itself

152
00:06:55,979 --> 00:07:00,539
it'll start at itself and then copy some

153
00:06:58,020 --> 00:07:02,460
stuff down it'll copy itself over there

154
00:07:00,539 --> 00:07:03,960
and then that's going to be the

155
00:07:02,460 --> 00:07:05,880
decompression program that it's going to

156
00:07:03,960 --> 00:07:08,250
run off in that memory and then it's

157
00:07:05,880 --> 00:07:10,250
going to decompress that other blob into

158
00:07:08,250 --> 00:07:13,349
the original space starting around

159
00:07:10,250 --> 00:07:17,130
400,000 and then based on that the

160
00:07:13,349 --> 00:07:18,960
original program that one I believe when

161
00:07:17,130 --> 00:07:20,819
it's done it is going to copy some stuff

162
00:07:18,960 --> 00:07:22,949
into the 600,000 range as well because

163
00:07:20,819 --> 00:07:25,259
the original hello static had stuff in

164
00:07:22,949 --> 00:07:27,330
the 600,000 range but this is where my

165
00:07:25,259 --> 00:07:33,630
my memory of exactly what's happening

166
00:07:27,330 --> 00:07:36,539
here is uh has decayed all right so this

167
00:07:33,630 --> 00:07:38,069
is just notionally these are the kind of

168
00:07:36,539 --> 00:07:40,469
changes that a Packer will make to the

169
00:07:38,069 --> 00:07:44,520
headers you know it'll make some space

170
00:07:40,469 --> 00:07:47,849
for the data and it is going to

171
00:07:44,520 --> 00:07:51,750
decompress that data into a large range

172
00:07:47,849 --> 00:07:53,130
of memory so this is upx on linux upx on

173
00:07:51,750 --> 00:07:55,080
windows it's going to behave basically

174
00:07:53,130 --> 00:07:57,150
the same way I'm gonna show you that

175
00:07:55,080 --> 00:08:02,719
quick as well and we'll see that the

176
00:07:57,150 --> 00:08:02,719
change is made to the headers all right

177
00:08:02,990 --> 00:08:09,270
all right so I just took my template 32

178
00:08:05,810 --> 00:08:12,029
DXE and I ran that through upx it said

179
00:08:09,270 --> 00:08:14,009
add a compression ratio move through six

180
00:08:12,029 --> 00:08:17,939
six five six bytes down to five

181
00:08:14,009 --> 00:08:20,249
to zero bytes so the final size is 77%

182
00:08:17,939 --> 00:08:24,629
of the original size I'm gonna look at

183
00:08:20,249 --> 00:08:39,479
it in cff Explorer this is crashing

184
00:08:24,629 --> 00:08:43,169
apparently other cff explorer all right

185
00:08:39,479 --> 00:08:45,449
so this is our original template 32 DXE

186
00:08:43,169 --> 00:08:47,190
we go look at the section headers you

187
00:08:45,449 --> 00:08:49,620
see it's got a dot text section which

188
00:08:47,190 --> 00:08:53,040
starts at 1,000 and goes for a 0 or

189
00:08:49,620 --> 00:08:55,850
sorry goes for 8:04 in memory got the

190
00:08:53,040 --> 00:09:01,139
our data starts at 2,000 goes for 5d for

191
00:08:55,850 --> 00:09:07,680
dot data 3,000 to 384 resource and

192
00:09:01,139 --> 00:09:10,529
relocation information in the you px 1

193
00:09:07,680 --> 00:09:14,399
it's nice and clear here we've got one

194
00:09:10,529 --> 00:09:16,430
section that has a name of you px 0 1

195
00:09:14,399 --> 00:09:19,490
section that has the name of you px 1

196
00:09:16,430 --> 00:09:23,190
alright now relative to the original one

197
00:09:19,490 --> 00:09:26,130
the first section starts at hex 1000 and

198
00:09:23,190 --> 00:09:30,839
it goes for 6000 from the original one

199
00:09:26,130 --> 00:09:33,750
starting at 1000 and goes to 804 so if

200
00:09:30,839 --> 00:09:36,240
we start at 1000 and go to 6000 will

201
00:09:33,750 --> 00:09:38,790
actually cover all of this data right so

202
00:09:36,240 --> 00:09:40,620
if the original file like we said you

203
00:09:38,790 --> 00:09:43,620
know what's the size of image size of

204
00:09:40,620 --> 00:09:46,199
image is go to this last section and add

205
00:09:43,620 --> 00:09:49,440
the total size of the virtual memory so

206
00:09:46,199 --> 00:09:52,649
total size of image here is like 5 194

207
00:09:49,440 --> 00:09:55,800
right AUP X is allocating its first you

208
00:09:52,649 --> 00:09:57,930
PX 0 section with the total size of 6000

209
00:09:55,800 --> 00:09:59,790
so this is more than enough to cover all

210
00:09:57,930 --> 00:10:01,800
of the memory space that this original

211
00:09:59,790 --> 00:10:03,209
program would have taken up so it's got

212
00:10:01,800 --> 00:10:08,639
enough space to like actually map

213
00:10:03,209 --> 00:10:12,120
everything alright and it's got you px 1

214
00:10:08,639 --> 00:10:15,120
@ virtual address 7000 total size of

215
00:10:12,120 --> 00:10:18,300
1000 and memory size of okay what I

216
00:10:15,120 --> 00:10:22,260
didn't point out here before is this

217
00:10:18,300 --> 00:10:23,699
first section it's got a raw size a size

218
00:10:22,260 --> 00:10:25,949
of raw data so how much stuff are we

219
00:10:23,699 --> 00:10:28,170
going to read off of disk and copy into

220
00:10:25,949 --> 00:10:30,120
memory 0 bytes actually

221
00:10:28,170 --> 00:10:31,980
so this is a section which all it does

222
00:10:30,120 --> 00:10:34,140
is allocate space but it doesn't copy

223
00:10:31,980 --> 00:10:36,720
any stuff into that space that's like

224
00:10:34,140 --> 00:10:39,870
that second segment that you saw over in

225
00:10:36,720 --> 00:10:42,900
the analytics thing and that confirms as

226
00:10:39,870 --> 00:10:44,910
I suspected that I was miss describing

227
00:10:42,900 --> 00:10:47,100
where stuff was gonna go so the other

228
00:10:44,910 --> 00:10:48,480
Linux thing the segment that has zero

229
00:10:47,100 --> 00:10:54,060
size that's what it's going to use for

230
00:10:48,480 --> 00:10:55,860
memory but so over here on on windows

231
00:10:54,060 --> 00:10:57,690
you've got the first section that's just

232
00:10:55,860 --> 00:11:01,010
allocating space they're not copying any

233
00:10:57,690 --> 00:11:05,100
data in there and then we've got the

234
00:11:01,010 --> 00:11:08,480
second thing that has c0 worth of data

235
00:11:05,100 --> 00:11:11,550
that it's going to read off the file and

236
00:11:08,480 --> 00:11:14,420
yeah and it's at 7,000 so it's beyond

237
00:11:11,550 --> 00:11:17,790
the address range of the original

238
00:11:14,420 --> 00:11:20,010
executable and then finally there's the

239
00:11:17,790 --> 00:11:21,720
resource of upx has the option to

240
00:11:20,010 --> 00:11:23,100
compress the resources and not compress

241
00:11:21,720 --> 00:11:25,380
the resources it's just kind of

242
00:11:23,100 --> 00:11:27,750
sometimes it'll mess up the program

243
00:11:25,380 --> 00:11:30,090
whether you pack the resources or not

244
00:11:27,750 --> 00:11:31,530
and so most Packers will give you the

245
00:11:30,090 --> 00:11:35,130
option whether you want to compress or

246
00:11:31,530 --> 00:11:36,960
not compress the resource section so

247
00:11:35,130 --> 00:11:38,760
here I believe it's just going to have

248
00:11:36,960 --> 00:11:41,580
tacked on the resources more or less as

249
00:11:38,760 --> 00:11:45,060
it so let's see it started in 8,000 it

250
00:11:41,580 --> 00:11:49,430
goes to 1,000 sizeof 400 the original

251
00:11:45,060 --> 00:11:51,480
one resource was you know total size of

252
00:11:49,430 --> 00:11:53,190
200 so it's got more than enough space

253
00:11:51,480 --> 00:11:55,680
for the resource actually you know I'm

254
00:11:53,190 --> 00:11:57,900
kind of curious whether the whether it

255
00:11:55,680 --> 00:12:01,580
really is the exact same thing yep so

256
00:11:57,900 --> 00:12:04,050
there's the manifest and in this one

257
00:12:01,580 --> 00:12:05,910
there's the manifest right so it's

258
00:12:04,050 --> 00:12:07,890
keeping the resources flowing in the

259
00:12:05,910 --> 00:12:10,380
clear right it's keeping the resource

260
00:12:07,890 --> 00:12:13,050
information outside of the proquest blob

261
00:12:10,380 --> 00:12:14,760
just in case it's necessary because

262
00:12:13,050 --> 00:12:17,490
actually this manifest information is

263
00:12:14,760 --> 00:12:18,870
used by potentially used by the OS

264
00:12:17,490 --> 00:12:21,000
loader so sometimes you want that

265
00:12:18,870 --> 00:12:22,830
information to be available ahead of

266
00:12:21,000 --> 00:12:29,490
time to set some configuration

267
00:12:22,830 --> 00:12:31,470
information all right so again just at

268
00:12:29,490 --> 00:12:34,230
the section have a header level we can

269
00:12:31,470 --> 00:12:35,820
see that upx allocates enough space to

270
00:12:34,230 --> 00:12:38,040
cover all of the memory range of the

271
00:12:35,820 --> 00:12:40,209
original executable and then it's going

272
00:12:38,040 --> 00:12:43,839
to just have a second segment

273
00:12:40,209 --> 00:12:47,050
section which has the real data and that

274
00:12:43,839 --> 00:12:48,910
data will decompress the data into the

275
00:12:47,050 --> 00:12:51,339
original virtual memory space so that

276
00:12:48,910 --> 00:12:53,319
everything Maps up to the same offsets

277
00:12:51,339 --> 00:12:56,920
that it would have had if you executed

278
00:12:53,319 --> 00:12:59,529
the original file so alright so any

279
00:12:56,920 --> 00:13:01,869
questions on the concept of packing this

280
00:12:59,529 --> 00:13:03,850
notion that we're like compressing down

281
00:13:01,869 --> 00:13:06,429
all of the data from an original file

282
00:13:03,850 --> 00:13:08,170
and then decompressing it back into the

283
00:13:06,429 --> 00:13:10,240
correct location in memory at runtime

284
00:13:08,170 --> 00:13:11,170
with some little stub code that does

285
00:13:10,240 --> 00:13:16,449
decompression

286
00:13:11,170 --> 00:13:20,350
any questions about that not really a

287
00:13:16,449 --> 00:13:22,809
question so much as a comment and or

288
00:13:20,350 --> 00:13:25,529
discussion I'm really surprised that

289
00:13:22,809 --> 00:13:27,999
Packers operate this way that is

290
00:13:25,529 --> 00:13:30,550
duplicating so many of the functions of

291
00:13:27,999 --> 00:13:33,910
the operating system when it would seem

292
00:13:30,550 --> 00:13:37,869
much simpler just to decompress a file

293
00:13:33,910 --> 00:13:40,209
write a write that file and then hand it

294
00:13:37,869 --> 00:13:42,579
to the operating system to do these

295
00:13:40,209 --> 00:13:45,699
functions do I understand that correctly

296
00:13:42,579 --> 00:13:49,240
and if so do you have any idea why they

297
00:13:45,699 --> 00:13:50,980
do it this way yep so that's actually we

298
00:13:49,240 --> 00:13:55,660
would call what you just described more

299
00:13:50,980 --> 00:13:57,279
like a dropper so in the reverse

300
00:13:55,660 --> 00:13:59,110
engineering class the intro to reverse

301
00:13:57,279 --> 00:14:00,160
engineering class Matt Briggs and

302
00:13:59,110 --> 00:14:03,040
Franklin lose me

303
00:14:00,160 --> 00:14:04,209
they cover sort of droppers and so a

304
00:14:03,040 --> 00:14:05,619
dropper would for instance pull

305
00:14:04,209 --> 00:14:08,769
something out of the resource section

306
00:14:05,619 --> 00:14:10,179
and drop it and then you know decompress

307
00:14:08,769 --> 00:14:11,499
it or decrypt it and things like that

308
00:14:10,179 --> 00:14:13,839
drop it on the PAS system and then put

309
00:14:11,499 --> 00:14:18,819
it up the core benefit that you get from

310
00:14:13,839 --> 00:14:21,579
a packer versus a dropper is the real

311
00:14:18,819 --> 00:14:24,429
quote real executable never touches disk

312
00:14:21,579 --> 00:14:26,259
if you've got a dropper then you're

313
00:14:24,429 --> 00:14:29,079
dumping the thing off to the file system

314
00:14:26,259 --> 00:14:31,059
and even if you delete it right file

315
00:14:29,079 --> 00:14:33,399
system forensics can potentially recover

316
00:14:31,059 --> 00:14:35,439
that so you just wrote the original file

317
00:14:33,399 --> 00:14:37,240
to disk and then you just executed it

318
00:14:35,439 --> 00:14:38,769
that original file even when it's

319
00:14:37,240 --> 00:14:41,199
deleted you know it's still potentially

320
00:14:38,769 --> 00:14:42,970
is resident in the sectors on hard drive

321
00:14:41,199 --> 00:14:45,730
so file system forensics can find it

322
00:14:42,970 --> 00:14:47,850
when you just decompress it into memory

323
00:14:45,730 --> 00:14:50,290
using this you know rigmarole of

324
00:14:47,850 --> 00:14:53,139
reconstructing stuff the way the OS

325
00:14:50,290 --> 00:14:53,930
would when you do it that way you make

326
00:14:53,139 --> 00:14:55,490
sure that the

327
00:14:53,930 --> 00:14:58,130
original program that you're trying to

328
00:14:55,490 --> 00:14:59,720
protect never was actually visible on

329
00:14:58,130 --> 00:15:01,790
the file system so it's you know pretty

330
00:14:59,720 --> 00:15:03,320
ephemeral someone shuts down the machine

331
00:15:01,790 --> 00:15:05,750
they'd never get a copy of the original

332
00:15:03,320 --> 00:15:06,860
thing so that said they still would

333
00:15:05,750 --> 00:15:08,839
potentially have a copy of the

334
00:15:06,860 --> 00:15:11,779
compressed thing and they can still work

335
00:15:08,839 --> 00:15:14,270
through the analysis process of deriving

336
00:15:11,779 --> 00:15:16,790
the original file so again it's

337
00:15:14,270 --> 00:15:21,050
basically the the I would say it's

338
00:15:16,790 --> 00:15:22,790
primarily due to you know malware

339
00:15:21,050 --> 00:15:25,250
authors wanting to avoid file system

340
00:15:22,790 --> 00:15:26,630
forensics and the potential for evidence

341
00:15:25,250 --> 00:15:30,350
showing up later of what the original

342
00:15:26,630 --> 00:15:41,029
file do yeah that's a good comment also

343
00:15:30,350 --> 00:15:43,040
you're using the the the pact file a

344
00:15:41,029 --> 00:15:57,860
depends on what you mean by log well

345
00:15:43,040 --> 00:15:59,000
it's like well so yeah it really comes

346
00:15:57,860 --> 00:16:00,529
down to your assumptions how the

347
00:15:59,000 --> 00:16:03,610
system's configured and what sort of

348
00:16:00,529 --> 00:16:05,900
logging is going on but in the sense of

349
00:16:03,610 --> 00:16:10,040
the difference between a dropper and a

350
00:16:05,900 --> 00:16:13,190
packer a packer runs itself this you

351
00:16:10,040 --> 00:16:15,050
know template 32 upx dot exe the

352
00:16:13,190 --> 00:16:18,650
invocation of that process would still

353
00:16:15,050 --> 00:16:20,720
get logged right just like but now if we

354
00:16:18,650 --> 00:16:22,790
were doing a dropper you would see you

355
00:16:20,720 --> 00:16:24,620
know the dropper load and then it would

356
00:16:22,790 --> 00:16:26,420
exit it would drop the file and then

357
00:16:24,620 --> 00:16:28,640
load that so you can see Q versus one

358
00:16:26,420 --> 00:16:30,650
whether or not that's any sort of the

359
00:16:28,640 --> 00:16:32,320
thing that would be useful for defenders

360
00:16:30,650 --> 00:16:34,760
I don't think is necessarily the case

361
00:16:32,320 --> 00:16:37,730
because you know it's just some random

362
00:16:34,760 --> 00:16:39,980
binary and implicitly just saying a log

363
00:16:37,730 --> 00:16:43,370
file of you know this hash binary ran or

364
00:16:39,980 --> 00:16:44,839
not didn't run it's useful in like an

365
00:16:43,370 --> 00:16:46,700
application whitelisting sort of

366
00:16:44,839 --> 00:16:48,770
environment but like just if there's

367
00:16:46,700 --> 00:16:51,200
just a log file that says some random

368
00:16:48,770 --> 00:16:52,310
file name X cubed Ram I don't think

369
00:16:51,200 --> 00:16:55,420
there would really be any difference

370
00:16:52,310 --> 00:16:58,070
between a dropper version versus a

371
00:16:55,420 --> 00:17:01,450
packed version there's no real

372
00:16:58,070 --> 00:17:01,450
difference in terms of protection


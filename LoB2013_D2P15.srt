1
00:00:03,680 --> 00:00:08,129
all right back to the data directory I

2
00:00:06,240 --> 00:00:09,150
do believe this is our second to last

3
00:00:08,129 --> 00:00:11,610
thing that we're going to talk about

4
00:00:09,150 --> 00:00:13,320
load config and this is kind of just a

5
00:00:11,610 --> 00:00:14,670
miscellaneous place where there's some

6
00:00:13,320 --> 00:00:16,410
miscellaneous stuff that I'm only

7
00:00:14,670 --> 00:00:19,290
talking about because its security

8
00:00:16,410 --> 00:00:20,579
relevant so again there's a time date

9
00:00:19,290 --> 00:00:22,079
stamp not going to be filled in

10
00:00:20,579 --> 00:00:24,180
otherwise I'd be like yeah there's

11
00:00:22,079 --> 00:00:27,090
another time day today well we care

12
00:00:24,180 --> 00:00:30,599
about is security cookie table uh se

13
00:00:27,090 --> 00:00:32,040
handler table and se handler count first

14
00:00:30,599 --> 00:00:33,690
of all the security cookie and I see

15
00:00:32,040 --> 00:00:35,070
handler table those are both absolute

16
00:00:33,690 --> 00:00:36,870
virtual addresses again you see as we

17
00:00:35,070 --> 00:00:38,880
get towards the end here got lots of

18
00:00:36,870 --> 00:00:40,500
absolute virtual addresses and offsets

19
00:00:38,880 --> 00:00:42,300
that are relative to the start of its

20
00:00:40,500 --> 00:00:45,480
own data structure instead of the RBA's

21
00:00:42,300 --> 00:00:48,780
but and here's the 64-bit version of

22
00:00:45,480 --> 00:00:52,530
that so the security cookie absolute

23
00:00:48,780 --> 00:00:54,360
virtual address and it will be fixed up

24
00:00:52,530 --> 00:00:56,100
by the relocation information so there

25
00:00:54,360 --> 00:00:58,050
will be a relocation entry that says if

26
00:00:56,100 --> 00:01:01,079
you randomize this thing in memory make

27
00:00:58,050 --> 00:01:03,780
sure you fix up that header and what it

28
00:01:01,079 --> 00:01:06,689
is is it's the location where the stack

29
00:01:03,780 --> 00:01:10,200
cookie that's used with the GS flag will

30
00:01:06,689 --> 00:01:11,940
be sword there's a single stack cookie

31
00:01:10,200 --> 00:01:15,000
that gets randomized at the beginning

32
00:01:11,940 --> 00:01:18,840
bill can we go over the board and so for

33
00:01:15,000 --> 00:01:21,090
those of you I think this has talked a

34
00:01:18,840 --> 00:01:23,580
little bit maybe in the intro x86 or

35
00:01:21,090 --> 00:01:27,600
intro exploits class or up but you know

36
00:01:23,580 --> 00:01:30,990
just the general canonical way that you

37
00:01:27,600 --> 00:01:32,490
stop buffer overflows is that well the

38
00:01:30,990 --> 00:01:37,500
way that you do buffer overflows is

39
00:01:32,490 --> 00:01:39,510
you'll have a saved eip on the stack so

40
00:01:37,500 --> 00:01:42,390
this is going to be low this is going to

41
00:01:39,510 --> 00:01:45,420
be high memory and so the stack grows

42
00:01:42,390 --> 00:01:51,150
towards low memory and so we've got say

43
00:01:45,420 --> 00:01:53,070
VIP wave EBP and so these are registers

44
00:01:51,150 --> 00:01:57,479
x86 registers and then you've got your

45
00:01:53,070 --> 00:01:58,619
local variables equal VARs right and so

46
00:01:57,479 --> 00:02:01,590
what happens when you have a buffer

47
00:01:58,619 --> 00:02:03,900
overflow is that you know you've got one

48
00:02:01,590 --> 00:02:07,320
local variable here that maybe takes

49
00:02:03,900 --> 00:02:08,909
user input and the code is wrong so it

50
00:02:07,320 --> 00:02:12,469
allows the user to put too much input

51
00:02:08,909 --> 00:02:15,989
and so while the stack grows low-to-high

52
00:02:12,469 --> 00:02:17,170
code rights happen from sorry while the

53
00:02:15,989 --> 00:02:19,870
stack grows towards

54
00:02:17,170 --> 00:02:21,940
dresses you know memory rights go from

55
00:02:19,870 --> 00:02:23,890
low addresses hi addresses so when

56
00:02:21,940 --> 00:02:27,670
you're doing you know string copy or mem

57
00:02:23,890 --> 00:02:29,920
copy or whatever screen copy copies data

58
00:02:27,670 --> 00:02:34,780
from some input buffer that the attacker

59
00:02:29,920 --> 00:02:38,170
is giving stuff to you from attacker

60
00:02:34,780 --> 00:02:39,550
data comes in here and he writes any

61
00:02:38,170 --> 00:02:41,380
rights and your rights until he finally

62
00:02:39,550 --> 00:02:42,819
overflows one local variable and he

63
00:02:41,380 --> 00:02:44,620
keeps writing and he's overflowing other

64
00:02:42,819 --> 00:02:46,060
local variables and eventually he's

65
00:02:44,620 --> 00:02:48,819
overflowing and he keeps going and

66
00:02:46,060 --> 00:02:50,500
depending on how the copy is setup

67
00:02:48,819 --> 00:02:52,569
depends on how far he's able to write

68
00:02:50,500 --> 00:02:54,610
stuff like that and so the canonical way

69
00:02:52,569 --> 00:02:56,560
that you explain it up for overflow is

70
00:02:54,610 --> 00:02:58,810
you override this instruction pointer

71
00:02:56,560 --> 00:03:00,880
that's saved so that it points that you

72
00:02:58,810 --> 00:03:03,069
know somewhere down into here where

73
00:03:00,880 --> 00:03:08,980
you've got attacker code that's embedded

74
00:03:03,069 --> 00:03:10,390
and so basically when this look variable

75
00:03:08,980 --> 00:03:12,130
is done and it's cleaning up at stack

76
00:03:10,390 --> 00:03:14,110
and it's popping the eip off the stack

77
00:03:12,130 --> 00:03:16,750
it goes back into here and it goes next

78
00:03:14,110 --> 00:03:19,060
feeds Tucker code the GS compiler flag

79
00:03:16,750 --> 00:03:21,250
is for stack cookies or security cookies

80
00:03:19,060 --> 00:03:24,100
or for stack Canaries it's called a lot

81
00:03:21,250 --> 00:03:27,130
of different things and all it is is

82
00:03:24,100 --> 00:03:33,250
that the compiler will basically put in

83
00:03:27,130 --> 00:03:35,260
a random value right here so it'll put a

84
00:03:33,250 --> 00:03:37,510
random value between your saved stuff

85
00:03:35,260 --> 00:03:40,030
and your local variables so that if the

86
00:03:37,510 --> 00:03:42,730
attacker start smashing local variables

87
00:03:40,030 --> 00:03:44,290
trying to get up to this eip between the

88
00:03:42,730 --> 00:03:45,459
eip and between the local variables

89
00:03:44,290 --> 00:03:47,799
there's going to be a cookie that's a

90
00:03:45,459 --> 00:03:50,350
random value that happens to be selected

91
00:03:47,799 --> 00:03:52,900
based on this virtual address that's

92
00:03:50,350 --> 00:03:54,489
given here that's that's a location

93
00:03:52,900 --> 00:03:55,989
where when do I starts the program it

94
00:03:54,489 --> 00:03:57,430
takes that virtual address and it plops

95
00:03:55,989 --> 00:03:59,799
a random value into there for this

96
00:03:57,430 --> 00:04:01,329
particular program and then inside of

97
00:03:59,799 --> 00:04:02,410
the code when it's calling functions

98
00:04:01,329 --> 00:04:05,019
that it thinks are going to be

99
00:04:02,410 --> 00:04:06,819
potentially buffer overflow when it sees

100
00:04:05,019 --> 00:04:09,010
there's a string copy and a function or

101
00:04:06,819 --> 00:04:10,780
if it sees as a mem copy it does some

102
00:04:09,010 --> 00:04:12,609
heuristics and it says if I think this

103
00:04:10,780 --> 00:04:15,220
is going to be vulnerable and if you've

104
00:04:12,609 --> 00:04:18,700
got the GS flag set in your compiler

105
00:04:15,220 --> 00:04:21,430
then it'll insert this cookie d word of

106
00:04:18,700 --> 00:04:22,870
random value so that when you return and

107
00:04:21,430 --> 00:04:24,340
when you're cleaning up off of this

108
00:04:22,870 --> 00:04:25,420
thing and you're removing the local

109
00:04:24,340 --> 00:04:27,730
variables and you're about

110
00:04:25,420 --> 00:04:29,290
pop off this first you check the cookie

111
00:04:27,730 --> 00:04:31,270
and you say is this still that same

112
00:04:29,290 --> 00:04:33,340
random value that's stored in memory

113
00:04:31,270 --> 00:04:34,750
there so basically there's a little bit

114
00:04:33,340 --> 00:04:37,210
of code at the beginning that pushes a

115
00:04:34,750 --> 00:04:38,530
random value from there onto the stack

116
00:04:37,210 --> 00:04:40,660
and there's a little bit of code at the

117
00:04:38,530 --> 00:04:42,970
end that takes the value reads it off

118
00:04:40,660 --> 00:04:45,100
and compares it against what's in memory

119
00:04:42,970 --> 00:04:47,620
at the security cookie virtual address

120
00:04:45,100 --> 00:04:49,840
and so that's how they they check the

121
00:04:47,620 --> 00:04:54,700
cookies in order to make sure that no

122
00:04:49,840 --> 00:04:57,700
one's buffer overflow this program so

123
00:04:54,700 --> 00:05:01,540
that's security cookie and the SE

124
00:04:57,700 --> 00:05:04,840
handler table this has to do with the

125
00:05:01,540 --> 00:05:06,310
safe seh flag what linker flag and this

126
00:05:04,840 --> 00:05:08,500
is also meant to be an exploit

127
00:05:06,310 --> 00:05:11,410
mitigation thing I kind of mentioned it

128
00:05:08,500 --> 00:05:13,060
before there's the safety IP on the

129
00:05:11,410 --> 00:05:14,320
stack but if you go farther you'll

130
00:05:13,060 --> 00:05:15,880
eventually hit these structured

131
00:05:14,320 --> 00:05:17,830
exception handler information and this

132
00:05:15,880 --> 00:05:19,990
is how Windows handles one particular

133
00:05:17,830 --> 00:05:22,270
error conditions and so the structured

134
00:05:19,990 --> 00:05:24,250
exception handler will be a function

135
00:05:22,270 --> 00:05:26,740
pointer where it'll say if at this type

136
00:05:24,250 --> 00:05:28,420
of exception execute this function and

137
00:05:26,740 --> 00:05:30,820
so it's just another thing that the

138
00:05:28,420 --> 00:05:32,800
attacker can override on the stack so

139
00:05:30,820 --> 00:05:35,140
the SE handler table is supposed to be

140
00:05:32,800 --> 00:05:37,360
something where your pre programming

141
00:05:35,140 --> 00:05:39,400
into your binary these are the exception

142
00:05:37,360 --> 00:05:40,810
handlers which may be called and no

143
00:05:39,400 --> 00:05:42,820
other exception idler to accept these

144
00:05:40,810 --> 00:05:44,500
may be called so if an attacker buffer

145
00:05:42,820 --> 00:05:45,550
overflows and writes their own function

146
00:05:44,500 --> 00:05:48,460
pointer into one of those like

147
00:05:45,550 --> 00:05:50,230
structured exception handler entries the

148
00:05:48,460 --> 00:05:51,610
OS will be checking well I'm about to

149
00:05:50,230 --> 00:05:53,140
call this exception because something

150
00:05:51,610 --> 00:05:55,270
bad happened but let me first check that

151
00:05:53,140 --> 00:05:57,210
against this table here and so that's

152
00:05:55,270 --> 00:06:01,870
the point of the SE handler cable and

153
00:05:57,210 --> 00:06:05,140
the count is basically just how many of

154
00:06:01,870 --> 00:06:07,030
these things the OS needs to check

155
00:06:05,140 --> 00:06:10,380
through before it determines yes this is

156
00:06:07,030 --> 00:06:13,900
a valid exception handler or no it's not

157
00:06:10,380 --> 00:06:15,340
and so unfortunately that is not a

158
00:06:13,900 --> 00:06:18,550
foolproof mechanism and both of those

159
00:06:15,340 --> 00:06:21,780
can be black ops basically but but

160
00:06:18,550 --> 00:06:24,880
that's where that information comes from

161
00:06:21,780 --> 00:06:26,740
all right so safe seh they're used to

162
00:06:24,880 --> 00:06:28,390
not be a linker option for it now I

163
00:06:26,740 --> 00:06:31,990
believe there is actually in two

164
00:06:28,390 --> 00:06:34,840
thousand and a new or something like

165
00:06:31,990 --> 00:06:36,850
that and GS has been there for a while

166
00:06:34,840 --> 00:06:38,330
it's on by default now I think that's a

167
00:06:36,850 --> 00:06:41,090
safe seh may be on by

168
00:06:38,330 --> 00:06:43,340
fault as well no but basically you just

169
00:06:41,090 --> 00:06:45,740
in your compiler options you set the GS

170
00:06:43,340 --> 00:06:48,860
flag you said yes and then it will turn

171
00:06:45,740 --> 00:06:50,960
on the GS flag and then the compiler

172
00:06:48,860 --> 00:06:52,250
will do heuristics in order to say so

173
00:06:50,960 --> 00:06:53,750
it's actually kind of interesting some

174
00:06:52,250 --> 00:06:56,060
buffer overflows that you see still

175
00:06:53,750 --> 00:06:59,210
there in things that have the GS flag

176
00:06:56,060 --> 00:07:00,710
set but the heuristics failed and they

177
00:06:59,210 --> 00:07:02,719
said we don't think that this particular

178
00:07:00,710 --> 00:07:04,250
function is vulnerable and so they

179
00:07:02,719 --> 00:07:06,860
didn't add the cookie to that function

180
00:07:04,250 --> 00:07:09,229
and they just turned out to be wrong so

181
00:07:06,860 --> 00:07:11,719
they may improve the heuristics on that

182
00:07:09,229 --> 00:07:12,949
over time so that they see you know oh

183
00:07:11,719 --> 00:07:14,719
well we really should have considered

184
00:07:12,949 --> 00:07:16,430
this as a potentially exploitable case

185
00:07:14,719 --> 00:07:17,990
but the point is they're trying to not

186
00:07:16,430 --> 00:07:19,520
put that everywhere because again it's

187
00:07:17,990 --> 00:07:21,139
just a performance versus security

188
00:07:19,520 --> 00:07:23,419
trade-off they could put it checking

189
00:07:21,139 --> 00:07:25,159
everything but if you go into a leaf

190
00:07:23,419 --> 00:07:27,469
node function and all it does is add a

191
00:07:25,159 --> 00:07:28,789
plus B and that it returns back that's

192
00:07:27,469 --> 00:07:31,490
never going to have the buffer overflow

193
00:07:28,789 --> 00:07:32,750
in it so you can add it everywhere but

194
00:07:31,490 --> 00:07:34,310
you really want to trade it off so that

195
00:07:32,750 --> 00:07:36,770
you're not doing this extra push and

196
00:07:34,310 --> 00:07:38,569
then pop when you don't have to be it's

197
00:07:36,770 --> 00:07:40,699
not just pushing pop push and then you

198
00:07:38,569 --> 00:07:42,529
know pop plus check against you know

199
00:07:40,699 --> 00:07:46,659
thing out of the security cookie and

200
00:07:42,529 --> 00:07:49,279
then so forth yes begin talking about vu

201
00:07:46,659 --> 00:07:53,029
exception handling mechanism in 64-bit

202
00:07:49,279 --> 00:07:55,039
apps I'm not that's the thing I put that

203
00:07:53,029 --> 00:07:56,719
p data question in there and I wanted to

204
00:07:55,039 --> 00:07:58,610
get to like actually covering that but

205
00:07:56,719 --> 00:07:59,810
that's part of why it's why that it

206
00:07:58,610 --> 00:08:02,029
wasn't in the slides and I kept

207
00:07:59,810 --> 00:08:04,610
forgetting to cover it so no because I

208
00:08:02,029 --> 00:08:06,560
don't know it welcome yeah basically so

209
00:08:04,610 --> 00:08:09,610
until I know where well enough I'm not

210
00:08:06,560 --> 00:08:09,610
gonna try to muddle through it


1
00:00:09,840 --> 00:00:13,620
yeah we've got enough time I think for

2
00:00:11,969 --> 00:00:14,940
this so everyone go ahead and follow

3
00:00:13,620 --> 00:00:18,599
along we'll start playing around with

4
00:00:14,940 --> 00:00:20,189
readout you should have a bun too it'll

5
00:00:18,599 --> 00:00:23,400
either already be running on your system

6
00:00:20,189 --> 00:00:24,630
or if it's not currently running we

7
00:00:23,400 --> 00:00:26,939
should be able to find VMware

8
00:00:24,630 --> 00:00:29,759
Workstation on your desktop and just

9
00:00:26,939 --> 00:00:31,469
double click on that once you're in

10
00:00:29,759 --> 00:00:34,110
VMware there should be basically only

11
00:00:31,469 --> 00:00:36,930
one VM that you have opted the option to

12
00:00:34,110 --> 00:00:40,649
start up we called your bun - 64-bit -

13
00:00:36,930 --> 00:00:42,899
and so if you suspended it unsuspended

14
00:00:40,649 --> 00:00:45,179
by pressing play he's already suspended

15
00:00:42,899 --> 00:00:48,210
resumed the virtual machine which ever

16
00:00:45,179 --> 00:00:50,789
you should get to the login screen and

17
00:00:48,210 --> 00:00:53,190
then the user is user and the password

18
00:00:50,789 --> 00:00:54,780
is awesome password for the win once you

19
00:00:53,190 --> 00:01:00,379
get into your view open the terminal

20
00:00:54,780 --> 00:01:07,550
that's on your desktop 12 your desktop

21
00:01:00,379 --> 00:01:07,550
CD 2 code CD code /hello

22
00:01:07,909 --> 00:01:19,200
press return and the first command we're

23
00:01:12,960 --> 00:01:20,790
gonna do is read out - H and hello you

24
00:01:19,200 --> 00:01:23,250
can press return on that I'm just going

25
00:01:20,790 --> 00:01:24,180
to leave that up there for bit what

26
00:01:23,250 --> 00:01:25,740
you're gonna see is this header

27
00:01:24,180 --> 00:01:27,780
structure that I just talked about on

28
00:01:25,740 --> 00:01:30,000
the slide we've got things like the elf

29
00:01:27,780 --> 00:01:32,090
at the beginning we've got the entry

30
00:01:30,000 --> 00:01:34,170
point which is where the code starts

31
00:01:32,090 --> 00:01:38,189
right so there's the magic at the

32
00:01:34,170 --> 00:01:40,469
beginning we've got the type which is

33
00:01:38,189 --> 00:01:44,130
saying exec here so it's an executable

34
00:01:40,469 --> 00:01:46,680
file we've got the entry point address

35
00:01:44,130 --> 00:01:48,000
this is the absolute virtual address

36
00:01:46,680 --> 00:01:53,369
where this thing is going to start

37
00:01:48,000 --> 00:01:55,799
executing code we've got the start of

38
00:01:53,369 --> 00:01:58,530
the sex program headers so that's 64

39
00:01:55,799 --> 00:02:00,000
bytes into the file and we've got the

40
00:01:58,530 --> 00:02:01,710
started section editor so this is kind

41
00:02:00,000 --> 00:02:03,689
of why on my pictures I showed program

42
00:02:01,710 --> 00:02:05,280
header is right at the beginning and the

43
00:02:03,689 --> 00:02:07,380
section headers are kind of tacked on to

44
00:02:05,280 --> 00:02:08,820
the end they don't necessarily have to

45
00:02:07,380 --> 00:02:11,009
be there you can chop them off in the

46
00:02:08,820 --> 00:02:12,780
program will run just fine so they're

47
00:02:11,009 --> 00:02:18,989
kind of optional stuff that's tacked on

48
00:02:12,780 --> 00:02:22,140
to the end all right and this header

49
00:02:18,989 --> 00:02:23,069
itself size of the program enters 56

50
00:02:22,140 --> 00:02:26,519
bytes that's

51
00:02:23,069 --> 00:02:28,620
appropriate for a 64-bit binary and size

52
00:02:26,519 --> 00:02:30,329
of the section enters 64 bytes again

53
00:02:28,620 --> 00:02:34,049
that would be the appropriate size for

54
00:02:30,329 --> 00:02:36,629
64-bit we had 32-bit binaries we would

55
00:02:34,049 --> 00:02:39,120
small see smaller things there and then

56
00:02:36,629 --> 00:02:41,430
last thing section header string table

57
00:02:39,120 --> 00:02:43,469
index so I said there's one particular

58
00:02:41,430 --> 00:02:45,569
section header that's special and it

59
00:02:43,469 --> 00:02:48,989
gets mentioned way up here in the

60
00:02:45,569 --> 00:02:50,939
alfetta its index 28 in this particular

61
00:02:48,989 --> 00:02:52,439
binary is going to be the string table

62
00:02:50,939 --> 00:02:55,319
it's just going to be a big array of

63
00:02:52,439 --> 00:02:58,799
strings and other things will reference

64
00:02:55,319 --> 00:03:02,719
into that string table all right

65
00:02:58,799 --> 00:03:09,989
everybody tracks with that any questions

66
00:03:02,719 --> 00:03:14,730
all right cool on we go so this is just

67
00:03:09,989 --> 00:03:18,629
kind of a picture of breaking down the

68
00:03:14,730 --> 00:03:21,120
thing on disk versus in memory the eph

69
00:03:18,629 --> 00:03:22,500
off that's going to point you where the

70
00:03:21,120 --> 00:03:24,180
program headers start and there's going

71
00:03:22,500 --> 00:03:25,560
to be an array of program headers so

72
00:03:24,180 --> 00:03:28,259
there's many program headers all in a

73
00:03:25,560 --> 00:03:31,290
right here all right how many are there

74
00:03:28,259 --> 00:03:33,900
there's eph num numbers and of program

75
00:03:31,290 --> 00:03:35,400
headers in that array right down

76
00:03:33,900 --> 00:03:36,900
somewhere else there's the section

77
00:03:35,400 --> 00:03:40,530
headers that's gonna be another array

78
00:03:36,900 --> 00:03:42,239
and we're gonna have esh num worth of

79
00:03:40,530 --> 00:03:44,159
those section headers down somewhere

80
00:03:42,239 --> 00:03:46,919
else and then there's our one little

81
00:03:44,159 --> 00:03:49,769
special section adder which is the

82
00:03:46,919 --> 00:03:52,220
string table section and we'll have the

83
00:03:49,769 --> 00:03:54,569
index for that particular string too

84
00:03:52,220 --> 00:03:57,060
alright so that's how the things we care

85
00:03:54,569 --> 00:03:59,759
about in the elf header point into the

86
00:03:57,060 --> 00:04:01,949
various places in the file overall and

87
00:03:59,759 --> 00:04:05,129
not doing anything with this in memory

88
00:04:01,949 --> 00:04:06,870
lynx-o and the entry point is going to

89
00:04:05,129 --> 00:04:10,069
be somewhere in the code that's saying

90
00:04:06,870 --> 00:04:10,069
that's where it should actually start


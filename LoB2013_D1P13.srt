1
00:00:03,550 --> 00:00:11,330
so let's go on to doing round 4 i'll be

2
00:00:08,120 --> 00:00:13,009
doing basically asking questions like

3
00:00:11,330 --> 00:00:15,200
you know how many Oh Dee deep Dee

4
00:00:13,009 --> 00:00:17,240
lawless is this thing import where does

5
00:00:15,200 --> 00:00:19,400
the importance table start why does the

6
00:00:17,240 --> 00:00:21,470
imported dress table start what's the

7
00:00:19,400 --> 00:00:23,990
RBA of a particular entry on things like

8
00:00:21,470 --> 00:00:26,870
that so go ahead and start playing with

9
00:00:23,990 --> 00:00:30,380
round floor and asking any questions it

10
00:00:26,870 --> 00:00:32,360
can't actually or how funny so here's an

11
00:00:30,380 --> 00:00:34,610
interesting thing on this screen right

12
00:00:32,360 --> 00:00:36,140
now actually arguably in this sort of

13
00:00:34,610 --> 00:00:37,730
question I said this is what we're I'm

14
00:00:36,140 --> 00:00:39,800
not asking any interpretation I'm just

15
00:00:37,730 --> 00:00:41,690
saying tell me the literal value I said

16
00:00:39,800 --> 00:00:44,600
image optional header data directory

17
00:00:41,690 --> 00:00:47,359
index and then you know I use the

18
00:00:44,600 --> 00:00:49,609
constant image directory entry import so

19
00:00:47,359 --> 00:00:52,190
that's the next one you'd have to go

20
00:00:49,609 --> 00:00:54,710
back and look that up sorry for index 1

21
00:00:52,190 --> 00:00:56,479
dot RB asar yeah I probably should have

22
00:00:54,710 --> 00:00:58,999
said dot virtual address because that

23
00:00:56,479 --> 00:01:00,920
the actual structure field name but

24
00:00:58,999 --> 00:01:03,649
again there's an art game there's a size

25
00:01:00,920 --> 00:01:06,800
the RBA is actually named virtual dress

26
00:01:03,649 --> 00:01:08,299
oh I missed I forgot I accidentally

27
00:01:06,800 --> 00:01:11,810
skipped on thing with you guys actually

28
00:01:08,299 --> 00:01:13,790
our oscillates like the next slide one

29
00:01:11,810 --> 00:01:19,130
miscellaneous thing I didn't cover that

30
00:01:13,790 --> 00:01:20,960
I should have yeah there's one last

31
00:01:19,130 --> 00:01:23,000
thing I was seeing actually follow my

32
00:01:20,960 --> 00:01:27,140
slides wait until I get to the game and

33
00:01:23,000 --> 00:01:29,420
under one last thing in the native

34
00:01:27,140 --> 00:01:31,100
directory there's actually an extra

35
00:01:29,420 --> 00:01:37,009
thing that's basically like a shortcut

36
00:01:31,100 --> 00:01:39,890
to the IAT there's a shortcut to the IAT

37
00:01:37,009 --> 00:01:42,109
the base of the IAT itself and its this

38
00:01:39,890 --> 00:01:44,539
image directory entry IIT so you have to

39
00:01:42,109 --> 00:01:47,710
keep the difference between this index

40
00:01:44,539 --> 00:01:51,350
one is image directory entry import and

41
00:01:47,710 --> 00:01:53,509
then index I don't know 14 or 13 or

42
00:01:51,350 --> 00:01:55,820
something like that in this directory I

43
00:01:53,509 --> 00:01:57,950
80 and so where I say with director

44
00:01:55,820 --> 00:02:01,399
import points at this array of you know

45
00:01:57,950 --> 00:02:05,210
/ dll structures image directory iat

46
00:02:01,399 --> 00:02:07,280
goes directly to the start of the iat so

47
00:02:05,210 --> 00:02:10,220
if I'm asking you something about this

48
00:02:07,280 --> 00:02:11,659
index that's what I'm asking for see I'm

49
00:02:10,220 --> 00:02:12,180
trying to be nice to you by not like

50
00:02:11,659 --> 00:02:13,439
asking

51
00:02:12,180 --> 00:02:15,329
for the constant like what's data

52
00:02:13,439 --> 00:02:17,989
directory entry 13 or something like

53
00:02:15,329 --> 00:02:21,299
that trying to say directory entry I 80

54
00:02:17,989 --> 00:02:26,069
versus English directory entry import so

55
00:02:21,299 --> 00:02:28,980
sorry got some of that data g question

56
00:02:26,069 --> 00:02:32,819
was what section is the import directly

57
00:02:28,980 --> 00:02:35,790
table in to find that out we look in

58
00:02:32,819 --> 00:02:38,879
these section headers so there's a

59
00:02:35,790 --> 00:02:40,230
couple of ways you can okay there's a

60
00:02:38,879 --> 00:02:42,780
couple of ways you can figure that out

61
00:02:40,230 --> 00:02:45,030
so it says you know what section is

62
00:02:42,780 --> 00:02:47,730
particular thing and if you can parse it

63
00:02:45,030 --> 00:02:50,519
with be view the simple easy one might

64
00:02:47,730 --> 00:02:53,010
say cheating way it's just wherever you

65
00:02:50,519 --> 00:02:55,049
find that data so like on my screen

66
00:02:53,010 --> 00:02:57,030
right now I have the dot our data

67
00:02:55,049 --> 00:02:59,280
section and you can see that the import

68
00:02:57,030 --> 00:03:01,919
directory table is you know within the

69
00:02:59,280 --> 00:03:06,389
Aadhaar data section the right way to do

70
00:03:01,919 --> 00:03:08,579
it is that you go to you know you find

71
00:03:06,389 --> 00:03:10,349
the RBA's of interest and then you

72
00:03:08,579 --> 00:03:12,599
figure out are those rba's within a

73
00:03:10,349 --> 00:03:14,189
particular section so you look at the

74
00:03:12,599 --> 00:03:17,310
section enter information which we learn

75
00:03:14,189 --> 00:03:21,959
about the previous section I can say all

76
00:03:17,310 --> 00:03:24,540
right well if I optional if my optional

77
00:03:21,959 --> 00:03:28,049
entry says that the import table is at

78
00:03:24,540 --> 00:03:31,349
22 24 right that's the RBA a to say

79
00:03:28,049 --> 00:03:33,750
which section contains 2224 look at the

80
00:03:31,349 --> 00:03:38,760
dot text section I'd say goes from RP +

81
00:03:33,750 --> 00:03:40,769
1000 two 1000 804 so k 2 2 to 4 is

82
00:03:38,760 --> 00:03:43,259
greater than 1000 804 so it's not an

83
00:03:40,769 --> 00:03:47,699
about text moves in the our data goes

84
00:03:43,259 --> 00:03:51,930
from 2002 2005b for all right to 2 to 4

85
00:03:47,699 --> 00:03:54,000
is less than 250 for therefore it's in

86
00:03:51,930 --> 00:03:57,239
the dot our data section in this

87
00:03:54,000 --> 00:03:59,459
particular case but yeah so it can only

88
00:03:57,239 --> 00:04:00,840
be in one section wrong yes so when we

89
00:03:59,459 --> 00:04:03,030
say you know what section it's in

90
00:04:00,840 --> 00:04:05,370
because now the table is going to be put

91
00:04:03,030 --> 00:04:07,169
somewhere in memory that memory is going

92
00:04:05,370 --> 00:04:10,500
to be in some way you know be within a

93
00:04:07,169 --> 00:04:12,090
section is to ask if it wasn't

94
00:04:10,500 --> 00:04:16,769
intersection would never get mapped from

95
00:04:12,090 --> 00:04:19,639
filing all right let's I go on to the

96
00:04:16,769 --> 00:04:23,729
competition everybody cancel out your

97
00:04:19,639 --> 00:04:29,599
existing pythons one pay phones been

98
00:04:23,729 --> 00:04:29,599
hooked up I or and they're 24 class mode

99
00:04:29,840 --> 00:04:48,139
and a wait 2c c is 74 31

100
00:05:00,490 --> 00:05:08,330
there you go that's the question he was

101
00:05:02,630 --> 00:05:10,340
just asking about how you find that is

102
00:05:08,330 --> 00:05:13,610
you have to go to the optional header in

103
00:05:10,340 --> 00:05:15,770
a directory index 1 i'm going to say

104
00:05:13,610 --> 00:05:18,620
which are VA is it you figure out which

105
00:05:15,770 --> 00:05:20,450
section it's in or you can cheap it over

106
00:05:18,620 --> 00:05:23,620
to pee view and just find wherever it is

107
00:05:20,450 --> 00:05:26,420
whichever section it happens to be under

108
00:05:23,620 --> 00:05:31,100
eff explore also has a way that tells

109
00:05:26,420 --> 00:05:33,530
you where it is is wrong from there ok

110
00:05:31,100 --> 00:05:36,680
I'm getting this question to wat so let

111
00:05:33,530 --> 00:05:38,090
me go over this one more time this is

112
00:05:36,680 --> 00:05:42,530
the thing I just read you quick before

113
00:05:38,090 --> 00:05:46,280
we jumped into the game alright so in

114
00:05:42,530 --> 00:05:49,000
the data directory the main thing I was

115
00:05:46,280 --> 00:05:57,260
talking you first was the import address

116
00:05:49,000 --> 00:05:58,760
directory in the native directory the

117
00:05:57,260 --> 00:06:01,669
first thing we were talking about was

118
00:05:58,760 --> 00:06:04,190
this the little stream you know image

119
00:06:01,669 --> 00:06:08,000
directory entry import right this is

120
00:06:04,190 --> 00:06:10,910
index 1 so up here if I ever ask you for

121
00:06:08,000 --> 00:06:13,610
image directory and reimport that's

122
00:06:10,910 --> 00:06:16,190
index wound up here but I said there's

123
00:06:13,610 --> 00:06:18,169
actually a shortcut so this if you want

124
00:06:16,190 --> 00:06:20,090
to get to the IAT from this you got to

125
00:06:18,169 --> 00:06:22,340
go through this directory table and then

126
00:06:20,090 --> 00:06:24,890
go down to this and you go to this will

127
00:06:22,340 --> 00:06:29,330
all over the place there's built-in a

128
00:06:24,890 --> 00:06:34,160
shortcut directly to the IIT that's from

129
00:06:29,330 --> 00:06:37,970
the direct data directory but its index

130
00:06:34,160 --> 00:06:39,950
12 down here so you go from the data

131
00:06:37,970 --> 00:06:44,000
directory at index 12 and so if I'm

132
00:06:39,950 --> 00:06:48,110
asking you a question about and in the

133
00:06:44,000 --> 00:06:49,940
directory country I 80 and I know it

134
00:06:48,110 --> 00:06:51,290
kind of wraps around your screen on some

135
00:06:49,940 --> 00:06:53,030
of these questions right it's kind of

136
00:06:51,290 --> 00:06:56,180
hard to see but key thing is whether

137
00:06:53,030 --> 00:06:58,790
it's AIT it's down here if it's

138
00:06:56,180 --> 00:07:01,580
important it's up here and all of these

139
00:06:58,790 --> 00:07:03,320
in this big array are just tacked onto

140
00:07:01,580 --> 00:07:06,740
the end of the optional matters the data

141
00:07:03,320 --> 00:07:09,090
directory anything about the data

142
00:07:06,740 --> 00:07:10,680
directory is all just a bigger

143
00:07:09,090 --> 00:07:12,840
at the end of the optional header you'll

144
00:07:10,680 --> 00:07:14,669
see that in the cff explorer or p view

145
00:07:12,840 --> 00:07:17,310
so it's really a question of whether you

146
00:07:14,669 --> 00:07:20,310
go to the in a directory entry 12 or

147
00:07:17,310 --> 00:07:22,860
need to drink reentry one that's top and

148
00:07:20,310 --> 00:07:26,040
each entry though one or 12 it's two

149
00:07:22,860 --> 00:07:28,020
things it's an RDA and it's a size so

150
00:07:26,040 --> 00:07:30,350
each of them is a struct with RV a size

151
00:07:28,020 --> 00:07:38,760
or video size but each of those is one

152
00:07:30,350 --> 00:07:41,760
index espressos so 12 you know are you

153
00:07:38,760 --> 00:07:48,300
able to demonstrate how to do that in

154
00:07:41,760 --> 00:07:51,180
either of these tools sure ah which part

155
00:07:48,300 --> 00:07:53,430
did you won't show specifically how to

156
00:07:51,180 --> 00:08:00,200
get to the important entry IEP for

157
00:07:53,430 --> 00:08:04,350
instance get speech ok so in cff explore

158
00:08:00,200 --> 00:08:05,820
you would go to be underneath optional

159
00:08:04,350 --> 00:08:08,460
header see if i could score pulls it out

160
00:08:05,820 --> 00:08:09,960
independently so if you go to the bottom

161
00:08:08,460 --> 00:08:12,240
of optional headers you won't see you

162
00:08:09,960 --> 00:08:13,919
data directly but as this indented thing

163
00:08:12,240 --> 00:08:15,180
they've got data directory really it's

164
00:08:13,919 --> 00:08:16,470
just tacked on to the end of the

165
00:08:15,180 --> 00:08:18,870
optional enter that they're showing it

166
00:08:16,470 --> 00:08:20,880
like it separately so you need to treat

167
00:08:18,870 --> 00:08:23,970
these first two entries like these are

168
00:08:20,880 --> 00:08:27,330
the entry 0 so entry 0 r-va entry zero

169
00:08:23,970 --> 00:08:30,570
size they go to entry 1 RBA entry one

170
00:08:27,330 --> 00:08:32,969
size and so if I'm asking you a question

171
00:08:30,570 --> 00:08:35,940
where i'm literally asking for image

172
00:08:32,969 --> 00:08:40,950
directory entry import based on that

173
00:08:35,940 --> 00:08:42,300
slide 45 and section 2 that's index 1 so

174
00:08:40,950 --> 00:08:44,670
you would just go right here you need to

175
00:08:42,300 --> 00:08:49,350
pull out the RBA or you'd pull out the

176
00:08:44,670 --> 00:08:52,800
size and i didn't really specify this as

177
00:08:49,350 --> 00:08:55,020
well as I thought expedient but in cff

178
00:08:52,800 --> 00:08:57,390
explore when you're looking for the

179
00:08:55,020 --> 00:08:58,980
values it's in this value column be

180
00:08:57,390 --> 00:09:00,480
aware that this off side column it's

181
00:08:58,980 --> 00:09:02,910
really just trying to tell you where

182
00:09:00,480 --> 00:09:05,700
that particular data entry is into the

183
00:09:02,910 --> 00:09:07,860
file it's actually a file offset so it's

184
00:09:05,700 --> 00:09:09,270
saying basically ignore of an offset

185
00:09:07,860 --> 00:09:12,300
column of myself asking me what's the

186
00:09:09,270 --> 00:09:14,220
file offset of something so if I'm

187
00:09:12,300 --> 00:09:16,440
asking you for the RBA you would go and

188
00:09:14,220 --> 00:09:18,480
you pull it out of the value column and

189
00:09:16,440 --> 00:09:19,340
you throw it in now if I was asking you

190
00:09:18,480 --> 00:09:23,930
for the image

191
00:09:19,340 --> 00:09:26,990
factory entry import IMT or just image

192
00:09:23,930 --> 00:09:29,540
directory entry underscore IAT that's

193
00:09:26,990 --> 00:09:31,730
that index 12 and so that's down here in

194
00:09:29,540 --> 00:09:35,180
this thing called import address table

195
00:09:31,730 --> 00:09:37,400
directory RBA again it's you know they

196
00:09:35,180 --> 00:09:38,720
threw their interpretation of it that's

197
00:09:37,400 --> 00:09:42,140
what they're calling it and you'd pull

198
00:09:38,720 --> 00:09:44,870
stuff out of here or there in CFS are

199
00:09:42,140 --> 00:09:46,910
sorry in PE view the equivalent thing is

200
00:09:44,870 --> 00:09:49,190
basically you just go to the optional

201
00:09:46,910 --> 00:09:52,730
headers over here in the image on

202
00:09:49,190 --> 00:09:53,900
theaters and then just scroll all the

203
00:09:52,730 --> 00:09:56,960
way to the bottom of the optional

204
00:09:53,900 --> 00:09:59,600
headers and you see that index one it be

205
00:09:56,960 --> 00:10:02,420
he calls it the import table we would

206
00:09:59,600 --> 00:10:04,820
call it a no image directory entry

207
00:10:02,420 --> 00:10:07,940
underscore import because its index one

208
00:10:04,820 --> 00:10:11,540
and if you're asking for image directory

209
00:10:07,940 --> 00:10:14,990
entry image directory entry underscore

210
00:10:11,540 --> 00:10:18,710
iat at the next 12 as that stuff here so

211
00:10:14,990 --> 00:10:20,600
again we have two thousand eight here if

212
00:10:18,710 --> 00:10:26,450
two thousand while these are the same

213
00:10:20,600 --> 00:10:28,400
policies or less to say so in this game

214
00:10:26,450 --> 00:10:30,020
you're basically being asked question I

215
00:10:28,400 --> 00:10:32,210
could ask you questions about any of

216
00:10:30,020 --> 00:10:33,380
these and i will later Rome's I'll be

217
00:10:32,210 --> 00:10:35,690
asking you about you know different

218
00:10:33,380 --> 00:10:37,280
injuries but for how it's the only two

219
00:10:35,690 --> 00:10:41,080
things in the data directory that have

220
00:10:37,280 --> 00:10:49,490
relevance in quartz is in x1 right here

221
00:10:41,080 --> 00:10:54,710
and it's 12 down here yes number

222
00:10:49,490 --> 00:10:56,390
ventures maturation to know I think if

223
00:10:54,710 --> 00:10:58,550
yeah so if I'm asking about the number

224
00:10:56,390 --> 00:11:00,620
of entries in the direct image directory

225
00:10:58,550 --> 00:11:01,910
table or import descriptor table or

226
00:11:00,620 --> 00:11:04,760
anything like that don't count the Nall

227
00:11:01,910 --> 00:11:07,040
entry I want to come visit very well so

228
00:11:04,760 --> 00:11:11,320
this is another thing that you know is

229
00:11:07,040 --> 00:11:11,320
down to ambiguity of the tools but

230
00:11:11,530 --> 00:11:21,190
everybody sort of flip your slides to

231
00:11:15,790 --> 00:11:21,190
we're going to say slide

232
00:11:24,080 --> 00:11:28,250
I mean the thing is I have to go through

233
00:11:26,990 --> 00:11:30,080
this just like you're having to go

234
00:11:28,250 --> 00:11:33,830
through this but I had nobody to tell me

235
00:11:30,080 --> 00:11:39,920
the answer so slide 69 import descriptor

236
00:11:33,830 --> 00:11:41,540
we then th all right this is the thing

237
00:11:39,920 --> 00:11:44,600
where we have three blue things and we

238
00:11:41,540 --> 00:11:51,080
say we only care about we only care

239
00:11:44,600 --> 00:11:54,260
about so on slide 69 if we're looking at

240
00:11:51,080 --> 00:11:57,260
this thing in PDA it's relatively

241
00:11:54,260 --> 00:11:59,180
straightforward so this is the data

242
00:11:57,260 --> 00:12:02,780
structure for one of these image

243
00:11:59,180 --> 00:12:08,690
directory entries right or 2-inch

244
00:12:02,780 --> 00:12:10,370
descriptor table the first entry you

245
00:12:08,690 --> 00:12:12,740
know it's not their original first off

246
00:12:10,370 --> 00:12:14,570
what is that right well at least in cff

247
00:12:12,740 --> 00:12:16,880
explorer and as well as in the actual

248
00:12:14,570 --> 00:12:18,500
text description later I say you know

249
00:12:16,880 --> 00:12:20,780
that just first thing points to the

250
00:12:18,500 --> 00:12:22,460
important Ames table and thankfully PP

251
00:12:20,780 --> 00:12:25,610
view tells you right there it says the

252
00:12:22,460 --> 00:12:27,260
four names table RVA and so you can find

253
00:12:25,610 --> 00:12:29,900
me in poor name stable RDA for this

254
00:12:27,260 --> 00:12:31,850
particular dll at that offset the last

255
00:12:29,900 --> 00:12:33,560
entry is the imported dressed table are

256
00:12:31,850 --> 00:12:36,770
get you know in this data structure it

257
00:12:33,560 --> 00:12:38,420
calls it first the reason I wanted you

258
00:12:36,770 --> 00:12:40,790
to pull these slides up too because this

259
00:12:38,420 --> 00:12:43,040
same data structure we're in cff

260
00:12:40,790 --> 00:12:46,430
Explorer it's kind of a little weirder

261
00:12:43,040 --> 00:12:48,800
right we've got a horizontal gear of

262
00:12:46,430 --> 00:12:50,420
this data structure and we've got you

263
00:12:48,800 --> 00:12:52,700
know the name out here at the front but

264
00:12:50,420 --> 00:12:54,620
then we've also got this name r-va over

265
00:12:52,700 --> 00:12:56,060
here so it's pulling out the name are

266
00:12:54,620 --> 00:12:57,950
they trying to show that this is the

267
00:12:56,060 --> 00:13:01,820
stuff for user32 this is the stuff for

268
00:12:57,950 --> 00:13:03,260
Emma's VCR but these columns okay and

269
00:13:01,820 --> 00:13:05,240
then it adds an extra field that has the

270
00:13:03,260 --> 00:13:07,130
imports it's trying to like tell you its

271
00:13:05,240 --> 00:13:08,330
pre counted the number of imports for

272
00:13:07,130 --> 00:13:11,390
you so you don't have to lighten it up

273
00:13:08,330 --> 00:13:14,030
yourself but the things that we care

274
00:13:11,390 --> 00:13:16,280
about here are this first thing there's

275
00:13:14,030 --> 00:13:18,260
one two three four five and so let's

276
00:13:16,280 --> 00:13:20,480
start from the very end well actually

277
00:13:18,260 --> 00:13:22,250
you can see the iat is mentioned right

278
00:13:20,480 --> 00:13:24,290
up here in this common things as ft

279
00:13:22,250 --> 00:13:25,790
xiety and for the record I don't know

280
00:13:24,290 --> 00:13:27,830
what the f key stand or that's actually

281
00:13:25,790 --> 00:13:29,110
what I went to do list is class the guy

282
00:13:27,830 --> 00:13:33,490
with the oft in the

283
00:13:29,110 --> 00:13:35,649
t stand for but so we've got a column I

284
00:13:33,490 --> 00:13:37,480
80 well this is going to be this first

285
00:13:35,649 --> 00:13:40,690
spar so we're just taking this and we're

286
00:13:37,480 --> 00:13:42,519
rotating at 90 degrees last entry up

287
00:13:40,690 --> 00:13:45,640
here is first up the second to last

288
00:13:42,519 --> 00:13:47,890
entry neymar VA is named third-to-last

289
00:13:45,640 --> 00:13:49,660
forwarder chain is forwarder chain right

290
00:13:47,890 --> 00:13:51,760
so this all just maps up with this data

291
00:13:49,660 --> 00:13:54,130
structure here so if I'm asking you

292
00:13:51,760 --> 00:13:55,540
about the import names table that's the

293
00:13:54,130 --> 00:13:57,459
first thing I'm asking about the

294
00:13:55,540 --> 00:13:59,649
imported dressed table that's the last

295
00:13:57,459 --> 00:14:03,100
thing in that sort of data director

296
00:13:59,649 --> 00:14:05,140
injury so it's clear in PE view that

297
00:14:03,100 --> 00:14:07,660
import names table is first import

298
00:14:05,140 --> 00:14:10,269
address table as last but if you happen

299
00:14:07,660 --> 00:14:12,190
to be using cff explorer you just kind

300
00:14:10,269 --> 00:14:13,209
of have to know like I mean this oft s

301
00:14:12,190 --> 00:14:15,010
how are you going to know that the

302
00:14:13,209 --> 00:14:17,890
pointer to be important ancient you're

303
00:14:15,010 --> 00:14:20,380
not right it may be know that this app

304
00:14:17,890 --> 00:14:23,170
keys it says I 80 so maybe even know it

305
00:14:20,380 --> 00:14:24,970
but really it just requires a prince

306
00:14:23,170 --> 00:14:27,220
right what was their interpretation that

307
00:14:24,970 --> 00:14:29,950
they apply to this data structure to

308
00:14:27,220 --> 00:14:31,959
give you this dude that's a nice very

309
00:14:29,950 --> 00:14:33,820
much in here so just wanted to cover

310
00:14:31,959 --> 00:14:36,760
that everybody sense of having a go at

311
00:14:33,820 --> 00:14:38,790
least two tons or VA's are always just

312
00:14:36,760 --> 00:14:42,970
take the base address and add some

313
00:14:38,790 --> 00:14:44,709
relative virtual address to it alright

314
00:14:42,970 --> 00:14:46,360
so I think we got to go on I know people

315
00:14:44,709 --> 00:14:49,449
are still struggling with this a little

316
00:14:46,360 --> 00:14:51,190
bit but that's okay you know we can ask

317
00:14:49,449 --> 00:14:52,990
more questions after the end of the

318
00:14:51,190 --> 00:14:54,550
class today you'll have play time to

319
00:14:52,990 --> 00:14:56,620
continue to play with the game and so

320
00:14:54,550 --> 00:14:57,730
forth you really are you don't ever in

321
00:14:56,620 --> 00:14:59,529
your head against something you're not

322
00:14:57,730 --> 00:15:01,570
getting it you have to ask me I said

323
00:14:59,529 --> 00:15:07,660
don't be that guy so thank you for

324
00:15:01,570 --> 00:15:09,100
asking right so as we saw you know we

325
00:15:07,660 --> 00:15:10,540
just dramatically increase the

326
00:15:09,100 --> 00:15:12,790
complexity by going from like just

327
00:15:10,540 --> 00:15:15,640
asking you what's the value out of some

328
00:15:12,790 --> 00:15:17,320
simple optional header to going to go

329
00:15:15,640 --> 00:15:19,329
from here to there to there there and

330
00:15:17,320 --> 00:15:21,399
now add a basic drops onto that is so

331
00:15:19,329 --> 00:15:28,269
important right the next round is even

332
00:15:21,399 --> 00:15:33,730
heard about them all right so right here

333
00:15:28,269 --> 00:15:39,839
um alright so I actually don't know well

334
00:15:33,730 --> 00:15:39,839
the next round smart because well yeah

335
00:15:40,310 --> 00:15:46,680
the next topic is easier to mex topic

336
00:15:44,340 --> 00:15:49,320
after that is harder holy because the

337
00:15:46,680 --> 00:15:51,180
tools not interpret the data so this is

338
00:15:49,320 --> 00:15:53,790
the next round after that is where I

339
00:15:51,180 --> 00:15:55,320
said there's potential for topic we

340
00:15:53,790 --> 00:15:56,790
literally have to look at the hex you

341
00:15:55,320 --> 00:16:01,200
and then interpret it as this data

342
00:15:56,790 --> 00:16:03,800
structure yourself so I'll cover the

343
00:16:01,200 --> 00:16:03,800
easy topic


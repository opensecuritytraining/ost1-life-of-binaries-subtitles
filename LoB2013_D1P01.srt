1
00:00:04,060 --> 00:00:11,360
okay welcome everyone

2
00:00:07,599 --> 00:00:13,190
zero coma and we're going to talk about

3
00:00:11,360 --> 00:00:15,440
binaries a lot on the next couple days

4
00:00:13,190 --> 00:00:17,960
it's really going to be drilling fun of

5
00:00:15,440 --> 00:00:20,660
detail into your head my classes are

6
00:00:17,960 --> 00:00:22,910
very much firehose style teaching but

7
00:00:20,660 --> 00:00:26,990
you're lucky because this is the first

8
00:00:22,910 --> 00:00:28,760
class which actually isn't just a you

9
00:00:26,990 --> 00:00:30,470
know kind of lecturing with a little bit

10
00:00:28,760 --> 00:00:32,390
of labs we have actually a game this

11
00:00:30,470 --> 00:00:34,790
time that helps reinforce material gives

12
00:00:32,390 --> 00:00:36,800
you hands-on experience with using the

13
00:00:34,790 --> 00:00:41,120
tools in order to go figure out the

14
00:00:36,800 --> 00:00:42,949
information about binaries so so this

15
00:00:41,120 --> 00:00:45,379
will be quite a bit more fun than the

16
00:00:42,949 --> 00:00:46,609
previous version this the previous

17
00:00:45,379 --> 00:00:49,550
version of this class was literally

18
00:00:46,609 --> 00:00:51,800
almost entirely lecture and now we break

19
00:00:49,550 --> 00:00:53,659
it up on teach you one thing we'll go

20
00:00:51,800 --> 00:00:57,859
and we'll run this game on it and then

21
00:00:53,659 --> 00:01:00,159
teach you another so as with all my

22
00:00:57,859 --> 00:01:02,749
classes this is going to be released

23
00:01:00,159 --> 00:01:04,489
public release it's going to be a

24
00:01:02,749 --> 00:01:06,530
Creative Commons other people can take

25
00:01:04,489 --> 00:01:08,630
this Lodge or use them in presentations

26
00:01:06,530 --> 00:01:10,640
literally I don't care if people just

27
00:01:08,630 --> 00:01:12,200
you know take the material and go off

28
00:01:10,640 --> 00:01:14,750
and get paid to teach it somewhere else

29
00:01:12,200 --> 00:01:16,640
I just ask that you know people give

30
00:01:14,750 --> 00:01:18,170
credit where credit's due and if you

31
00:01:16,640 --> 00:01:23,119
take the material in adapt that you

32
00:01:18,170 --> 00:01:26,240
share it so for context of where you are

33
00:01:23,119 --> 00:01:28,090
in the larger scope of things I see a

34
00:01:26,240 --> 00:01:30,079
lot of new faces here who aren't

35
00:01:28,090 --> 00:01:32,450
necessarily involved with the school of

36
00:01:30,079 --> 00:01:34,369
sock classes this year so if you just

37
00:01:32,450 --> 00:01:35,960
you know saw the announcement and based

38
00:01:34,369 --> 00:01:37,729
on that you said hey that sounds

39
00:01:35,960 --> 00:01:40,100
interesting this is just to let you know

40
00:01:37,729 --> 00:01:42,829
that actually this class fits into a

41
00:01:40,100 --> 00:01:45,289
much broader curriculum and so in

42
00:01:42,829 --> 00:01:47,869
particular this year was a little weird

43
00:01:45,289 --> 00:01:50,479
in terms of the ordering of classes just

44
00:01:47,869 --> 00:01:52,969
because we were trying to try to

45
00:01:50,479 --> 00:01:54,259
shoehorn things in for this larger

46
00:01:52,969 --> 00:01:57,560
school of thought curriculum which is

47
00:01:54,259 --> 00:01:59,509
much more focused on people who work in

48
00:01:57,560 --> 00:02:01,369
security operation centers so this year

49
00:01:59,509 --> 00:02:04,429
what combiners is being taught before

50
00:02:01,369 --> 00:02:06,259
the intro x86 class you'll see why you

51
00:02:04,429 --> 00:02:09,289
probably would have liked to have the

52
00:02:06,259 --> 00:02:11,989
x86 class before when we get to the

53
00:02:09,289 --> 00:02:14,000
virus lab in the Packer lab especially

54
00:02:11,989 --> 00:02:16,490
the virus lab that will be kind of at

55
00:02:14,000 --> 00:02:18,350
the end it'll be of that

56
00:02:16,490 --> 00:02:20,990
so you'll see a lot of you know assembly

57
00:02:18,350 --> 00:02:22,700
implementing particular features parsing

58
00:02:20,990 --> 00:02:24,320
through particular data structures that

59
00:02:22,700 --> 00:02:27,590
were we're going to be talking about in

60
00:02:24,320 --> 00:02:29,360
this class so it should be fun I'm I'm

61
00:02:27,590 --> 00:02:31,880
you know what soup this year I'm

62
00:02:29,360 --> 00:02:35,030
assuming that people haven't necessarily

63
00:02:31,880 --> 00:02:36,980
or don't necessarily have x86 knowledge

64
00:02:35,030 --> 00:02:38,390
so I'll just kind of explain things at a

65
00:02:36,980 --> 00:02:39,830
high level but if you do have it it will

66
00:02:38,390 --> 00:02:42,860
be better to go back through and read

67
00:02:39,830 --> 00:02:46,700
the virus example on your own later as

68
00:02:42,860 --> 00:02:51,590
well but so this is some of the malware

69
00:02:46,700 --> 00:02:53,480
classes that we have the thing some of

70
00:02:51,590 --> 00:02:55,790
you may have taken the dynamic lore

71
00:02:53,480 --> 00:02:57,560
analysis class first years offered this

72
00:02:55,790 --> 00:02:59,660
year we've actually got a document

73
00:02:57,560 --> 00:03:01,310
analysis class that's sort of underway

74
00:02:59,660 --> 00:03:05,030
we've got a person who's provisionally

75
00:03:01,310 --> 00:03:07,970
committed to doing it he's just got he's

76
00:03:05,030 --> 00:03:10,370
got other things interfering with in

77
00:03:07,970 --> 00:03:12,950
nailing that down so we're hoping to get

78
00:03:10,370 --> 00:03:15,440
that in late spring basically but no

79
00:03:12,950 --> 00:03:17,060
guarantees there and then this year will

80
00:03:15,440 --> 00:03:19,880
be reverse engineering and malware

81
00:03:17,060 --> 00:03:22,610
analysis malware static analysis classes

82
00:03:19,880 --> 00:03:25,850
are gonna be talk by Frank floozy who's

83
00:03:22,610 --> 00:03:27,860
Lou Smith and he'll be taking over from

84
00:03:25,850 --> 00:03:32,000
Matt Briggs who went off and got a job

85
00:03:27,860 --> 00:03:36,470
doing a training for other people and

86
00:03:32,000 --> 00:03:37,790
then just these classes have sometimes

87
00:03:36,470 --> 00:03:39,410
they fit into only one curriculum

88
00:03:37,790 --> 00:03:41,180
sometimes they fit into many the x86

89
00:03:39,410 --> 00:03:41,870
class fits into any more than the life

90
00:03:41,180 --> 00:03:44,240
liners

91
00:03:41,870 --> 00:03:47,840
my combiner is also there's this sort of

92
00:03:44,240 --> 00:03:49,250
more trusted computing side of research

93
00:03:47,840 --> 00:03:50,690
ease out of things that I'm involved

94
00:03:49,250 --> 00:03:53,510
with as well and so we're trying to

95
00:03:50,690 --> 00:03:56,920
build up classes on things like pxg and

96
00:03:53,510 --> 00:03:59,540
bios and reload and things like that so

97
00:03:56,920 --> 00:04:02,020
we're working on those as well but but

98
00:03:59,540 --> 00:04:05,000
the intro to trusted computing class

99
00:04:02,020 --> 00:04:06,860
we're actually in the process of editing

100
00:04:05,000 --> 00:04:08,660
the videos but right now so that will be

101
00:04:06,860 --> 00:04:10,190
publicly available soon the

102
00:04:08,660 --> 00:04:12,560
virtualization class unfortunately we

103
00:04:10,190 --> 00:04:14,620
didn't get enough attendance because we

104
00:04:12,560 --> 00:04:16,790
tried to expand it to three days and

105
00:04:14,620 --> 00:04:18,820
we're able to get enough attendance

106
00:04:16,790 --> 00:04:21,380
there so it'll be rescheduled for

107
00:04:18,820 --> 00:04:23,540
probably later spring again at work and

108
00:04:21,380 --> 00:04:26,210
cut it back to two days again

109
00:04:23,540 --> 00:04:27,950
we'll get more people taking it all

110
00:04:26,210 --> 00:04:29,750
right so this is what you're going to

111
00:04:27,950 --> 00:04:32,060
learn in this class this is a sort of

112
00:04:29,750 --> 00:04:34,850
famous picture by Eero Pereira

113
00:04:32,060 --> 00:04:39,500
it's kind of just breaking down all the

114
00:04:34,850 --> 00:04:41,720
sort of data structures so you know just

115
00:04:39,500 --> 00:04:43,910
like in the interim intermediate x86

116
00:04:41,720 --> 00:04:45,080
class we got you know big picture of

117
00:04:43,910 --> 00:04:48,550
here's everything we're going to dig

118
00:04:45,080 --> 00:04:53,360
into so obviously this is madness but

119
00:04:48,550 --> 00:04:56,090
this is life of bodies there you go

120
00:04:53,360 --> 00:04:58,430
I messed up the this is intro exit

121
00:04:56,090 --> 00:05:00,140
intermediate x86 see I did it again for

122
00:04:58,430 --> 00:05:01,970
the other class but I'd see later on

123
00:05:00,140 --> 00:05:04,280
when I'm gonna have like a nice little

124
00:05:01,970 --> 00:05:06,010
clip show where I pull up my uppercuts

125
00:05:04,280 --> 00:05:08,950
and things like that that I do in the

126
00:05:06,010 --> 00:05:13,100
various classes shouting at people

127
00:05:08,950 --> 00:05:15,080
anyways alright so introductions kind of

128
00:05:13,100 --> 00:05:17,030
about me I finally figured out so

129
00:05:15,080 --> 00:05:18,140
previously on my about me slide I said

130
00:05:17,030 --> 00:05:20,960
you know I'm a general ice not a

131
00:05:18,140 --> 00:05:23,180
specialist but hard - hard to argue that

132
00:05:20,960 --> 00:05:25,370
when I have increasing detailed

133
00:05:23,180 --> 00:05:26,690
knowledge and specialty areas so I

134
00:05:25,370 --> 00:05:30,110
finally found something that I liked

135
00:05:26,690 --> 00:05:32,150
that sort of describes me better there's

136
00:05:30,110 --> 00:05:34,190
this notion of a t-shaped employee so

137
00:05:32,150 --> 00:05:35,660
your broad you have a lot of general

138
00:05:34,190 --> 00:05:37,280
knowledge so you know I haven't been

139
00:05:35,660 --> 00:05:39,680
doing lots of stuff with network

140
00:05:37,280 --> 00:05:42,500
security but still back in the day I

141
00:05:39,680 --> 00:05:46,250
read all my RFC's and you know I know

142
00:05:42,500 --> 00:05:49,700
the details of you know ipv6 multicast

143
00:05:46,250 --> 00:05:51,560
and things like that so a lot of broad

144
00:05:49,700 --> 00:05:53,090
knowledge but increasingly I've been you

145
00:05:51,560 --> 00:05:54,800
know specializing one area and I think

146
00:05:53,090 --> 00:05:56,600
this is you know a very good thing to

147
00:05:54,800 --> 00:05:58,010
aspire to you basically you want to at

148
00:05:56,600 --> 00:06:00,290
least know what's going on in a lot of

149
00:05:58,010 --> 00:06:01,910
different areas but then you become

150
00:06:00,290 --> 00:06:05,240
specialized in one area this picture is

151
00:06:01,910 --> 00:06:07,550
from the from the valve employee

152
00:06:05,240 --> 00:06:12,350
handbook valve are the people behind

153
00:06:07,550 --> 00:06:14,560
steam the game game club game engine

154
00:06:12,350 --> 00:06:17,150
more like game portal I guess these days

155
00:06:14,560 --> 00:06:18,860
you know people behind portal behind

156
00:06:17,150 --> 00:06:20,210
half-life things like that they have an

157
00:06:18,860 --> 00:06:21,680
employee handbook it's really good read

158
00:06:20,210 --> 00:06:24,830
you should check it out I think the

159
00:06:21,680 --> 00:06:28,250
citations in the notes section they

160
00:06:24,830 --> 00:06:30,350
talked about how as valve there they

161
00:06:28,250 --> 00:06:33,950
they look for these t-shaped employees

162
00:06:30,350 --> 00:06:35,660
and they have a very flat organizational

163
00:06:33,950 --> 00:06:36,200
structure so they don't like you know

164
00:06:35,660 --> 00:06:39,290
have men

165
00:06:36,200 --> 00:06:40,700
Jers and the engineers basically it's a

166
00:06:39,290 --> 00:06:42,470
bunch of people they self organize

167
00:06:40,700 --> 00:06:44,180
around projects where if you can

168
00:06:42,470 --> 00:06:45,740
convince your colleagues that you know

169
00:06:44,180 --> 00:06:47,720
this is a good idea and they should work

170
00:06:45,740 --> 00:06:49,660
on it and they all get together on it so

171
00:06:47,720 --> 00:06:52,220
they look for t-shaped employees because

172
00:06:49,660 --> 00:06:53,600
when you get 10 people on a project one

173
00:06:52,220 --> 00:06:54,770
of them's deep in this area one of

174
00:06:53,600 --> 00:06:56,330
them's deep in that air and one of

175
00:06:54,770 --> 00:06:59,390
them's deep that are you know you

176
00:06:56,330 --> 00:07:01,010
overall have a lot of deep expertise you

177
00:06:59,390 --> 00:07:04,610
have at least one person who's who's

178
00:07:01,010 --> 00:07:08,330
good at everything so so this is more

179
00:07:04,610 --> 00:07:12,110
what I'm what I think describes me these

180
00:07:08,330 --> 00:07:16,490
days what I aspire to more the funny

181
00:07:12,110 --> 00:07:19,130
thing is I'm basically lifelong Mac

182
00:07:16,490 --> 00:07:21,020
person and I had to learn Windows for

183
00:07:19,130 --> 00:07:23,180
work so this material that we're going

184
00:07:21,020 --> 00:07:24,740
to be talking about today it's basically

185
00:07:23,180 --> 00:07:26,960
something I had to learn for a project

186
00:07:24,740 --> 00:07:39,350
where I was looking at creating a hacker

187
00:07:26,960 --> 00:07:41,060
and so I was working on a project where

188
00:07:39,350 --> 00:07:42,860
I had to create a packer for Windows and

189
00:07:41,060 --> 00:07:46,160
so that required you know digging down

190
00:07:42,860 --> 00:07:47,810
into the Windows PE file format then had

191
00:07:46,160 --> 00:07:49,910
to port it to Linux and that required

192
00:07:47,810 --> 00:07:53,210
digging down into the Alpha format and

193
00:07:49,910 --> 00:07:54,890
so it's basically just once I learned

194
00:07:53,210 --> 00:07:56,570
this knowledge for this one project I

195
00:07:54,890 --> 00:07:57,950
found it ended up being valuable for

196
00:07:56,570 --> 00:07:59,600
other projects and that's why I wanted

197
00:07:57,950 --> 00:08:01,970
to make a class on it because I think

198
00:07:59,600 --> 00:08:05,240
it's useful for malware people it's

199
00:08:01,970 --> 00:08:07,490
useful for know in where I use it these

200
00:08:05,240 --> 00:08:09,230
guys is on system measurement basically

201
00:08:07,490 --> 00:08:10,880
so measuring binaries in memory if you

202
00:08:09,230 --> 00:08:12,770
know how the P structure is formatted

203
00:08:10,880 --> 00:08:14,030
you can pull out the chunks of memory

204
00:08:12,770 --> 00:08:16,220
based on what they would have looked

205
00:08:14,030 --> 00:08:20,120
like on disk and then measure them so

206
00:08:16,220 --> 00:08:22,220
it's just knowledge that has found a lot

207
00:08:20,120 --> 00:08:25,510
of different uses for me all right so

208
00:08:22,220 --> 00:08:25,510
now we're going to do introductions

209
00:08:29,700 --> 00:08:36,090
all right excellent here so I should

210
00:08:33,300 --> 00:08:41,670
even say you know my own coverage of the

211
00:08:36,090 --> 00:08:45,150
compiler topics own coverage of the

212
00:08:41,670 --> 00:08:46,650
compiler topics was basically more from

213
00:08:45,150 --> 00:08:48,570
so I actually hadn't had a compilers

214
00:08:46,650 --> 00:08:49,980
class but I had adult the thought about

215
00:08:48,570 --> 00:08:52,320
clearing things like that bursting

216
00:08:49,980 --> 00:08:55,110
context-free grammar ISM that was enough

217
00:08:52,320 --> 00:08:56,670
to to cover the majority of material but

218
00:08:55,110 --> 00:08:58,320
I really was more interested in the part

219
00:08:56,670 --> 00:09:00,300
where it spits out assembly and so I was

220
00:08:58,320 --> 00:09:02,340
disappointed what I had seen that the

221
00:09:00,300 --> 00:09:05,720
majority of compiler classes didn't

222
00:09:02,340 --> 00:09:10,280
actually get to that place all right so

223
00:09:05,720 --> 00:09:12,960
that the agenda is no longer accurate um

224
00:09:10,280 --> 00:09:14,940
so the big thing is make sure you ask

225
00:09:12,960 --> 00:09:16,740
your questions when you have them and

226
00:09:14,940 --> 00:09:19,380
you'll especially see on the game that

227
00:09:16,740 --> 00:09:21,510
we play you definitely have to ask

228
00:09:19,380 --> 00:09:24,090
questions there there's the potential

229
00:09:21,510 --> 00:09:26,190
for bugs the previous class got to do

230
00:09:24,090 --> 00:09:28,440
lots of beta testing and lots of lots

231
00:09:26,190 --> 00:09:30,540
and lots of bugs on the game for the

232
00:09:28,440 --> 00:09:32,540
last class so make sure you ask

233
00:09:30,540 --> 00:09:34,920
questions on the material as well as

234
00:09:32,540 --> 00:09:37,980
when you're trying to understand

235
00:09:34,920 --> 00:09:41,850
material like for this game that's good

236
00:09:37,980 --> 00:09:44,070
time to reiterated I find that people

237
00:09:41,850 --> 00:09:45,660
who are you know check can't tear

238
00:09:44,070 --> 00:09:47,760
themselves away from the email they're

239
00:09:45,660 --> 00:09:49,440
going to be the ones who are not

240
00:09:47,760 --> 00:09:51,420
actually absorbing this tutorial as I

241
00:09:49,440 --> 00:09:53,070
said it's going to be kind of fire phone

242
00:09:51,420 --> 00:09:56,640
style it's going to cover a lot of

243
00:09:53,070 --> 00:09:58,410
information a lot of depth so if you're

244
00:09:56,640 --> 00:10:00,630
multitasking especially this is a

245
00:09:58,410 --> 00:10:03,000
warning to the guys only because I know

246
00:10:00,630 --> 00:10:05,340
that you know when you've got just one

247
00:10:03,000 --> 00:10:07,620
window up it's tempting the swap over to

248
00:10:05,340 --> 00:10:09,930
the email client but but that's

249
00:10:07,620 --> 00:10:13,530
basically the leading cause of failure

250
00:10:09,930 --> 00:10:15,180
in my classes so and I can see failure

251
00:10:13,530 --> 00:10:17,310
and the nice thing about this game is it

252
00:10:15,180 --> 00:10:19,250
shows me failure very clearly I'll show

253
00:10:17,310 --> 00:10:22,590
you a picture of failure later so

254
00:10:19,250 --> 00:10:24,260
alright so don't provoke about the class

255
00:10:22,590 --> 00:10:27,380
schedule anymore because the

256
00:10:24,260 --> 00:10:29,240
dissipater claws I'm going to try to go

257
00:10:27,380 --> 00:10:31,250
for two hours here at the beginning when

258
00:10:29,240 --> 00:10:33,920
you still have energy then we're going

259
00:10:31,250 --> 00:10:36,140
to take 10 minute break then we'll do

260
00:10:33,920 --> 00:10:38,780
one and a half hours to lunch and you

261
00:10:36,140 --> 00:10:40,700
take a one-hour lunch meant 1 hour 5

262
00:10:38,780 --> 00:10:42,380
minute breaks every after that but at

263
00:10:40,700 --> 00:10:43,700
least now it's not like 1 hour straight

264
00:10:42,380 --> 00:10:49,040
lecturing will be one hour with

265
00:10:43,700 --> 00:10:52,250
interspersed games alright so as I said

266
00:10:49,040 --> 00:10:54,320
before the intention of this class was

267
00:10:52,250 --> 00:10:55,490
to cover basically starting from source

268
00:10:54,320 --> 00:10:58,100
code all the way through running

269
00:10:55,490 --> 00:11:00,350
binaries that still is the intention if

270
00:10:58,100 --> 00:11:02,360
I could you know it's just the nature of

271
00:11:00,350 --> 00:11:04,160
things here and later that you know

272
00:11:02,360 --> 00:11:05,870
three-day classes are much harder to get

273
00:11:04,160 --> 00:11:08,420
people in for you know people can't pull

274
00:11:05,870 --> 00:11:09,770
themselves away from that long so so

275
00:11:08,420 --> 00:11:11,510
someday I'm going to teach the real

276
00:11:09,770 --> 00:11:13,070
three-day class where we do get to go

277
00:11:11,510 --> 00:11:14,900
into all the compiler stuff than all the

278
00:11:13,070 --> 00:11:18,620
PD and all the elephant and some more

279
00:11:14,900 --> 00:11:20,900
[ __ ] but but this the intent really is

280
00:11:18,620 --> 00:11:24,350
to show you know the full lifecycle of a

281
00:11:20,900 --> 00:11:29,240
program and so as I said this has been

282
00:11:24,350 --> 00:11:30,830
useful for me part of it was saying when

283
00:11:29,240 --> 00:11:32,480
I was first thinking this it was for

284
00:11:30,830 --> 00:11:35,120
this Packer project like I was saying

285
00:11:32,480 --> 00:11:36,590
and part of it was like okay can we get

286
00:11:35,120 --> 00:11:37,820
the next people who are going to work

287
00:11:36,590 --> 00:11:39,440
this or people who are going to maintain

288
00:11:37,820 --> 00:11:41,480
this software can we get them the

289
00:11:39,440 --> 00:11:43,400
knowledge so that they can continue on

290
00:11:41,480 --> 00:11:45,740
this much faster you know it took us a

291
00:11:43,400 --> 00:11:47,030
lot of time people who are working on it

292
00:11:45,740 --> 00:11:48,710
you know it took us a lot of time

293
00:11:47,030 --> 00:11:50,900
reading a bunch of documents and so how

294
00:11:48,710 --> 00:11:54,500
can we optimize this knowledge transfer

295
00:11:50,900 --> 00:11:56,870
basically so this this class will have

296
00:11:54,500 --> 00:11:58,640
relevance for the type of areas that

297
00:11:56,870 --> 00:12:00,440
we're already sort of indicated some of

298
00:11:58,640 --> 00:12:02,390
it like the trusted computing stuff has

299
00:12:00,440 --> 00:12:03,980
to do with measurement and that's one of

300
00:12:02,390 --> 00:12:05,720
the defensive side some of it's more the

301
00:12:03,980 --> 00:12:11,120
offensive side things like understanding

302
00:12:05,720 --> 00:12:15,560
DLL injection and viruses and package

303
00:12:11,120 --> 00:12:17,900
programs etc and I already said that all

304
00:12:15,560 --> 00:12:21,050
right so right when I was talking about

305
00:12:17,900 --> 00:12:22,670
the full lifecycle of mayuri's what I

306
00:12:21,050 --> 00:12:24,230
was talking about was really you have

307
00:12:22,670 --> 00:12:26,840
source code we're going to assume it's a

308
00:12:24,230 --> 00:12:28,820
seed language got many different dot C

309
00:12:26,840 --> 00:12:30,089
files if you'd them into a compiler and

310
00:12:28,820 --> 00:12:32,670
what you get out is actually

311
00:12:30,089 --> 00:12:34,439
many different object files the object

312
00:12:32,670 --> 00:12:37,499
files are basically just the compiler

313
00:12:34,439 --> 00:12:39,029
saying for this piece of source code I'm

314
00:12:37,499 --> 00:12:41,160
going to take it I'm going to parse it

315
00:12:39,029 --> 00:12:43,139
I'm going to sit out some assembly I'm

316
00:12:41,160 --> 00:12:47,779
going to slice that assembly even here

317
00:12:43,139 --> 00:12:50,339
at this object platform of stage we have

318
00:12:47,779 --> 00:12:52,499
it's typically we'll be using the binary

319
00:12:50,339 --> 00:12:54,300
format so these object files will be

320
00:12:52,499 --> 00:12:56,220
held files these other files will be P

321
00:12:54,300 --> 00:12:57,660
files they're not something that you can

322
00:12:56,220 --> 00:12:59,490
just double click on an X cube because

323
00:12:57,660 --> 00:13:01,559
they're each a piece of the program

324
00:12:59,490 --> 00:13:03,749
right just like each source code file

325
00:13:01,559 --> 00:13:07,050
this piece of the program there are each

326
00:13:03,749 --> 00:13:09,569
a independent piece of the program and

327
00:13:07,050 --> 00:13:10,860
so what happens then is you take all the

328
00:13:09,569 --> 00:13:13,529
object files and you feed them into

329
00:13:10,860 --> 00:13:16,769
linker and so each of the object files

330
00:13:13,529 --> 00:13:18,689
will have some code some data and some

331
00:13:16,769 --> 00:13:20,459
references to other code and data that

332
00:13:18,689 --> 00:13:22,259
it maybe needs to access and so the

333
00:13:20,459 --> 00:13:24,209
linkers job is basically splice all

334
00:13:22,259 --> 00:13:28,259
these things together into one

335
00:13:24,209 --> 00:13:30,269
well-formed binary and so then that one

336
00:13:28,259 --> 00:13:31,889
we're thinking more about runtime when

337
00:13:30,269 --> 00:13:34,170
you have a binary you don't like out of

338
00:13:31,889 --> 00:13:36,779
your executed at the command line the

339
00:13:34,170 --> 00:13:39,929
binary is then processed by the OS

340
00:13:36,779 --> 00:13:41,999
loader and the dynamic linker and so

341
00:13:39,929 --> 00:13:44,129
loader linker kind of together here at

342
00:13:41,999 --> 00:13:46,679
this point dynamic linker basically

343
00:13:44,129 --> 00:13:48,959
means runtime linker and so what happens

344
00:13:46,679 --> 00:13:50,910
then is the binary after having been

345
00:13:48,959 --> 00:13:53,370
spliced together from all these object

346
00:13:50,910 --> 00:13:54,779
files has many references to external

347
00:13:53,370 --> 00:13:56,699
libraries and things like that so when

348
00:13:54,779 --> 00:13:58,649
you use printf you're you know

349
00:13:56,699 --> 00:14:00,540
referencing a standard c library you're

350
00:13:58,649 --> 00:14:02,519
not reimplemented all the code in order

351
00:14:00,540 --> 00:14:05,279
to you know print characters to the

352
00:14:02,519 --> 00:14:06,959
screen so you use printf the linker says

353
00:14:05,279 --> 00:14:08,910
okay looks like you didn't define

354
00:14:06,959 --> 00:14:11,220
anything called printf anywhere here

355
00:14:08,910 --> 00:14:12,990
okay you're looking for printf um in

356
00:14:11,220 --> 00:14:15,629
some other library and then it marks

357
00:14:12,990 --> 00:14:18,449
down in your binary this person needs

358
00:14:15,629 --> 00:14:19,800
printf from this library and later on

359
00:14:18,449 --> 00:14:21,839
when the loader is executing your

360
00:14:19,800 --> 00:14:23,459
program it's going to say okay which

361
00:14:21,839 --> 00:14:28,100
libraries does this thing import

362
00:14:23,459 --> 00:14:31,470
functions from okay standard C the eye

363
00:14:28,100 --> 00:14:33,300
between Linux and Joe's I want even say

364
00:14:31,470 --> 00:14:36,240
other examples so you're importing

365
00:14:33,300 --> 00:14:39,050
libraries from from external to the

366
00:14:36,240 --> 00:14:41,149
binary and so then the loader takes in

367
00:14:39,050 --> 00:14:43,820
to memory and you get a running program

368
00:14:41,149 --> 00:14:46,970
it just results all the outstanding

369
00:14:43,820 --> 00:14:49,550
references to code and data convenient

370
00:14:46,970 --> 00:14:51,800
importing data that's exported by other

371
00:14:49,550 --> 00:14:54,760
things as well and so finally we get a

372
00:14:51,800 --> 00:14:58,100
roni program and the running program

373
00:14:54,760 --> 00:15:05,329
doing it or running program will kill us

374
00:14:58,100 --> 00:15:07,760
all all right so then there we go so

375
00:15:05,329 --> 00:15:10,220
this is what we would have basically

376
00:15:07,760 --> 00:15:12,410
been gone going into we would have went

377
00:15:10,220 --> 00:15:14,209
down into each of these lexical analysis

378
00:15:12,410 --> 00:15:17,000
and text analysis and so forth

379
00:15:14,209 --> 00:15:19,730
I'm so this is just you know the process

380
00:15:17,000 --> 00:15:21,649
between source code and object file

381
00:15:19,730 --> 00:15:25,160
there's a lot of sub components to that

382
00:15:21,649 --> 00:15:28,700
as optimizations there's different

383
00:15:25,160 --> 00:15:29,899
phases of analysis and fit unfortunately

384
00:15:28,700 --> 00:15:33,200
we're not going to be able to cover that

385
00:15:29,899 --> 00:15:36,500
today so again just to give you sort of

386
00:15:33,200 --> 00:15:38,120
a notional view of things the linker

387
00:15:36,500 --> 00:15:41,120
like I said you have to object files

388
00:15:38,120 --> 00:15:44,750
spit out by the previous process they

389
00:15:41,120 --> 00:15:46,940
have some headers because these as I

390
00:15:44,750 --> 00:15:49,730
said they're well-formed pede by nerds

391
00:15:46,940 --> 00:15:51,950
they're well-formed elf binaries we've

392
00:15:49,730 --> 00:15:53,959
got your typical binary format headers

393
00:15:51,950 --> 00:15:56,779
and then you've got things like code and

394
00:15:53,959 --> 00:15:59,450
data and so forth as I said the linkers

395
00:15:56,779 --> 00:16:00,709
job is just to put them all together in

396
00:15:59,450 --> 00:16:02,450
the dynamic malware class you probably

397
00:16:00,709 --> 00:16:04,459
actually saw a better slide than this

398
00:16:02,450 --> 00:16:06,110
work where it showed like the dot text

399
00:16:04,459 --> 00:16:07,790
sections of everybody goes together in

400
00:16:06,110 --> 00:16:11,570
the data sections where everybody goes

401
00:16:07,790 --> 00:16:13,670
together right and then so at one time

402
00:16:11,570 --> 00:16:15,350
the output of that was this well-formed

403
00:16:13,670 --> 00:16:17,540
binary which has code and data and it

404
00:16:15,350 --> 00:16:19,990
has these imports and that's sort of the

405
00:16:17,540 --> 00:16:22,699
critical outstanding thing so at runtime

406
00:16:19,990 --> 00:16:26,209
you try to run we can suite app dot exe

407
00:16:22,699 --> 00:16:27,740
that gets loaded into memory it has its

408
00:16:26,209 --> 00:16:29,449
own particular memory space that's

409
00:16:27,740 --> 00:16:31,699
isolated from other memory spaces with

410
00:16:29,449 --> 00:16:34,730
its own stack its own deep and then its

411
00:16:31,699 --> 00:16:37,250
own copies of other libraries mapped

412
00:16:34,730 --> 00:16:38,990
into its memory if these copies already

413
00:16:37,250 --> 00:16:40,910
existed in some other processes memory

414
00:16:38,990 --> 00:16:42,589
the operating system will generally just

415
00:16:40,910 --> 00:16:44,930
try to share yours between the different

416
00:16:42,589 --> 00:16:47,000
processes so so this is the virtual

417
00:16:44,930 --> 00:16:48,560
memory space if we can suite up that exe

418
00:16:47,000 --> 00:16:50,900
but off to the side that we know

419
00:16:48,560 --> 00:16:52,550
to the side that we explore and so the

420
00:16:50,900 --> 00:16:55,130
operating system is job is to kind of

421
00:16:52,550 --> 00:16:56,990
you know use memory efficiently so if

422
00:16:55,130 --> 00:16:59,750
it's already got they've cement in

423
00:16:56,990 --> 00:17:01,490
somebody else's memory all it'll do is

424
00:16:59,750 --> 00:17:03,680
take this virtual memory and point it at

425
00:17:01,490 --> 00:17:06,560
the same physical memory that is mapped

426
00:17:03,680 --> 00:17:08,600
by some other process and that's how the

427
00:17:06,560 --> 00:17:10,870
operating system will efficiently use

428
00:17:08,600 --> 00:17:10,870
memory

